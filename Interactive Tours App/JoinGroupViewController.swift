//
//  JoinGroupViewController.swift
//  Interactive Tours App
//
//  Created by Brst on 22/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class JoinGroupViewController: UIViewController,UITextFieldDelegate,UITabBarControllerDelegate,PayPalPaymentDelegate,UIAlertViewDelegate {
    var environment:String = PayPalEnvironmentNoNetwork {
    willSet(newEnvironment) {
    if (newEnvironment != environment) {
    PayPalMobile.preconnect(withEnvironment: newEnvironment)
    }
    }
    }
  var resultText = ""
    @IBOutlet weak var groupNameTxt : UITextField?
    @IBOutlet weak var submitButton : UIButton?
    var strAlertMsg = NSString()
    var alert = UIAlertController()
    var profielAlertView = UIAlertController()
    
    @IBOutlet weak var amountDesclabel : UILabel?
   @IBOutlet weak var amountLabel : UILabel?
    @IBOutlet weak var buyButton : UIButton?
      var payPalConfig = PayPalConfiguration()
    
    var group_Id = String()
    var group_Price = String()
    var getUser_id = String()
     var txnID = String()
        var successStr = String()
    var JoinGroupCheck = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set("3", forKey: "ClickedTabIndex")
          UserDefaults.standard.removeObject(forKey: "TourController")
        // Do any additional setup after loading the view.
        groupNameTxt?.layer.borderColor=UIColor.black .cgColor
        groupNameTxt?.layer.borderWidth=0.5
        groupNameTxt?.layer.cornerRadius=20
        groupNameTxt?.delegate=self
        
       
        submitButton?.layer.cornerRadius=20
        
        
        txtPaddingVw(txt: groupNameTxt!)
        
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "Turs"
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        payPalConfig.payPalShippingAddressOption = .payPal;
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
       // buyButton?.layer.borderWidth = 1
        buyButton?.layer.masksToBounds = false
        buyButton?.layer.cornerRadius = (buyButton?.frame.height)!/2
        buyButton?.clipsToBounds = true

        
        
        
        
    }
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        // successView.isHidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            print(completedPayment.description)
            print(completedPayment.confirmation)
            let dictresult = completedPayment.confirmation as NSDictionary
            let response = dictresult.value(forKey: "response") as! NSDictionary
            print(response.value(forKey: "id") ?? NSString())
            
            let strID = response.value(forKey: "id") as? String
            
            self.txnID = strID!
      
            self.resultText = completedPayment.description
            
           
            self.purchaseGroup(completion:  {(result) in
            
            })
        })
    }
    // MARK: Helpers
    
    func saveView() {
        //Save View
        UserDefaults.standard.set("JOIN_VIEW", forKey: "SavedViewController")
    }
    override func viewWillAppear(_ animated: Bool) {
        saveView()
        IQKeyboardManager.shared().isEnabled = true
        amountLabel?.isHidden = true
        amountDesclabel?.isHidden = true
        buyButton?.isHidden = true
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = true
        UserDefaults.standard.set("3", forKey: "ClickedTabIndex")
        if (UserDefaults.standard.value(forKey: "JoinGroupCheck") != nil) {
            JoinGroupCheck = UserDefaults.standard.value(forKey: "JoinGroupCheck") as! NSString
        }
        groupNameTxt?.text = ""
        tabBarController?.delegate = self
        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
            groupNameTxt?.isHidden = false
            submitButton?.isHidden = false
        }
        else
        {
            groupNameTxt?.isHidden = true
            submitButton?.isHidden = true
            profileAlertbar()
        }
    }
   
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool
    {
        let selectedIndex = tabBarController.selectedIndex
        let selectInd = String(selectedIndex)
        UserDefaults.standard.set(selectInd, forKey: "ClickedTabIndex")
        return true
    }
    @IBAction func buyButtonAction (sender: UIButton)
    {
      paypal()  
    }
    func progessDismiss()
    {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func progessShow()
    {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func getJoinGroupData(completion:@escaping (_ result: Bool) -> Void)
    {
    
         var user_id = NSString()
        if UserDefaults.standard.value(forKey: "user_id") != nil {
            user_id = UserDefaults.standard.value(forKey: "user_id")as! NSString
        }
        
        
        SVProgressHUD .show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 6){
            self.progessDismiss()
        }
        let url = "http://beta.brstdev.com/tours/api/web/v1/tours/joingroup"
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let groupNam: String! = groupNameTxt?.text
        if groupNam.contains("₹")
        {
            self.progessDismiss()
            self.strAlertMsg = "Group Not Found"
            self.JoinGroupAlertbar()
        }
        else
        {
        let params:[String: AnyObject] = ["user_id" : user_id as AnyObject,"group_name" :groupNam as AnyObject]
        
        print(params)
        
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code = \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                self.progessDismiss()
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print ("Json Data is = \(jsonResponse)")
                        
                        
                        var dataDic = NSDictionary()
                        var strSuccess_code = NSString()
                        var strDescription = NSString()
                        if let Suc_Code = jsonResponse ["status_code"] as? NSString {
                            strSuccess_code = Suc_Code
                        }
                        if let description = jsonResponse ["description"] as? NSString {
                            strDescription = description
                        }
                        
                        
                        if strSuccess_code .isEqual(to: "400")
                        {
                            if strDescription .isEqual(to: "You have already joined this group")
                            {
                                 UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
                                dataDic = jsonResponse.value(forKey: "error") as! NSDictionary

                                let tour_id = dataDic.object(forKey: "tour_id")
                                UserDefaults.standard.set(tour_id, forKey: "tour_id")
                                //self.strAlertMsg = "You have already joined this group. Do you want to start Tour?"
                                 self.strAlertMsg = "You have already joined this group."
                                self.JoinGroupAlertbar()
                            }
                            else
                            {
                            self.strAlertMsg = strDescription
                            self.JoinGroupAlertbar()
                            }
                        }
                            
                        else
                        {
                     
                            if let dataDic = jsonResponse.value(forKey: "data") as? NSDictionary {
                            print ("Profile Data = \(dataDic)")
                           
                                if let Suc_Code = dataDic ["success_code"] as? NSString {
                                    strSuccess_code = Suc_Code
                                }
                                if let group_id = dataDic.value(forKey: "group_id") as? Int {
                                    self.group_Id = String(group_id)
                                }
                                if let group_price = dataDic.value(forKey: "group_price") as? Double {
                                    self.group_Price = String(group_price)
                                }
                            // strSuccess_code = dataDic.value(forKey: "success_code") as! NSString
                            
//                            let group_id = dataDic.value(forKey: "group_id") as! Int
//                            self.group_Id = String(group_id)
//
//                            let group_price = dataDic.value(forKey: "group_price") as! Double
//                            self.group_Price = String(group_price)
//
                            if strSuccess_code .isEqual(to: "201")
                            {
                                  DispatchQueue.main.async {
                                UserDefaults.standard.set(self.groupNameTxt?.text, forKey: "GroupName")
                                self.successStr = "Successfule"
                                self.strAlertMsg = strDescription
                                
                                if (dataDic.value(forKey: "user_id") != nil)
                                {
                                    let user_id = dataDic.value(forKey: "user_id") as Any
                                    UserDefaults.standard.set(user_id, forKey: "user_id")
                                    }
                                    
                                    if strDescription == "Group Joined Successfully" {
                                        self.strAlertMsg = "Group Joined Successfully. Do you want to start tour?"
                                        self.JoinGroupAlertbar()
                                    }
                                    else {
                                        let showPrice: String? = "Amount "+"€"+self.group_Price
                                        
                                        self.amountLabel?.text = showPrice
                                        self.amountLabel?.isHidden = false
                                        self.amountDesclabel?.isHidden = false
                                        self.buyButton?.isHidden = false
                                        self.JoinGroupAlertbar()
                                    }
                                }
                            }
                                
                            else
                            {
                                self.strAlertMsg = strDescription
                                self.JoinGroupAlertbar()
                                
                                
                                
                            }
                        }
                        }
                        
                         completion(true)
                    }catch _ {
                         completion(false)
                        self.strAlertMsg = "Server error"
                        self.alertBar()
                    }
                }
            })
            task.resume()
        }catch _ {
            print ("Oops something happened buddy")
        }
        }
    }
    

    func purchaseGroup(completion:@escaping (_ result:Bool)->Void) {
        
        var user_id = NSString()
        if UserDefaults.standard.value(forKey: "user_id") != nil {
            user_id = UserDefaults.standard.value(forKey: "user_id")as! NSString
        }
        SVProgressHUD .show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 6){
            self.progessDismiss()
        }
        let url = "http://beta.brstdev.com/tours/api/web/v1/tours/joinpaidgroup"
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let params:[String: AnyObject] = ["user_id" : user_id as AnyObject,"group_id" : self.group_Id as AnyObject,"group_price" : self.group_Price as AnyObject,"txn_id" : self.txnID as AnyObject]
        
        print(params)
        
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code = \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                
                self.progessDismiss()
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print ("Json Data is = \(jsonResponse)")
                        
                        
                       // let dataDic = NSDictionary()
                        
                        var strSuccess_code = NSString()
                        var strDescription = NSString()
                        if let Suc_Code = jsonResponse ["status_code"] as? NSString {
                            strSuccess_code = Suc_Code
                        }
                        if let description = jsonResponse ["description"] as? NSString {
                            strDescription = description
                        }
                        
                          self.strAlertMsg = strDescription
                         DispatchQueue.main.async {
                        self.amountLabel?.isHidden = true
                        self.amountDesclabel?.isHidden = true
                        self.buyButton?.isHidden = true
                        }
                        if strSuccess_code .isEqual(to: "400") {
                            self.strAlertMsg = strDescription
                            self.JoinGroupAlertbar()
                            
                           completion(false)
                        }
                        else
                        {
                         
                            if let dataDic = jsonResponse.value(forKey: "data") as? NSDictionary {
                            print ("Profile Data = \(dataDic)")
                          
                                if let success_code =  dataDic.value(forKey: "success_code") as? NSString {                            strSuccess_code = success_code
                                }
                                
                            if strSuccess_code .isEqual(to: "201")
                            {
                                self.strAlertMsg = strDescription
                                if strDescription .isEqual(to: "Group Joined Successfully")
                                {
                                    
                                    
                                    UserDefaults.standard.removeObject(forKey: "SavedTour")
                                    UserDefaults.standard.removeObject(forKey: "saveTourDetail")
                                    UserDefaults.standard.removeObject(forKey: "correctAns")
                                    UserDefaults.standard.removeObject(forKey: "wrongAns")
                                    UserDefaults.standard.removeObject(forKey: "arrID")
                                    UserDefaults.standard.removeObject(forKey: "SavedID")
                                    
                                   self.strAlertMsg = "Group Joined Successfully. Do you want to start tour?"
                                  
                                    if let tour =  dataDic.value(forKey: "tour_id") as? Int {
                                        let tour_id = tour
                                        UserDefaults.standard.set(tour_id, forKey: "tour_id")
                                        UserDefaults.standard.set(tour_id, forKey: "JoinGroupTourID")
                                    }
                                    
                                    
                                    
//                                    var saveTourIDArr = [Int]()
//                                    if  UserDefaults.standard.object(forKey: "JoinGroupSavedTourID") != nil {
//                                        
//                                        saveTourIDArr = UserDefaults.standard.object(forKey: "JoinGroupSavedTourID") as! [Int]
//                                        saveTourIDArr.append(tour_id)
//                                        UserDefaults.standard.set(saveTourIDArr, forKey: "JoinGroupSavedTourID")
//                                    }
//                                    else{
//                                        saveTourIDArr.append(tour_id)
//                                        UserDefaults.standard.set(saveTourIDArr, forKey: "JoinGroupSavedTourID")
//                                    }
                                 
                                    self.JoinGroupAlertbar()
                                }
                            }
                            else {
                                self.strAlertMsg = strDescription
                                self.JoinGroupAlertbar()
                     
                            }
                        }
                        }
                        
                          completion(true)
                    }catch _ {
                          completion(false)
                        self.strAlertMsg = "Server error"
                        self.alertBar()
                    }
                }
            })
            task.resume()
        }catch _ {
            print ("Oops something happened buddy")
        }
    }
    func alertBar() {
        let alertView = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        //alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        self.present(alertView, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alertView.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func submitButton (sender:UIButton) {
        if (groupNameTxt?.text?.isEmpty)! {
            strAlertMsg = "Please input Group Code to join"
            JoinGroupAlertbar()
        }
        else{
                getJoinGroupData(completion: {(result) in
                })
        }
    }
    
    func txtPaddingVw(txt:UITextField) {
        let paddingView = UIView(frame: CGRect (x:0, y:0 , width:10 , height:20))
        txt.leftViewMode = .always
        txt.leftView = paddingView
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  // called when 'return' key pressed. return NO to ignore.
    {
        groupNameTxt?.resignFirstResponder()
        return true;
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)

        groupNameTxt?.resignFirstResponder()
    }
    
    //}
    func profileAlertbar()
    {
        profielAlertView = UIAlertController(title: nil, message:"Firstly, You have to create Account Profile" as String?, preferredStyle: UIAlertControllerStyle.alert)
        let cancelBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.profileCancelAction()
        }
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.profileOkAction()
        }
         profielAlertView.addAction(cancelBtn)
        profielAlertView.addAction(okBtn)
          DispatchQueue.main.async {
            self.present(self.profielAlertView, animated: true, completion: nil)
          }
        
        
        
    }
    //dfdsfdsfdasfadsfdsfds
    func JoinGroupAlertbar()
    {
        
        let JoinGroupAlert = UIAlertController(title: nil, message:self.strAlertMsg  as String?, preferredStyle: UIAlertControllerStyle.alert)
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            
            if self.strAlertMsg .isEqual(to: "Group Joined Successfully. Do you want to start tour?") {
                UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
                 self.moveToClass()
            }
        }
        if self.strAlertMsg .isEqual(to: "You have already joined this group.") {
        }
        else {
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (alertAction) -> Void in
            }
            JoinGroupAlert.addAction(cancel)
        }
        
        JoinGroupAlert.addAction(okBtn)
            self.present(JoinGroupAlert, animated: true, completion: nil)
        
        
    }
    func profileOkAction()
    {
       
        let  createCheck = "CreateProfile"
        UserDefaults.standard.set("JoinGroupCheck", forKey: "JoinGroupCheck")
        UserDefaults.standard.set(createCheck, forKey: "CreateProfile")

         self.tabBarController?.selectedIndex = 4
    }
    func profileCancelAction() {
        self.tabBarController?.selectedIndex = 0
       UserDefaults.standard.removeObject(forKey: "JoinGroupCheck")
        self.alert.dismiss(animated: true, completion: nil)
    }


    func moveToClass()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "QuesAnsViewController") as! QuesAnsViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    func paypal()
    {
        // Remove our last completed payment, just for demo purposes.
        //resultText = ""
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        // let tour_price = self.group_Price
        let item1 = PayPalItem(name: "Tour", withQuantity: 1, withPrice: NSDecimalNumber(string: self.group_Price), withCurrency: "EUR", withSku: "")
        
        print("The Items Are\(item1)")
        //        let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
        //        let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.0")
        let tax = NSDecimalNumber(string: "0.0")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "EUR", shortDescription: "Price", intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
            
            
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
    }
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

