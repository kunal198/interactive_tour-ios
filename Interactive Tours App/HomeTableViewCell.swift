//
//  HomeTableViewCell.swift
//  Interactive Tours App
//
//  Created by brst on 20/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var homeScrenMainView: UIView!
    @IBOutlet weak var homeScrenSubView: UIView!
    @IBOutlet weak var homeScrenSecSubView: UIView!
    @IBOutlet weak var mainViewImageView: UIImageView!
    @IBOutlet weak var subViewImageView: UIImageView!
    @IBOutlet weak var secSubImageView: UIImageView!
    @IBOutlet weak var mainViewCellButton: UIButton!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var subViewHeadLabel: UILabel!
    @IBOutlet weak var subViewDescLabel: UILabel!
    @IBOutlet weak var secSubViewHeadLabel: UILabel!
    @IBOutlet weak var secSubViewTitleLabel: UILabel!
    @IBOutlet weak var secSubViewDescLabel: UILabel!
    @IBOutlet weak var mainViewCellLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
