//
//  ToursMapViewController.swift
//  Interactive Tours App
//
//  Created by Brst on 19/09/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import GoogleMaps

enum TravelModes: Int {
    case driving
    case walking
    case bicycling
}
class ToursMapViewController: UIViewController, CLLocationManagerDelegate,GMSMapViewDelegate {
    
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var headerlabel: UILabel!
    var locationManager = CLLocationManager()
    
    var didFindMyLocation = false
    var mapTasks = MapTasks()
    var locationMarker: GMSMarker!
    
    var originMarker: GMSMarker!
    
    var destinationMarker: GMSMarker!
    
    var routePolyline: GMSPolyline!
    
    var markersArray: Array<GMSMarker> = []
    
    var waypointsArray: Array<String> = []
    
    var travelMode = TravelModes.driving
    
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    
    var selectedRoute: Dictionary<NSObject, AnyObject>!
    var originCoordinate: CLLocationCoordinate2D!
    
    var destinationCoordinate: CLLocationCoordinate2D!
    
    var originAddress: String!
    
    var destinationAddress: String!
    var zoom: Float = 15
    

    var tourID = Int()
    var latituteArr = NSArray()
    var longituteArr = NSArray()
    var toursNameArr = NSArray()
    
    @IBOutlet weak var gpsButton = UIButton()
     @IBOutlet weak var gpsIcon = UIImageView()
    var groupCodeImgView = UIImageView()
    
  //  var marker = GMSMarker()
    var model = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()
        
          UserDefaults.standard.set("QuestionView", forKey: "QuestionView")
        let modelName = UIDevice.current.model
        model = modelName as NSString
        viewMap.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false

        self.navigationController?.navigationBar.barTintColor = UIColor(red: 35/255, green: 230/255, blue: 6/255, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white
       // self.viewMap.mapType = kGMSTypeHybrid
    
        if UserDefaults.standard.object(forKey: "latituteArr") != nil && UserDefaults.standard.object(forKey: "longituteArr") != nil && UserDefaults.standard.object(forKey: "toursNameArr") != nil
        {
            latituteArr = NSMutableArray(array:UserDefaults.standard.object(forKey: "latituteArr")as! NSArray)
            longituteArr = NSMutableArray(array:UserDefaults.standard.object(forKey: "longituteArr")as! NSArray)
            toursNameArr = NSMutableArray(array:UserDefaults.standard.object(forKey: "toursNameArr")as! NSArray)
        }
        if UserDefaults.standard.object(forKey: "tour_name") != nil
        {
            headerlabel.text = UserDefaults.standard.object(forKey: "tour_name") as! String?
            
        }
        viewMap.delegate = self

        groupCodeImgView = UIImageView(frame: CGRect(x: self.view.frame.size.width-105 , y: -3, width: 100, height: 40))
        groupCodeImgView.image = UIImage(named:"greenBlank.png")
        self.navigationController?.navigationBar .addSubview(groupCodeImgView)
      
        
    }
    
    @IBAction func gpsButtonAction(sender: UIButton)
    {
        self.locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude, zoom: 17);
        self.viewMap.camera = camera
        self.viewMap.isMyLocationEnabled = true
        
        locationManager.stopUpdatingLocation()

        SVProgressHUD.dismiss()
    }
//    DispatchQueue.main.async {
//    let coordinate = CLLocationCoordinate2D(latitude: 52.08715, longitude: 7.61696)
//    let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 52.08715, longitude: 7.61696, zoom: 10.0)
//    // self.viewMap = GMSMapView.map(withFrame: self.view.frame, camera: camera)
//    self.viewMap.camera = camera
//    
//    
//    self.setuplocationMarker(coordinate: coordinate)
//    //
//    //
//    //                        }
//    //                    }
//    //
//    }
//    
//    
//}
    override func viewWillAppear(_ animated: Bool) {
        //============================== ==============================
        if Reachability.shared.isConnectedToNetwork(){
           
                SVProgressHUD.show()
                self.mapTasks.geocodeAddress(address: "Dungarvan Town Park", withCompletionHandler: { (status, success) -> Void in
                    if !success {
                        print(status)
                         DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        if status == "ZERO_RESULTS" {
                            //self.showAlertWithMessage(message: "The location could not be found.")
                            self.routeInfoAlert(message: "The location could not be found.")
                        }
                        else  if status == "OVER_QUERY_LIMIT" {
                            
                            self.routeInfoAlert(message: "Something went wrong. Please try again")
                        }
                        }
                    }
                    else {
                        let coordinate = CLLocationCoordinate2D(latitude: self.mapTasks.fetchedAddressLatitude, longitude: self.mapTasks.fetchedAddressLongitude)
                        DispatchQueue.main.async {
                             SVProgressHUD.dismiss()

                                self.viewMap.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: 17)
                  
                            self.setuplocationMarker(coordinate: coordinate)
                            
                            
                        }
                    }
                    
                })
            
        }
        else
        {
            routeInfoAlert(message: "The network connection was lost")
        }
    }
    func setuplocationMarker(coordinate: CLLocationCoordinate2D) {
        if locationMarker != nil {
            locationMarker.map = nil
        }
         viewMap.isHidden = false
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = viewMap
        locationMarker.title = mapTasks.fetchedFormattedAddress
        locationMarker.appearAnimation = kGMSMarkerAnimationPop
        locationMarker.icon = GMSMarker.markerImage(with: UIColor.init(red: 108/255, green: 2/255, blue: 111/255, alpha: 1))
        locationMarker.opacity = 0.75
        locationMarker.isFlat = true
        
//        let label = UILabel()
//        label.frame = CGRect(x:0, y:0, width: 50, height: 20)
//        label.text = "test"
//        label.backgroundColor = UIColor.red.withAlphaComponent(0.5)
//        label.textColor = UIColor.white
//        label.adjustsFontSizeToFitWidth = true
//        UIGraphicsBeginImageContextWithOptions(label.frame.size, false, UIScreen.main.scale)
//        if let currentContext = UIGraphicsGetCurrentContext()
//        {
//            label.layer.render(in: currentContext)
//            var imageMarker = UIImage()
//            imageMarker = UIGraphicsGetImageFromCurrentImageContext()!
//            let marker = GMSMarker(position: CLLocationCoordinate2DMake(52.08666, -7.61844))
//            marker.icon = imageMarker
//            marker.map = viewMap
//        }

        //locationMarker.snippet = "The best place on earth."
   
            
//            self.mapTasks.getDirections(origin: "Mohali", destination: "patiala", waypoints: nil, travelMode: nil, completionHandler: { (status, success) -> Void in
//                if success {
//                    self.configureMapAndMarkersForRoute()
//                    self.drawRoute()
//                    self.displayRouteInfo()
//                }
//                else {
//                    print(status)
//                }
//            })
        
        if self.tourID < self.latituteArr.count
        {

        let Origin = (self.latituteArr[self.tourID] as! NSString).doubleValue
         let Destination = (self.longituteArr[self.tourID] as! NSString).doubleValue
        
        self.originCoordinate = CLLocationCoordinate2DMake(Origin, Destination)
        
      //  tourID = tourID + 1
        
        
        let Origin1 = (self.latituteArr[self.tourID] as! NSString).doubleValue
        let Destination1 = (self.longituteArr[self.tourID] as! NSString).doubleValue
         self.destinationCoordinate = CLLocationCoordinate2DMake(Origin1, Destination1)
        for index in 0..<self.latituteArr.count
        {
            let Origin = (self.latituteArr[index] as! NSString).doubleValue
            let Destination = (self.longituteArr[index] as! NSString).doubleValue
            let tourName = (self.toursNameArr[index] as! NSString)
            self.originCoordinate = CLLocationCoordinate2DMake(Origin, Destination)
            
//                    let label = UILabel()
//                    label.frame = CGRect(x:0, y:0, width: 300, height: 20)
//                    label.text = tourName as String
//                    label.backgroundColor = UIColor.clear
//                    label.textColor = UIColor.init(colorLiteralRed: 108/255, green: 2/255, blue: 111/255, alpha: 1)
//                  //  label.adjustsFontSizeToFitWidth = true
//            label.font = UIFont.systemFont(ofSize: 15)
//                    UIGraphicsBeginImageContextWithOptions(label.frame.size, false, UIScreen.main.scale)
//                    if let currentContext = UIGraphicsGetCurrentContext()
//                    {
//                        label.layer.render(in: currentContext)
//                        var imageMarker = UIImage()
//                        imageMarker = UIGraphicsGetImageFromCurrentImageContext()!
//                        let marker = GMSMarker(position: CLLocationCoordinate2DMake(Origin, Destination))
//                        marker.icon = imageMarker
//                        marker.map = viewMap
//                    }
        

            self.sourceDestinationMarker(markLocation: self.originCoordinate,tourName:tourName)
            
            
            
        }
      
//            let when = DispatchTime.now() + 2
//                 DispatchQueue.main.asyncAfter(deadline: when){
//                    SVProgressHUD.dismiss()
//            }
                  // getPolylineRoute(from: originCoordinate, to: destinationCoordinate)
        
        
        }
    }
    func sourceDestinationMarker(markLocation: CLLocationCoordinate2D,tourName: NSString ) {
//        if locationMarker != nil {
//            locationMarker.map = nil
//        }
//        
        locationMarker = GMSMarker(position: markLocation)
        
        locationMarker.map = viewMap
        locationMarker.title = tourName as String
      //  locationMarker.snippet = tourName as String
        locationMarker.appearAnimation = kGMSMarkerAnimationPop
        locationMarker.icon = GMSMarker.markerImage(with: UIColor.green)
        locationMarker.opacity = 10
        locationMarker.isFlat = true
        
        
        //locationMarker.snippet = "The best place on earth."
    }
    
    //make the current location appear on the map once the user’s position has been spotted,
    override func observeValue(forKeyPath keyPath: String?,of object: Any?,change: [NSKeyValueChangeKey : Any]?,context: UnsafeMutableRawPointer?)
    {
        if !didFindMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
            viewMap.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 10.0)
            viewMap.settings.myLocationButton = true
            
            didFindMyLocation = true
        }
    }
    func configureMapAndMarkersForRoute() {
        viewMap.camera = GMSCameraPosition.camera(withTarget: mapTasks.originCoordinate, zoom: 9.0)
        
        
        
        originMarker = GMSMarker(position: self.mapTasks.originCoordinate)
        originMarker.map = self.viewMap
        originMarker.icon = GMSMarker.markerImage(with: UIColor.green)
        originMarker.title = self.mapTasks.originAddress
        
        destinationMarker = GMSMarker(position: self.mapTasks.destinationCoordinate)
        destinationMarker.map = self.viewMap
        destinationMarker.icon = GMSMarker.markerImage(with: UIColor.red)
        destinationMarker.title = self.mapTasks.destinationAddress
        if waypointsArray.count > 0 {
            for waypoint in waypointsArray {
                let lat: Double = (waypoint.components(separatedBy: ",")[0] as NSString).doubleValue
                let lng: Double = (waypoint.components(separatedBy: ",")[1] as NSString).doubleValue
                let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
                marker.map = viewMap
                marker.icon = GMSMarker.markerImage(with: UIColor.purple)
                
                markersArray.append(marker)
            }
        }
        
    }
    func drawRoute() {
        let route = mapTasks.overviewPolyline["points"] as! String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)!
        routePolyline = GMSPolyline(path: path)
        routePolyline.map = viewMap
    }
    func displayRouteInfo() {
        routeInfoAlert(message: mapTasks.totalDistance + "\n" + mapTasks.totalDuration)
    }
    func recreateRoute() {
        if (routePolyline) != nil {
            clearRoute()
            
            mapTasks.getDirections(origin: mapTasks.originAddress, destination: mapTasks.destinationAddress, waypoints: waypointsArray, travelMode: travelMode, completionHandler: { (status, success) -> Void in
                
                if success {
                    self.configureMapAndMarkersForRoute()
                    self.drawRoute()
                    self.displayRouteInfo()
                }
                else {
                    print(status)
                }
            })
        }
        
    }
    // MARK: CLLocationManagerDelegate method implementation
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            viewMap.isMyLocationEnabled = true
        }
    }
    
    func clearRoute() {
        originMarker.map = nil
        destinationMarker.map = nil
        routePolyline.map = nil
        
        originMarker = nil
        destinationMarker = nil
        routePolyline = nil
        
        if markersArray.count > 0 {
            for marker in markersArray {
                marker.map = nil
            }
            
            markersArray.removeAll(keepingCapacity: false)
        }
    }
    func routeInfoAlert(message: String) {
        let alertView = UIAlertController(title: nil, message:message, preferredStyle: UIAlertControllerStyle.alert)
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            //alertView.dismiss(animated: true, completion: nil)
            _ = self.navigationController?.popViewController(animated: true)
        }
        alertView.addAction(okBtn)
        self.present(alertView, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: GMSMapViewDelegate method implementation
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if (routePolyline) != nil {
            let positionString = String(format: "%f", coordinate.latitude) + "," + String(format: "%f", coordinate.longitude)
            waypointsArray.append(positionString)
            
            recreateRoute()
        }

    }
    
    // Pass your source and destination coordinates in this method.
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        self.sourceDestinationMarker(markLocation: source,tourName: "")
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        // DispatchQueue.global(qos: .background).async {
        let url = URL(string: "http://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }else{
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        //print("json \(json)")
                        if let routes = json["routes"] as? [Any] {
                            
                            if routes.count != 0
                            {
                            if let route = routes[0] as? [String:Any] {
                                
                                if let legs = route["legs"] as? [Any] {
                                    
                                    if let leg = legs[0] as? [String:Any] {
                                        
                                        if let steps = leg["steps"] as? [Any] {
                                            
                                            if let step = steps[0] as? [String:Any] {
                                                
                                                
                                                if let polyline = step["polyline"] as? [String:Any] {
                                                    
                                                    if let points = polyline["points"] as? String {
                                                       // print("points \(points)")
                                                        DispatchQueue.main.async {
                                                             self.showPath(polyStr: points, source: source, to: destination)
                                                            self.sourceDestinationMarker(markLocation: destination,tourName: "")
                                                        }
                                                       
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                            else{
                                self.routeInfoAlert(message: "You have exceeded your daily request quota for this API. We recommend registering for a key at the Google Developers Console: https://console.developers.google.com/apis/credentials?project=_,status: OVER_QUERY_LIMIT, routes: ")
                            }
                        }
                    }
                    
                }catch{
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
   // }
    }
    func showPath(polyStr :String,source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = viewMap // Your map view
        
        polyline.strokeColor = .blue
        polyline.geodesic = true
        polyline.map = viewMap
        updateMarker(coordinates: source)
        
       // view = viewMap
        
    }
    func updateMarker(coordinates: CLLocationCoordinate2D) {
        CATransaction.begin()
        CATransaction.setAnimationDuration(10.0)
        locationMarker.position = coordinates
        CATransaction.commit()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
