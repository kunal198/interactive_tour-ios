//
//  GroupNameTableViewCell.swift
//  Interactive Tours App
//
//  Created by Brst on 11/07/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class GroupNameTableViewCell: UITableViewCell {

    @IBOutlet weak var cellContentView = UIView()
    @IBOutlet weak var groupNamelabel = UILabel()
    @IBOutlet weak var purcCellContentView = UIView()
     @IBOutlet weak var label1 = UILabel()
      @IBOutlet weak var label2 = UILabel()
      @IBOutlet weak var completedDateLbl = UILabel()
    @IBOutlet weak var tourNameLabel: UILabel!
    @IBOutlet weak var tourNamLblWidth: NSLayoutConstraint!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var purcImageView = UIImageView()
    @IBOutlet weak var scoreView = UIView()
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var cellProfileImg = UIImageView()
     @IBOutlet weak var completeLabel = UILabel()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
