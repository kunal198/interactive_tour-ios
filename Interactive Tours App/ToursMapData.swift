//
//  ToursMapData.swift
//  Interactive Tours App
//
//  Created by Brst on 20/09/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class ToursMapData: NSObject {
  
    class func getMapData(completion:@escaping (_ locResult: NSArray) -> Void)
    {
        if Reachability.shared.isConnectedToNetwork(){
            if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
            {
                var userID = NSString()
                var tourID = Int()
                if (UserDefaults.standard.value(forKey: "user_id") != nil)
                {
                    userID =  (UserDefaults.standard.value(forKey: "user_id") as? NSString)!
                }
                if (UserDefaults.standard.value(forKey: "tour_id") != nil)
                {
                    tourID =  (UserDefaults.standard.value(forKey: "tour_id") as? Int)!
                }
                //   let tour_id = String(tourID)
                
                
                SVProgressHUD .show()
               // DispatchQueue.global(qos: .background).async {
                    let url = "http://beta.brstdev.com/tours/api/web/v1/tours/latlong"
                    let session = URLSession.shared
                    let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
                    request.httpMethod = "POST"
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    
                    
                    let params:[String: AnyObject] = ["tour_id" : "1" as AnyObject]
                    
                    do{
                        request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                        let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                            if let response = response {
                                let nsHTTPResponse = response as! HTTPURLResponse
                                let statusCode = nsHTTPResponse.statusCode
                                print ("status code = \(statusCode)")
                            }
                            if let error = error {
                                print ("\(error)")
                            }
                            if let data = data {
                                do{
                                    let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                                    print ("Json Data is = \(jsonResponse)")
                                    
                                    var dataDic = NSDictionary()
                                    
                                    DispatchQueue.main.async {
                                        SVProgressHUD.dismiss()
                                    }
                                    
                                    if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                                    {
                                        dataDic = jsonResponse.object(forKey: "data") as! NSDictionary
                                        let tour_coordinates = dataDic.object(forKey: "tour_coordinates") as! NSArray
                                        
                                        let latituteArr = NSMutableArray()
                                        let longituteArr = NSMutableArray()
                                        let placeNameArr = NSMutableArray()
                                        for index in 0..<tour_coordinates.count
                                        {
                                            let arrDataDic = tour_coordinates.object(at: index) as! NSDictionary
                                            
                                            let lat = arrDataDic.object(forKey: "lat") as! NSString
                                            let long = arrDataDic.object(forKey: "long") as! NSString
                                            let place = arrDataDic.object(forKey: "place") as! NSString
                                            latituteArr.add(lat)
                                            longituteArr.add(long)
                                           placeNameArr.add(place)
                                            
                                            
                                        }
                                        
                                        UserDefaults.standard.set(latituteArr, forKey: "latituteArr")
                                        UserDefaults.standard.set(longituteArr, forKey: "longituteArr")
                                        UserDefaults.standard.set(placeNameArr, forKey: "toursNameArr")
                                        
                                  completion(latituteArr)
                                    }
                            
                                }catch _ {

                                    print ("OOps not good JSON formatted response")
                                }
                            }
                        })
                        task.resume()
                    }catch let error {
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                        }

                        print(error)
                    }       // }
            }
            else
            {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                }
            }
        }
        
       
    }
   
        
        
   

}
