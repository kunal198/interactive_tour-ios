//
//  HelpViewController.swift
//  Interactive Tours App
//
//  Created by Brst on 28/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import MessageUI
class HelpViewController: UIViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var comingSoonLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    var alertView = UIAlertController()
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set("2", forKey: "ClickedTabIndex")
          UserDefaults.standard.removeObject(forKey: "TourController")
        let tap = UITapGestureRecognizer(target: self, action: #selector(HelpViewController.tapFunction))
        emailLabel.isUserInteractionEnabled = true
        emailLabel.addGestureRecognizer(tap)
    }
    func tapFunction(sender:UITapGestureRecognizer) {
        sendMail()
    }
    override func viewWillAppear(_ animated: Bool) {
        //helpAlerBar()
        UserDefaults.standard.removeObject(forKey: "JoinGroupCheck")
    }
    func helpAlerBar()
    {
        alertView = UIAlertController(title: nil, message:"This Feature is Currently in development mode. We will be adding tutorial videos later" as String?, preferredStyle: UIAlertControllerStyle.alert)
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.okAction()
        }
        alertView.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(self.alertView, animated: true, completion: nil)
        }
        
        
        
    }
   
    func okAction()
    {
        
        let getVal = UserDefaults.standard.value(forKey:"ClickedTabIndex") as? String
        let selectInd = Int(getVal!)
        self.tabBarController?.selectedIndex = selectInd!
     }
    func sendMail() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["Highjinxtours@gmail.com"])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
