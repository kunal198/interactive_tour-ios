//
//  ToursTableCell.swift
//  Interactive Tours App
//
//  Created by brst on 20/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class ToursTableCell: UITableViewCell {
    
    @IBOutlet weak var tourScrenMainView: UIView?
    @IBOutlet weak var tourScrenSubView: UIView?
    @IBOutlet weak var cellImageView : UIImageView?
    @IBOutlet weak var cellSubImageView : UIImageView?
    @IBOutlet weak var headerLabel : UILabel?
    @IBOutlet weak var DescLabel : UILabel?
    @IBOutlet weak var subHeaderLabel : UILabel?
    @IBOutlet weak var subTitleLabel : UILabel?
    @IBOutlet weak var subDescLabel : UILabel?
    @IBOutlet weak var buyButton: UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
