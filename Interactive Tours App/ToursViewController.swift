
//  ToursViewController.swift
//  Interactive Tours App
//
//  Created by brst on 20/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import AVFoundation
class ToursViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITabBarControllerDelegate ,PuzzleSolvedProtocol,UITextFieldDelegate,PayPalPaymentDelegate,UIPickerViewDelegate,UIPickerViewDataSource,AVAudioPlayerDelegate{
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    var avPlayer = AVPlayer()
    var playerItem: AVPlayerItem?
    var descriptionAudioUrl = String()
    
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    
    var panRecognizer = UIPanGestureRecognizer()
    var panRecognizer2 = UIPanGestureRecognizer()
    var panRecognizer3 = UIPanGestureRecognizer()
    
    var itemsArray2 = NSMutableArray()
    var itemsArray3 = NSMutableArray()
    var tagVal = NSInteger()
    var itemsArray = NSMutableArray()
    var itemsArray4 = NSMutableArray()
    var itemsArray5 = NSMutableArray()
     var historicalSubArr = NSMutableArray()
   // var historicalSubArr2 = NSMutableArray()
    var ArrCountries = NSMutableArray()
    var showDataArr = NSMutableArray()
    var ArrTours = NSMutableArray()
    var ArrCountries_And_Tours = NSMutableArray()
    var toursCategoriesArr = NSMutableArray()
    // var ArrDicValuesItems = NSMutableArray()
    var cellHeight = NSInteger()
    var cellHeightTag = NSInteger()
    var tourBackTag = NSString()
       var selectedValue = Int()
    var alertView = UIAlertController()
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var table_View: UITableView!
    var numOfRowsArr = NSMutableArray()
    
    @IBOutlet weak var dropCorrectImgLabel: UILabel!
    @IBOutlet weak var strtBtnContentView: UIView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var MapContentView: UIView!
    @IBOutlet weak var PurcOrderContentView: UIView!
    @IBOutlet weak var toursHeaderlabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var purchaseTourButton: UIButton!
    
    
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var LastNameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneNumTxt: UITextField!
    @IBOutlet weak var paymentTxt: UITextField!
    @IBOutlet weak var purchaseOrderBtn: UIButton!
    @IBOutlet weak var countryTxt: UITextField!
    
    //Start View
    @IBOutlet weak var mainImageView: UIImageView!
    let userDefaults = UserDefaults.standard
    var currentImagePackage : ImagePackage?
    @IBOutlet weak var puzzleView: UIView!
    // @IBOutlet weak var origionalImageView: UIImageView!
    @IBOutlet weak var tileArea:  TileAreaView!
    var congratsMessages : [String]!
    var tilesPerRow = 3
    var originalImageShown = false
    // MARK: VARS
    let colorPalette = ColorPalette()
    var topGrips = [UIImageView]()
    var leftGrips = [UIImageView]()
    var rowColumnGrip : UIImageView?
    var firstGripOriginalFrame : CGRect?
    var firstLineOfTiles: [Tile]?
    
    // MARK: VIEWS
    @IBOutlet weak var originalImageView: UIImageView!
    @IBOutlet weak var congratsMessage: UILabel!
    @IBOutlet weak var topBank: UIView!
    @IBOutlet weak var leftBank: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var imageCaptionLabel: UILabel!
    
    // MARK: BUTTONS
    @IBOutlet weak var hintButton: UIButton!
    @IBOutlet weak var solveButton: UIButton!
    @IBOutlet weak var showOriginalButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backIcon: UIImageView!
    var dropImageView1: UIImageView!
    var dropImageView2: UIImageView!
    var dropImageView3: UIImageView!
   
    @IBOutlet weak var leftBankMarginConstraint: NSLayoutConstraint!
    ///////
    
    
    @IBOutlet weak var scrollViewImageView = UIImageView()
    
    @IBOutlet weak var roundBtn1: UIButton!
    @IBOutlet weak var roundBtn2: UIButton!
    @IBOutlet weak var roundBtn3: UIButton!
    @IBOutlet weak var uploadImaegTxtField: UITextField!
    @IBOutlet weak var uploadIcon: UIImageView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var uploadAnswerLabel: UILabel!
    @IBOutlet weak var dragAndDroplabel: UILabel!
    @IBOutlet weak var uploadSubmitButton: UIButton!
    
    
    @IBOutlet weak var dragImageBtn :UIButton!
    
    
    @IBOutlet weak var dropImgView: UIView!
    @IBOutlet weak var paymentTypeView: UIView!
    @IBOutlet weak var payPalBtn: UIButton!
    @IBOutlet weak var couponCodeBtn: UIButton!
    @IBOutlet weak var paymentViaTextBtn: UIButton!
    @IBOutlet weak var upDownArrowImgView: UIImageView!
    var paymentBtntag = NSInteger()
    var uploadSubmitBtnTag = NSInteger()
    
    @IBOutlet weak var submitBtnSubView1: UIView!
    @IBOutlet weak var submitTxtView: UITextView!
    @IBOutlet weak var yourAnswerTxtField: UITextField!
    @IBOutlet weak var submitBtnSubView2: UIView!
    @IBOutlet weak var submitBtnSubView3: UIView!
    @IBOutlet weak var radioBtn1: UIButton!
    @IBOutlet weak var radioBtn2: UIButton!
    @IBOutlet weak var radioBtn3: UIButton!
    @IBOutlet weak var radioBtn4: UIButton!
    @IBOutlet weak var submitTxtView1: UITextView!
    @IBOutlet weak var submitTxtView2: UITextView!
    @IBOutlet weak var submitTxtView3: UITextView!
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var couponCodeTxt: UITextField!
    
    @IBOutlet weak var tourPriceLabel: UILabel!
    var tour_price = NSString()
    
    var strAlertMsg = NSString()
    var strCountryID = NSNumber()
    var txnID = NSString()
     var tourNotPurchased = NSString()
    
    @IBOutlet weak var descriptionTxtview: UITextView!
    @IBOutlet weak var tourImagview: UIImageView!
    
    var groupNameStr = NSString()
    var groupIDStr = NSString()
    var speechTag = Int()
    var paymentSuccessAlert = UIAlertController()
    var profielAlertView = UIAlertController()
    var payAlertView = UIAlertController()
    var notInterNetAlert = UIAlertController()
    @IBOutlet weak var numOfMemToJoinLbl: UILabel!
    @IBOutlet weak var numOfMemToJoinTxt: UITextField!
    
    var pickerCount = NSInteger()
    
    var doneButton: UIBarButtonItem!
    
     var createProfileTag = NSString()
    var priceOfTour =  NSString()
    
    var headerTagVal = NSInteger()
    var headerTagVal2 = NSInteger()
    var pickOption :NSMutableArray = NSMutableArray()
  
    var tourPursedOrNot = Bool()
     var loactionID = NSString()
    
    var isOwner = Bool()
    var mapCheck = NSString()
    var tourPurchased = NSString()

    var strTourTypeID = NSNumber()
    var strtourSubTypeID = NSNumber()
    var strLocationID = NSNumber()
    var readMoreCheck = Int()
    var readMoreButton = UIButton()
    @IBOutlet weak var speechButton = UIButton()
    @IBOutlet weak var tourImgViewHeight: NSLayoutConstraint!
     let speechSynthesizer = AVSpeechSynthesizer()
    @IBOutlet weak var tourDetailsLabel: UILabel!
    var is_Joiner = Bool()
    var startChk = NSString()
     var isCompletedTour = Bool()
    var checkInt = Int()
      var speechUtterance: AVSpeechUtterance!
    // var player: AVAudioPlayer?
    
    @IBOutlet weak var teamLeaderPriLbl: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
      
            self.cityNameLabel?.text = "Highjinx Tours"
        isOwner = false
        is_Joiner = false
        headerTagVal = 0
        headerTagVal2 = 0
        speechTag = 0
         mapCheck = ""
        
        readMoreCheck = 0
        
        descriptionTxtview.frame.size.height = 130
        tourDetailsLabel.frame.size.height = 130
        readMoreCheck = 0
         let pos = self.view.frame.size.width/2 as CGFloat
        readMoreButton = UIButton(frame: CGRect(x:pos, y: ((tourDetailsLabel?.frame.size.height)!-20), width: self.view.frame.size.width, height: 20))
        readMoreButton.setTitle("Read More...", for: .normal)
        readMoreButton.backgroundColor = UIColor .white
        readMoreButton.setTitleColor(UIColor (red: 108/255, green: 2/255, blue: 111/255, alpha: 1), for: .normal)
        
        readMoreButton.contentHorizontalAlignment = .left
        readMoreButton.titleLabel?.font = UIFont.italicSystemFont(ofSize: 16)
        readMoreButton.addTarget(self, action:#selector(readMoreAction(sender:)), for: .touchUpInside)
        scrollContentView.addSubview(readMoreButton)

        makeAwareOfKeyboard()
        descriptionTxtview.isScrollEnabled = false
        descriptionTxtview.isUserInteractionEnabled = false

        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        numOfMemToJoinTxt.inputView = pickerView
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 33/255, green: 123/255, blue: 255/255, alpha: 1)
        doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ToursViewController.donePressed))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ToursViewController.cancelPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        numOfMemToJoinTxt.delegate = self
        numOfMemToJoinTxt.inputAccessoryView = toolBar
        

        for i in 0 ..< 100
        {
            let val = String(i)
            pickOption.add(val)
        }
        numOfMemToJoinLbl.isHidden = true
        numOfMemToJoinTxt.isHidden = true
        

        pickerCount = 0
        numOfMemToJoinTxt.isUserInteractionEnabled = true
        numOfMemToJoinTxt.isEnabled = true
        let border = CALayer()
        let width = CGFloat(1.0)
        border.frame = CGRect(x: 0, y:  numOfMemToJoinTxt.frame.size.height - width, width:   self.view.frame.size.width, height: numOfMemToJoinTxt.frame.size.height)
        border.borderWidth = width
        numOfMemToJoinTxt.layer.addSublayer(border)
        numOfMemToJoinTxt.layer.masksToBounds = true
        descriptionTxtview.isEditable = false
        purchaseTourButton.titleLabel?.numberOfLines = 2
        submitTxtView1.isUserInteractionEnabled = false
        submitTxtView1.isUserInteractionEnabled = false
        submitTxtView1.isUserInteractionEnabled = false
        uploadImaegTxtField.delegate=self
        
        self.congratsMessages = [ // Populated from Localizable.strings
            NSLocalizedString("Message01", comment: ""),
            NSLocalizedString("Message02", comment: ""),
            NSLocalizedString("Message03", comment: ""),
            NSLocalizedString("Message04", comment: ""),
            NSLocalizedString("Message05", comment: ""),
            NSLocalizedString("Message06", comment: ""),
            NSLocalizedString("Message07", comment: ""),
            NSLocalizedString("Message08", comment: ""),
            NSLocalizedString("Message09", comment: ""),
            NSLocalizedString("Message10", comment: ""),
            NSLocalizedString("Message11", comment: ""),
            NSLocalizedString("Message12", comment: ""),
            NSLocalizedString("Message13", comment: ""),
            NSLocalizedString("Message14", comment: ""),
            NSLocalizedString("Message15", comment: ""),
            NSLocalizedString("Message16", comment: ""),
            NSLocalizedString("Message17", comment: "")
        ]
        
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "Awesome Shirts, Inc."
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
                payPalConfig.payPalShippingAddressOption = .payPal;
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
        numOfRowsArr = NSMutableArray(array: itemsArray2)
        
        
        tagVal=0
        scrollView.delegate=self
        
        startButton.layer.cornerRadius=20
        
        mapButton.layer.cornerRadius=20
        mapButton.layer.borderColor=UIColor.purple.cgColor
        mapButton.layer.borderWidth=2
        
        purchaseTourButton.layer.cornerRadius=20
        purchaseTourButton.layer.borderColor=UIColor.purple.cgColor
        purchaseTourButton.layer.borderWidth=2
        
        
        //Purchase Order
        firstNameTxt.layer.cornerRadius = 20
        firstNameTxt.layer.masksToBounds = true
        firstNameTxt.layer.borderColor=UIColor.black.cgColor
        firstNameTxt.layer.borderWidth=1
        
        LastNameTxt.layer.cornerRadius = 20
        LastNameTxt.layer.masksToBounds = true
        LastNameTxt.layer.borderColor=UIColor.black.cgColor
        LastNameTxt.layer.borderWidth=1
        
        emailTxt.layer.cornerRadius = 20
        emailTxt.layer.masksToBounds = true
        emailTxt.layer.borderColor=UIColor.black.cgColor
        emailTxt.layer.borderWidth=1
        
        phoneNumTxt.layer.cornerRadius = 20
        phoneNumTxt.layer.masksToBounds = true
        phoneNumTxt.layer.borderColor=UIColor.black.cgColor
        phoneNumTxt.layer.borderWidth=1
        
        paymentTxt.layer.cornerRadius = 20
        paymentTxt.layer.masksToBounds = true
        paymentTxt.layer.borderColor=UIColor.black.cgColor
        paymentTxt.layer.borderWidth=1
        
        uploadImaegTxtField.layer.cornerRadius = 20
        uploadImaegTxtField.layer.masksToBounds = true
        uploadImaegTxtField.layer.borderColor=UIColor.darkGray.cgColor
        uploadImaegTxtField.layer.borderWidth=1
        
        yourAnswerTxtField.layer.cornerRadius = 20
        yourAnswerTxtField.layer.masksToBounds = true
        yourAnswerTxtField.layer.borderColor=UIColor.black.cgColor
        yourAnswerTxtField.layer.borderWidth=1
        
        
        paymentBtntag = 0
        uploadSubmitBtnTag = 0
        paymentTypeView.isHidden=true
        dropCorrectImgLabel.isHidden = true
        submitBtnSubView1.isHidden=true
        submitBtnSubView2.isHidden=true
        submitBtnSubView3.isHidden=true
        
        uploadImaegTxtField.isHidden=true
        uploadIcon.isHidden=true
        uploadBtn.isHidden=true
        dragAndDroplabel.isHidden=true
        uploadAnswerLabel.isHidden=true
        purchaseOrderBtn.layer.cornerRadius = 20
        purchaseOrderBtn.layer.masksToBounds = true
        
        uploadSubmitButton.isHidden=true
        uploadSubmitButton.layer.cornerRadius = 20
        uploadSubmitButton.layer.masksToBounds = true
        scrollView.isHidden=true
        
        
        puzzleView.isHidden=true
        
        roundBtn1?.layer.masksToBounds = false
        roundBtn1?.layer.cornerRadius = (roundBtn1?.frame.height)!/2
        roundBtn1?.clipsToBounds = true
        
        roundBtn2?.layer.masksToBounds = false
        roundBtn2?.layer.cornerRadius = (roundBtn1?.frame.height)!/2
        roundBtn2?.clipsToBounds = true
        
        roundBtn3?.layer.masksToBounds = false
        roundBtn3?.layer.cornerRadius = (roundBtn1?.frame.height)!/2
        roundBtn3?.clipsToBounds = true
        
        
        roundBtn3?.layer.masksToBounds = false
        roundBtn3?.layer.cornerRadius = (roundBtn1?.frame.height)!/2
        roundBtn3?.clipsToBounds = true
        
        couponCodeTxt.layer.cornerRadius = 20
        couponCodeTxt.layer.masksToBounds = true
        couponCodeTxt.layer.borderColor=UIColor.black.cgColor
        couponCodeTxt.layer.borderWidth=1
        
        countryTxt.layer.cornerRadius = 20
        countryTxt.layer.masksToBounds = true
        countryTxt.layer.borderColor=UIColor.black.cgColor
        countryTxt.layer.borderWidth=1
        paymentTxt.isUserInteractionEnabled = false
        
        txtPaddingVw(txt: uploadImaegTxtField!)
        txtPaddingVw(txt: firstNameTxt!)
        txtPaddingVw(txt: LastNameTxt!)
        txtPaddingVw(txt: emailTxt!)
        txtPaddingVw(txt: paymentTxt!)
        txtPaddingVw(txt: phoneNumTxt!)
        txtPaddingVw(txt: couponCodeTxt!)
        txtPaddingVw(txt: countryTxt!)
         txtPaddingVw(txt: numOfMemToJoinTxt!)
        //Upload button Right Corner radius
        let rectShape1 = CAShapeLayer()
        rectShape1.bounds = self.uploadBtn.frame
        rectShape1.position = self.uploadBtn.center
        
        rectShape1.path = UIBezierPath(roundedRect: self.uploadBtn.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 15, height: 15)).cgPath
        
        self.uploadBtn.layer.mask = rectShape1
        
        scoreLabel.layer.borderColor=UIColor.black.cgColor
        scoreLabel.layer.borderWidth=1
        
        firstNameTxt.delegate=self
        LastNameTxt.delegate=self
        emailTxt.delegate=self
        phoneNumTxt.delegate=self
        paymentTxt.delegate=self
        
        phoneNumTxt.keyboardType = .numberPad
        txtPaddingVw(txt: yourAnswerTxtField!)
        
        firstNameTxt.isUserInteractionEnabled = false
        LastNameTxt.isUserInteractionEnabled = false
        emailTxt.isUserInteractionEnabled = false
        phoneNumTxt.isUserInteractionEnabled = false
        firstNameTxt.isUserInteractionEnabled = false
        
        
    
        // Do any additional setup after loading the view.
        PayPalMobile.preconnect(withEnvironment: environment)
        arrData()
       //
//        self.ArrCountries_And_Tours = NSMutableArray(array: self.ArrCountries)
        couponCodeTxt.isHidden = true
        
        table_View.delegate = self
       // cityNameLabel.text = "HighJinx Tours";
        backButton.isHidden = true
        backIcon.isHidden = true
        table_View.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
       scrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
        createImageViews()
        roundBtn1.backgroundColor = UIColor(red: 92/255, green: 10/255, blue: 99/255, alpha: 1)
        dropCorrectImgLabel.isHidden = true
        upDownArrowImgView.image = UIImage(named: "dropDown.png")
        paymentTypeView.isHidden = true
        paymentBtntag = 0
        self.tabBarController?.delegate=self
    
        cellHeight=0
        
        scrollView.isHidden=true
        scrollContentView.isHidden=true
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 650)
        
        table_View.isHidden=false
        scrollContentView.isHidden=true
        strtBtnContentView.isHidden=true
        PurcOrderContentView.isHidden=true
        MapContentView.isHidden=true
        
        mainImageView.image=UIImage(named: "lineBorder.jpeg")
        self.currentImagePackage = ImagePackage(baseFileName: "", caption: "", photographer: "")
        self.currentImagePackage?.image = UIImage(named: "11medium.jpg")
        
        submitBtnSubView3.isHidden = true
        submitBtnSubView2.isHidden = true
        submitBtnSubView1.isHidden = true
        firstNameTxt.resignFirstResponder()
        
        getCountriesData(completion: { (result) in
            self.ArrCountries = NSMutableArray(array: result!)
            self.ArrCountries_And_Tours = NSMutableArray(array: self.ArrCountries)
            
        })
        
        let modelName = UIDevice.current.model
        let model = modelName as NSString
        if model .isEqual(to: "iPhone")
        {
        }
        else
        {
            tourImgViewHeight.constant = 350
        }
        
        moveToIntialView()
        
        numOfMemToJoinTxt.tag = 108;
    }
 
    func playTourDetailsSong() {
        
        DispatchQueue.main.async {
            if(!self.descriptionAudioUrl .isEqual("")) {
                //  self.audioUrlString = urlArray .object(at: self.audioCount) as! NSString
                let audUrl: URL! = URL(string: self.descriptionAudioUrl as String)
                self.avPlayer = AVPlayer(url: audUrl!)
                self.playerItem = AVPlayerItem(url: audUrl)
                self.avPlayer = AVPlayer(playerItem: self.playerItem)
                
            }
        }
        
    }
    func moveToIntialView() {
        UserDefaults.standard.set("TOURS_VIEW", forKey: "TourController")
        if UserDefaults.standard.object(forKey: "SavedViewController") != nil {
            let savedView = UserDefaults.standard.object(forKey: "SavedViewController") as! NSString
            if UserDefaults.standard.object(forKey: "QuestionView") == nil {
                
            }  else {
                if savedView .isEqual(to: "QUESTION_VIEW") {
                    //Questionnaire View Check
                    if UserDefaults.standard.object(forKey: "QuestionView") == nil {
                        
                    }  else {
                        //Questionnaire Back Button Check
                        if UserDefaults.standard.object(forKey: "QuesBack") == nil {
                           self.moveToClass()
                        }
                    }
                    
                }
            }
        }
    }
    @IBAction func readMoreAction(sender: UIButton)
    {
        if readMoreCheck == 0
        {
            readMoreButton.setTitle("Show Less", for: .normal)
            let fixedWidth = descriptionTxtview.frame.size.width
            descriptionTxtview.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = descriptionTxtview.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = descriptionTxtview.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            descriptionTxtview.frame = newFrame;
            tourDetailsLabel.frame = newFrame;
            startButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
            mapButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
            purchaseTourButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
            speechButton?.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+1
            readMoreButton.frame.origin.y = tourDetailsLabel.frame.origin.y + tourDetailsLabel.frame.size.height-10
            readMoreButton.frame.origin.x = 10
            readMoreButton.frame.size.width = 200
            readMoreButton.frame.size.height = 20
            // readMoreButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
            readMoreCheck = 1
            scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: startButton.frame.origin.y + startButton.frame.size.height+80)
            
            readMoreButton.backgroundColor = UIColor .clear
        }
        else
        {
            movePositions()
        }
    }
    func movePositions()
    {
        let pos = self.view.frame.size.width/2 as CGFloat
        readMoreButton.frame.origin.x = pos
        readMoreButton.backgroundColor = UIColor .white
       // readMoreButton.frame.origin.x = self.view.frame.size.width/2
        readMoreButton.setTitle("Read More...", for: .normal)
        descriptionTxtview.frame.size.height = 130
        tourDetailsLabel.frame.size.height = 130
        startButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
        mapButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
        purchaseTourButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
        readMoreButton.frame.origin.y = tourDetailsLabel.frame.origin.y + tourDetailsLabel.frame.size.height-20
        speechButton?.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+1
        readMoreButton.frame.size.width = self.view.frame.size.width
        readMoreCheck = 0
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: startButton.frame.origin.y + startButton.frame.size.height+80)
    }
    func textSpeech()
    {
        speechUtterance = AVSpeechUtterance(string: self.tourDetailsLabel.text!)
        //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
        speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 2.5
        // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        speechSynthesizer.speak(speechUtterance)
    }
    override func viewWillDisappear(_ animated: Bool) {
       speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
        checkInt = 0;
        speechTag = 0
        self.avPlayer.pause()
    }
    @IBAction func purcTourTxtSpeech(sender: UIButton) {
        mapCheck = "map"
        
        if speechTag == 0 {
//            if checkInt == 0 {
//                speechTag = 1;
//                // textSpeech()
//                playTourDetailsSong()
//                strAlertMsg = "Audio started"
//                alertBar()
//                checkInt = 1;
//            } else {
                //  speechSynthesizer.continueSpeaking()
               self.avPlayer.play()
                speechTag = 1;
                strAlertMsg = "Audio started"
                alertBar()
            //}
        }
        else {
            
            // speechSynthesizer.pauseSpeaking(at: AVSpeechBoundary .word)
           self.avPlayer.pause()
            speechTag = 0;
            strAlertMsg = "Audio stopped"
            alertBar()
        }
    }

    func donePressed (sender: UIButton) {
        paymentViaTextBtn.isEnabled = true
        let valueSelected = pickOption[sender.tag] as! String
        let val = String(describing: valueSelected)
        numOfMemToJoinTxt.text = val as String
        view.endEditing(true)
    }
    
    func cancelPressed() {
        paymentViaTextBtn.isEnabled = true
        view.endEditing(true) // or do something
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        if textField.tag != 108 {
            let toolBar = UIToolbar()
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.tintColor = UIColor(red: 33/255, green: 123/255, blue: 255/255, alpha: 1)
            let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(donePressed))
            
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            toolBar.setItems([ spaceButton, doneButton], animated: false)
            
            
            toolBar.isUserInteractionEnabled = true
            toolBar.sizeToFit()
            
            textField.delegate = self
            textField.inputAccessoryView = toolBar
        }
        
        paymentViaTextBtn.isEnabled = false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
         paymentViaTextBtn.isEnabled = true
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        print( "The value is\(pickOption[row])")
        return pickOption[row] as? String
        
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let val = pickOption[row] as! String
        doneButton.tag = Int(val)!
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return ArrCountries_And_Tours.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToursTableCell") as! ToursTableCell
        table_View.separatorColor = UIColor.clear
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        cell.cellImageView?.layer.cornerRadius = 12
        cell.cellImageView?.layer.masksToBounds = true
        
        cell.cellSubImageView?.layer.cornerRadius = 12
        cell.cellSubImageView?.layer.masksToBounds = true
        
        
        cell.tourScrenMainView?.layer.borderColor = UIColor .lightGray .cgColor
        cell.tourScrenMainView?.layer.borderWidth = 0.5;
        
        cell.tourScrenSubView?.layer.borderColor = UIColor .lightGray .cgColor
        cell.tourScrenSubView?.layer.borderWidth = 0.5;
        
        cell.cellSubImageView?.backgroundColor = .white
        
        cell.buyButton?.tag = indexPath.row
        
        scrollView.isHidden=true

        cell.cellSubImageView?.contentMode = .scaleAspectFill
        if tagVal==0 {
            
             cell.subHeaderLabel?.textColor = UIColor.black
            backButton.isHidden = true
            backIcon.isHidden = true
            cell.tourScrenMainView?.isHidden=false
            cell.tourScrenSubView?.isHidden=true
            if self.ArrCountries.count != 0
            {
            let strCities = self.ArrCountries[indexPath.row] as! NSDictionary
            cell.headerLabel?.text =  strCities.value(forKey: "name") as? String
            
            cell.subHeaderLabel?.frame.size.height  = 45
            cell.subHeaderLabel?.frame.origin.y = 25
            cell.subTitleLabel?.frame.origin.y = 55
             cell.subHeaderLabel?.numberOfLines = 2
                
                  cell.cellImageView?.image = nil
                cell.cellImageView?.setShowActivityIndicator(true)
                cell.cellImageView?.setIndicatorStyle(.gray)
                cell.cellImageView?.sd_setImage(with: URL(string: strCities.value(forKey: "image") as! String), completed: { (image, error, cache, url) in
                    cell.cellImageView?.setShowActivityIndicator(false)
                })
            
            }
        }
        else  if tagVal==2 {
           
            cell.buyButton?.isHidden = true
            cell.tourScrenMainView?.isHidden=true
            cell.tourScrenSubView?.isHidden=false
            cell.subTitleLabel?.isHidden = false
            cell.subHeaderLabel?.numberOfLines = 2
            cell.subHeaderLabel?.frame.size.height  = 45
            
           
            cell.subHeaderLabel?.frame.origin.y = 25
            cell.subTitleLabel?.frame.origin.y = 55
            if self.historicalSubArr.count != 0
            {
            let strTours = self.historicalSubArr[indexPath.row] as! NSDictionary
            print(self.ArrTours)

            cell.subHeaderLabel?.text = strTours.value(forKey: "name") as? String
            cell.subHeaderLabel?.textColor = UIColor.purple
        
                
                let tourCount = strTours.value(forKey: "count") as! Int
                let strTourCount = String(tourCount)

                
            cell.subTitleLabel?.text = strTourCount + " Tours"
        
                DispatchQueue.main.async {
                cell.cellSubImageView?.contentMode = .scaleAspectFit
               
                cell.cellSubImageView?.setShowActivityIndicator(true)
                cell.cellSubImageView?.setIndicatorStyle(.gray)
                cell.cellImageView?.image = nil
                
                    cell.cellSubImageView?.sd_setImage(with: URL(string: strTours.value(forKey: "image") as! String), completed: { (image, error, cache, url) in
                        cell.cellSubImageView?.setShowActivityIndicator(false)
                    })
            }
            }
        }
        else  if tagVal==3 {
            cell.buyButton?.isHidden = false

            cell.buyButton?.setTitle("INFO", for:UIControlState.normal)
            
            cell.tourScrenMainView?.isHidden=true
            cell.tourScrenSubView?.isHidden=false
            cell.subTitleLabel?.isHidden = false
            cell.subHeaderLabel?.frame.size.height  = 50
            
            cell.subHeaderLabel?.textColor = UIColor.black
           
            cell.subHeaderLabel?.frame.origin.y = 35
           
            if self.toursCategoriesArr.count != 0
            {
            let value: AnyObject? = self.toursCategoriesArr[indexPath.row] as AnyObject?
            if(value is NSDictionary)
            {
                let strTours = self.toursCategoriesArr[indexPath.row] as! NSDictionary
                cell.subHeaderLabel?.text = strTours.value(forKey: "tour_name") as? String
                
                cell.subHeaderLabel?.numberOfLines = 2
                
                
                cell.subTitleLabel?.isHidden = true
                cell.cellSubImageView?.image = nil
                cell.cellSubImageView?.setShowActivityIndicator(true)
                cell.cellSubImageView?.setIndicatorStyle(.gray)
                
                
                cell.cellSubImageView?.sd_setImage(with: URL(string: strTours.value(forKey: "image_small") as! String), completed: { (image, error, cache, url) in
                    cell.cellSubImageView?.setShowActivityIndicator(false)
                })
                }
            } else {
                let strTours = self.toursCategoriesArr[indexPath.row] as! String
                cell.subHeaderLabel?.text = strTours
            }
            
            
        }
        else
        {
             cell.subHeaderLabel?.textColor = UIColor.black
            cell.buyButton?.isHidden = true
            cell.tourScrenMainView?.isHidden=true
            cell.tourScrenSubView?.isHidden=false
            
            cell.subHeaderLabel?.numberOfLines = 2
            cell.subHeaderLabel?.frame.size.height  = 45
            cell.subHeaderLabel?.frame.origin.y = 25
            cell.subTitleLabel?.frame.origin.y = 58
            cell.cellSubImageView?.contentMode = .scaleAspectFit
            if self.ArrTours.count != 0
            {
            let strTours = self.ArrTours[indexPath.row] as! NSDictionary
            
            
            cell.subHeaderLabel?.text = strTours.value(forKey: "name") as? String
            
            cell.subHeaderLabel?.lineBreakMode = .byWordWrapping

              let tourCount = strTours.value(forKey: "count") as! Int
                var strTourCount = String()
                strTourCount = String(tourCount)

                cell.subTitleLabel?.text = strTourCount + " Tours"
                cell.cellSubImageView?.image = nil
                cell.subDescLabel?.isHidden = true
                
            
                cell.cellSubImageView?.setShowActivityIndicator(true)
                cell.cellSubImageView?.setIndicatorStyle(.gray)
                cell.cellSubImageView?.sd_setImage(with: URL(string: strTours.value(forKey: "image") as! String), placeholderImage: UIImage(named: "demoImage.jpg"))
                cell.cellSubImageView?.setShowActivityIndicator(false)
            }
        }
        
        cell.buyButton?.layer.masksToBounds = false
        cell.buyButton?.layer.cornerRadius = (cell.buyButton?.frame.height)!/2
        cell.buyButton?.clipsToBounds = true
        
        if cell.tourScrenSubView?.isHidden == false
        {
            cellHeightTag = 125
        }
        else
        {
            cellHeightTag=0
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
      
        selectedValue = indexPath.row
        if tagVal==0 {
           
            if self.ArrCountries.count != 0 {
                headerTagVal = indexPath.row
                tagVal=1;
                backButton.isHidden = false
                backIcon.isHidden = false
                tourBackTag = "tourScrenMainView"
                 if ArrCountries.count > 0 && ArrCountries.count > indexPath.row {
            let strCountries = self.ArrCountries[indexPath.row] as! NSDictionary
            cityNameLabel.text = (strCountries.value(forKey: "name") as! String?)!+(" Tours")
            
            print(strCountries)
            
            strLocationID = (strCountries.value(forKey: "location_id") as? NSNumber)!
            UserDefaults.standard.set(strCountryID, forKey: "location_id")

            getToursData(loaction_ID: strLocationID, completion: { (result) in
                
                DispatchQueue.main.sync {
                self.ArrCountries_And_Tours = NSMutableArray(array: (result)!)
                self.ArrTours = NSMutableArray(array: (result)!)
                
                
                    self.table_View.reloadData()
                }
            })
            }
            }
        }
        else if tagVal==1
        {
            
            if self.ArrCountries.count != 0
            {
                tourBackTag = "tourScrenSubView"
                
                headerTagVal2 = indexPath.row
                
                if ArrCountries.count > 0 {
                    let strCountries = self.ArrCountries[0] as! NSDictionary
                    let countryName = strCountries.value(forKey: "name") as? String
                    let locID = (strCountries.value(forKey: "location_id") as? NSNumber)!
                    loactionID = String(describing: locID) as NSString
                    
                       if ArrTours.count > 0 {
                    let strToures = self.ArrTours[indexPath.row] as! NSDictionary
                    
                    
                    strTourTypeID = (strToures.value(forKey: "tourtype_id") as? NSNumber)!
                    let tourID = String(describing: strTourTypeID)
                    UserDefaults.standard.set(tourID, forKey: "tourtype_id")
                    getTourSubTypesData(tourtype_id: strTourTypeID , completion: { (result) in
                        
                        DispatchQueue.main.sync {
                            let toursName = strToures.value(forKey: "name") as? String
                            self.cityNameLabel.text = countryName!+" "+toursName!
                            self.historicalSubArr = NSMutableArray(array: (result)!)
                            self.ArrCountries_And_Tours = NSMutableArray(array: self.historicalSubArr)
                            if self.historicalSubArr.count != 0
                            {
                                self.tagVal=2
                            }
                            self.table_View.reloadData()
                        }
                        
                    })
                    }
                }
                else{
                    //Array is empty,handle as you needed
                }
                
            
            }
        }
        else if tagVal==2 {
            if self.historicalSubArr.count != 0
            {
                headerTagVal2  = indexPath.row;
                tourBackTag = "tourScrenSubView"
                if historicalSubArr.count > 0 {
            let strTours = self.historicalSubArr[indexPath.row] as! NSDictionary
            
            strTourTypeID = (strTours.value(forKey: "tourtype_id") as? NSNumber)!
            strtourSubTypeID = (strTours.value(forKey: "toursubtype_id") as? NSNumber)!

                if ArrCountries.count > 0  {
                    let strCountries = self.ArrCountries[0] as! NSDictionary
                    strLocationID = (strCountries.value(forKey: "location_id") as? NSNumber)!
                    
                    getTourListData(location_id: strLocationID, tourtype_id: strTourTypeID , toursubtype_id: strtourSubTypeID , completion: { (result) in
                        
                        DispatchQueue.main.sync {
                            self.toursCategoriesArr = NSMutableArray(array: (result)!)
                            self.ArrCountries_And_Tours = NSMutableArray(array: self.toursCategoriesArr)
                            if self.toursCategoriesArr.count != 0
                            {
                                self.tagVal=3
                            }
                            var toursSubCat = self.historicalSubArr[indexPath.row] as? String
                            toursSubCat = strTours.value(forKey: "name") as? String
                            self.cityNameLabel.text = toursSubCat!
                            self.table_View.reloadData()
                        }
                        
                    })
                }
                else{                    //Array is empty,handle as you needed
                    
                }
                }
            }
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if cellHeightTag == 125
        {
            return 130;//Choose your custom row height
        }
        else
        {
            if self.view.frame.size.height > 1000
            {
                return 160
            }
            else
                
            {
                return 150
            }
        }
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        // successView.isHidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
            self.resultText = completedPayment.description
            
            let dictresult = completedPayment.confirmation as NSDictionary
            let response = dictresult.value(forKey: "response") as! NSDictionary
            print(response.value(forKey: "id") ?? NSString())
            
            let strID = response.value(forKey: "id") as? String
            
            self.txnID = strID! as NSString
            
            self.purchaseTour()
            
        })
    }
    // MARK: Helpers
    
    func showSuccess() {
        paymentSuccessAlertView()
    }
    @IBAction func purchaseMainAction (sender:UIButton)
    {
        if (couponCodeTxt.text?.isEmpty)! && couponCodeTxt.isHidden  == false
        {
            strAlertMsg = "Please enter coupon code"
            PurchaseAlertBar()
        }
        else if (paymentTxt.text == "Coupon Code" && !(couponCodeTxt.text?.isEmpty)!) {
            
            
            self.purchaseTour()
        }
        else if paymentTxt.text == "Paypal" {
            // paypal()
            
              self.tour_price = UserDefaults.standard.value(forKey: "tour_price") as! NSString
            let getVal = numOfMemToJoinTxt.text! as String
            let tourPri = self.tour_price as String
            
            
            let intGetVal: Float = Float(getVal)!
            let intTourPri: Float = Float(tourPri)!
            
            let divTourPri: Float = intTourPri/2
            
            let selectedUserPri: Float = divTourPri * intGetVal
            let sumVal : Float = selectedUserPri + intTourPri
            
            priceOfTour  = String(sumVal) as NSString
            
            // You have to pay the following amount. Press confirm to pay
            
            let str = " €"
            let str2 = ". Press to confirm payment"
            strAlertMsg = "Total cost is "+(str as String)+(priceOfTour as String)+(str2 as String) as String as NSString
    
            payAlertViewbar()
            
        }
        
    }
    func payAlertViewbar()
    {
        payAlertView = UIAlertController(title: nil, message:strAlertMsg as String , preferredStyle: UIAlertControllerStyle.alert)
        let canclBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.payAlertViewCancelAction()
        }
        let okBtn = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.payAlertViewOkAction()
        }
        payAlertView.addAction(canclBtn)
        payAlertView.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(self.payAlertView, animated: true, completion: nil)
        }
        
    }
    func payAlertViewOkAction()
    {
             self.tour_price = priceOfTour
        paypal()
    }
    
    func payAlertViewCancelAction()
    {
        strAlertMsg = ""
        payAlertView.dismiss(animated: true, completion: nil)
    }
    @IBAction func paypalAction (sender:UIButton)
    {
        couponCodeTxt.isHidden = true
        purchaseOrderBtn.frame.origin.y = 622
        numOfMemToJoinLbl.isHidden = false
        numOfMemToJoinTxt.isHidden = false

        paymentTxt.text = "Paypal"
        paymentBtntag = 0
        phoneNumTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        paymentTypeView.isHidden = true
        upDownArrowImgView.image = UIImage(named: "dropDown.png")
        
        teamLeaderPriLbl.isHidden = false
        checkImageView.isHidden = false
        
        
    }
    @IBAction func couponCodeAction (sender:UIButton)
    {
        
        numOfMemToJoinLbl.isHidden = true
        numOfMemToJoinTxt.isHidden = true
        couponCodeTxt.isUserInteractionEnabled = true
        couponCodeTxt.isEnabled = true
        paymentViaTextBtn.frame.origin.y = 413
        couponCodeTxt.frame.origin.y = paymentViaTextBtn.frame.origin.y+couponCodeTxt.frame.size.height+25
        purchaseOrderBtn.frame.origin.y =  couponCodeTxt.frame.origin.y+couponCodeTxt.frame.size.height+20
        
        couponCodeTxt.isHidden = false
        
        
        phoneNumTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        paymentBtntag = 0
        paymentTxt.text = "Coupon Code"
        paymentTypeView.isHidden = true
        upDownArrowImgView.image = UIImage(named: "dropDown.png")
        
        teamLeaderPriLbl.isHidden = true
        checkImageView.isHidden = true
    }
  
    func getCountriesData(completion:@escaping (_ result: NSArray?) -> Void)
        {
            if Reachability.shared.isConnectedToNetwork(){
            
        SVProgressHUD .show()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                    self.progessDismiss()
                }
                
        //tagVal = 0
       //  DispatchQueue.global(qos: .background).async {
        guard let url = URL(string: "http://beta.brstdev.com/tours/api/web/v1/tours/locationlist") else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil else { return }
            guard let data = data else { return }
            do {
                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? NSDictionary {
                    print ("Country data = \(jsonResponse)")
                     //var code = NSNumber()
                    if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                    {
                                         let dataDic : NSDictionary = (jsonResponse as AnyObject).value(forKey: "data") as! NSDictionary
                    
                    // if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary {
                    
                    let strSuccess_code = dataDic ["Success_code"] as! NSString
                    let strDescription = dataDic ["description"] as! NSString
                    
                    self.progessDismiss()
                    if strSuccess_code .isEqual(to: "201")
                    {
                        let dicArrData :NSArray = dataDic.object(forKey: "location_list") as! NSArray
                       
                         completion(dicArrData)
                     
                        
                        
                        DispatchQueue.main.sync {
                            self.table_View.reloadData()
                            
                        }
                        
                        // ArrCountries_And_Tours = NSMutableArray(array: ArrCountries)
                        
                    }
                    else
                    {
                        self.strAlertMsg = strDescription
                        self.alertBar()
                        
                    //}
                        }
                    }
                    else
                    {
                        self.strAlertMsg = "Server Not Responding"
                        self.alertBar()
                    }
                }
            } catch let error {
                print(error)
            }
            }.resume()
          //  }
            }

    else
    {
    //strAlertMsg = "No internet connection"
    self.notInterNetConn(message: "No internet connection")
    }
    }

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool
    {
        //let selectedIndex = tabBarController.selectedIndex
      //  let selectInd = String(selectedIndex)
       // UserDefaults.standard.set(selectInd, forKey: "ClickedTabIndex")
       // print("Selected shouldSelect: \(tabBarController.selectedIndex)")
        UserDefaults.standard.set("1", forKey: "ClickedTabIndex")

        return (viewController != tabBarController.selectedViewController);
    }
    func reloadViewData()
    {
        paymentBtntag = 0
        uploadSubmitBtnTag = 0
        paymentTypeView.isHidden=true
        dropCorrectImgLabel.isHidden = true
        submitBtnSubView1.isHidden=true
        submitBtnSubView2.isHidden=true
        submitBtnSubView3.isHidden=true
        
        uploadImaegTxtField.isHidden=true
        uploadIcon.isHidden=true
        uploadBtn.isHidden=true
        dragAndDroplabel.isHidden=true
        uploadAnswerLabel.isHidden=true
        purchaseOrderBtn.layer.cornerRadius = 20
        purchaseOrderBtn.layer.masksToBounds = true
        
        uploadSubmitButton.isHidden=true
        uploadSubmitButton.layer.cornerRadius = 20
        uploadSubmitButton.layer.masksToBounds = true
        // table_View.isHidden=true
        scrollView.isHidden=true
        
        
        puzzleView.isHidden=true
    }
    func createImageViews()
    {
        
        if self.view.frame.size.height < 580
        {
            dropImageView1  = UIImageView(frame:CGRect(x:self.view.frame.origin.x+5, y:strtBtnContentView.frame.origin.y+442, width:100, height:100));
            dropImageView2  = UIImageView(frame:CGRect(x:self.view.frame.size.width/2.9, y:strtBtnContentView.frame.origin.y+442, width:100, height:100));
            dropImageView3  = UIImageView(frame:CGRect(x:self.view.frame.size.width-105, y:strtBtnContentView.frame.origin.y+442, width:100, height:100));
        }
        else  if self.view.frame.size.height > 600 && self.view.frame.size.height < 700
        {
            dropImageView1  = UIImageView(frame:CGRect(x:self.view.frame.origin.x+8, y:strtBtnContentView.frame.origin.y+442, width:115, height:120));
            dropImageView2  = UIImageView(frame:CGRect(x:self.view.frame.size.width/2.9, y:strtBtnContentView.frame.origin.y+442, width:115, height:120));
            dropImageView3  = UIImageView(frame:CGRect(x:self.view.frame.size.width-124, y:strtBtnContentView.frame.origin.y+442, width:115, height:120));
        }
        else
        {
            dropImageView1  = UIImageView(frame:CGRect(x:self.view.frame.origin.x+10, y:strtBtnContentView.frame.origin.y+442, width:127, height:125));
            if self.view.frame.size.height > 1000
            {
                dropImageView2  = UIImageView(frame:CGRect(x:self.view.frame.size.width/2.3, y:strtBtnContentView.frame.origin.y+442, width:122, height:125));
            }
            else
            {
                dropImageView2  = UIImageView(frame:CGRect(x:self.view.frame.size.width/2.8, y:strtBtnContentView.frame.origin.y+442, width:122, height:125));
            }
            dropImageView3  = UIImageView(frame:CGRect(x:self.view.frame.size.width-133, y:strtBtnContentView.frame.origin.y+442, width:122, height:125));
        }
        
        
   
        
        dropImageView1.image = UIImage(named:"11medium.jpg")
        strtBtnContentView.addSubview(dropImageView1)
        dropImageView1.tag=1
        
        
        dropImageView2.image = UIImage(named:"23medium.jpg")
        strtBtnContentView.addSubview(dropImageView2)
        dropImageView2.tag=2
        
        
        dropImageView3.image = UIImage(named:"24medium.jpg")
        strtBtnContentView.addSubview(dropImageView3)
        dropImageView3.tag=3
        
        
        
        panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.move))
        panRecognizer.minimumNumberOfTouches = 1
        panRecognizer.maximumNumberOfTouches = 1
        dropImageView1.addGestureRecognizer(panRecognizer)
        
        
        panRecognizer2 = UIPanGestureRecognizer(target: self, action: #selector(self.move))
        panRecognizer2.minimumNumberOfTouches = 1
        panRecognizer2.maximumNumberOfTouches = 1
        dropImageView2.addGestureRecognizer(panRecognizer2)
        
        panRecognizer3 = UIPanGestureRecognizer(target: self, action: #selector(self.move))
        panRecognizer3.minimumNumberOfTouches = 1
        panRecognizer3.maximumNumberOfTouches = 1
        dropImageView3.addGestureRecognizer(panRecognizer3)
        
        
        dropImageView1.isUserInteractionEnabled=true
        dropImageView2.isUserInteractionEnabled=true
        dropImageView3.isUserInteractionEnabled=true
        
    }
    func move(_ sender: UIPanGestureRecognizer) {
        view.bringSubview(toFront: sender.view!)
        var translatedPoint: CGPoint = sender.translation(in: sender.view!.superview)
        var firstX: CGFloat
        var firstY: CGFloat
        if sender.state == .began {
            firstX = (sender.view?.center.x)!
            firstY = (sender.view?.center.y)!
            
        }
        translatedPoint = CGPoint(x: CGFloat((sender.view?.center.x)! + translatedPoint.x), y: CGFloat((sender.view?.center.y)! + translatedPoint.y))
        sender.view?.center = translatedPoint
        sender.setTranslation(CGPoint.zero, in: sender.view)
        
        if sender.state == .ended {
            let velocityX: CGFloat = (0.2 * sender.velocity(in: view).x)
            let velocityY: CGFloat = (0.2 * sender.velocity(in: view).y)
            var finalX: CGFloat = translatedPoint.x + velocityX
            var finalY: CGFloat = translatedPoint.y + velocityY
            // translatedPoint.y + (.35*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
            if finalX < 0 {
                finalX = 0
            }
            else if finalX > view.frame.size.width {
                finalX = view.frame.size.width
            }
            
            if finalY < 50 {
                // to avoid status bar
                finalY = 50
            }
            else if finalY > view.frame.size.height {
                finalY = view.frame.size.height
            }
            
            
            scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 750)
            puzzleView.isHidden=false
            
            if puzzleView.isHidden == false
            {
                dismissView()
            }
            self.currentImagePackage = ImagePackage(baseFileName: "", caption: "", photographer: "")
            self.currentImagePackage?.image = dropImageView3.image
            
            dropImageView1.removeFromSuperview()
            dropImageView2.removeFromSuperview()
            dropImageView3.removeFromSuperview()
            dropCorrectImgLabel.isHidden = true
            dropImageView1.isHidden=true
            dropImageView2.isHidden=true
            dropImageView3.isHidden=true
            tourBackTag = "StartBtnContentView"
        }
    }
    
    @IBAction func dropImgAction (sender:UIButton)
    {
        
    }
    func txtPaddingVw(txt:UITextField) {
        let paddingView = UIView(frame: CGRect (x:0, y:0 , width:10 , height:20))
        txt.leftViewMode = .always
        txt.leftView = paddingView
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func showInitialScreen() {
        //If Question View opened then don't initial view
         if UserDefaults.standard.object(forKey: "QuestionView") != nil || UserDefaults.standard.object(forKey: "MoreView") != nil {
            UserDefaults.standard.removeObject(forKey: "MoreView")
        } else {
        //self.tabBarController?.tabBar.isHidden = false
        table_View.isHidden = false
        tagVal = 0
        self.ArrCountries_And_Tours = NSMutableArray(array: self.ArrCountries)
        table_View.reloadData()
        BackInitial()
        tourBackTag = "tourScrenMainView"
        //Save View
        UserDefaults.standard.set("TOURS_VIEW", forKey: "SavedViewController")
        }
 }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared().isEnabled = true
        // set initial Screen/View
        showInitialScreen()
       // speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
        self.avPlayer.pause()
        speechTag = 0;
        
       UserDefaults.standard.set("1", forKey: "ClickedTabIndex")
         UserDefaults.standard.removeObject(forKey: "JoinGroupCheck")
         UserDefaults.standard.removeObject(forKey: "PuzzleScreen")
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = true
 
        if (UserDefaults.standard.value(forKey: "profileCreated") == nil)
        {
            isOwner = false
            tourPursedOrNot = false
        }
        
        
        numOfMemToJoinTxt.text = "0"
        // UserDefaults.standard.set("0", forKey: "ClickedTabIndex")
        self.tabBarController?.delegate=self
        table_View.separatorColor = UIColor.clear
        PayPalMobile.preconnect(withEnvironment: environment)

        if (UserDefaults.standard.value(forKey: "TourView") != nil)
        {
            if scrollContentView.isHidden == false {
            numOfMemToJoinTxt.isHidden = false
            numOfMemToJoinLbl.isHidden = false
            paymentTxt.text = "Paypal"
            PurcOrderContentView.isHidden = false
            scrollContentView.isHidden = true
            UserDefaults.standard.removeObject(forKey: "TourView")
            tourBackTag = "scrollView"
            self.purchaseToursData()
            }
        
        }

        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
            self.purchasedTourOrNot(completion:{ (result) in
            })
        }
    }
    func arrData()
    {
        itemsArray2 = NSMutableArray(array: ["Dubai","Dubai","Dubai","Dubai","Dubai","Dubai"])
        itemsArray3 = NSMutableArray(array: ["1 Tour of 5 available","1 Tour of 5 available","1 Tour of 5 available","1 Tour of 5 available","1 Tour of 5 available","1 Tour of 5 available"])
        itemsArray4 = NSMutableArray(array: ["Beaches","Beaches","Beaches","Beaches","Beaches","Beaches"])
        itemsArray5 = NSMutableArray(array:  ["$5","$5","$5","$5","$5","$5"])
        
        //historicalSubArr = NSMutableArray(array:  ["Family Fun Tours","Tours for Grown Ups"])
        
        toursCategoriesArr = NSMutableArray(array:["The Medieval Market Town","The New Market Town"])
        
    }
    @IBAction func BuyCellButton(_ sender: UIButton) {
        checkInt = 0
        speechTag = 0
          readMoreButton.frame.origin.y = tourDetailsLabel.frame.origin.y + tourDetailsLabel.frame.size.height-20
         scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 600)
        var indexPath = IndexPath()
        print(sender.tag)
        
        movePositions()
        if sender.tag==3  {
            indexPath = IndexPath(row: 0, section: 0)
            sender.tag = 0
        }
        else {
            indexPath = IndexPath(row: sender.tag, section: 0)
        }
        //cityNameLabel.text = toursCategoriesArr[sender.tag] as? String
        
        let cell: ToursTableCell! = (table_View.cellForRow(at: indexPath)) as! ToursTableCell
        tourImagview.image = cell.cellSubImageView?.image
        
       
        
        let value: AnyObject? = self.toursCategoriesArr[indexPath.row] as AnyObject?
        if(value is NSDictionary)
        {
            let strToures = self.toursCategoriesArr[sender.tag] as! NSDictionary
            cityNameLabel.text = strToures.value(forKey: "tour_name") as? String
            let tourID = strToures.value(forKey: "tour_id") as? NSNumber
            UserDefaults.standard.set(tourID, forKey: "tour_id")
            
            self.tour_price = strToures.value(forKey: "tour_price") as! NSString
            UserDefaults.standard.set(self.tour_price, forKey: "tour_price")
            
            let tour_name = strToures.value(forKey: "tour_name")
            UserDefaults.standard.set(tour_name, forKey: "tour_name")
            
            let description = strToures.value(forKey: "description")
            UserDefaults.standard.set(description, forKey: "description")
            
            
            if let audio = strToures.value(forKey: "audio") as? String {
                print(audio)
                playTourDetailsSong()
                descriptionAudioUrl = strToures.value(forKey: "audio") as! String
            }
            
            self.descriptionTxtview.text = description as! String!
             self.tourDetailsLabel.text = description as! String!
            
            if tourDetailsLabel.text!.characters.count < 200 {
                readMoreButton.isHidden = true;
            }
            else {
                readMoreButton.isHidden = false;
            }
            

            //================================================
            var tourPri = self.tour_price as String
            
            if tourPri.contains("$") {
                tourPri.remove(at: tourPri.startIndex)
            }//else {
            
            let intTourPri: Float = Float(tourPri)!
            let divTourPri: Float = intTourPri/2
            let sumVal = String(divTourPri) as NSString
            
            let completePrice = "1 App €"+(tourPri as String)+(" all additional App's €")+(sumVal as String)
            
            priceOfTour = tour_price
            //let completePrice = "€" + (priceOfTour as String) + (str as String) + (sumVal as String)
            // self.tour_price  = String(sumVal) as NSString
            //=======================
            self.tourPriceLabel.text = completePrice
        }
        
            else {
                let strToures = self.toursCategoriesArr[sender.tag] as! String
                print(self.ArrTours)
                cityNameLabel.text = strToures

        }
        tourBackTag = "tourScrenSecSubView2"
        
        table_View.isHidden=true
        scrollView.isHidden=false
        scrollContentView.isHidden=false
        MapContentView.isHidden=true
        PurcOrderContentView.isHidden=true
        strtBtnContentView.isHidden=true
     PurcOrderContentView.isHidden=true
        //Checked tour is purchased or not
        
        
        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
            //Checked tour is purchased or not
            self.purchasedTourOrNot(completion: { (result) in
            UserDefaults.standard.set("IsOwner", forKey: "IsOwner")
            })
            
        }
        else
        {
            
            //tourNotPurchased = "Tour Not Purchased"
            //  if tourPursedOrNot == false
            
        }
    
    }
    
    //    func buyCellBtn()
    //    {
    //         self.tourPriceLabel.text = "$" + (tour_price as String)
    //        strBackBtnTag = "tourScrenSubView"
    //
    //        table_View.isHidden=true
    //        scrollView.isHidden=false
    //        scrollContentView.isHidden=false
    //        MapContentView.isHidden=true
    //        PurcOrderContentView.isHidden=true
    //        strtBtnContentView.isHidden=true
    //
    //    }
    @IBAction func moveToMainScreen (sender:UIButton)
    {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 750)
        
        dropImageView1.isHidden=true
        dropImageView2.isHidden=true
        dropImageView3.isHidden=true
        puzzleView.isHidden=false
        self.originalImageView.image = UIImage(named: "02medium.jpg")
        self.originalImageView.layer.borderWidth = 2
        self.originalImageView.alpha = 0
        dragImageBtn.isHidden=true
        self.view.sendSubview(toBack: originalImageView)
        
    }
    //===================================================================
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
    }
//    func didAppearFunc()
//    {
//        // Initialize tileArea
//        self.tileArea.delegate = self
//        self.tileArea.imageToSolve = (self.currentImagePackage?.image!)!
//        self.tileArea.tilesPerRow = self.tilesPerRow
//        self.view.bringSubview(toFront: self.tileArea)
//        self.tileArea.initialize()
//        self.tileArea.layer.borderWidth = 2
//        
//        // Add row/column gesture
//        let rowColumnGripPanGesture = UIPanGestureRecognizer(target: self, action: #selector(handleRowColumnGripPan(_:)))
//        self.puzzleView.addGestureRecognizer(rowColumnGripPanGesture)
//        
//        //  self.initializeRowColumnGrips()
//        
//        // Set text fields
//        //congratsMessage.text = ""
//        if self.currentImagePackage?.caption == "" {
//            // self.imageCaptionLabel.text = ""
//        } else {
//            if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
//                self.imageCaptionLabel.text = "\"\(self.currentImagePackage?.caption!)\"" + "\nby " + (self.currentImagePackage?.photographer)!
//            }
//            if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
//                self.imageCaptionLabel.text = (self.currentImagePackage?.caption)! + " — " + (self.currentImagePackage?.photographer)!
//            }
//        }
//   }
//    func handleRowColumnGripPan(_ gesture:UIPanGestureRecognizer) {
//        if  !self.originalImageShown { // Gesture should be allowed
//            switch gesture.state {
//            case .began:
//                let startingPoint:CGPoint = gesture.location(in: self.view)
//                self.rowColumnGrip = self.findRowColumnGripWithPoint(startingPoint, first: true)
//                if self.rowColumnGrip != nil { // The first handle was detected and stored for later
//                    self.firstLineOfTiles = self.tileArea.makeLineOfTiles(self.rowColumnGrip!.tag)
//                    for tile in self.firstLineOfTiles! {
//                        self.tileArea.bringSubview(toFront: tile.imageView)
//                        tile.originalFrame = tile.imageView.frame
//                    }
//                    self.view.bringSubview(toFront: self.rowColumnGrip!)
//                    self.firstGripOriginalFrame = self.rowColumnGrip!.frame
//                }
//            case .changed:
//                if self.rowColumnGrip != nil { // There is a grip selected. Determine translation
//                    let translation = gesture.translation(in: self.view)
//                    if (self.rowColumnGrip!.tag - 100) < 0 { // line 1 is a column
//                        if self.rowColumnGrip!.frame.minX + translation.x > self.tileArea.frame.minX && self.rowColumnGrip!.frame.maxX + translation.x < self.tileArea.frame.maxX {
//                            // Translation is valid - translate the line of tiles and grip
//                            for tile in self.firstLineOfTiles! {
//                                tile.imageView.center.x = tile.imageView.center.x + translation.x
//                            }
//                            self.rowColumnGrip!.center.x = self.rowColumnGrip!.center.x + translation.x
//                        }
//                    } else { // line 1 is a Row
//                        if self.rowColumnGrip!.frame.minY + translation.y > self.tileArea.frame.minY && self.rowColumnGrip!.frame.maxY + translation.y < self.tileArea.frame.maxY {
//                            // Translation is valid - translate the line of tiles and grip
//                            for tile in self.firstLineOfTiles! {
//                                tile.imageView.center.y = tile.imageView.center.y + translation.y
//                            }
//                            self.rowColumnGrip!.center.y = self.rowColumnGrip!.center.y + translation.y
//                        }
//                    }
//                    gesture.setTranslation(CGPoint.zero, in: self.view)
//                }
//            case .ended:
//                if self.rowColumnGrip != nil {
//                    let endingPoint :CGPoint = gesture.location(in: self.view)
//                    let secondRowColumnGrip = self.findRowColumnGripWithPoint(endingPoint, first: false)
//                    if secondRowColumnGrip != nil { // A valid second grip was found at the endingPoint
//                        // Swap the lines of tiles
//                        let secondLineOfTiles = self.tileArea.makeLineOfTiles(secondRowColumnGrip!.tag)
//                        self.tileArea.swapLines(self.firstLineOfTiles!, line2: secondLineOfTiles)
//                        
//                        // Swap the grips
//                        UIView.animate(withDuration: 0.3, animations: { () -> Void in
//                            self.rowColumnGrip!.frame = secondRowColumnGrip!.frame
//                            secondRowColumnGrip!.frame = self.firstGripOriginalFrame!
//                        })
//                        
//                        // Swap the tags of the buttons
//                        let tempTag = self.rowColumnGrip!.tag
//                        self.rowColumnGrip!.tag = secondRowColumnGrip!.tag
//                        secondRowColumnGrip!.tag = tempTag
//                        self.firstGripOriginalFrame = nil
//                        self.rowColumnGrip = nil
//                        
//                    } else { // Send the button and line of tiles back to where they started
//                        UIView.animate(withDuration: 0.3, animations: { () -> Void in
//                            for tile in self.firstLineOfTiles! {
//                                tile.imageView.frame = tile.originalFrame!
//                            }
//                            self.rowColumnGrip!.frame = self.firstGripOriginalFrame!
//                            self.firstGripOriginalFrame = nil
//                            self.rowColumnGrip = nil
//                        })
//                    }
//                }
//            case .possible:
//                print("possible")
//            case .cancelled:
//                print("cancelled")
//            case .failed:
//                print("failed")
//            }
//        }
//    }
    
//    // This returns a grip that contains a CGPoint. Used to find the initial grip when first is true. Else, use a larger target space to find a grip when the pan gesture ends
//    func findRowColumnGripWithPoint(_ point: CGPoint, first: Bool) -> UIImageView? {
//        for index in 0..<self.tilesPerRow {
//            var topArea : CGRect
//            var leftArea : CGRect
//            let topGrip = self.topGrips[index]
//            let leftGrip = self.leftGrips[index]
//            if first { // Find the initial grip
//                topArea = topGrip.frame
//                leftArea = leftGrip.frame
//                if topArea.contains(point) {
//                    return topGrip
//                }
//                if leftArea.contains(point) {
//                    return leftGrip
//                }
//            } else {
//                // The target areas are larger to make it easier to find the second grip / column to swap
//                // Ensure that it is not the same as the first grip
//                // Check if it is of similar type (row vs column)
//                topArea = CGRect(x: topGrip.frame.origin.x, y: topGrip.frame.origin.y - 200, width: topGrip.frame.size.width, height: topGrip.frame.size.height + 400)
//                leftArea = CGRect(x: leftGrip.frame.origin.x - 200, y: leftGrip.frame.origin.y, width: leftGrip.frame.size.width + 400, height: leftGrip.frame.size.height)
//                if topArea.contains(point) && topGrip != self.rowColumnGrip && abs(self.rowColumnGrip!.tag - topGrip.tag) < 11 {
//                    return topGrip
//                }
//                if leftArea.contains(point) && leftGrip != self.rowColumnGrip && abs(self.rowColumnGrip!.tag - leftGrip.tag) < 11  {
//                    return leftGrip
//                }
//            }
//        }
//        return nil
//    }
    // MARK: Other class methods
    func puzzleIsSolved() {
        strAlertMsg = "Puzzle Completed"
        UserDefaults.standard.removeObject(forKey: "PuzzleScreen")
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
           
            
            self.moveToClass()
            
        }
    
    
//        dropImageView1.removeFromSuperview()
//        dropImageView2.removeFromSuperview()
//        dropImageView3.removeFromSuperview()
//        backButton.isHidden = false
//        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 650)
//        puzzleView.isHidden=true
//        uploadImaegTxtField.isHidden=false
//        uploadIcon.isHidden=false
//        uploadBtn.isHidden=false
//        dropImageView1.isHidden=true
//        dropImageView2.isHidden=true
//        dropImageView3.isHidden=true
//        mainImageView.isHidden=true
//        dragAndDroplabel.isHidden=true
//        uploadSubmitButton.isHidden=false
//        uploadAnswerLabel.isHidden=false
//        dropCorrectImgLabel.isHidden = true
//        
//        
//        alertBar()
//        // Update stats
//        let stats = Stats()
//        stats.updateSolveStats(self.tilesPerRow)
//        strBackBtnTag = "submitBtnSubView1"
//
//        
//        let when = DispatchTime.now() + 1
//        DispatchQueue.main.asyncAfter(deadline: when){
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "QuesAnsViewController") as! QuesAnsViewController
//            // self.navigationController?.present(nextViewController, animated: true)
//            self.navigationController?.pushViewController(nextViewController, animated: true)
//        }
//        
    }
    func moveToClass() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
  
            let nextViewController = storyBoard.instantiateViewController(withIdentifier:
                "QuesAnsViewController") as! QuesAnsViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
      
    }
    func alertBar()
    {
        alertView = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        //alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        self.present(alertView, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            self.alertView.dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func startButton (sender:UIButton)
    {
        if (UserDefaults.standard.value(forKey: "profileCreated") == nil)
        {
               self.strAlertMsg = "To start the tour:\n\n"+"1. Click on JOIN TOUR and enter group code\n"+"or\n"+"2. Click PURCHASE TOUR." as NSString
           self.paymentSuccessAlertView()
        }
        else
        {
            var tourID = Int()
            
            if UserDefaults.standard.object(forKey: "tour_id") != nil{
                tourID = UserDefaults.standard.object(forKey: "tour_id") as! Int
                UserDefaults.standard.set(tourID, forKey: "tour_id")
            }
        
            var getID = String()
     
        if UserDefaults.standard.value(forKey: "TourId") != nil
        {
            getID = UserDefaults.standard.value(forKey: "TourId") as! String
           // tourID = UserDefaults.standard.object(forKey: "tour_id") as! Int
        }
//        //Check tour is complted or not
//        if getID.contains(String(describing:tourID))
//        {
//            UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
//        }
       
        
        
        if self.isCompletedTour == true
        {
            self.notInterNetConn(message: "You have already completed this tour. You can see the details under My Profile/Tour History. If you would like to take the tour again, you will need to purchase it.")
        }

        else
        {
        self.startChk = "startButton"
        
        self.tourBackTag =  "tourScrenSecSubView2"
       
        if self.is_Joiner == true {
            //Joiner Can't be a Owner...
            UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
             self.moveToClass()
        }
        else{
        self.mapCheck = ""

        if self.tourPursedOrNot == false
        {
            UserDefaults.standard.removeObject(forKey: "IsOwner")
            self.strAlertMsg = "To start the tour:\n\n"+"1. Click on JOIN TOUR and enter group code\n"+"or\n"+"2. Click PURCHASE TOUR." as NSString
            self.paymentSuccessAlertView()

        }
        else if self.isOwner == false
        {
            UserDefaults.standard.removeObject(forKey: "IsOwner")
            
            self.strAlertMsg = "You are not an owner of this Tour, Please click Purchase button to PURCHASE TOUR this Tour"
            
            self.paymentSuccessAlertView()
        }
        else
        {
            //speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
           self.avPlayer.pause()
            //Get all ID saved when Joined Group
            if  UserDefaults.standard.object(forKey: "JoinGroupSavedTourID") != nil {
                
                var arrjoinId = [Int]()
                
                arrjoinId = UserDefaults.standard.object(forKey: "JoinGroupSavedTourID")as! [Int]
                print(arrjoinId)
                if arrjoinId.contains(tourID)
                {
                    UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
                }
                else
                {
                    UserDefaults.standard.set("IsOwner", forKey: "IsOwner")
                }
                // print(UserDefaults.standard.object(forKey: "JoinGroupSavedTourID") as! NSArray)
                
            }
            else
            {
                UserDefaults.standard.set("IsOwner", forKey: "IsOwner")
            }
            
             if self.is_Joiner == true {
               UserDefaults.standard.set("IsOwner", forKey: "IsOwner")
            }
//            else
//            {
//              UserDefaults.standard.set("IsOwner", forKey: "IsOwner")
//            }
            UserDefaults.standard.set("HomeScreen", forKey: "PuzzleScreen")
            let when = DispatchTime.now() + 0.2
            DispatchQueue.main.asyncAfter(deadline: when){
                self.movePositions()
                
                
             self.moveToClass()
            }
            //self.dropPuzzleImages()
        }
    //}
    }
    }
            
    }
    }
    func dropPuzzleImages()
    {
        cityNameLabel.text = "Highjinx Tours";
        
        tourBackTag = "scrollView"
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 700)
        scrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
        dropImageView1.isHidden = false
        dropImageView2.isHidden = false
        dropImageView3.isHidden = false
        mainImageView.isHidden = false
        
        dropCorrectImgLabel.isHidden = false
        dragAndDroplabel.isHidden = false
        
        scrollView.addSubview(strtBtnContentView)
        
        table_View.isHidden=true
        scrollContentView.isHidden=true
        strtBtnContentView.isHidden=false
        
        submitBtnSubView1.isHidden = true
        submitBtnSubView2.isHidden = true
        submitBtnSubView3.isHidden = true
        
        uploadAnswerLabel.isHidden = true
        dropCorrectImgLabel.isHidden = false
        uploadSubmitButton . isHidden = true

    }
    @IBAction func MapButton (sender:UIButton)
    {
        
        if tourPursedOrNot == false && is_Joiner == false {
            mapCheck = "map"
            strAlertMsg = "Please purchase tour first to use this feature"
            paymentSuccessAlertView()
        }
        else {
            if UserDefaults.standard.object(forKey: "tour_id") != nil{
            //    let tourID = UserDefaults.standard.object(forKey: "tour_id") as! Int
                
              //  if  tourID == 4 {
                    //speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
                   self.avPlayer.pause()
                    ToursMapData.getMapData(completion: {(locResult)in
                        
                    })
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "ToursMapViewController") as! ToursMapViewController
                    self.navigationController?.pushViewController(newViewController, animated: true)
//                }
//                else {
//                    self.strAlertMsg = "Map is not available for this tour"
//                    self.alertBar()
//                }
            }
        }
    }
    func progessDismiss() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func progessShow() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    @IBAction func purchaseTourBtn (sender:UIButton) {
        
        teamLeaderPriLbl.isHidden = false
        checkImageView.isHidden = false
        
        var strTourID = Int()
        var tourID = Int()
        if  UserDefaults.standard.object(forKey: "JoinGroupTourID") != nil {
            strTourID = UserDefaults.standard.object(forKey: "JoinGroupTourID") as! Int
            tourID =  (UserDefaults.standard.value(forKey: "tour_id") as? Int)!
            
        }
        else {
            strTourID = 1111
        }
        if  is_Joiner == true {
            if is_Joiner == true && isCompletedTour == false {
                tourValCheck()
            }
            else {
                if tourPursedOrNot == true {
                    purcTourData()
                }
                else {
                    if  isCompletedTour == false{
                        tourValCheck()
                    } else {
                        purcTourData()
                    }
                }
            }
        }
        else {
            if strTourID == tourID {
                tourValCheck()
            }
            else {
                purcTourData()
            }
            
        }
            
    
    }
    func tourValCheck()
    {
        self.tourPurchased = "purchased"
        UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
        isOwner = false
        tourPursedOrNot = false
        strAlertMsg = "You are subscriber to this tour. You can not purchase this tour, you just start tour"
        paymentSuccessAlertView()
    }
    func purcTourData()
    {
       
        speechTag = 0
        if tourPursedOrNot == true && isCompletedTour == false {
            tourBackTag = "tourScrenSecSubView2"
            self.tourPurchased = "purchased"
            strAlertMsg = "You have already purchased this tour"
            paymentSuccessAlertView()
        }
        else {
             startChk = "purchaseTourBtn"
            speechTag = 0
            checkInt = 0
            //speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
            self.avPlayer.pause()
            firstNameTxt.textColor = UIColor .lightGray
            LastNameTxt.textColor = UIColor .lightGray
            emailTxt.textColor = UIColor .lightGray
            phoneNumTxt.textColor = UIColor .lightGray
            countryTxt.textColor = UIColor .lightGray
            numOfMemToJoinTxt.text = "0"
            countryTxt.isUserInteractionEnabled = false
            countryTxt.isEnabled = true
            purchaseOrderBtn.frame.origin.y = 622
            couponCodeTxt.isHidden = true
            
            if (UserDefaults.standard.value(forKey: "profileCreated") != nil) {
                movePositions()
                purchaseToursData()
                paymentTypeView.isHidden = true
                paymentBtntag = 0
                paymentTxt.text = "Paypal"
                upDownArrowImgView.image = UIImage(named: "dropDown.png")
                
                tourBackTag = "scrollView"
                scrollView.addSubview(PurcOrderContentView)
                scrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                MapContentView.isHidden=true
                strtBtnContentView.isHidden=true
                PurcOrderContentView.isHidden=false
                numOfMemToJoinLbl.isHidden = false
                numOfMemToJoinTxt.isHidden = false
            }
            else {
                profileAlertbar()
            }
           self.scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 700)
        }

    }
    @IBAction func uploadSubmitBtn (sender:UIButton)
    {
        submitBtnSubView3.isHidden = true
        submitBtnSubView2.isHidden = true
        submitBtnSubView1.isHidden = true
        if uploadSubmitBtnTag == 0 {
            strtBtnContentView.addSubview(submitBtnSubView1)
            uploadSubmitBtnTag = 1
            submitBtnSubView3.isHidden = true
            submitBtnSubView2.isHidden = true
            submitBtnSubView1.isHidden = false
            roundBtn1.backgroundColor = UIColor(red: 92/255, green: 10/255, blue: 99/255, alpha: 1)
            roundBtn2.backgroundColor = UIColor.black
            roundBtn3.backgroundColor = UIColor.black
            tourBackTag = "submitBtnSubView1"
        }
        else if uploadSubmitBtnTag == 1 {
            strtBtnContentView.addSubview(submitBtnSubView2)
            uploadSubmitBtnTag = 2
            submitBtnSubView3.isHidden = true
            submitBtnSubView2.isHidden = false
            submitBtnSubView1.isHidden = true
            roundBtn2.backgroundColor = UIColor(red: 92/255, green: 10/255, blue: 99/255, alpha: 1)
            roundBtn1.backgroundColor = UIColor.black
            roundBtn3.backgroundColor = UIColor.black
            tourBackTag = "submitBtnSubView2"
        }
        else if uploadSubmitBtnTag == 2 {
            strtBtnContentView.addSubview(submitBtnSubView3)
            submitBtnSubView2.isHidden = true
            submitBtnSubView1.isHidden = true
            uploadSubmitButton.isHidden = true
            submitBtnSubView3.isHidden = false
            
            roundBtn3.backgroundColor = UIColor(red: 92/255, green: 10/255, blue: 99/255, alpha: 1)
            roundBtn2.backgroundColor = UIColor.black
            roundBtn1.backgroundColor = UIColor.black
            tourBackTag = "submitBtnSubView3"
        }
        else {
            
        }
        
    }
    
    @IBAction func openPaymentTypeView (sender:UIButton)
    {
        if paymentBtntag == 0
        {
            paymentTypeView.isHidden=false
            paymentBtntag = 1
            upDownArrowImgView.image = UIImage(named: "dropUp.png")
        }
        else
        {
            upDownArrowImgView.image = UIImage(named: "dropDown.png")
            paymentBtntag = 0
            paymentTypeView.isHidden=true
        }
        
        
    }
    @IBAction func radioFirstBtn (sender:UIButton)
    {
        radioBtn1.setImage(UIImage(named: "selectedradio.png")!, for: UIControlState.normal)
        radioBtn2.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn3.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn4.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
    }
    @IBAction func radioSecondBtn (sender:UIButton)
    {
        radioBtn2.setImage(UIImage(named: "selectedradio.png")!, for: UIControlState.normal)
        radioBtn1.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn3.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn4.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
    }
    @IBAction func radioThirdBtn (sender:UIButton)
    {
        radioBtn3.setImage(UIImage(named: "selectedradio.png")!, for: UIControlState.normal)
        radioBtn1.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn2.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn4.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
    }
    @IBAction func radioFourthBtn (sender:UIButton)
    {
        radioBtn4.setImage(UIImage(named: "selectedradio.png")!, for: UIControlState.normal)
        radioBtn2.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn1.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn3.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        hideFunc()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        hideFunc()
        
    }
    func hideFunc()
    {
        couponCodeTxt.resignFirstResponder()
        uploadImaegTxtField.resignFirstResponder()
        firstNameTxt.resignFirstResponder()
        LastNameTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        phoneNumTxt.resignFirstResponder()
        paymentTxt.resignFirstResponder()
        countryTxt.resignFirstResponder()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == table_View || scrollView == scrollView {
            let
            a = scrollView.contentOffset
            if a.y <= 0 {
                scrollView.contentOffset = CGPoint.zero
                // this is to disable tableview bouncing at top.
            }
        }
        
    }
    func dismissView() {
        view.removeGestureRecognizer(panRecognizer)
        view.removeGestureRecognizer(panRecognizer2)
        view.removeGestureRecognizer(panRecognizer3)
    }
    func purchaseTour()
    {
        if Reachability.shared.isConnectedToNetwork(){
           
        tour_price = UserDefaults.standard.value(forKey: "tour_price") as! NSString
        let tour_Id = UserDefaults.standard.value(forKey: "tour_id")
        var user_ID = NSString()
        
        user_ID =  UserDefaults.standard.value(forKey: "user_id") as! NSString
        SVProgressHUD .show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }

      //   DispatchQueue.global(qos: .background).async {
        let url = "http://beta.brstdev.com/tours/api/web/v1/tours/tourpurchase"
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
            var params = [String: AnyObject]()
            let paymenttype = self.paymentTxt.text! as String
            
            let strpaymenttype = paymenttype as NSString
            
            
            if strpaymenttype .isEqual(to: "Paypal")
            {
                params = ["firstname" : self.firstNameTxt.text as AnyObject,"lastname" : self.LastNameTxt.text as AnyObject,"email" : self.emailTxt.text as AnyObject,"phone" : self.phoneNumTxt.text as AnyObject,"payment_type" : "paypal" as AnyObject,"location_id" : self.strLocationID as AnyObject,"tourtype_id": self.strTourTypeID as AnyObject,"toursubtype_id": self.strtourSubTypeID as AnyObject,"tour_id" : tour_Id as AnyObject,"txn_id" : self.txnID as AnyObject,"coupon_code" : self.couponCodeTxt.text as AnyObject,"tour_price" :  self.tour_price as AnyObject,"user_id" : user_ID as AnyObject,"participant": self.numOfMemToJoinTxt.text as AnyObject]
            }
            else
            {
                let couponcode = self.couponCodeTxt.text! as String
                
                let strCop = couponcode as NSString
                params = ["firstname" : self.firstNameTxt.text as AnyObject,"lastname" : self.LastNameTxt.text as AnyObject,"email" : self.emailTxt.text as AnyObject,"phone" : self.phoneNumTxt.text as AnyObject,"payment_type" : "coupon" as AnyObject,"location_id" : self.strLocationID as AnyObject,"tourtype_id": self.strTourTypeID as AnyObject,"toursubtype_id": self.strtourSubTypeID as AnyObject,"tour_id" : tour_Id as AnyObject,"txn_id" : self.txnID as AnyObject,"coupon_code" : strCop as NSString,"tour_price" :  self.tour_price as AnyObject,"user_id" : user_ID as AnyObject]
            }
            
            do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code = \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print ("purchaseTour() = \(jsonResponse)")
                        
                        var dataDic = NSDictionary()
                        
                        var strSuccess_code = NSString()
                        var strDescription = NSString()
                        
                        if let strSuccess_code1 = jsonResponse ["status_code"] as? NSString {
                            strSuccess_code = strSuccess_code1
                        }
                        
                        self.paymentViaTextBtn.isEnabled = true
                        if let strDesc = jsonResponse ["description"] as? NSString {
                            strDescription = strDesc
                        }
                        if strSuccess_code .isEqual(to: "400")
                        {
                            self.tourNotPurchased = ""
                            
                            self.strAlertMsg = strDescription
                            self.PurchaseAlertBar()
                        }
                        else
                        {
                           
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                            self.tourNotPurchased = ""
                            dataDic = jsonResponse.value(forKey: "data") as! NSDictionary
                            print ("Profile Data = \(dataDic)")
                            strSuccess_code = dataDic ["Success_code"] as! NSString
                            self.groupNameStr = dataDic ["group_name"] as! NSString
                            // let group_id = dataDic.value(forKey: "group_id") as! Int
                            
                            
                            let group_id = dataDic.value(forKey: "group_id") as! Int
                            self.groupIDStr = String(group_id) as NSString
                                if self.numOfMemToJoinTxt.text == "0" {
                                    self.strAlertMsg = "Congratulations, you have successfully purchased the tour. Now you can start"
                                }
                                else {
                                self.strAlertMsg = "\(strDescription) \("Group Code:") \(self.groupNameStr)" as NSString
                                }
                            self.progessDismiss()
                            if strSuccess_code .isEqual(to: "201")
                            {
                                self.tourPurchased = "purchased"
                                self.tourPursedOrNot = true
                                self.tourNotPurchased = ""
                                self.paymentSuccessAlertView()
                                
                            }
                                
                            else
                            {
                                self.strAlertMsg = strDescription
                                self.PurchaseAlertBar()
                                
                                
                                
                            }
                    
                        }
                            else
                            {
                                self.strAlertMsg = "Server Not Responding"
                                self.alertBar()
                            }
                        }
                       

                    }catch _ {
                        self.progessDismiss()
                        print ("OOps not good JSON formatted response")
                    }
                }
            })
            task.resume()
        }catch _ {
            self.progessDismiss()
            print ("Oops something happened buddy")
        }
       // }
        }
        else
        {
            self.notInterNetConn(message: "No internet connection")
        }
    }
    func BackInitial() {
        cityNameLabel.text  = "Highjinx Tours"
        tourBackTag = ""
        scrollView.isHidden = true
        table_View.isHidden = false
        ArrCountries_And_Tours = NSMutableArray(array: ArrCountries)
        table_View.reloadData()
        tagVal = 0
    }
    
    @IBAction func backButton(sender: UIButton)   {
        progessDismiss()
       //speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
       self.avPlayer.pause()
        speechTag = 0
        if  tourBackTag .isEqual(to: "tourScrenMainView") {
           BackInitial()
        }
        else if  tourBackTag .isEqual(to: "tourScrenSubView") && tagVal != 3 {
            let strCountries = self.ArrCountries[headerTagVal] as! NSDictionary
            cityNameLabel.text = (strCountries.value(forKey: "name") as! String?)!+(" Tours")

            
            tourBackTag = "tourScrenMainView"
            tagVal = 1
            scrollView.isHidden = true
            table_View.isHidden = false
            ArrCountries_And_Tours = NSMutableArray(array: ArrTours)
            table_View.reloadData()
        }
            //new
            
        else if  tourBackTag .isEqual(to: "tourScrenSubView") && tagVal == 3 {
            
            let strCountries = self.ArrCountries[0] as! NSDictionary
            let countryName = strCountries.value(forKey: "name") as? String
            
            let strToures = self.historicalSubArr[headerTagVal2] as! NSDictionary
            let toursName = strToures.value(forKey: "tourtype_name") as? String
            cityNameLabel.text = countryName!+" "+toursName!
            tourBackTag = "tourScrenSubView"
    
            tagVal = 2
            table_View.isHidden=false

            ArrCountries_And_Tours = NSMutableArray(array: historicalSubArr)
            table_View.reloadData()
            
            
        }
            //new
        else if  tourBackTag .isEqual(to: "tourScrenSecSubView2") {
             movePositions()
            tagVal = 3
            let strToures = self.historicalSubArr[headerTagVal2] as! NSDictionary
            let toursName = strToures.value(forKey: "name") as? String
            cityNameLabel.text = toursName

            tourBackTag = "tourScrenSubView"
            scrollView.isHidden = true
            table_View.isHidden = false
            ArrCountries_And_Tours = NSMutableArray(array: self.toursCategoriesArr)
            table_View.reloadData()
        }
            
        else if  tourBackTag .isEqual(to: "scrollView") {
            movePositions()
             //cityNameLabel.text = toursCategoriesArr[selectedValue] as? String
            scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 600)
            tourBackTag = "tourScrenSecSubView2"
            table_View.isHidden=true
            scrollContentView.isHidden = false
            strtBtnContentView.isHidden = true
            MapContentView.isHidden = true
            PurcOrderContentView.isHidden = true
            scrollView.isHidden = false
            
        }
        
        else if tourBackTag .isEqual(to: "StartBtnContentView") {
            tourBackTag = "scrollView"
            strtBtnContentView.isHidden = false
            mainImageView.isHidden = false
            tileArea.isHidden = false
            dropCorrectImgLabel.isHidden = false
            uploadImaegTxtField.isHidden = true
            uploadBtn.isHidden = true
            uploadIcon.isHidden = true
            uploadAnswerLabel.isHidden = true
            uploadSubmitButton.isHidden = true
            dragAndDroplabel.isHidden = false
            puzzleView.isHidden = true
            createImageViews()
        }
            
        else if tourBackTag .isEqual(to: "submitBtnSubView1") {
            tourBackTag = "StartBtnContentView"
            strtBtnContentView.isHidden = false
            uploadBtn.isHidden = true
            uploadIcon.isHidden = true
            uploadAnswerLabel.isHidden = true
            uploadSubmitButton.isHidden = true
            puzzleView.isHidden = false
            
        }
    }
    func getToursData(loaction_ID: NSNumber,  completion:@escaping (_ result: NSArray?) -> Void)
    {
   
        if Reachability.shared.isConnectedToNetwork(){
       
        SVProgressHUD .show()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }

         //DispatchQueue.global(qos: .background).async {
        let url = "http://beta.brstdev.com/tours/api/web/v1/tours/tourtypelist"
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let params:[String: AnyObject] = ["location_id" : loaction_ID as AnyObject]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code = \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        //print ("Json Data is = \(jsonResponse)")
                        self.progessDismiss()
                        if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                        {
                            
                           
                        let dataDic : NSDictionary = jsonResponse.value(forKey: "data") as! NSDictionary
                        print ("Tours data = \(dataDic)")
                        let strSuccess_code = dataDic ["Success_code"] as! NSString
                         let strDescription = dataDic ["description"] as! NSString
                         self.progessDismiss()
                        if strSuccess_code .isEqual(to: "201")
                        {
                            let dicArrData :NSArray = dataDic.object(forKey: "tourtype_list") as! NSArray
                            
                            completion(dicArrData)
                            
                        }
                        
                        else{
                            self.strAlertMsg = strDescription
                            self.alertBar()
                        }
                        }
                        else
                        {
                            self.strAlertMsg = "Server Not Responding"
                            self.alertBar()
                        }
                        
                    }catch _ {
                         self.progessDismiss()
                        print ("OOps not good JSON formatted response")
                    }
                }
            })
            task.resume()
        }catch _ {
             self.progessDismiss()
            print ("Oops something happened buddy")
        }
       // }
        }
        else
        {
             self.progessDismiss()
          self.notInterNetConn(message: "No internet connection")
        }
    }
    //Tour Sub Types:
    
    func getTourSubTypesData(tourtype_id: NSNumber,  completion:@escaping (_ result: NSArray?) -> Void)
    {
        if Reachability.shared.isConnectedToNetwork(){
       
        SVProgressHUD .show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
       // DispatchQueue.global(qos: .background).async {
            let url = "http://beta.brstdev.com/tours/api/web/v1/tours/toursubtypelist"
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let params:[String: AnyObject] = ["tourtype_id" : tourtype_id as AnyObject]
            
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                    if let response = response {
                        let nsHTTPResponse = response as! HTTPURLResponse
                        let statusCode = nsHTTPResponse.statusCode
                        print ("status code = \(statusCode)")
                    }
                    if let error = error {
                        print ("\(error)")
                    }
                    if let data = data {
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            print ("getTourSubTypesData = \(jsonResponse)")
                            
                            self.progessDismiss()
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                               
                            let dataDic : NSDictionary = jsonResponse.value(forKey: "data") as! NSDictionary
                            //print ("Tours data = \(dataDic)")
                            let strSuccess_code = dataDic ["Success_code"] as! NSString
                             let strDescription = dataDic ["description"] as! String
                                if strSuccess_code .isEqual(to: "201")
                                {
                                    
                                    let dicArrData :NSArray = dataDic.object(forKey: "toursubtype_list") as! NSArray
                                    completion(dicArrData)
                                }
                                    
                                else
                                {
                                    self.notInterNetConn(message: strDescription)
                                }
                            }
                            else
                            {
                              
                                self.strAlertMsg = "Server Not Responding"
                                self.alertBar()
                            }
                        }catch _ {
                            self.progessDismiss()
                            print ("OOps not good JSON formatted response")
                        }
                    }
                })
                task.resume()
            }catch _ {
                self.progessDismiss()
                print ("Oops something happened buddy")
            }
        //}
        }
    else
    {
        self.progessDismiss()
        self.notInterNetConn(message: "No internet connection")
    }
    }
    //Get Tour List
    func getTourListData(location_id: NSNumber,tourtype_id: NSNumber,toursubtype_id: NSNumber,  completion:@escaping (_ result: NSArray?) -> Void)
    {
        if Reachability.shared.isConnectedToNetwork(){
           
        SVProgressHUD .show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
            let url = "http://beta.brstdev.com/tours/api/web/v1/tours/tourlist"
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
           let params:[String: AnyObject] = ["location_id" : location_id as AnyObject,"tourtype_id" : tourtype_id as AnyObject,"toursubtype_id" : toursubtype_id as AnyObject]
            
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                    if let response = response {
                        let nsHTTPResponse = response as! HTTPURLResponse
                        let statusCode = nsHTTPResponse.statusCode
                        print ("status code = \(statusCode)")
                    }
                    if let error = error {
                        print ("\(error)")
                    }
                    if let data = data {
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            print ("getTourListData = \(jsonResponse)")
                            self.progessDismiss()
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                                
                            let dataDic : NSDictionary = jsonResponse.value(forKey: "data") as! NSDictionary
                            //print ("Tours data = \(dataDic)")
                            let strSuccess_code = dataDic ["Success_code"] as! NSString
                             let strDescription = dataDic ["description"] as! NSString

                            if strSuccess_code .isEqual(to: "201")
                            {
                                
                                //  print(dataDic)
                                let dicArrData :NSArray = dataDic.object(forKey: "tour_list") as! NSArray
                                completion(dicArrData)
                                
                            }
                            else
                            {
                                self.strAlertMsg = strDescription
                                self.alertBar()
                            }
                            }
                            else
                            {
                                
                                self.strAlertMsg = "Server Not Responding"
                                self.alertBar()
                            }
                            
                        }catch _ {
                            self.progessDismiss()
                            print ("OOps not good JSON formatted response")
                        }
                    }
                })
                task.resume()
            }catch _ {
                self.progessDismiss()
                print ("Oops something happened buddy")
            }
      //  }
        }
        else
        {
            self.progessDismiss()
           self.notInterNetConn(message: "No internet connection")
        }
    }
    func purchasedTourOrNot(completion: @escaping(_ result: Bool)-> Void)
    {
        if Reachability.shared.isConnectedToNetwork(){
       
        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
        var userID = NSString()
        var tourID = Int()
        if (UserDefaults.standard.value(forKey: "user_id") != nil)
        {
            userID =  (UserDefaults.standard.value(forKey: "user_id") as? NSString)!
        }
        if (UserDefaults.standard.value(forKey: "tour_id") != nil)
        {
            tourID =  (UserDefaults.standard.value(forKey: "tour_id") as? Int)!
        }
        let tour_id = String(tourID)
        
        
        SVProgressHUD .show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
       // DispatchQueue.global(qos: .background).async {
            let url = "http://beta.brstdev.com/tours/api/web/v1/tours/purchasedOrNot"
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            

            
            var groupName = String()
            if UserDefaults.standard.object(forKey: "GroupName") != nil
            {
                groupName = UserDefaults.standard.object(forKey: "GroupName") as! String
            }
            let params:[String: AnyObject] = ["user_id" : userID as AnyObject,"tour_id" : tour_id as AnyObject,"group_name" : groupName as AnyObject]
                
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                    if let response = response {
                        let nsHTTPResponse = response as! HTTPURLResponse
                        let statusCode = nsHTTPResponse.statusCode
                        print ("status code = \(statusCode)")
                    }
                    if let error = error {
                        print ("\(error)")
                    }
                    if let data = data {
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            print ("Json Data is = \(jsonResponse)")
                            self.progessDismiss()
                            var dataDic = NSDictionary()
                            
                            var strSuccess_code = jsonResponse ["status_code"] as! NSString
                            
                            let strDescription = jsonResponse ["description"] as! NSString
                            
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                                
                            dataDic = jsonResponse.value(forKey: "data") as! NSDictionary
                            print ("Purchase tour List = \(dataDic)")
                            strSuccess_code = dataDic ["Success_code"] as! NSString
                                let owner = Bool()
                                self.tourPursedOrNot = false
                                self.isOwner = false
                                self.isOwner = false
                                self.is_Joiner = false
                                self.tourPursedOrNot = false
                                self.isCompletedTour = false
                                
                                if dataDic["is_joiner"] != nil {
                                    self.is_Joiner = dataDic.object(forKey: "is_joiner") as! Bool
                                }
                                
                                if dataDic["is_owner"] != nil {
                                    self.isOwner = dataDic.object(forKey: "is_owner") as! Bool
                                }
                            
                                if  dataDic ["is_complete"] as? Bool != nil {
                                    self.isCompletedTour = (dataDic ["is_complete"] as? Bool)!
                                    print(self.isCompletedTour)
                                }
                                if  dataDic ["tour_purchased"] as? String != nil {
                                    let purc = dataDic ["tour_purchased"] as? String
                                    if purc == "Yes" {
                                        self.tourPursedOrNot = true
                                    }
                                }
                                
                             self.progessDismiss()
                            
                            if strSuccess_code .isEqual(to: "401") {
                                if strDescription .isEqual(to: "Tour Not Purchased") {
                                    self.strAlertMsg = "Please purchase tour first to use this feature"
                                    self.tourPursedOrNot = false
                                    self.isOwnerOrNot(owner: owner)
                                }
                            }

                            }
                            else {
                                self.strAlertMsg = "Server Not Responding"
                                self.alertBar()
                            }
                            completion(true)
                        }catch _ {
                            self.progessDismiss()
                            print ("OOps not good JSON formatted response")
                        }
                    }
                })
                task.resume()
            }catch _ {
                self.progessDismiss()
                print ("Oops something happened buddy")
            }
        //}
        }
        else
        {
            self.progessDismiss()
        }
        }
    else
    {
        self.notInterNetConn(message: "No internet connection")
    }
    }
    func isOwnerOrNot(owner : Bool)
    {
        if owner == false
        {
            UserDefaults.standard.set("IsOwner", forKey: "IsOwner")
            self.isOwner = false
        }
        else
        {
            UserDefaults.standard.set("IsOwner", forKey: "IsOwner")
            self.isOwner = true
        }
    }
    func purchaseToursData()
    {
        //strProfileCheck = "EditProfile"
        let firstName = UserDefaults.standard.value(forKey: "firstname")as! String
        firstNameTxt.text = firstName
        
        let LastName = UserDefaults.standard.value(forKey: "lastname")as! String
        LastNameTxt.text = LastName
        
        let email = UserDefaults.standard.value(forKey: "email")as! String
        emailTxt.text = email
        
        let phone = UserDefaults.standard.value(forKey: "phone")as! String
        phoneNumTxt.text = phone
        
        let country = UserDefaults.standard.value(forKey: "country")as! String
        countryTxt.text = country
        
        
        
        
    }
    func paypal()
    {
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        let item1 = PayPalItem(name: "Tour", withQuantity: 1, withPrice: NSDecimalNumber(string: priceOfTour as String), withCurrency: "EUR", withSku: "")
        
        print("The Items Are\(item1)")
        //        let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
        //        let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.0")
        let tax = NSDecimalNumber(string: "0.0")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "EUR", shortDescription: "Price", intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
    }
    func PurchaseAlertBar()
    {
        // the alert view
        let alert = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
    }
   
    func profileAlertbar()
    {
        
        profielAlertView = UIAlertController(title: nil, message:"Firstly, you have to create Account Profile" as String?, preferredStyle: UIAlertControllerStyle.alert)
        let canclBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.cancelAction()
        }
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.profileOkAction()
        }
        profielAlertView.addAction(canclBtn)
        profielAlertView.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(self.profielAlertView, animated: true, completion: nil)
        }
        
        
        
    }
    func notInterNetConn(message: String)
    {
        notInterNetAlert = UIAlertController(title: nil, message:message as String?, preferredStyle: UIAlertControllerStyle.alert)

        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.notConnOkAction()
        }
        notInterNetAlert.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(self.notInterNetAlert, animated: true, completion: nil)
        }
        
        
        
    }
    func notConnOkAction()
    {
         notInterNetAlert.dismiss(animated: true, completion: nil)
    }
    func profileOkAction()
    {
         scrollContentView.isHidden = false
        let  createCheck = "CreateProfile"
        UserDefaults.standard.set(createCheck, forKey: "CreateProfile")
        self.tabBarController?.selectedIndex = 4
    }
    
    func cancelAction()
    {
        profielAlertView.dismiss(animated: true, completion: nil)
    }
    func paymentSuccessAlertView()
    {
        paymentSuccessAlert = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        //var cancelButton = UIAlertAction()
        if tourPursedOrNot == false
        {
            if  mapCheck .isEqual(to: "map")
            {
                
            }
            else
            {
                //cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (alertAction) -> Void in
                    //self.cancelPaymentAlert()
                    
                //}
            }
        }
        
        if  mapCheck .isEqual(to: "map")
        {
            
        }
        else
        {
            if tourPurchased .isEqual(to: "purchased")
            {
                
            }
            else
            {
               
                   // self.paymentSuccessAlert.addAction(cancelButton)
            
               
            }
        }
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.PaymentOkAction()
        }
        
        paymentSuccessAlert.addAction(okBtn)
        // DispatchQueue.main.sync {
        self.present(self.paymentSuccessAlert, animated: true, completion: nil)
        // }
        
    }
    func PaymentOkAction()
    {
//        if is_Joiner == true{
//            
//        }
//        else
//        {
        if tourPurchased .isEqual(to: "purchased")
        {
            self.purchasedTourOrNot(completion:{ (result)in
            
            })
        }
        tourPurchased = ""
        if tourPursedOrNot == false
        {

            if  mapCheck .isEqual(to: "map")
            {
            }
            else
            {
                UserDefaults.standard.set("HomeScreen", forKey: "PuzzleScreen")
                //let when = DispatchTime.now() + 0.5
                //DispatchQueue.main.asyncAfter(deadline: when){
                
                
                if startChk .isEqual(to: "purchaseTourBtn")
                {
                    
                }
                else
                {
                  //  self.moveToClass()
                }
                   
                    // self.navigationController?.present(nextViewController, animated: true)
                    
                //}

            table_View.isHidden = true
            scrollView.isHidden = false
//            self.dropPuzzleImages()
            }
            
        }
        else
        {
            scrollContentView.isHidden = false
            strtBtnContentView.isHidden = true
            PurcOrderContentView.isHidden = true
            MapContentView.isHidden = true
            
            self.ArrTours = NSMutableArray(array: self.showDataArr)
            
            // print("The Default data is\( self.ArrTours)")
           // tourBackTag = "homeScrenSecSubView2"
            
            
            DispatchQueue.main.async {
                self.paymentSuccessAlert.dismiss(animated: true, completion: nil)
            }
        }
        //}
    }
    func cancelPaymentAlert()
    {
        paymentSuccessAlert.dismiss(animated: true, completion: nil)
    }
    func okAction()
    {
        if strAlertMsg .isEqual(to: "Tour Not Purchased")
        {
            DispatchQueue.main.async {
                self.paymentSuccessAlert.dismiss(animated: true, completion: nil)
            }
        }
        else
        {
            scrollContentView.isHidden = false
            strtBtnContentView.isHidden = true
            PurcOrderContentView.isHidden = true
            MapContentView.isHidden = true
            self.ArrTours = NSMutableArray(array: self.showDataArr)
            //tourBackTag = "tourScrenSecSubView2"
            tourBackTag = "scrollView"
            DispatchQueue.main.async {
                self.paymentSuccessAlert.dismiss(animated: true, completion: nil)
            }
        }
    }
    /// listen to keyboard event
    func makeAwareOfKeyboard() {
        //NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        
       // NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidHide), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    /// move scroll view up
    func keyBoardDidShow(notification: Notification) {
        let info = notification.userInfo as NSDictionary?
        let rectValue = info![UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let kbSize = rectValue.cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0)
        self.scrollView?.contentInset = contentInsets
        self.scrollView?.scrollIndicatorInsets = contentInsets
        
    }
    
    /// move scrollview back down
    func keyBoardDidHide(notification: Notification) {
        // restore content inset to 0
        let contentInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.scrollView?.contentInset = contentInsets
        self.scrollView?.scrollIndicatorInsets = contentInsets
    }
    
}


