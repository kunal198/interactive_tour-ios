 //
//  AppDelegate.swift
//  Interactive Tours App
//
//  Created by brst on 19/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import Photos
 
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {
    
    var window: UIWindow?
    let locationManager = CLLocationManager()
    var alert = UIAlertController()
      var topWindow = UIWindow()
    let imagePicker = UIImagePickerController()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey("AIzaSyC14bRsVfj-TdP06vjwi4vCF7_pQ8IBxZ0")
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.black], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: .selected)
     
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "YOUR_CLIENT_ID_FOR_PRODUCTION", PayPalEnvironmentSandbox: "AeI9RovLlyoNBmZAEnFGZClKuZMeeCoivGoiEszDp3p-KLhbwZeQ8hPIuMrmZZw8nv6tvoc1ZDFSDoFh"])
        
        iSpeechSDK.shared().apiKey = "developerdemokeydeveloperdemokey"
     
        
        //FireBase Crash Analytics
        FIRApp.configure()
        //"${PODS_ROOT}"/FirebaseCrash/upload-sym "/${PROJECT_DIR}/highjinx-tours-3e59a-firebase-crashreporting-igdmy-8b64c224ea.json"

        //self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        
        //Camera
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { response in
            if response {
                //access granted
            } else {
                
            }
        }
        
        //Photos
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                   // ...
                } else {}
            })
        }
        
        locationManager.delegate = self
     //  IQKeyboardManager.shared().isEnabled = true
        //Check inital View
         //checkInitialView()

    return true
    }
    func checkInitialView() {

//             if savedView .isEqual(to: "QUESTION_VIEW") {
//                openInitialView(indentifier: "NavigationController")
//            }
//           else {
                openInitialView(indentifier: "TabBarViewController")
         //  }
        //}
    }
    func openInitialView(indentifier: String) {
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewControlleripad : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: indentifier) as UIViewController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = initialViewControlleripad
        self.window?.makeKeyAndVisible()
    }
    func enableLocationAlertBar(message: String)
    {
        self.topWindow.removeFromSuperview()
        self.alert.dismiss(animated: true, completion: nil)
        
         topWindow = UIWindow(frame: UIScreen.main.bounds)
        topWindow.rootViewController = UIViewController()
        topWindow.windowLevel = UIWindowLevelAlert + 1
       
       
        
        alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "confirm"), style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            // continue your work
            // important to hide the window after work completed.
            // this also keeps a reference to the window until the action is invoked.
            self.topWindow.isHidden = true
            self.alert.dismiss(animated: true, completion: nil)
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                // If general location settings are enabled then open location settings for the app
                UIApplication.shared.openURL(url)
            }
            
        }))
        topWindow.makeKeyAndVisible()
        topWindow.rootViewController?.present(alert, animated: true, completion: { _ in })
        }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            // permission denied
            print("permission denied")
            enableLocationAlertBar(message: "Please Turn ON your location services from device settings.")
        }
        else if status == .authorized {
            // permission granted
             print("permission granted")
            self.checkCameraAuthorization()
        }
        
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                enableLocationAlertBar(message: "Please Turn ON your location services from device settings.")
             
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                  self.checkCameraAuthorization()
                
            
              
            }
        }
        else {
            
            print("Location services are not enabled")
            
            enableLocationAlertBar(message: "Please Turn ON your location services from device settings.")
            
          
        }

    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized
//            let picker = UIImagePickerController()
//            picker.delegate = self
//            picker.allowsEditing = true
//            picker.sourceType = .camera
//            present(picker, animated: true, completion: { _ in })
            checkPhotoAuthorization()
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
//                        DispatchQueue.main.async(execute: {() -> Void in
//                            let picker = UIImagePickerController()
//                            picker.delegate = self
//                            picker.allowsEditing = true
//                            picker.sourceType = .camera
//                            self.present(picker, animated: true, completion: { _ in })
//                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                           // self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                                self.enableLocationAlertBar(message: "Please go to Settings and enable the camera for this app to use this feature")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                //self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                   self.enableLocationAlertBar(message: "Please go to Settings and enable the camera for this app to use this feature")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in

                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        //self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                           self.enableLocationAlertBar(message: "Please go to Settings and enable the camera for this app to use this feature")
                    })
                    
                }
            })
        }
    }
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                   self.enableLocationAlertBar(message: "Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                      
                        self.enableLocationAlertBar(message: "Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }
    
//    func accessMethod(_ message: String) {
//        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
//        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
//        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
//        })
//        alertController.addAction(Settings)
//        alertController.addAction(cancel)
//        present(alertController, animated: true, completion: { _ in })
//    }
}

