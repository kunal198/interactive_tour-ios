//
//  HomeCollectionViewCell.swift
//  Interactive Tours App
//
//  Created by Brst on 24/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleBackView: UIView!
    @IBOutlet weak var collCellbackImgView: UIImageView!
    @IBOutlet weak var cellArrowIcon: UIImageView!
}
