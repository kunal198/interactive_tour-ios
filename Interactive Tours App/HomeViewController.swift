


//
//  HomeViewController.swift
//  Interactive Tours App
//
//  Created by Brst on 21/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import GoogleMaps
import AVFoundation
class HomeViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate,UITabBarControllerDelegate,UINavigationControllerDelegate, PuzzleSolvedProtocol,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UITabBarDelegate,UIGestureRecognizerDelegate,PayPalPaymentDelegate,UIPickerViewDelegate,UIPickerViewDataSource,SDWebImageManagerDelegate,AVAudioPlayerDelegate {
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
     var avPlayer = AVPlayer()
    var playerItem: AVPlayerItem?
    var descriptionAudioUrl = String()
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    var panRecognizer = UIPanGestureRecognizer()
    var panRecognizer2 = UIPanGestureRecognizer()
    var panRecognizer3 = UIPanGestureRecognizer()
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    var itemsArray = NSMutableArray()
    var itemsArray2 = NSMutableArray()
    var itemsArray3 = NSMutableArray()
    var itemsArray4 = NSMutableArray()
    var itemsArray5 = NSMutableArray()
    var itemsArray6 = NSMutableArray()
    var historicalSubArr = NSMutableArray()
    var toursCategoriesArr = NSMutableArray()
    var cellImageArr = NSMutableArray()
     var tourWorksubArr = NSMutableArray()
    //Home Screen New Arr
    var ArrCountries = NSMutableArray()
    var ArrTours = NSMutableArray()
    var strAlertMsg = NSString()
    var ArrCountries_And_Tours = NSMutableArray()

    //map view
    var alertView = UIAlertController()
    var paymentSuccessAlert = UIAlertController()
    var profielAlertView = UIAlertController()
     var payAlertView = UIAlertController()
    var mapViewArr = NSMutableArray()
    @IBOutlet weak var mapListView: UIView!
    //================================
    //Start View
    @IBOutlet weak var mainImageView: UIImageView!
    
    let userDefaults = UserDefaults.standard
    var currentImagePackage : ImagePackage?
    @IBOutlet weak var puzzleView: UIView!
    @IBOutlet weak var origionalImageView: UIImageView!
    @IBOutlet weak var tileArea:  TileAreaView!
    var congratsMessages : [String]!
    var tilesPerRow = 3
    var originalImageShown = false
    // MARK: VARS
    let colorPalette = ColorPalette()
    var topGrips = [UIImageView]()
    var leftGrips = [UIImageView]()
    var rowColumnGrip : UIImageView?
    var firstGripOriginalFrame : CGRect?
    var firstLineOfTiles: [Tile]?
    
    // MARK: VIEWS
    @IBOutlet weak var originalImageView: UIImageView!
    @IBOutlet weak var congratsMessage: UILabel!
    @IBOutlet weak var topBank: UIView!
    @IBOutlet weak var leftBank: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var imageCaptionLabel: UILabel!
    @IBOutlet weak var dropCorrectImgLabel: UILabel!
    // MARK: BUTTONS
    @IBOutlet weak var hintButton: UIButton!
    @IBOutlet weak var solveButton: UIButton!
    @IBOutlet weak var showOriginalButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backIcon: UIImageView!
    var dropImageView1: UIImageView!
    var dropImageView2: UIImageView!
    var dropImageView3: UIImageView!
    @IBOutlet weak var leftBankMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var tourImgViewHeight: NSLayoutConstraint!
    ///////
    var collCellTag = NSInteger()
    var cellHeightTag = NSInteger()
    var tagVal = NSInteger()
    var cellHeight = NSInteger()
    var cellTag = NSInteger()
    var strCountryID = NSNumber()
     var selectedValue = Int()
    var tourWorksArr = NSMutableArray()
    var historicSubCatArr = NSMutableArray()
    var showDataArr = NSMutableArray()
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var purchaseTourButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewImageView = UIImageView()
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var strtBtnContentView: UIView!
    @IBOutlet weak var toursContentView: UIView!
    @IBOutlet weak var toursHeaderlabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var MapContentView: UIView!
    @IBOutlet weak var PurcOrderContentView: UIView!
    
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var LastNameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneNumTxt: UITextField!
    @IBOutlet weak var paymentTxt: UITextField!
    @IBOutlet weak var countryTxt: UITextField!
    
    @IBOutlet weak var purchaseOrderBtn: UIButton!
    
    @IBOutlet weak var roundBtn1: UIButton!
    @IBOutlet weak var roundBtn2: UIButton!
    @IBOutlet weak var roundBtn3: UIButton!
    @IBOutlet weak var uploadImaegTxtField: UITextField!
    @IBOutlet weak var couponCodeTxt: UITextField!
    @IBOutlet weak var uploadIcon: UIImageView!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var uploadAnswerLabel: UILabel!
    @IBOutlet weak var dragAndDroplabel: UILabel!
    @IBOutlet weak var uploadSubmitButton: UIButton!
    // @IBOutlet weak var viewMap: UIView!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var dragImageBtn :UIButton!
    var selcttagVal = NSInteger()
    
    @IBOutlet weak var dropImgView: UIView!
    @IBOutlet weak var paymentTypeView: UIView!
    @IBOutlet weak var payPalBtn: UIButton!
    @IBOutlet weak var couponCodeBtn: UIButton!
    @IBOutlet weak var paymentViaTextBtn: UIButton!
    @IBOutlet weak var upDownArrowImgView: UIImageView!
    var paymentBtntag = NSInteger()
    var uploadSubmitBtnTag = NSInteger()
    @IBOutlet weak var submitBtnSubView1: UIView!
    @IBOutlet weak var submitTxtView: UITextView!
    @IBOutlet weak var yourAnswerTxtField: UITextField!
    @IBOutlet weak var submitBtnSubView2: UIView!
    @IBOutlet weak var submitBtnSubView3: UIView!
    @IBOutlet weak var radioBtn1: UIButton!
    @IBOutlet weak var radioBtn2: UIButton!
    @IBOutlet weak var radioBtn3: UIButton!
    @IBOutlet weak var radioBtn4: UIButton!
    @IBOutlet weak var submitTxtView1: UITextView!
    @IBOutlet weak var submitTxtView2: UITextView!
    @IBOutlet weak var submitTxtView3: UITextView!
    var strBackBtnTag = NSString()
    var backIntVal = NSInteger()
    var backBtnCheck = NSInteger()
    var firstX: CGFloat = 0.0
    var firstY: CGFloat = 0.0
    
    var strBackTag = NSString()
    var tour_price = NSString()
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var successView: UIView!
    
    @IBOutlet weak var tourPriceLabel: UILabel!
    @IBOutlet weak var descriptionTxtview: UITextView!
    @IBOutlet weak var tourImagview: UIImageView!
    
    @IBOutlet weak var scrollContentViewHeight: NSLayoutConstraint!
    
    var groupNameStr = NSString()
    var groupIDStr = NSString()
    var buyBtntag = NSIndexPath()
    var txnID = NSString()
     var priceOfTour = NSString()
       var tourNotPurchased = NSString()
    
      var arrGroupName = NSMutableArray()
  //  @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var numOfMemToJoinLbl: UILabel!
    @IBOutlet weak var numOfMemToJoinTxt: UITextField!

   
    var pickerCount = NSInteger()
    var headerTagVal = NSInteger()
    var headerTagVal2 = NSInteger()
    
     var doneButton: UIBarButtonItem!
   // @IBOutlet weak var pickerViewHeight: NSLayoutConstraint!
   
   var pickOption :NSMutableArray = NSMutableArray()
    var notInterNetAlert = UIAlertController()

    @IBOutlet weak var textSpeechBtn: UIButton!
    @IBOutlet weak var speakTextView: UITextView!
    
     var tourPursedOrNot = Bool()
     let speechSynthesizer = AVSpeechSynthesizer()
    
    var speechTag = Int()
   var loactionID = NSString()
    var isOwner = Bool()
    var mapCheck = NSString()
     var tourPurchased = NSString()
    
    var strTourTypeID = NSNumber()
    var strtourSubTypeID = NSNumber()
    var strLocationID = NSNumber()
    var readMoreCheck = Int()
     var readMoreButton = UIButton()
    @IBOutlet weak var speechButton = UIButton()
    var readMoreBackLbl = UILabel()
    @IBOutlet weak var tourDetailsLabel: UILabel!
    var model = NSString()
    @IBOutlet weak var scrollContentViewheight: NSLayoutConstraint!
    var is_Joiner = Bool()
    var startChk = NSString()
     var speechUtterance: AVSpeechUtterance!
     var isCompletedTour = Bool()
    var checkInt = Int()
    
//    var player: AVAudioPlayer?
    
     @IBOutlet weak var teamLeaderPriLbl: UILabel!
     @IBOutlet weak var checkImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        //Set Initial Tour
       moveToIntialView()
        tourDetailsLabel.numberOfLines = 0;
        
                self.cityNameLabel?.text = "Home"
                isOwner = false
                is_Joiner = false
                
                let modelName = UIDevice.current.model
                model = modelName as NSString
                scrollContentViewheight.constant = 1500
                descriptionTxtview.frame.size.height = 130
                tourDetailsLabel.frame.size.height = 130
                readMoreCheck = 0
                
                let pos = self.view.frame.size.width/2 as CGFloat
                readMoreButton = UIButton(frame: CGRect(x:pos, y: ((tourDetailsLabel?.frame.size.height)!-20), width: self.view.frame.size.width, height: 20))
                readMoreButton.setTitle("Read More...", for: .normal)
                readMoreButton.backgroundColor = UIColor .white
                readMoreButton.setTitleColor(UIColor (red: 108/255, green: 2/255, blue: 111/255, alpha: 1), for: .normal)
                
                readMoreButton.contentHorizontalAlignment = .left
                readMoreButton.titleLabel?.font = UIFont.italicSystemFont(ofSize: 16)
                readMoreButton.addTarget(self, action:#selector(readMoreAction(sender:)), for: .touchUpInside)
                scrollContentView.addSubview(readMoreButton)
                
                descriptionTxtview.isScrollEnabled = false
                descriptionTxtview.isUserInteractionEnabled = false
                
                
                speechTag = 0
                headerTagVal = 0
                headerTagVal2 = 0
                mapCheck = ""
                
                makeAwareOfKeyboard()
                
                descriptionTxtview.isEditable = false
                
                let pickerView = UIPickerView()
                pickerView.delegate = self
                numOfMemToJoinTxt.inputView = pickerView
                
                //New
                let toolBar = UIToolbar()
                toolBar.barStyle = UIBarStyle.default
                toolBar.isTranslucent = true
                toolBar.tintColor = UIColor(red: 33/255, green: 123/255, blue: 255/255, alpha: 1)
                doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(HomeViewController.donePressed))
                let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(HomeViewController.cancelPressed))
                let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
                toolBar.sizeToFit()
                
                
                numOfMemToJoinTxt.delegate = self
                numOfMemToJoinTxt.inputAccessoryView = toolBar
                
                for i in 0 ..< 100
                {
                    let val = String(i)
                    pickOption.add(val)
                }
                
                
                numOfMemToJoinLbl.isHidden = true
                numOfMemToJoinTxt.isHidden = true
                pickerCount = 0
                numOfMemToJoinTxt.isUserInteractionEnabled = true
                numOfMemToJoinTxt.isEnabled = true
                
                let border = CALayer()
                let width = CGFloat(1.0)
                border.frame = CGRect(x: 0, y:  numOfMemToJoinTxt.frame.size.height - width, width:   self.view.frame.size.width, height: numOfMemToJoinTxt.frame.size.height)
                border.borderWidth = width
                numOfMemToJoinTxt.layer.addSublayer(border)
                numOfMemToJoinTxt.layer.masksToBounds = true
                
                
                payPalConfig.acceptCreditCards = true
                payPalConfig.merchantName = "Tours"
                payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
                payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
                payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
                
                // Setting the payPalShippingAddressOption property is optional.
                //
                // See PayPalConfiguration.h for details.
                
                payPalConfig.payPalShippingAddressOption = .payPal;
                
                print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
                
                backBtnCheck = 0
                
                
                arrData()
                purchaseTourButton.titleLabel?.numberOfLines = 2
                paymentBtntag = 0
                uploadSubmitBtnTag = 0
                submitTxtView1.isUserInteractionEnabled = false
                submitTxtView1.isUserInteractionEnabled = false
                submitTxtView1.isUserInteractionEnabled = false
                
                self.congratsMessages = [ // Populated from Localizable.strings
                    NSLocalizedString("Message01", comment: ""),
                    NSLocalizedString("Message02", comment: ""),
                    NSLocalizedString("Message03", comment: ""),
                    NSLocalizedString("Message04", comment: ""),
                    NSLocalizedString("Message05", comment: ""),
                    NSLocalizedString("Message06", comment: ""),
                    NSLocalizedString("Message07", comment: ""),
                    NSLocalizedString("Message08", comment: ""),
                    NSLocalizedString("Message09", comment: ""),
                    NSLocalizedString("Message10", comment: ""),
                    NSLocalizedString("Message11", comment: ""),
                    NSLocalizedString("Message12", comment: ""),
                    NSLocalizedString("Message13", comment: ""),
                    NSLocalizedString("Message14", comment: ""),
                    NSLocalizedString("Message15", comment: ""),
                    NSLocalizedString("Message16", comment: ""),
                    NSLocalizedString("Message17", comment: "")
                ]
                
                
                scrollView.delegate=self
                self.tabBarController?.tabBar.frame = CGRect.init(x: 0, y: 20, width: self.view.frame.size.width, height: 80)
                
                self.tabBarController?.tabBar.backgroundColor = UIColor.black
                
                
                roundBtn1?.layer.masksToBounds = false
                roundBtn1?.layer.cornerRadius = (roundBtn1?.frame.height)!/2
                roundBtn1?.clipsToBounds = true
                
                roundBtn2?.layer.masksToBounds = false
                roundBtn2?.layer.cornerRadius = (roundBtn1?.frame.height)!/2
                roundBtn2?.clipsToBounds = true
                
                roundBtn3?.layer.masksToBounds = false
                roundBtn3?.layer.cornerRadius = (roundBtn1?.frame.height)!/2
                roundBtn3?.clipsToBounds = true
                
                
                roundBtn3?.layer.masksToBounds = false
                roundBtn3?.layer.cornerRadius = (roundBtn1?.frame.height)!/2
                roundBtn3?.clipsToBounds = true
        
                
                startButton.layer.cornerRadius=20
                
                mapButton.layer.cornerRadius=20
                mapButton.layer.borderColor=UIColor.purple.cgColor
                mapButton.layer.borderWidth=2
                
                purchaseTourButton.layer.cornerRadius=20
                purchaseTourButton.layer.borderColor=UIColor.purple.cgColor
                purchaseTourButton.layer.borderWidth=2
                
                //Map View
                mapListView.layer.borderColor=UIColor.lightGray.cgColor
                mapListView.layer.borderWidth=1
                
                
                tagVal=0;
                
                table_View.delegate=self
                
                
                scoreLabel.layer.borderColor=UIColor.black.cgColor
                scoreLabel.layer.borderWidth=1
                itemsArray = NSMutableArray(array: itemsArray6)
                
                //Purchase Order
                
                changeFieldSetting(textField: firstNameTxt)
                changeFieldSetting(textField: LastNameTxt)
                changeFieldSetting(textField: emailTxt)
                changeFieldSetting(textField: phoneNumTxt)
                changeFieldSetting(textField: paymentTxt)
                changeFieldSetting(textField: uploadImaegTxtField)
                changeFieldSetting(textField: yourAnswerTxtField)
                changeFieldSetting(textField: couponCodeTxt)
                changeFieldSetting(textField: countryTxt)
                
                
                firstNameTxt.isUserInteractionEnabled = false
                LastNameTxt.isUserInteractionEnabled = false
                emailTxt.isUserInteractionEnabled = false
                phoneNumTxt.isUserInteractionEnabled = false
                firstNameTxt.isUserInteractionEnabled = false
                
                paymentTypeView.isHidden=true
                dropCorrectImgLabel.isHidden = true
                submitBtnSubView1.isHidden=true
                submitBtnSubView2.isHidden=true
                submitBtnSubView3.isHidden=true
                puzzleView.isHidden=true
                PurcOrderContentView.isHidden=true
                MapContentView.isHidden=true
                uploadImaegTxtField.isHidden=true
                uploadIcon.isHidden=true
                uploadBtn.isHidden=true
                dragAndDroplabel.isHidden=true
                uploadAnswerLabel.isHidden=true
                purchaseOrderBtn.layer.cornerRadius = 20
                purchaseOrderBtn.layer.masksToBounds = true
                
                uploadSubmitButton.isHidden=true
                uploadSubmitButton.layer.cornerRadius = 20
                uploadSubmitButton.layer.masksToBounds = true
                table_View.isHidden=true
                scrollView.isHidden=true
                collectionView.delegate=self
                collectionView.isHidden=false
                
                
                txtPaddingVw(txt: uploadImaegTxtField!)
                txtPaddingVw(txt: couponCodeTxt!)
                txtPaddingVw(txt: countryTxt!)
                txtPaddingVw(txt: numOfMemToJoinTxt!)
                
                let rectShape1 = CAShapeLayer()
                rectShape1.bounds = self.uploadBtn.frame
                rectShape1.position = self.uploadBtn.center
                
                rectShape1.path = UIBezierPath(roundedRect: self.uploadBtn.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: 15, height: 15)).cgPath
                
                self.uploadBtn.layer.mask = rectShape1
                
                
                firstNameTxt.delegate=self
                LastNameTxt.delegate=self
                emailTxt.delegate=self
                phoneNumTxt.delegate=self
                paymentTxt.delegate=self
                
                phoneNumTxt.keyboardType = .numberPad
                txtPaddingVw(txt: yourAnswerTxtField!)
                
                couponCodeTxt.isHidden = true
                
                self.table_View.delegate = self
                backButton.isHidden = true
                backIcon.isHidden = true
                backIntVal = 0
                collectionView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                table_View.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                scrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                cellTag = 0
                firstNameTxt.resignFirstResponder()
                arrData()
                reloadViewData()
                submitBtnSubView1.isHidden = true
                dropCorrectImgLabel.isHidden = true
                createImageViews()
                tagVal = 0;
                collCellTag = 0
                cellHeight=0
                txtPaddingVw(txt: firstNameTxt!)
                txtPaddingVw(txt: LastNameTxt!)
                txtPaddingVw(txt: emailTxt!)
                txtPaddingVw(txt: paymentTxt!)
                txtPaddingVw(txt: phoneNumTxt!)
                
                scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 710)
                
                tabBarControllerAction()
                table_View.isHidden=true
                scrollView.isHidden=true
                puzzleView.isHidden=true
                collectionView.delegate=self
                collectionView.isHidden=false
                
                submitBtnSubView3.isHidden = true
                submitBtnSubView2.isHidden = true
                submitBtnSubView1.isHidden = true
                
                scrollContentView.addSubview(speechButton!)
                
                model = modelName as NSString
                if model .isEqual(to: "iPhone")
                {
                }
                else
                {
                    tourImgViewHeight.constant = 350
                }
                
                roundBtn1.backgroundColor = UIColor(red: 92/255, green: 10/255, blue: 99/255, alpha: 1)
                roundBtn2.backgroundColor = UIColor.black
                roundBtn3.backgroundColor = UIColor.black
                
        
                

                
//            }
//        } else {
//            print("Location services are not enabled")
//            enableLocationAlertBar()
//        }
//
        numOfMemToJoinTxt.tag = 108
    }
    
    func playTourDetailsSong() {

        DispatchQueue.main.async {
            if(!self.descriptionAudioUrl .isEqual("")) {
              //  self.audioUrlString = urlArray .object(at: self.audioCount) as! NSString
                let audUrl: URL! = URL(string: self.descriptionAudioUrl as String)
                self.avPlayer = AVPlayer(url: audUrl!)
                self.playerItem = AVPlayerItem(url: audUrl)
                self.avPlayer = AVPlayer(playerItem: self.playerItem)
                
            }
        }
        
    }
    func moveToIntialView() {
        if UserDefaults.standard.object(forKey: "SavedViewController") != nil {
            var savedView = UserDefaults.standard.object(forKey: "SavedViewController") as! NSString
            
            if UserDefaults.standard.object(forKey: "TourController") != nil {
              savedView = UserDefaults.standard.object(forKey: "TourController") as! NSString
            }
            if savedView .isEqual(to: "HELP_VIEW") {
                tabBarController?.selectedIndex = 2
            }
            else if savedView .isEqual(to: "JOIN_VIEW") {
                tabBarController?.selectedIndex = 3
            }
            else if savedView .isEqual(to: "MORE_VIEW") {
                tabBarController?.selectedIndex = 4
            }
            else if savedView .isEqual(to: "TOURS_VIEW") {
                tabBarController?.selectedIndex = 1
            }
            else if savedView .isEqual(to: "HOME_VIEW") {
                tabBarController?.selectedIndex = 0
            }
            else {
                 //Questionnaire View Check
                if UserDefaults.standard.object(forKey: "QuestionView") == nil {
                    
                }  else {
                    //Questionnaire Back Button Check
                    if UserDefaults.standard.object(forKey: "QuesBack") == nil {
                  self.moveToClass()
                    }
                }
            }
        }
    }
//    func enableLocationAlertBar()  {
//        let alert = UIAlertController(title: nil, message:" Please Turn ON your location services from device settings." as String , preferredStyle: UIAlertControllerStyle.alert)
//        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
//           // exit(0)
//         ///   UIApplication.shared.openURL(NSURL(string: "prefs:root=LOCATION_SERVICES")! as URL)
//            
//            if let url = URL(string: UIApplicationOpenSettingsURLString) {
//                // If general location settings are enabled then open location settings for the app
//                UIApplication.shared.openURL(url)
//            }
//        }
//        alert.addAction(okBtn)
//        self.present(alert, animated: true, completion: nil)
//    }

    func changeFieldSetting(textField: UITextField)
    {
        textField.layer.cornerRadius = 20
        textField.layer.masksToBounds = true
        textField.layer.borderColor=UIColor.black.cgColor
        textField.layer.borderWidth=1
    }
    @IBAction func readMoreAction(sender: UIButton)
    {
        if readMoreCheck == 0
        {
            
            readMoreButton.setTitle("Show Less", for: .normal)
            let fixedWidth = descriptionTxtview.frame.size.width
            descriptionTxtview.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = descriptionTxtview.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = descriptionTxtview.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            descriptionTxtview.frame = newFrame;
            tourDetailsLabel.frame = newFrame;
            startButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
            mapButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
            purchaseTourButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
            speechButton?.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+1
             readMoreButton.frame.origin.y = tourDetailsLabel.frame.origin.y + tourDetailsLabel.frame.size.height-10
            readMoreButton.frame.origin.x = 10
            readMoreButton.frame.size.width = 200
             readMoreButton.frame.size.height = 20
            readMoreCheck = 1
             scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: startButton.frame.origin.y + startButton.frame.size.height+80)
            
            readMoreButton.backgroundColor = UIColor .clear
        }
        else
        {
          movePositions()
        }
    }
    func movePositions()
    {
        let pos = self.view.frame.size.width/2 as CGFloat
          readMoreButton.frame.origin.x = pos
        readMoreButton.frame.size.height = 20
        readMoreButton.backgroundColor = UIColor .white
        readMoreButton.setTitle("Read More...", for: .normal)
        descriptionTxtview.frame.size.height = 130
         tourDetailsLabel.frame.size.height = 130
        startButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
        mapButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
        purchaseTourButton.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+36
        readMoreButton.frame.origin.y = tourDetailsLabel.frame.origin.y + tourDetailsLabel.frame.size.height-20
        speechButton?.frame.origin.y = descriptionTxtview.frame.origin.y + descriptionTxtview.frame.size.height+1
        readMoreButton.frame.size.width = self.view.frame.size.width
        readMoreCheck = 0
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: startButton.frame.origin.y + startButton.frame.size.height+80)
        
       
    }
 
    @IBAction func purcTourTxtSpeech(sender: UIButton)
    {
        mapCheck = "map"

        if speechTag == 0 {
//            if checkInt == 0 {
//                speechTag = 1;
//               // textSpeech()
//
//                strAlertMsg = "Audio started"
//                 self.avPlayer.play()
//                alertBar()
//                checkInt = 1;
//            } else {
//          //  speechSynthesizer.continueSpeaking()
//                player?.play()
//            speechTag = 1;
//            strAlertMsg = "Audio started"
//            alertBar()
//            }
            
             self.avPlayer.play()
            speechTag = 1;
            strAlertMsg = "Audio started"
            alertBar()
        }
        else {
            
           // speechSynthesizer.pauseSpeaking(at: AVSpeechBoundary .word)
             self.avPlayer.pause()
            speechTag = 0;
            strAlertMsg = "Audio stopped"
            alertBar()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
        checkInt = 0;
        speechTag = 0
        self.avPlayer.pause()
        
    }
    func textSpeech()
    {
        speechUtterance = AVSpeechUtterance(string: self.speakTextView.text!)
        //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
        speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 2.5
        // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        speechSynthesizer.speak(speechUtterance)
    }
    //
    @IBAction func textToSpeech(sender: UIButton)
    {
        
        if checkInt == 0
        {
           
            textSpeech()
            
            checkInt = 1;
        }
        
        if speechTag == 0
        {
            // Line 1. Create an instance of AVSpeechSynthesizer.
            
            // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.
            //let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: speakTextView.text)
//            //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
//            speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 3.0
//            // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
//            speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            // Line 5. Pass in the urrerance to the synthesizer to actually speak.
            //speechSynthesizer.speak(speechUtterance)
            speechSynthesizer.continueSpeaking()
            speechTag = 1;
        }
        else
        {
              speechSynthesizer.pauseSpeaking(at: AVSpeechBoundary .word)
             speechTag = 0;
        }
        
    
}
    func donePressed (sender: UIButton) {
        paymentViaTextBtn.isEnabled = true
        let valueSelected = pickOption[sender.tag] as! String
        let val = String(describing: valueSelected)
        numOfMemToJoinTxt.text = val as String
        view.endEditing(true)
    }
    
    func cancelPressed() {
        paymentViaTextBtn.isEnabled = true
        view.endEditing(true) // or do something
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag != 108 {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 33/255, green: 123/255, blue: 255/255, alpha: 1)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(donePressed))
       
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        
        
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        textField.delegate = self
        textField.inputAccessoryView = toolBar
        }
        paymentViaTextBtn.isEnabled = false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
         paymentViaTextBtn.isEnabled = true
    }
     public func numberOfComponents(in pickerView: UIPickerView) -> Int
     {
        return 1
     }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
         return pickOption.count
    }
    

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
       return pickOption[row] as? String
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        let val = pickOption[row] as! String
        doneButton.tag = Int(val)!
       
    }
    
        func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
            self.resultText = completedPayment.description
            let dictresult = completedPayment.confirmation as NSDictionary
            let response = dictresult.value(forKey: "response") as! NSDictionary
            print(response.value(forKey: "id") ?? NSString())
            
            let strID = response.value(forKey: "id") as? String
            
            self.txnID = strID! as NSString
            self.purchaseTour()
        })
    }
    // MARK: Helpers
    
    func showSuccess() {
    paymentSuccessAlertView()
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool
    {
        
        UserDefaults.standard.set("0", forKey: "ClickedTabIndex")
        return (viewController != tabBarController.selectedViewController);
    }
    func reloadViewData()
    {
        
        
        paymentBtntag = 0
        uploadSubmitBtnTag = 0
        
        paymentTypeView.isHidden=true
        dropCorrectImgLabel.isHidden = true
        submitBtnSubView1.isHidden=true
        submitBtnSubView2.isHidden=true
        submitBtnSubView3.isHidden=true
        puzzleView.isHidden=true
        scrollView.isHidden=true
        PurcOrderContentView.isHidden=true
        MapContentView.isHidden=true
        uploadImaegTxtField.isHidden=true
        uploadIcon.isHidden=true
        uploadBtn.isHidden=true
        dragAndDroplabel.isHidden=true
        uploadAnswerLabel.isHidden=true
        purchaseOrderBtn.layer.cornerRadius = 20
        purchaseOrderBtn.layer.masksToBounds = true
        
        uploadSubmitButton.isHidden=true
        uploadSubmitButton.layer.cornerRadius = 20
        uploadSubmitButton.layer.masksToBounds = true
        
        
    }
    func move(_ sender: UIPanGestureRecognizer) {
        view.bringSubview(toFront: sender.view!)
        var translatedPoint: CGPoint = sender.translation(in: sender.view!.superview)
        //   print(translatedPoint)
        
        if sender.state == .began {
            firstX = (sender.view?.center.x)!
            firstY = (sender.view?.center.y)!
            //        firstX = [[sender view] center].x;
            //        firstY = [[sender view] center].y;
        }
        print(firstX)
        print( mainImageView.frame.origin.x)
        print(firstY)
        print(firstY == mainImageView.frame.origin.y)
        
        
        translatedPoint = CGPoint(x: CGFloat((sender.view?.center.x)! + translatedPoint.x), y: CGFloat((sender.view?.center.y)! + translatedPoint.y))
        sender.view?.center = translatedPoint
        sender.setTranslation(CGPoint.zero, in: sender.view)
        
        if sender.state == .ended {
            let velocityX: CGFloat = (0.2 * sender.velocity(in: view).x)
            let velocityY: CGFloat = (0.2 * sender.velocity(in: view).y)
            var finalX: CGFloat = translatedPoint.x + velocityX
            var finalY: CGFloat = translatedPoint.y + velocityY
            // translatedPoint.y + (.35*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
            if finalX < 0 {
                finalX = 0
            }
            else if finalX > view.frame.size.width {
                finalX = view.frame.size.width
            }
            
            if finalY < 50 {
                // to avoid status bar
                finalY = 50
            }
            else if finalY > view.frame.size.height {
                finalY = view.frame.size.height
            }
            
            
            
            scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 750)
            puzzleView.isHidden=false
            
            self.currentImagePackage?.image = dropImageView1.image
            dropImageView1.removeFromSuperview()
            dropImageView2.removeFromSuperview()
            dropImageView3.removeFromSuperview()
            
            dropImageView1.isHidden=true
            dropImageView2.isHidden=true
            dropImageView3.isHidden=true
            dropCorrectImgLabel.isHidden = true
            collectionView.isHidden=false
            mainImageView.isHidden = false
            tileArea.isHidden = false
            
            strtBtnContentView.addSubview(puzzleView)
            strBackBtnTag = "StartBtnContentView"
            
        }
    }
    func txtPaddingVw(txt:UITextField) {
        let paddingView = UIView(frame: CGRect (x:0, y:0 , width:10 , height:20))
        txt.leftViewMode = .always
        txt.leftView = paddingView
    }
    
    
    func homeViewControllerData()
    {
        tagVal = 0;
        collCellTag = 0
        cellHeight=0
        
        txtPaddingVw(txt: firstNameTxt!)
        txtPaddingVw(txt: LastNameTxt!)
        txtPaddingVw(txt: emailTxt!)
        txtPaddingVw(txt: paymentTxt!)
        txtPaddingVw(txt: phoneNumTxt!)
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 750)
        tabBarControllerAction()
        
        
        table_View.isHidden=true
        scrollView.isHidden=true
        puzzleView.isHidden=true
        collectionView.delegate=self
        collectionView.isHidden=false
        collectionView.reloadData()
    }
    
    
    func arrData()
    {
        itemsArray6 = NSMutableArray(array: ["Browse Tours","How the tour works"])
        
        itemsArray2 = NSMutableArray(array: ["Dubai","Dubai","Dubai","Dubai","Dubai","Dubai"])
        itemsArray3 = NSMutableArray(array: ["1 Tour of 5 available","1 Tour of 5 available","1 Tour of 5 available","1 Tour of 5 available","1 Tour of 5 available","1 Tour of 5 available"])
        itemsArray4 = NSMutableArray(array: ["Beaches","Beaches","Beaches","Beaches","Beaches","Beaches"])
        itemsArray5 = NSMutableArray(array:  ["$5","$5","$5","$5","$5","$5"])
        tourWorksArr = NSMutableArray(array:  ["Family Fun Tours","Historic Tours","Hens & Stags","Party Nights"])
        cellImageArr = NSMutableArray(array:  ["familyfuntours.jpg","historictoursforgrownups.jpg","hensandstages.jpg","partynights.jpg"])

        historicSubCatArr = NSMutableArray(array:  ["tripp.jpg","rose.jpg","rose4.jpg","rose.jpg","rose4.jpg"])
        toursCategoriesArr = NSMutableArray(array:["The Medieval Market Town","The New Market Town"])
        
    }
    override func viewWillLayoutSubviews() {
         //self.tabBarController?.frame.origin.y=20
        //self.tabBarController?.frame.size.height=65
        var newTabBarFrame = self.tabBarController?.tabBar.frame
        newTabBarFrame?.size.height = 65
        self.tabBarController?.tabBar.frame = newTabBarFrame!
         self.tabBarController?.tabBar.frame.origin.y = 20
        self.tabBarController?.tabBar.barTintColor = UIColor(red: 35/255, green: 253/255, blue: 6/255, alpha: 1)
        
    }
    func showInitialScreen() {
        //If Question View opened then don't initial view
        if UserDefaults.standard.object(forKey: "QuestionView") != nil || UserDefaults.standard.object(forKey: "MoreView") != nil {
            UserDefaults.standard.removeObject(forKey: "MoreView")
        } else {
        collectionView.isHidden = false
        collCellTag = 0
        backIntVal = 0
        strBackBtnTag = "homeScrenMainView"
        scrollView.isHidden = true
        BackInitial()
        //Save View
        UserDefaults.standard.set("HOME_VIEW", forKey: "SavedViewController")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared().isEnabled = true
        // set initial Screen/View
        showInitialScreen()
         self.navigationController?.isNavigationBarHidden = true
         UserDefaults.standard.set("0", forKey: "ClickedTabIndex")
         UserDefaults.standard.removeObject(forKey: "JoinGroupCheck")
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
         UserDefaults.standard.set("HomeScreen", forKey: "PuzzleScreen")
   
      //  speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
        self.avPlayer.pause()
        checkInt = 0
        speechTag = 0;
        if (UserDefaults.standard.value(forKey: "profileCreated") == nil)
        {
            is_Joiner = false
            isOwner = false
            tourPursedOrNot = false
        }
        numOfMemToJoinTxt.text = "0"
        self.tabBarController?.delegate=self
        table_View.separatorColor = UIColor.clear
        PayPalMobile.preconnect(withEnvironment: environment)
        if (UserDefaults.standard.value(forKey: "HomeView") != nil)
        {
            if scrollContentView.isHidden == false {
            numOfMemToJoinTxt.isHidden = false
            numOfMemToJoinLbl.isHidden = false
            paymentTxt.text = "Paypal"
            PurcOrderContentView.isHidden = false
            scrollContentView.isHidden = true
              UserDefaults.standard.removeObject(forKey: "HomeView")
            strBackBtnTag = "scrollView"
            purchaseToursData()
            }
        }
        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
         purchasedTourOrNot(completion:{ (result) in
            
         })
        }
    }
    func createImageViews()
    {
        
        if self.view.frame.size.height < 580
        {
            dropImageView1  = UIImageView(frame:CGRect(x:self.view.frame.origin.x+5, y:strtBtnContentView.frame.origin.y+442, width:100, height:100));
            dropImageView2  = UIImageView(frame:CGRect(x:self.view.frame.size.width/2.9, y:strtBtnContentView.frame.origin.y+442, width:100, height:100));
            dropImageView3  = UIImageView(frame:CGRect(x:self.view.frame.size.width-105, y:strtBtnContentView.frame.origin.y+442, width:100, height:100));
        }
        else  if self.view.frame.size.height > 600 && self.view.frame.size.height < 700
        {
            dropImageView1  = UIImageView(frame:CGRect(x:self.view.frame.origin.x+8, y:strtBtnContentView.frame.origin.y+442, width:115, height:120));
            dropImageView2  = UIImageView(frame:CGRect(x:self.view.frame.size.width/2.9, y:strtBtnContentView.frame.origin.y+442, width:115, height:120));
            dropImageView3  = UIImageView(frame:CGRect(x:self.view.frame.size.width-124, y:strtBtnContentView.frame.origin.y+442, width:115, height:120));
        }
        else
        {
            dropImageView1  = UIImageView(frame:CGRect(x:self.view.frame.origin.x+10, y:strtBtnContentView.frame.origin.y+442, width:127, height:125));
            if self.view.frame.size.height > 1000
            {
                dropImageView2  = UIImageView(frame:CGRect(x:self.view.frame.size.width/2.3, y:strtBtnContentView.frame.origin.y+442, width:122, height:125));
            }
            else
            {
                dropImageView2  = UIImageView(frame:CGRect(x:self.view.frame.size.width/2.8, y:strtBtnContentView.frame.origin.y+442, width:122, height:125));
            }
            dropImageView3  = UIImageView(frame:CGRect(x:self.view.frame.size.width-133, y:strtBtnContentView.frame.origin.y+442, width:122, height:125));
        }
    
   
        
       dropImageView1.image = UIImage(named:"rose4.jpg")
        strtBtnContentView.addSubview(dropImageView1)
        dropImageView1.tag=1
        
        
  
        let newSize = CGSize(width: 900, height: 900)
        dropImageView1.image = imageView(with: dropImageView1.image!, scaledTo: newSize)
        
        
        dropImageView2.image = UIImage(named:"11medium.jpg")
        strtBtnContentView.addSubview(dropImageView2)
        dropImageView2.tag=2
        
        mainImageView.image=UIImage(named: "lineBorder.jpeg")
        self.currentImagePackage = ImagePackage(baseFileName: "", caption: "", photographer: "")
        self.currentImagePackage?.image =  dropImageView1.image
        
        dropImageView3.image = UIImage(named:"11medium.jpg")
        strtBtnContentView.addSubview(dropImageView3)
        dropImageView3.tag=3
        
        
        panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.move))
        panRecognizer.minimumNumberOfTouches = 1
        panRecognizer.maximumNumberOfTouches = 1
        dropImageView1.addGestureRecognizer(panRecognizer)
        
        
        panRecognizer2 = UIPanGestureRecognizer(target: self, action: #selector(self.move))
        panRecognizer2.minimumNumberOfTouches = 1
        panRecognizer2.maximumNumberOfTouches = 1
        dropImageView2.addGestureRecognizer(panRecognizer2)
        
        panRecognizer3 = UIPanGestureRecognizer(target: self, action: #selector(self.move))
        panRecognizer3.minimumNumberOfTouches = 1
        panRecognizer3.maximumNumberOfTouches = 1
        dropImageView3.addGestureRecognizer(panRecognizer3)
        
        
        dropImageView1.isUserInteractionEnabled=true
        dropImageView2.isUserInteractionEnabled=true
        dropImageView3.isUserInteractionEnabled=true
        
    }
    //Customize the size of Uiimage
    func imageView(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

    func tabBarControllerAction()
    {
        itemsArray = NSMutableArray(array: itemsArray6)
        // toursTable_View.isHidden=true
        table_View.isHidden=false
        scrollContentView.isHidden=true
        strtBtnContentView.isHidden=true
        PurcOrderContentView.isHidden=true
        MapContentView.isHidden=true
        
        //table_View.reloadData()
        
    }
    
    //////Collection View
    //===================================================================
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemsArray.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "COLLCELL", for: indexPath as IndexPath) as! HomeCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        //        cell.titleButton.text = self.items[indexPath.item]
        //        cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        
        let font = UIFont(name: "Helvetica", size: 20.0)
        let width = widthForView(text: (itemsArray[indexPath.row] as? String)!, font: font!, height: 49)
        print(width)
        // print(itemsArray as? String)
        let strImgName = cellImageArr .object(at: indexPath.row)
        let strVal = itemsArray[indexPath.row] as? NSString
       
        
        

        
           //cell.collCellbackImgView.contentMode.
        //let wid : Float = width+50
        cell.titleLabel.frame.size.width=width+49
        cell.titleLabel.frame.origin.x=self.view.frame.size.width-(width+68)
        
        let appendString2 = " "+(strVal! as String)
        
        cell.titleLabel?.text = appendString2
       
        cell.collCellbackImgView.layer.borderColor = UIColor.lightGray .cgColor
        cell.collCellbackImgView.layer.borderWidth = 4

        
         if (strVal?.isEqual(to: "How the tour works"))!
        {
            cell.collCellbackImgView.contentMode = .scaleAspectFit
             cell.titleLabel?.text = ""
            cell.titleLabel.isHidden = true
            cell.cellArrowIcon.isHidden = true
             cell.titleLabel.backgroundColor = UIColor .clear
            cell.collCellbackImgView.backgroundColor = UIColor .white
            
        }
        else
        {
            
            cell.collCellbackImgView.backgroundColor = UIColor .clear
            cell.titleLabel.isHidden = false
            cell.cellArrowIcon.isHidden = false
            cell.titleLabel.backgroundColor = UIColor (red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
            cell.collCellbackImgView.contentMode = .scaleToFill
            
            //cell.collCellbackImgView.layer.borderColor = UIColor.lightGray .cgColor
            //cell.collCellbackImgView.layer.borderWidth = 1
            
        }
         cell.titleLabel.isHidden = true
        if (strVal?.isEqual(to: "Browse Tours"))!
        {
            cell.collCellbackImgView.contentMode = .scaleAspectFit
            cell.collCellbackImgView.image = UIImage(named: "BrowserTour.jpg")
        }
        else if (strVal?.isEqual(to: "How the tour works"))!
        {
            cell.collCellbackImgView.image = UIImage(named: "work_tour.png")
        }
        else
        {
            cell.collCellbackImgView.image = UIImage(named: strImgName as! String)
        }

        
        return cell
    }
    
    func widthForView(text:String, font:UIFont, height:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: 49))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.width
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if model .isEqual(to: "iPhone")
        {
             return CGSize(width: self.view.frame.size.width-10 , height: 250)
        }
        else
            
        {
        if self.view.frame.size.height > 1000
        {
            return CGSize(width: self.view.frame.size.width-10 , height: 350)
        }else
        {
            return CGSize(width: self.view.frame.size.width-10 , height: 175)
        }
        }
        
    }
    
    @IBAction func dropImageBtn1 (sender:UIButton)
    {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 750)
        puzzleView.isHidden=false
        self.originalImageView.image = UIImage(named: "water.jpeg")
        self.originalImageView.layer.borderWidth = 2
        self.originalImageView.alpha = 0
        self.view.sendSubview(toBack: originalImageView)
        originalImageView.image=dropImageView1.image
    }
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        print(collCellTag)
        UserDefaults.standard.removeObject(forKey: "TourController")
        self.startChk = ""
        if indexPath.item==0 && collCellTag == 0{
             cityNameLabel.text = "Highjinx Tour";
            backButton.isHidden = false
             backIcon.isHidden = false
            strBackBtnTag = "homeScrenMainView"
            collCellTag = 1
            backIntVal = 0
            tagVal = 0
            table_View.isHidden=false
            collectionView.isHidden=true
            getCountriesData()
            
            
        }
        else if indexPath.item==1 && collCellTag == 0 {
            
             scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 500)
            collCellTag = 1
            backIntVal = 1
             tagVal = 1
            backButton.isHidden = false
             backIcon.isHidden = false
            strBackBtnTag = "homeScrenMainView"
            collCellTag = indexPath.item
            table_View.isHidden=true
            collectionView.isHidden=false
            itemsArray = NSMutableArray(array: tourWorksArr)
            collectionView.reloadData()
            cellImageArr = NSMutableArray(array:  ["familyfuntours.jpg","historictoursforgrownups.jpg","hensandstages.jpg","partynights.jpg"])
            
            PurcOrderContentView.isHidden = true
            scrollContentView.isHidden = true
            MapContentView.isHidden = true
            
        }
        else if indexPath.item==2 && collCellTag == 0 {
            scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 500)
            collCellTag = 1
            backIntVal = 1
             tagVal = 1
            strBackBtnTag = "SubCollection"
            backButton.isHidden = false
             backIcon.isHidden = false
            collCellTag = indexPath.item
            table_View.isHidden=true
            collectionView.isHidden=false
            PurcOrderContentView.isHidden = true
            scrollContentView.isHidden = true
            MapContentView.isHidden = true
            itemsArray = NSMutableArray(array: tourWorksArr)
            
            //collectionView.reloadData()
        }
            /////
        else if indexPath.item==0 && collCellTag == 1 {
            strBackBtnTag = "homeScrenMainView"
            collCellTag = 2
            backIntVal = 2
             tagVal = 2
            backButton.isHidden = false
            backIcon.isHidden = false
            scrollView.isHidden=false
            toursContentView.isHidden=false
            collectionView.isHidden=true
            PurcOrderContentView.isHidden = true
            scrollContentView.isHidden = true
            MapContentView.isHidden = true
            toursHeaderlabel.text=tourWorksArr[indexPath.row] as? String
            cityNameLabel.text =  toursHeaderlabel.text
        }
        else if indexPath.item==1 && collCellTag == 1 {
            
            
            collCellTag = 2
            backIntVal = 2
             tagVal = 2
            backButton.isHidden = false
            backIcon.isHidden = false
            strBackBtnTag = "homeScrenMainView"
            scrollView.isHidden=false
            toursContentView.isHidden=false
            table_View.isHidden=true
            PurcOrderContentView.isHidden = true
            scrollContentView.isHidden = true
            MapContentView.isHidden = true
            toursHeaderlabel.text=tourWorksArr[indexPath.row] as? String
            cityNameLabel.text =  toursHeaderlabel.text
        }
                  
        else if indexPath.item==2 && collCellTag == 1 {
            collCellTag = 2
            backIntVal = 2
            backButton.isHidden = false
            backIcon.isHidden = false
            strBackBtnTag = "homeScrenMainView"
            scrollView.isHidden=false
            toursContentView.isHidden=false
            table_View.isHidden=true
            PurcOrderContentView.isHidden = true
            scrollContentView.isHidden = true
            MapContentView.isHidden = true
            toursHeaderlabel.text=tourWorksArr[indexPath.row] as? String
            cityNameLabel.text =  toursHeaderlabel.text
        }
        else if indexPath.item==3 && collCellTag == 1 {
            collCellTag = 2
            backIntVal = 2
            backButton.isHidden = false
            backIcon.isHidden = false
            strBackBtnTag = "homeScrenMainView"
            scrollView.isHidden=false
            toursContentView.isHidden=false
            table_View.isHidden=true
            
            PurcOrderContentView.isHidden = true
            scrollContentView.isHidden = true
            MapContentView.isHidden = true
            toursHeaderlabel.text=tourWorksArr[indexPath.row] as? String
            cityNameLabel.text =  toursHeaderlabel.text
        }
        
            /////
        else if indexPath.item==0 && collCellTag == 2 {
            collCellTag = 2
            backIntVal = 2
            scrollView.isHidden=false
            toursContentView.isHidden=false
            table_View.isHidden=true
            toursHeaderlabel.text=tourWorksArr[indexPath.row] as? String
            cityNameLabel.text =  toursHeaderlabel.text
        }
        else if indexPath.item==1 && collCellTag == 2 {
            collCellTag = 2
            backIntVal = 2
            backButton.isHidden = false
            backIcon.isHidden = false
            strBackBtnTag = "homeScrenMainView"
            scrollView.isHidden=false
            toursContentView.isHidden=false
            table_View.isHidden=true
            PurcOrderContentView.isHidden = true
            scrollContentView.isHidden = true
            MapContentView.isHidden = true
            toursHeaderlabel.text=tourWorksArr[indexPath.row] as? String
            cityNameLabel.text =  toursHeaderlabel.text
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //  print( itemsArray.count)
        return  self.ArrCountries_And_Tours.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = table_View.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as! HomeTableViewCell
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        cell.buyButton?.layer.cornerRadius = 12
        scrollView.isHidden=true
        
        table_View.separatorColor = UIColor.clear
        cell.buyButton.tag = indexPath.row
        cell.secSubImageView.contentMode = .scaleAspectFill
        
      
        if tagVal==1 {
            cell.secSubViewHeadLabel?.textColor = UIColor.black
               cell.secSubViewHeadLabel?.textColor = UIColor.black
            cell.buyButton?.isHidden = true
            cell.homeScrenMainView?.isHidden=true
            cell.homeScrenSubView?.isHidden=true
            cell.homeScrenSecSubView?.isHidden=false
            
            if ArrTours.count != 0
            {
            let strTours = self.ArrTours[indexPath.row] as! NSDictionary
            print(self.ArrTours)
            cell.secSubViewHeadLabel?.numberOfLines = 2
                 cell.secSubViewHeadLabel.lineBreakMode = .byWordWrapping
            cell.secSubViewHeadLabel?.frame.size.height  = 45
            cell.secSubViewHeadLabel?.text = strTours.value(forKey: "name") as? String
            
            let tourCount = strTours.value(forKey: "count") as! Int
         
             var strTourCount = String()
                
                strTourCount = String(describing: tourCount)

            cell.secSubViewTitleLabel?.text = strTourCount + " Tours"

            cell.secSubViewTitleLabel.isHidden = false
            cell.secSubViewHeadLabel?.frame.origin.y = 25
            cell.secSubViewTitleLabel?.frame.origin.y = 58
            
                 cell.secSubImageView.contentMode = .scaleAspectFit
               
                
                
                cell.secSubImageView.image = nil
                  cell.secSubImageView.setShowActivityIndicator(true)
                cell.secSubImageView.setIndicatorStyle(.gray)
                
                cell.secSubImageView.sd_setImage(with: URL(string: strTours.value(forKey: "image") as! String), completed: { (image, error, cache, url) in
                    cell.secSubImageView.setShowActivityIndicator(false)
                })
    
            }
            
        }
        else  if tagVal==2 {
            cell.buyButton?.isHidden = true
            cell.homeScrenSubView?.isHidden=true
            cell.homeScrenSecSubView?.isHidden=false
            cell.secSubViewTitleLabel?.isHidden = false
            cell.secSubViewHeadLabel?.frame.size.height  = 40
            cell.secSubViewHeadLabel?.frame.origin.y = 25
            cell.secSubViewTitleLabel?.frame.origin.y = 55
            
            if historicalSubArr.count != 0
            {
            let strTours = self.historicalSubArr[indexPath.row] as! NSDictionary
            print(self.ArrTours)
            cell.secSubViewHeadLabel?.numberOfLines = 2
            cell.secSubViewHeadLabel?.frame.size.height  = 45
             cell.secSubViewHeadLabel.lineBreakMode = .byWordWrapping
            cell.secSubViewHeadLabel?.text = strTours.value(forKey: "name") as? String
            let strCities = self.historicalSubArr[indexPath.row] as! NSDictionary
            cell.secSubViewHeadLabel.numberOfLines = 2
            cell.secSubViewHeadLabel?.textColor = UIColor.purple
          
                 let tourCount = strTours.value(forKey: "count") as! Int
                  let strTourCount = String(tourCount)
            cell.secSubViewTitleLabel?.text = strTourCount + " Tours"
            
            cell.secSubImageView.contentMode = .scaleAspectFit
              
                cell.secSubImageView.setShowActivityIndicator(true)
                cell.secSubImageView.setIndicatorStyle(.gray)
                cell.secSubImageView.image = nil
                cell.secSubImageView.sd_setImage(with: URL(string: strCities.value(forKey: "image") as! String), completed: { (image, error, cache, url) in
                    cell.secSubImageView.setShowActivityIndicator(false)
                })

            }
        }
        else  if tagVal==3 {
            cell.buyButton.setTitle("INFO", for:UIControlState.normal)
            cell.secSubViewHeadLabel?.textColor = UIColor.black
            cell.buyButton?.isHidden = false
            cell.homeScrenSubView?.isHidden=true
            cell.homeScrenSecSubView?.isHidden=false
            cell.secSubViewTitleLabel?.isHidden = false
            cell.secSubViewHeadLabel?.frame.size.height  = 45
            cell.secSubViewHeadLabel?.frame.origin.y = 35
            cell.secSubViewHeadLabel.lineBreakMode = .byWordWrapping
            if toursCategoriesArr.count != 0
            {
                  print( self.toursCategoriesArr)
            let value: AnyObject? = self.toursCategoriesArr[indexPath.row] as AnyObject?
              
            if(value is NSDictionary)
            {
                let strTours = self.toursCategoriesArr[indexPath.row] as! NSDictionary
                cell.secSubViewHeadLabel?.text = strTours.value(forKey: "tour_name") as? String
                
               cell.secSubImageView.setShowActivityIndicator(true)
                cell.secSubImageView.setIndicatorStyle(.gray)

                cell.secSubImageView.image = nil
                cell.secSubImageView.sd_setImage(with: URL(string: strTours.value(forKey: "image_small") as! String), completed: { (image, error, cache, url) in
                    cell.secSubImageView.setShowActivityIndicator(false)
                })
                
            } else {
                let strTours = self.toursCategoriesArr[indexPath.row] as! String
                cell.secSubViewHeadLabel?.text = strTours
            }
           
            cell.secSubViewHeadLabel.numberOfLines = 2
            cell.secSubViewTitleLabel.isHidden = true
            }
        }
        else {
            cell.secSubViewHeadLabel?.textColor = UIColor.black
            cell.secSubViewHeadLabel?.numberOfLines = 2
            cell.secSubViewHeadLabel?.frame.size.height  = 45
            cell.secSubViewHeadLabel?.frame.origin.y = 25
            cell.secSubViewTitleLabel?.frame.origin.y = 55
            cell.buyButton?.isHidden = true
            cell.homeScrenSecSubView.isHidden=true
            cell.homeScrenSubView.isHidden=false
            
            if ArrCountries.count != 0
            {
            let strCities = self.ArrCountries[indexPath.row] as! NSDictionary
            cell.subViewHeadLabel?.text =  strCities.value(forKey: "name") as? String
            
            let count = self.ArrCountries.count
            let strCityname:String = String(count) + " Tours"
            cell.subViewDescLabel?.text = strCityname
                
                cell.subViewImageView.setShowActivityIndicator(true)
                cell.subViewImageView.setIndicatorStyle(.gray)
                
                cell.subViewImageView.sd_setImage(with: URL(string: strCities.value(forKey: "image") as! String), completed: { (image, error, cache, url) in
                    cell.subViewImageView.setShowActivityIndicator(false)
                })
            }
        }
        cell.subViewImageView.backgroundColor = UIColor .white
        cell.secSubImageView.backgroundColor = UIColor .white
        
        cell.subViewImageView.layer.cornerRadius = 12
        cell.secSubImageView.layer.cornerRadius = 12
        cell.subViewImageView.layer.masksToBounds = true
        cell.secSubImageView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor .white .cgColor
        cell.contentView.layer.borderWidth = 6;
        
        cell.homeScrenSubView.layer.borderColor = UIColor .lightGray .cgColor
        cell.homeScrenSubView.layer.borderWidth = 0.5;
        cell.homeScrenSecSubView?.layer.borderColor = UIColor .lightGray .cgColor
        cell.homeScrenSecSubView?.layer.borderWidth = 0.5;
        
        
        cell.buyButton?.layer.masksToBounds = false
        cell.buyButton?.layer.cornerRadius = (cell.buyButton?.frame.height)!/2
        cell.buyButton?.clipsToBounds = true
        
        if cell.homeScrenSecSubView.isHidden == false
        {
            cellHeightTag = 125
        }
        else
        {
            cellHeightTag=0
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if cellHeightTag == 125
            
        {
            if self.view.frame.size.height > 100
            {
                return 130
            }else
            {
                return 130;//Choose your custom row height
            }
        }
        else
        {
            if self.view.frame.size.height > 1000
            {
                return 160
            }
            else
                
            {
                return 150
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedValue = indexPath.row

        if tagVal==0 {
            
            if self.ArrCountries.count != 0 {
                strBackBtnTag = "homeScrenSubView"
                tagVal=1
                cellTag = 1
                
                headerTagVal = indexPath.row
                
                //Check Array is empty or not...
                if ArrCountries.count > 0 && ArrCountries.count > indexPath.row {
                    
                    let strCountries = self.ArrCountries[indexPath.row] as! NSDictionary
                    cityNameLabel.text = (strCountries.value(forKey: "name") as! String?)!+(" Tours")
                    strLocationID = (strCountries.value(forKey: "location_id") as? NSNumber)!
                    UserDefaults.standard.set(strLocationID, forKey: "location_id")
                    
                    getToursData(loaction_ID: strLocationID, completion: { (result) in
                        
                        DispatchQueue.main.async {
                            
                            self.ArrCountries_And_Tours = NSMutableArray(array: (result)!)
                            self.ArrTours = NSMutableArray(array: (result)!)
                            self.table_View.reloadData()
                            
                            
                        }
                        
                    })
                }
            }
        }
            
        else if tagVal==1 {
            
            if self.ArrCountries.count != 0 {
                //Check Array is empty or not...
                let strCountries = self.ArrCountries[0] as! NSDictionary
                let countryName = strCountries.value(forKey: "name") as? String
                
                strLocationID = (strCountries.value(forKey: "location_id") as? NSNumber)!
                loactionID = String(describing: strLocationID) as NSString
                headerTagVal2 = indexPath.row
                strBackBtnTag = "homeScrenSecSubView"
                    //Check Array is empty or not...
                    if ArrTours.count > 0 {
                let strToures = self.ArrTours[indexPath.row] as! NSDictionary
                strTourTypeID = (strToures.value(forKey: "tourtype_id") as? NSNumber)!
                let tourID = String(describing: strTourTypeID)
                UserDefaults.standard.set(tourID, forKey: "tourtype_id")
                
                
                
                getTourSubTypesData(tourtype_id: strTourTypeID , completion: { (result) in
                    
                    DispatchQueue.main.async {
                        let toursName = strToures.value(forKey: "name") as? String
                        self.cityNameLabel.text = countryName!+" "+toursName!
                        
                        self.historicalSubArr = NSMutableArray(array: (result)!)
                        self.ArrCountries_And_Tours = NSMutableArray(array: self.historicalSubArr)
                        if self.historicalSubArr.count != 0
                        {
                            self.tagVal=2
                            self.cellTag = 2
                        }
                        
                        self.table_View.reloadData()
                        
                        
                    }
                    
                    
                })
            
                }
            }
            
            
        }
        else if tagVal==2 {
            
            
            if self.historicalSubArr.count != 0
            {
                strBackBtnTag = "homeScrenSecSubView"
                //Check Array is empty or not...
                headerTagVal2 = indexPath.row
                if historicalSubArr.count > 0 && historicalSubArr.count > indexPath.row {
                    let strTours = self.historicalSubArr[indexPath.row] as! NSDictionary
                    
                    strTourTypeID = (strTours.value(forKey: "tourtype_id") as? NSNumber)!
                    strtourSubTypeID = (strTours.value(forKey: "toursubtype_id") as? NSNumber)!
                    //Check Array is empty or not...
                    if ArrCountries.count > 0 {
                        let strCountries = self.ArrCountries[0] as! NSDictionary
                        let locID = (strCountries.value(forKey: "location_id") as? NSNumber)!
                        _ = String(describing: locID) as NSString
                        
                        getTourListData(location_id: strLocationID, tourtype_id: strTourTypeID , toursubtype_id: strtourSubTypeID, completion: { (result) in
                            
                            DispatchQueue.main.async {
                                self.toursCategoriesArr = NSMutableArray(array: (result)!)
                                self.ArrCountries_And_Tours = NSMutableArray(array: self.toursCategoriesArr)
                                
                                var toursSubCat = self.historicalSubArr[indexPath.row] as? String
                                toursSubCat = strTours.value(forKey: "name") as? String
                                self.cityNameLabel.text = toursSubCat!
                                if self.toursCategoriesArr.count != 0
                                {
                                    self.tagVal=3
                                    self.cellTag = 3
                                }
                                
                                self.table_View.reloadData()
                            }
                            
                        })
                    }
                }
            }
            else
            {
                
            }
        }
        
        
    }
    //===================================================================
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.reloadData()
       
        let tabbar = self.tabBarController?.tabBar
        
        for tab in (tabbar?.items)! {
            tab.image = tab.image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)

        }
        
    }
    // MARK: Other class methods
    func puzzleIsSolved() {
        strAlertMsg = "Puzzle Completed"
        mainImageView.isHidden = false
        tileArea.isHidden = false
        dropCorrectImgLabel.isHidden = false
        uploadImaegTxtField.isHidden = false
        uploadBtn.isHidden = false
        uploadIcon.isHidden = false
        backButton.isHidden = false
        backIcon.isHidden = false
        uploadAnswerLabel.isHidden = false
        dropCorrectImgLabel.isHidden = false
        uploadSubmitButton . isHidden = false
        
        
        dropImageView1.removeFromSuperview()
        dropImageView2.removeFromSuperview()
        dropImageView3.removeFromSuperview()
        dropImageView1.isHidden=true
        dropImageView2.isHidden=true
        dropImageView3.isHidden=true
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 650)
        puzzleView.isHidden=true
        uploadImaegTxtField.isHidden=false
        uploadIcon.isHidden=false
        uploadBtn.isHidden=false
        dragAndDroplabel.isHidden=true
        uploadSubmitButton.isHidden=false
        uploadAnswerLabel.isHidden=false
        mainImageView.isHidden=true
        dropCorrectImgLabel.isHidden = true
        alertBar()
        // Update stats
//        let stats = Stats()
//        stats.updateSolveStats(self.tilesPerRow)
        strBackBtnTag = "submitBtnSubView1"
        // Hide and disable buttons

    }
    
    // @IBAction func BuyCellButton(_ sender: UIButton)
    @IBAction func BuyCellButton(_ sender: UIButton) {
        checkInt = 0
        speechTag = 0
        readMoreButton.frame.origin.y = tourDetailsLabel.frame.origin.y + tourDetailsLabel.frame.size.height-20
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 600)

         movePositions()
        var indexPath = IndexPath()
        print(sender.tag)
        
        
        if sender.tag==3 {
            indexPath = IndexPath(row: 0, section: 0)
            sender.tag = 0
        }
        else{
            indexPath = IndexPath(row: sender.tag, section: 0)
        }
        
        let cell: HomeTableViewCell! = (table_View.cellForRow(at: indexPath)) as! HomeTableViewCell
        tourImagview.image = cell.secSubImageView.image
        

        
        
        
        let value: AnyObject? = self.toursCategoriesArr[indexPath.row] as AnyObject?
        if(value is NSDictionary)
        {
            let strToures = self.toursCategoriesArr[sender.tag] as! NSDictionary
            print(self.ArrTours)
            cityNameLabel.text = strToures.value(forKey: "tour_name") as? String
            
             UserDefaults.standard.set(cityNameLabel.text, forKey: "tour_name")
            
            let tourID = strToures.value(forKey: "tour_id") as? NSNumber
            UserDefaults.standard.set(tourID, forKey: "tour_id")
            
            self.tour_price = strToures.value(forKey: "tour_price") as! NSString
            UserDefaults.standard.set(self.tour_price, forKey: "tour_price")
            
            let tour_name = strToures.value(forKey: "tour_name")
            UserDefaults.standard.set(tour_name, forKey: "tour_name")
            
            let description = strToures.value(forKey: "description")
            UserDefaults.standard.set(description, forKey: "description")
            
            if let audio = strToures.value(forKey: "audio") as? String {
                print(audio)
                playTourDetailsSong()
                descriptionAudioUrl = strToures.value(forKey: "audio") as! String
            }
            
            
            
            self.descriptionTxtview.text = description as! String!
              self.tourDetailsLabel.text = description as! String!
            
            
            if tourDetailsLabel.text!.characters.count < 200 {
                readMoreButton.isHidden = true;
            }
            else {
                readMoreButton.isHidden = false;
            }
            
            
            var tourPri = self.tour_price as String
            
            if tourPri.contains("$") {
                tourPri.remove(at: tourPri.startIndex)
            }//else {
            let intTourPri: Float = Float(tourPri)!
            let divTourPri: Float = intTourPri/2
            let sumVal = String(divTourPri) as NSString
            
            let completePrice = "1 App €"+(tourPri as String)+(" all additional App's €")+(sumVal as String)
             self.tourPriceLabel.text = completePrice
           // }
        }
        else {
            let strToures = self.toursCategoriesArr[sender.tag] as! String
            print(self.ArrTours)
            cityNameLabel.text = strToures
        }
       
  
        
        
        //================================================
       
        priceOfTour = tour_price
        //let completePrice = "€" + (priceOfTour as String) + (str as String) + (sumVal as String)
        // self.tour_price  = String(sumVal) as NSString
        //=======================
       
       // priceOfTour = ""
        
        strBackBtnTag = "homeScrenSecSubView2"
        
        table_View.isHidden=true
        scrollView.isHidden=false
        toursContentView.isHidden=true
        scrollContentView.isHidden=false
        PurcOrderContentView.isHidden=true
       
        
        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
            //Checked tour is purchased or not
            purchasedTourOrNot(completion:{ (result) in
            })
        }
        else
        {
            
            //tourNotPurchased = "Tour Not Purchased"
               //  if tourPursedOrNot == false
           
        }
    
        
    }
    @IBAction func startButton (sender:UIButton)
    {
        if (UserDefaults.standard.value(forKey: "profileCreated") == nil) {
           self.strAlertMsg = "To start the tour:\n\n"+"1. Click on JOIN TOUR and enter group code\n"+"or\n"+"2. Click PURCHASE TOUR." as NSString
            self.paymentSuccessAlertView()
        }
        else {
            
            var tourID = Int()
            
            if UserDefaults.standard.object(forKey: "tour_id") != nil {
                tourID = UserDefaults.standard.object(forKey: "tour_id") as! Int
                UserDefaults.standard.set(tourID, forKey: "tour_id")
            }
            
            
            
            scrollView.isHidden=false
            scrollContentView.isHidden=false
            
            var getID = String()
            
            if UserDefaults.standard.value(forKey: "TourId") != nil {
                getID = UserDefaults.standard.value(forKey: "TourId") as! String
                
            }
            //Check tour is complted or not
            if getID.contains(String(describing:tourID)) {
                UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
            }
            
            if self.isCompletedTour == true {
                self.notInterNetConn(message: "You have already completed this tour. You can see the details under My Profile/Tour History. If you would like to take the tour again, you will need to purchase it.")
            }
            else {
             
                self.startChk = "startButton"
                self.strBackBtnTag = "homeScrenSecSubView2"
                
                if self.is_Joiner == true {
                    UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
                        self.moveToClass()
                }
                else {
                    self.mapCheck = ""
                    
                    if self.tourPursedOrNot == false {
                        UserDefaults.standard.removeObject(forKey: "IsOwner")
                        
                        self.strAlertMsg = "To start the tour:\n\n"+"1. Click on JOIN TOUR and enter group code\n"+"or\n"+"2. Click PURCHASE TOUR." as NSString
                        self.paymentSuccessAlertView()
                    }
                    else if self.isOwner == false {
                        UserDefaults.standard.removeObject(forKey: "IsOwner")
                        
                        self.strAlertMsg = "You are not an owner of this Tour, Please click Purchase button to PURCHASE TOUR this Tour"
                        self.paymentSuccessAlertView()
                    }
                    else {
                       // speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
                         self.avPlayer.pause()
                        //Get all ID saved when Joined Group
                        if  UserDefaults.standard.object(forKey: "JoinGroupSavedTourID") != nil {
                            
                            var arrjoinId = [Int]()
                            
                            arrjoinId = UserDefaults.standard.object(forKey: "JoinGroupSavedTourID")as! [Int]
                            print(arrjoinId)
                            
                            print(arrjoinId.count)
                            if arrjoinId.contains(tourID)
                            {
                                UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
                            }
                            else
                            {
                                UserDefaults.standard.set("IsOwner", forKey: "IsOwner")
                            }
                            // print(UserDefaults.standard.object(forKey: "JoinGroupSavedTourID") as! NSArray)
                            
                        }
                        else {
                            UserDefaults.standard.set("IsOwner", forKey: "IsOwner")
                        }
                        
                        if self.is_Joiner == true {
                            UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
                        }
                        
                        UserDefaults.standard.set("HomeScreen", forKey: "PuzzleScreen")
                        
                        let when = DispatchTime.now() + 0.2
                        DispatchQueue.main.asyncAfter(deadline: when){
                            self.movePositions()
                            self.moveToClass()
                        }
                    }
                    
                }
            }
        }
    }
    func dropPuzzleImages() {
        strBackBtnTag = "scrollView"
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 700)
        scrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
        dropImageView1.isHidden = false
        dropImageView2.isHidden = false
        dropImageView3.isHidden = false
        mainImageView.isHidden = false
        submitBtnSubView1.isHidden = true
        submitBtnSubView2.isHidden = true
        submitBtnSubView3.isHidden = true
        dropCorrectImgLabel.isHidden = false
        dragAndDroplabel.isHidden = false
        table_View.isHidden=true
        toursContentView.isHidden=true
        scrollContentView.isHidden=true
        scrollView.addSubview(strtBtnContentView)
        strtBtnContentView.isHidden=false
        
        uploadAnswerLabel.isHidden = true
        dropCorrectImgLabel.isHidden = false
        uploadSubmitButton . isHidden = true
    }
    @IBAction func MapButton (sender:UIButton)
    {
        if tourPursedOrNot == false && is_Joiner == false
        {
            mapCheck = "map"
            strAlertMsg = "Please purchase tour first to use this feature"
            paymentSuccessAlertView()
        }
        else
        {

                if UserDefaults.standard.object(forKey: "tour_id") != nil{
                  //  let tourID = UserDefaults.standard.object(forKey: "tour_id") as! Int
//
//                if  tourID == 4
//                {
                    //speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
                     self.avPlayer.pause()
                        ToursMapData.getMapData(completion: {(locResult)in
                        
                        })
                
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "ToursMapViewController") as! ToursMapViewController
                    self.navigationController?.pushViewController(newViewController, animated: true)

                    
//                    }
//                else
//                {
//                    self.strAlertMsg = "Map is not available for this tour"
//                    self.alertBar()
//                    }
                }
        }
    }
    @IBAction func purchaseTourBtn (sender:UIButton) {

        teamLeaderPriLbl.isHidden = false
        checkImageView.isHidden = false
        
          var strTourID = Int()
          var tourID = Int()
        if  UserDefaults.standard.object(forKey: "JoinGroupTourID") != nil {
             strTourID = UserDefaults.standard.object(forKey: "JoinGroupTourID") as! Int
              tourID =  (UserDefaults.standard.value(forKey: "tour_id") as? Int)!
        }
        else  {
            strTourID = 1111
        }
        if  is_Joiner == true {
            if is_Joiner == true && isCompletedTour == false {
                tourValCheck()
            }
            else {
                if tourPursedOrNot == true {
                    purcTourData()
                }
                else {
                    if  isCompletedTour == false{
                        tourValCheck()
                    } else {
                        purcTourData()
                    }
                }
            }
        }
        else {
            if strTourID == tourID {
                tourValCheck()
            }
            else {
                purcTourData()
            }
            
        }
    }
    func tourValCheck() {
        self.tourPurchased = "purchased"
        UserDefaults.standard.set("NotOwner", forKey: "IsOwner")
        isOwner = false
        tourPursedOrNot = false
        strAlertMsg = "You are subscriber to this tour. You can not purchase this tour, you just start tour"
        paymentSuccessAlertView()
    }
    func purcTourData() {
      
        couponCodeTxt.isHidden = true
        if tourPursedOrNot == true && isCompletedTour == false {
            self.tourPurchased = "purchased"
            strBackBtnTag = "scrollView"
            strAlertMsg = "You have already purchased this tour"
            paymentSuccessAlertView()
        }
        else {
            speechTag = 0
            checkInt = 0
            // speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
             self.avPlayer.pause()
            firstNameTxt.textColor = UIColor .lightGray
            LastNameTxt.textColor = UIColor .lightGray
            emailTxt.textColor = UIColor .lightGray
            phoneNumTxt.textColor = UIColor .lightGray
            countryTxt.textColor = UIColor .lightGray
            
            numOfMemToJoinTxt.text = "0"
            purchaseOrderBtn.frame.origin.y = 625
              startChk = "purchaseTourBtn"
            
            
        
            countryTxt.isUserInteractionEnabled = false
            if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
            {
                movePositions()
                purchaseToursData()
                
                paymentTypeView.isHidden = true
                paymentBtntag = 0
                paymentTxt.text = "Paypal"
                upDownArrowImgView.image = UIImage(named: "dropDown.png")
                
                strBackBtnTag = "scrollView"
                scrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                scrollView.addSubview(PurcOrderContentView)
                MapContentView.isHidden=true
                toursContentView.isHidden=true
                strtBtnContentView.isHidden=true
                PurcOrderContentView.isHidden=false
                
                numOfMemToJoinLbl.isHidden = false
                
                numOfMemToJoinTxt.isHidden = false
            }
            else
            {
                
                strAlertMsg = "Firstly, you have to create Account Profile"
                
                profileAlertbar()
            }
            
            self.scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 700)
        }
    }
    @IBAction func purchaseMainAction (sender:UIButton) {
      
       
        if (couponCodeTxt.text?.isEmpty)! && couponCodeTxt.isHidden  == false
        {
            strAlertMsg = "Please input coupon code"
            PurchaseAlertBar()
        }
        else if (paymentTxt.text == "Coupon Code" && !(couponCodeTxt.text?.isEmpty)!) {
            
            
            self.purchaseTour()
        }
        else if paymentTxt.text == "Paypal" {
           
            self.tour_price = UserDefaults.standard.value(forKey: "tour_price") as! NSString
            
            let getVal = numOfMemToJoinTxt.text! as String
            let tourPri = self.tour_price as String
            
            
            let intGetVal: Float = Float(getVal)!
            let intTourPri: Float = Float(tourPri)!
            
            let divTourPri: Float = intTourPri/2
            
            let selectedUserPri: Float = divTourPri * intGetVal
            let sumVal : Float = selectedUserPri + intTourPri
            
              priceOfTour  = String(sumVal) as NSString
            
           // You have to pay the following amount. Press confirm to pay
            
            let str = " €"
            let str2 = ". Press to confirm payment"
            strAlertMsg = "Total cost is "+(str as String)+(priceOfTour as String)+(str2 as String) as String as NSString
           self.tour_price = ""
            
           self.payAlertViewbar()
         
            
        }
        
    }
    
    func payAlertViewbar()
    {
        payAlertView = UIAlertController(title: nil, message:strAlertMsg as String , preferredStyle: UIAlertControllerStyle.alert)
        let canclBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.payAlertViewCancelAction()
        }
        let okBtn = UIAlertAction(title: "Confirm", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.payAlertViewOkAction()
        }
        payAlertView.addAction(canclBtn)
        payAlertView.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(self.payAlertView, animated: true, completion: nil)
        }
        
    }
    func payAlertViewOkAction()
    {
        paypal()
    }
    func payAlertViewCancelAction()
    {
        strAlertMsg = ""
        payAlertView.dismiss(animated: true, completion: nil)
    }
    func BackInitial() {
        strBackBtnTag = ""
        collCellTag = 0
        
        if collCellTag == 0 {
            cityNameLabel.text = "Home";
            backButton.isHidden = true
            backIcon.isHidden = true
        }
        table_View.isHidden=true
        collectionView.isHidden=false
        itemsArray = NSMutableArray(array: itemsArray6)
        collectionView.reloadData()
    }
    @IBAction func backButton(sender: UIButton)
    {
        progessDismiss()
       speechTag = 0
        checkInt = 0
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
         self.avPlayer.pause()
        hideFunc()
        if  strBackBtnTag .isEqual(to: "homeScrenMainView") && backIntVal == 0 {
           BackInitial()
        }
        else if  strBackBtnTag .isEqual(to: "homeScrenSubView") {
            cityNameLabel.text = "Highjinx Tour";
            strBackBtnTag = "homeScrenMainView"
            tagVal = 0
            cellTag = 0
            collCellTag = 1
            table_View.isHidden=false
            collectionView.isHidden=true
            ArrCountries_And_Tours = NSMutableArray(array: ArrCountries)
            table_View.reloadData()
        }
            
        else if  strBackBtnTag .isEqual(to: "homeScrenSecSubView") && tagVal != 3 {
            
           
            let strCountries = self.ArrCountries[self.headerTagVal] as! NSDictionary
            self.cityNameLabel.text = (strCountries.value(forKey: "name") as! String?)!+(" Tours")

            self.strBackBtnTag = "homeScrenSubView"
            self.cellHeight=115
            self.tagVal = 1
            self.table_View.isHidden=false
            self.collectionView.isHidden=true
            if self.ArrTours.count != 0 {
                 UserDefaults.standard.set(self.ArrTours, forKey: "self.ArrTours")
            }
            else {
                if UserDefaults.standard.object(forKey: "self.ArrTours") != nil {
                self.ArrTours.addObjects(from: UserDefaults.standard.object(forKey: "self.ArrTours") as! [Any])
                print(self.ArrTours)
                }
            }
           self.ArrCountries_And_Tours = NSMutableArray(array: self.ArrTours)
            //print("hdjsafhjddfasfjdhajsfhjdsfhjkdsfjjdsfjdfjhjds\(self.ArrCountries_And_Tours) ")
            self.table_View.reloadData()
            
            
        }
            //New
        else if  strBackBtnTag .isEqual(to: "homeScrenSecSubView") && tagVal == 3 {

            let strCountries = self.ArrCountries[0] as! NSDictionary
            let countryName = strCountries.value(forKey: "name") as? String
            
            let strToures = self.historicalSubArr[headerTagVal2] as! NSDictionary
            let toursName = strToures.value(forKey: "tourtype_name") as? String
            cityNameLabel.text = countryName!+" "+toursName!
            
            strBackBtnTag = "homeScrenSecSubView"
            cellHeight=115
            tagVal = 2
            table_View.isHidden=false
            collectionView.isHidden=true
            ArrCountries_And_Tours = NSMutableArray(array: historicalSubArr)
            table_View.reloadData()

            
        }
         //New
        else if  strBackBtnTag .isEqual(to: "homeScrenSecSubView2")
        {
             movePositions()
            if  headerTagVal2 < self.historicalSubArr.count {
            let strToures = self.historicalSubArr[headerTagVal2] as! NSDictionary
            let toursName = strToures.value(forKey: "name") as? String
            cityNameLabel.text = toursName
            }
            strBackBtnTag = "homeScrenSecSubView"
            cellHeight=115
            tagVal = 3
            table_View.isHidden=false
            collectionView.isHidden=true
           
            ArrCountries_And_Tours = NSMutableArray(array: self.toursCategoriesArr)
            table_View.reloadData()
        }

        else if  strBackBtnTag .isEqual(to: "scrollView")
        {
            movePositions()
             scrollView.contentSize = CGSize(width: self.view.frame.size.width,height: 600)
            //cityNameLabel.text = toursCategoriesArr[selectedValue] as? String
            print(cityNameLabel)
            strBackBtnTag = "homeScrenSecSubView2"
            table_View.isHidden=true
            scrollContentView.isHidden = false
            strtBtnContentView.isHidden = true
            MapContentView.isHidden = true
            PurcOrderContentView.isHidden = true
            scrollView.isHidden = false
            
        }
        else if strBackBtnTag .isEqual(to: "homeScrenMainView") &&  backIntVal == 1
        {
            //collCellTag = 0
            cellImageArr = NSMutableArray(array:  ["tours_two.jpg","tourWorks.png","tours_two.jpg","tourone.jpg"])
            backIntVal = 0
            collCellTag = 0
            backButton.isHidden = true
            backIcon.isHidden = true
            strBackBtnTag = "homeScrenMainView"
            itemsArray = NSMutableArray(array: itemsArray6)
            collectionView.reloadData()
        }
        else if strBackBtnTag .isEqual(to: "homeScrenMainView") &&  backIntVal == 2
        {
             cityNameLabel.text = "Home"
            backIntVal = 1
            collCellTag = 1
            strBackBtnTag = "homeScrenMainView"
            itemsArray = NSMutableArray(array: tourWorksArr)
            scrollView.isHidden = true
            collectionView.isHidden = false
            collectionView.reloadData()
        }
        else if strBackBtnTag .isEqual(to: "SubCollection") &&  backIntVal == 1
        {
            backIntVal = 0
            collCellTag = 0
            backButton.isHidden =  true
            backIcon.isHidden = true
            strBackBtnTag = "homeScrenMainView"
            itemsArray = NSMutableArray(array: itemsArray6)
            collectionView.reloadData()
        }
        else if strBackBtnTag .isEqual(to: "SubCollection") &&   backIntVal == 2
        {
            collCellTag = 1
            backIntVal = 1
            strBackBtnTag = "homeScrenMainView"
            itemsArray = NSMutableArray(array: tourWorksArr)
            scrollView.isHidden = true
            collectionView.isHidden = false
            collectionView.reloadData()
        }
        
        else if strBackBtnTag .isEqual(to: "StartBtnContentView") {
            strBackBtnTag = "scrollView"
            strtBtnContentView.isHidden = false
            mainImageView.isHidden = false
            tileArea.isHidden = false
            dropCorrectImgLabel.isHidden = false
            uploadImaegTxtField.isHidden = true
            uploadBtn.isHidden = true
            uploadIcon.isHidden = true
            uploadAnswerLabel.isHidden = true
            uploadSubmitButton.isHidden = true
            dragAndDroplabel.isHidden = false
            puzzleView.isHidden = true
            createImageViews()
        }

        else if strBackBtnTag .isEqual(to: "submitBtnSubView1") {
             strBackBtnTag = "StartBtnContentView"
            strtBtnContentView.isHidden = false
            uploadBtn.isHidden = true
            uploadIcon.isHidden = true
            uploadAnswerLabel.isHidden = true
            uploadSubmitButton.isHidden = true
            puzzleView.isHidden = false
 
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == table_View || scrollView == scrollView {
            let
            a = scrollView.contentOffset
            if a.y <= 0 {
                scrollView.contentOffset = CGPoint.zero
                // this is to disable tableview bouncing at top.
            }
        }
        
    }
    @IBAction func openPaymentTypeView (sender:UIButton)
    {
        phoneNumTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        if paymentBtntag == 0
        {
            paymentTypeView.isHidden = false
            paymentBtntag = 1
            upDownArrowImgView.image = UIImage(named: "dropUp.png")
        }
        else
        {
            upDownArrowImgView.image = UIImage(named: "dropDown.png")
            paymentBtntag = 0
            paymentTypeView.isHidden = true
        }
        
    }
    @IBAction func uploadSubmitBtn (sender:UIButton)
    {
        submitBtnSubView3.isHidden = true
        submitBtnSubView2.isHidden = true
        submitBtnSubView1.isHidden = true
        dropCorrectImgLabel.isHidden = true
        if uploadSubmitBtnTag == 0 {
            strtBtnContentView.addSubview(submitBtnSubView1)
            uploadSubmitBtnTag = 1
            submitBtnSubView3.isHidden = true
            submitBtnSubView2.isHidden = true
            submitBtnSubView1.isHidden = false
            roundBtn1.backgroundColor = UIColor(red: 92/255, green: 10/255, blue: 99/255, alpha: 1)
            roundBtn2.backgroundColor = UIColor.black
            roundBtn3.backgroundColor = UIColor.black
            
            strBackBtnTag = "submitBtnSubView1"
        }
        else if uploadSubmitBtnTag == 1 {
            strtBtnContentView.addSubview(submitBtnSubView2)
            uploadSubmitBtnTag = 2
            submitBtnSubView3.isHidden = true
            submitBtnSubView2.isHidden = false
            submitBtnSubView1.isHidden = true
            roundBtn2.backgroundColor = UIColor(red: 92/255, green: 10/255, blue: 99/255, alpha: 1)
            roundBtn1.backgroundColor = UIColor.black
            roundBtn3.backgroundColor = UIColor.black
            strBackBtnTag = "submitBtnSubView2"
        }
        else if uploadSubmitBtnTag == 2 {
            strtBtnContentView.addSubview(submitBtnSubView3)
            submitBtnSubView2.isHidden = true
            submitBtnSubView1.isHidden = true
            uploadSubmitButton.isHidden = true
            submitBtnSubView3.isHidden = false
            
            roundBtn3.backgroundColor = UIColor(red: 92/255, green: 10/255, blue: 99/255, alpha: 1)
            roundBtn2.backgroundColor = UIColor.black
            roundBtn1.backgroundColor = UIColor.black
            strBackBtnTag = "submitBtnSubView3"
        }
        else {
            
        }
        
    }
    @IBAction func radioFirstBtn (sender:UIButton)
    {
        radioBtn1.setImage(UIImage(named: "selectedradio.png")!, for: UIControlState.normal)
        radioBtn2.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn3.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn4.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
    }
    @IBAction func radioSecondBtn (sender:UIButton)
    {
        radioBtn2.setImage(UIImage(named: "selectedradio.png")!, for: UIControlState.normal)
        radioBtn1.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn3.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn4.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
    }
    @IBAction func radioThirdBtn (sender:UIButton)
    {
        radioBtn3.setImage(UIImage(named: "selectedradio.png")!, for: UIControlState.normal)
        radioBtn1.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn2.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn4.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
    }
    @IBAction func radioFourthBtn (sender:UIButton)
    {
        radioBtn4.setImage(UIImage(named: "selectedradio.png")!, for: UIControlState.normal)
        radioBtn2.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn1.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
        radioBtn3.setImage(UIImage(named: "unselectedRadio.png")!, for: UIControlState.normal)
    }
    
    @IBAction func paypalAction (sender:UIButton)
    {
        couponCodeTxt.isHidden = true
      purchaseOrderBtn.frame.origin.y = 625
        
        numOfMemToJoinLbl.isHidden = false
        numOfMemToJoinTxt.isHidden = false
        paymentTxt.text = "Paypal"
        paymentBtntag = 0
        phoneNumTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        paymentTypeView.isHidden = true
        upDownArrowImgView.image = UIImage(named: "dropDown.png")
        teamLeaderPriLbl.isHidden = false
        checkImageView.isHidden = false
    }
    @IBAction func couponCodeAction (sender:UIButton)
    {
        numOfMemToJoinLbl.isHidden = true
        numOfMemToJoinTxt.isHidden = true
        couponCodeTxt.isUserInteractionEnabled = true
        couponCodeTxt.isEnabled = true
        purchaseOrderBtn.frame.origin.y = 625
        couponCodeTxt.isHidden = false
        phoneNumTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        paymentBtntag = 0
        paymentTxt.text = "Coupon Code"
        paymentTypeView.isHidden = true
        upDownArrowImgView.image = UIImage(named: "dropDown.png")
        
        paymentViaTextBtn.frame.origin.y = 420
        couponCodeTxt.frame.origin.y = paymentViaTextBtn.frame.origin.y+couponCodeTxt.frame.size.height+25
        purchaseOrderBtn.frame.origin.y =  couponCodeTxt.frame.origin.y+couponCodeTxt.frame.size.height+20
        teamLeaderPriLbl.isHidden = true
        checkImageView.isHidden = true
        couponCodeTxt.isHidden = false
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  // called when 'return' key pressed. return NO to ignore.
    {
        textFieldResponder()
        hideFunc()
        return true;
    }
    
    func textFieldResponder()
    {
        
        hideFunc()
    }
    func hideFunc()
    {
        uploadImaegTxtField.resignFirstResponder()
        firstNameTxt.resignFirstResponder()
        LastNameTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        phoneNumTxt.resignFirstResponder()
        paymentTxt.resignFirstResponder()
        couponCodeTxt.resignFirstResponder()
        countryTxt.resignFirstResponder()
    }
    func alertBar()
    {
        alertView = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        //alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        self.present(alertView, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            self.alertView.dismiss(animated: true, completion: nil)
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        uploadImaegTxtField.resignFirstResponder()
        firstNameTxt.resignFirstResponder()
        LastNameTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        phoneNumTxt.resignFirstResponder()
        paymentTxt.resignFirstResponder()
      
        scrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
    }
    func purchaseToursData()
    {
//        var firstName = String()
//         var LastName = String()
//         var email = String()
//         var phone = String()
//         var country = String()
        if let firstName = UserDefaults.standard.value(forKey: "firstname")as? String  {
        firstNameTxt.text = firstName
        }
        
        if let LastName = UserDefaults.standard.value(forKey: "lastname")as? String  {
            LastNameTxt.text = LastName
        }
        if let email = UserDefaults.standard.value(forKey: "email")as? String  {
            emailTxt.text = email
        }
        if let phone = UserDefaults.standard.value(forKey: "phone")as? String  {
            phoneNumTxt.text = phone
        }
        if let country = UserDefaults.standard.value(forKey: "country")as? String  {
            countryTxt.text = country
        }
        
//
//        let LastName = UserDefaults.standard.value(forKey: "lastname")as! String
//        LastNameTxt.text = LastName
        
//        let email = UserDefaults.standard.value(forKey: "email")as! String
//        emailTxt.text = email
        
//        let phone = UserDefaults.standard.value(forKey: "phone")as! String
//        phoneNumTxt.text = phone
//        let country = UserDefaults.standard.value(forKey: "country")as! String
//        countryTxt.text = country
        
        
    }
    func getCountriesData()
    {
       
        if Reachability.shared.isConnectedToNetwork(){
            
    
           SVProgressHUD.show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
            guard let url = URL(string: "http://beta.brstdev.com/tours/api/web/v1/tours/locationlist") else { return }
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                guard error == nil else { return }
                guard let data = data else { return }
                do {
                    if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? NSDictionary {
                        print ("Country data = \(jsonResponse)")
                        
                        self.progessDismiss()
                        if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                        {
                        let dataDic : NSDictionary = (jsonResponse as AnyObject).value(forKey: "data") as! NSDictionary
                        let strSuccess_code = dataDic ["Success_code"] as! NSString
                         
                          
                        if strSuccess_code .isEqual(to: "201")
                        {
                            let dicArrData :NSArray = dataDic.object(forKey: "location_list") as! NSArray
                            self.ArrCountries = NSMutableArray(array: dicArrData)
                            self.ArrCountries_And_Tours = NSMutableArray(array: self.ArrCountries)
                          //  SVProgressHUD .dismiss()
                            
                            
                            DispatchQueue.main.async {
                                self.table_View.reloadData()
                                
                           }
                            
                            
                            
                        }
                        }
                        else{
                            self.strAlertMsg = "Server Not Responding"
                            self.serverNotResponding()
                            
                        }
                    }
                } catch let error {
                    self.progessDismiss()
                    self.strAlertMsg = "Server Not Responding"
                    self.serverNotResponding()
                    print(error)
                }
                }.resume()
            
        //}
        
        }
        else
        {
            self.progessDismiss()
             self.notInterNetConn(message: "No internet connection")
        }
    }
    
    func getToursData(loaction_ID:NSNumber, completion:@escaping (_ result: NSArray?) -> Void)
    {

        if Reachability.shared.isConnectedToNetwork(){
            
        SVProgressHUD .show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
            let url = "http://beta.brstdev.com/tours/api/web/v1/tours/tourtypelist"
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let params:[String: AnyObject] = ["location_id" : loaction_ID as AnyObject]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code = \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                 self.progessDismiss()
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print ("Json Data is = \(jsonResponse)")
                        
                        self.progessDismiss()
                        if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                        {
                        let dataDic : NSDictionary = jsonResponse.value(forKey: "data") as! NSDictionary
                        print ("Tours data = \(dataDic)")
                        let strSuccess_code = dataDic ["Success_code"] as! NSString
                        let strDescription = dataDic ["description"] as! NSString
                          
                        if strSuccess_code .isEqual(to: "201") {
                            let dicArrData :NSArray = dataDic.object(forKey: "tourtype_list") as! NSArray
                            completion(dicArrData)
                        }
                        else if strSuccess_code .isEqual(to: "401") {
                            self.strAlertMsg = strDescription
                            self.alertBar()
                        }
                        else {
                            self.strAlertMsg = strDescription
                            self.alertBar()
                        }
                        }
                        else {
                            self.progessDismiss()
                            self.strAlertMsg = "Server Not Responding"
                            self.serverNotResponding()
                        }
                        
                    }catch _ {
                         self.progessDismiss()
                        print ("OOps not good JSON formatted response")
                    }
                }
            })
            task.resume()
        }catch let error {
             self.progessDismiss()
            self.strAlertMsg = "Server Not Responding"
            self.serverNotResponding()
            print(error)
            }
        }
            else {
                 self.progessDismiss()
               self.notInterNetConn(message: "No internet connection")
            }
    }
   
    func progessDismiss() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func progessShow() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func getTourSubTypesData(tourtype_id: NSNumber,  completion:@escaping (_ result: NSArray?) -> Void) {
        if Reachability.shared.isConnectedToNetwork(){

        SVProgressHUD .show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
            let url = "http://beta.brstdev.com/tours/api/web/v1/tours/toursubtypelist"
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let params:[String: AnyObject] = ["tourtype_id" : tourtype_id as AnyObject]
            
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                    if let response = response {
                        let nsHTTPResponse = response as! HTTPURLResponse
                        let statusCode = nsHTTPResponse.statusCode
                        print ("status code = \(statusCode)")
                    }
                    if let error = error {
                        print ("\(error)")
                    }
                    if let data = data {
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            print ("getTourSubTypesData = \(jsonResponse)")
                            
                            self.progessDismiss()
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                            let dataDic : NSDictionary = jsonResponse.value(forKey: "data") as! NSDictionary
                            let strSuccess_code = dataDic ["Success_code"] as! NSString
                             let strDescription = dataDic ["description"] as! String
                            
                            if strSuccess_code .isEqual(to: "201")
                            {
                            let dicArrData :NSArray = dataDic.object(forKey: "toursubtype_list") as! NSArray
                              
                                completion(dicArrData)
                            }
                            else
                            {
                                self.notInterNetConn(message: strDescription)
                            }
                            }
                            else
                            {
                                self.strAlertMsg = "Server Not Responding"
                                self.serverNotResponding()
                            }
                            
                            
                            
                        }catch _ {
                            print ("OOps not good JSON formatted response")
                        }
                    }
                })
                task.resume()
            }catch let error {
                self.strAlertMsg = "Server Not Responding"
                self.serverNotResponding()
                print(error)
            }
        }
        else
        {
            self.notInterNetConn(message: "No internet connection")
        }
    }
    //Get Tour List
    func getTourListData(location_id: NSNumber,tourtype_id: NSNumber,toursubtype_id: NSNumber,  completion:@escaping (_ result: NSArray?) -> Void)
    {
        if Reachability.shared.isConnectedToNetwork(){
       
        SVProgressHUD .show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
            let url = "http://beta.brstdev.com/tours/api/web/v1/tours/tourlist"
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let params:[String: AnyObject] = ["location_id" : location_id as AnyObject,"tourtype_id" : tourtype_id as AnyObject,"toursubtype_id" : toursubtype_id as AnyObject]
            
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                    if let response = response {
                        let nsHTTPResponse = response as! HTTPURLResponse
                        let statusCode = nsHTTPResponse.statusCode
                        print ("status code = \(statusCode)")
                    }
                    if let error = error {
                        print ("\(error)")
                    }
                    if let data = data {
                        do {
                            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            print ("getTourListData = \(jsonResponse)")
                            self.progessDismiss()
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                            let dataDic : NSDictionary = jsonResponse.value(forKey: "data") as! NSDictionary
                            let strSuccess_code = dataDic ["Success_code"] as! NSString
                             let strDescription = dataDic ["description"] as! NSString

                            if strSuccess_code .isEqual(to: "201")
                            {
                                let dicArrData :NSArray = dataDic.object(forKey: "tour_list") as! NSArray
                                completion(dicArrData)
                               
                                
                            }
                            else
                            {
                                self.strAlertMsg = strDescription
                                self.alertBar()
                            }
                            
                            }
                            else
                            {
                                self.strAlertMsg = "Server Not Responding"
                                self.serverNotResponding()
                            }
                        }catch _ {
                            self.progessDismiss()
                            print ("OOps not good JSON formatted response")
                        }
                    }
                })
                task.resume()
            }catch let error {
                self.progessDismiss()
                self.strAlertMsg = "Server Not Responding"
                self.serverNotResponding()
                print(error)
            }
       // }
        }
        else
        {
            self.progessDismiss()
             self.notInterNetConn(message: "No internet connection")
        }
    }
    func paypal()
    {
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        let item1 = PayPalItem(name: "Tour", withQuantity: 1, withPrice: NSDecimalNumber(string: priceOfTour as String), withCurrency: "EUR", withSku: "")
        
        print("The Items Are\(item1)")
        //        let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
        //        let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.0")
        let tax = NSDecimalNumber(string: "0.0")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "EUR", shortDescription: "Price", intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
            
            
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
    }
    
    func purchaseTour()
    {
        if Reachability.shared.isConnectedToNetwork(){
       
        self.tour_price = UserDefaults.standard.value(forKey: "tour_price") as! NSString
        let tour_Id = UserDefaults.standard.value(forKey: "tour_id")
        var user_ID = NSString()
        
        user_ID =  UserDefaults.standard.value(forKey: "user_id") as! NSString
        SVProgressHUD .show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
        // DispatchQueue.global(qos: .background).async {
        let url = "http://beta.brstdev.com/tours/api/web/v1/tours/tourpurchase"
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
            
            var params = [String: AnyObject]()
            let paymenttype = self.paymentTxt.text! as String
            
            let strpaymenttype = paymenttype as NSString
            
            
            if strpaymenttype .isEqual(to: "Paypal")
            {
                  params = ["firstname" : self.firstNameTxt.text as AnyObject,"lastname" : self.LastNameTxt.text as AnyObject,"email" : self.emailTxt.text as AnyObject,"phone" : self.phoneNumTxt.text as AnyObject,"payment_type" : "paypal" as AnyObject,"location_id" : self.strLocationID as AnyObject,"tourtype_id": self.strTourTypeID as AnyObject,"toursubtype_id": self.strtourSubTypeID as AnyObject,"tour_id" : tour_Id as AnyObject,"txn_id" : self.txnID as AnyObject,"coupon_code" : self.couponCodeTxt.text as AnyObject,"tour_price" :  self.tour_price as AnyObject,"user_id" : user_ID as AnyObject,"participant": self.numOfMemToJoinTxt.text as AnyObject]
            }
            else
            {
                let couponcode = self.couponCodeTxt.text! as String
                
                let strCop = couponcode as NSString
                  params = ["firstname" : self.firstNameTxt.text as AnyObject,"lastname" : self.LastNameTxt.text as AnyObject,"email" : self.emailTxt.text as AnyObject,"phone" : self.phoneNumTxt.text as AnyObject,"payment_type" : "coupon" as AnyObject,"location_id" : self.strLocationID as AnyObject,"tourtype_id": self.strTourTypeID as AnyObject,"toursubtype_id": self.strtourSubTypeID as AnyObject,"tour_id" : tour_Id as AnyObject,"txn_id" : self.txnID as AnyObject,"coupon_code" : strCop as NSString,"tour_price" :  self.tour_price as AnyObject,"user_id" : user_ID as AnyObject]
            }
          
            
        print("The parm Are \(params)")
        
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code = \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print ("purchaseTour() = \(jsonResponse)")
                        
                        var dataDic = NSDictionary()
                        
                         var strSuccess_code = NSString()
                         var strDescription = NSString()
                        
                        if let strSuccess_code1 = jsonResponse ["status_code"] as? NSString {
                             strSuccess_code = strSuccess_code1
                        }
                        
                         self.paymentViaTextBtn.isEnabled = true
                        if let strDesc = jsonResponse ["description"] as? NSString {
                            strDescription = strDesc
                        }
                       self.progessDismiss()
                        if strSuccess_code .isEqual(to: "400")
                        {
                              self.tourNotPurchased = ""
                            self.strAlertMsg = strDescription
                            self.PurchaseAlertBar()
                        }
                        else
                        {
                            self.mapCheck = ""
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                            dataDic = jsonResponse.value(forKey: "data") as! NSDictionary
                            print ("Profile Data = \(dataDic)")
                            strSuccess_code = dataDic ["Success_code"] as! NSString
                            self.groupNameStr = dataDic ["group_name"] as! NSString
                            // let group_id = dataDic.value(forKey: "group_id") as! Int
                            
                            
                            let group_id = dataDic.value(forKey: "group_id") as! Int
                            self.groupIDStr = String(group_id) as NSString
                            
                                if self.numOfMemToJoinTxt.text == "0" {
                                    self.strAlertMsg = "Congratulations, you have successfully purchased the tour. Now you can start"
                                }
                                else {
                                    self.strAlertMsg = "\(strDescription) \("Group Code:") \(self.groupNameStr)" as NSString
                                }
                            print(  self.strAlertMsg)
                            self.progessDismiss()
                            if strSuccess_code .isEqual(to: "201")
                            {
                                self.tourPurchased = "purchased"
                               // self.tourPursedOrNot = false
                                self.tourPursedOrNot = true
                                self.tourNotPurchased = ""
                                self.arrGroupName .add( self.groupNameStr)
                                let defaults = UserDefaults.standard
                                defaults.set(self.arrGroupName, forKey: "SavedGroupName")
                                
                                self.paymentSuccessAlertView()
                            }
                                
                            else
                            {
                                self.strAlertMsg = strDescription
                                self.PurchaseAlertBar()
                        
                            }
                                
                        }
                            else
                            {
                                self.strAlertMsg = "Server Not Responding"
                                self.serverNotResponding()
                            }
                        
                      
                        }
                        
                    }catch _ {
                         self.progessDismiss()
                        print ("OOps not good JSON formatted response")
                    }
                }
            })
            task.resume()
        }catch let error {
            self.strAlertMsg = "Server Not Responding"
            self.serverNotResponding()
            print(error)
            }
       // }
        }
        else
        {
            self.progessDismiss()
            self.notInterNetConn(message: "No internet connection")
        }
        
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        print(emailTest)
        return emailTest.evaluate(with: testStr)
        
    }
   
    func PurchaseAlertBar()
    {
        // the alert view
        let alert = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil)
        }
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func paymentSuccessAlertView()
    {
        paymentSuccessAlert = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        if tourPursedOrNot == false
        {
            if  mapCheck .isEqual(to: "map")
            {
                
            }
            else
            {
    
            }
        }
        
        if  mapCheck .isEqual(to: "map")
        {
            
        }
        else
        {
            if tourPurchased .isEqual(to: "purchased")
            {
                
            }
            
        }
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.PaymentOkAction()
        }
        
        paymentSuccessAlert.addAction(okBtn)
       // DispatchQueue.main.async {
        self.present(self.paymentSuccessAlert, animated: true, completion: nil)
        //}
        
    }
    func PaymentOkAction()
    {
        

        if tourPurchased .isEqual(to: "purchased")
        {
            self.purchasedTourOrNot(completion:{ (result) in
                
            })
        }
        tourPurchased = ""
        if tourPursedOrNot == false
        {
          
            if  mapCheck .isEqual(to: "map")
            {
            }
            else
            {
                UserDefaults.standard.set("HomeScreen", forKey: "PuzzleScreen")
                if startChk .isEqual(to: "purchaseTourBtn")
                {
                    
                }
                else
                {
                //self.moveToClass()
                }
                collectionView.isHidden = true
                table_View.isHidden = true
                scrollView.isHidden = false
            }
            
        }
        else
        {
           
            scrollContentView.isHidden = false
            strtBtnContentView.isHidden = true
            PurcOrderContentView.isHidden = true
            MapContentView.isHidden = true
            
            self.ArrTours = NSMutableArray(array: self.showDataArr)
            
            // print("The Default data is\( self.ArrTours)")
            strBackBtnTag = "homeScrenSecSubView2"
            
            
            DispatchQueue.main.async {
                self.paymentSuccessAlert.dismiss(animated: true, completion: nil)
            }
        }
    //}
    }
    func moveToClass()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
      
            let nextViewController = storyBoard.instantiateViewController(withIdentifier:
                "QuesAnsViewController") as! QuesAnsViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    func cancelPaymentAlert()
    {
        paymentSuccessAlert.dismiss(animated: true, completion: nil)
    }
    func profileAlertbar()
    {
        profielAlertView = UIAlertController(title: nil, message:strAlertMsg as String , preferredStyle: UIAlertControllerStyle.alert)
        let canclBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.cancelAction()
        }
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.profileOkAction()
        }
        profielAlertView.addAction(canclBtn)
        profielAlertView.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(self.profielAlertView, animated: true, completion: nil)
        }
        
        
        
    }
    func profileOkAction()
    {
         scrollContentView.isHidden = false
        let  createCheck = "CreateProfile"
        UserDefaults.standard.set(createCheck, forKey: "CreateProfile")
        self.tabBarController?.selectedIndex = 4
    }
    
    func cancelAction()
    {
        profielAlertView.dismiss(animated: true, completion: nil)
    }
    func notInterNetConn(message: String)
    {
        notInterNetAlert = UIAlertController(title: nil, message: message as String?, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.notConnOkAction()
        }
        notInterNetAlert.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(self.notInterNetAlert, animated: true, completion: nil)
        }
        
        
        
    }
    func notConnOkAction()
    {
        notInterNetAlert.dismiss(animated: true, completion: nil)
    }
    /// listen to keyboard event
    func makeAwareOfKeyboard() {
       // NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        
       // NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidHide), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    /// move scroll view up
    func keyBoardDidShow(notification: Notification) {
        let info = notification.userInfo as NSDictionary?
        let rectValue = info![UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let kbSize = rectValue.cgRectValue.size
        
        let contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0)
        self.scrollView?.contentInset = contentInsets
        self.scrollView?.scrollIndicatorInsets = contentInsets
        
    }
    
    /// move scrollview back down
    func keyBoardDidHide(notification: Notification) {
        // restore content inset to 0
        let contentInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        self.scrollView?.contentInset = contentInsets
        self.scrollView?.scrollIndicatorInsets = contentInsets
    }
     

    func purchasedTourOrNot(completion: @escaping(_ result: Bool)-> Void)
    {
         if Reachability.shared.isConnectedToNetwork(){
        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
        var userID = NSString()
         var tourID = Int()
        if (UserDefaults.standard.value(forKey: "user_id") != nil)
        {
            userID =  (UserDefaults.standard.value(forKey: "user_id") as? NSString)!
        }
       if (UserDefaults.standard.value(forKey: "tour_id") != nil)
        {
            tourID =  (UserDefaults.standard.value(forKey: "tour_id") as? Int)!
        }
       let tour_id = String(tourID)

        
        SVProgressHUD .show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
    //    DispatchQueue.global(qos: .background).async {
            let url = "http://beta.brstdev.com/tours/api/web/v1/tours/purchasedOrNot"
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            var groupName = String()
            if UserDefaults.standard.object(forKey: "GroupName") != nil
            {
                groupName = UserDefaults.standard.object(forKey: "GroupName") as! String
            }
            let params:[String: String] = ["user_id" : userID as String ,"tour_id" : tour_id as String,"group_name" : groupName as String]
            print(params)
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                    if let response = response {
                        let nsHTTPResponse = response as! HTTPURLResponse
                        let statusCode = nsHTTPResponse.statusCode
                        print ("status code = \(statusCode)")
                    }
                    if let error = error {
                        print ("\(error)")
                    }
                    if let data = data {
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            print ("Json Data is = \(jsonResponse)")
                            
                            var dataDic = NSDictionary()
                            
                            var strSuccess_code = jsonResponse ["status_code"] as! NSString
                            
                            let strDescription = jsonResponse ["description"] as! NSString
                            
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                                dataDic = jsonResponse.value(forKey: "data") as! NSDictionary
                                print ("Purchase tour List = \(dataDic)")
                                strSuccess_code = dataDic ["Success_code"] as! NSString
                                self.tourPursedOrNot = false
                                self.isOwner = false
                                self.isOwner = false
                                self.is_Joiner = false
                                self.isCompletedTour = false
                                
                                if dataDic["is_joiner"] != nil {
                                self.is_Joiner = dataDic.object(forKey: "is_joiner") as! Bool
                                }
                                
                                if dataDic["is_owner"] != nil {
                                    self.isOwner = dataDic.object(forKey: "is_owner") as! Bool
                                }
                                
                                if  dataDic ["is_complete"] as? Bool != nil {
                                    self.isCompletedTour = (dataDic ["is_complete"] as? Bool)!
                                }
                                if  dataDic ["tour_purchased"] as? String != nil {
                                    let purc = dataDic ["tour_purchased"] as? String
                                    if purc == "Yes" {
                                        self.tourPursedOrNot = true
                                    }
                                }
                                completion(true)
                            if strSuccess_code .isEqual(to: "201")
                            {
//
//                                if self.is_Owner == true
//                                {
//                                    self.tourPursedOrNot = true
//                                    self.isOwner = true
//                                }
//                                else
//                                {
//                                    self.tourPursedOrNot = false
//                                    self.isOwner = false
//
//                                }
                                
                            }
                            if strSuccess_code .isEqual(to: "401")
                            {
                                
                                if strDescription .isEqual(to: "Tour Not Purchased")
                                {
                                    self.strAlertMsg = "Please purchase tour first to use this feature"
                                    self.tourPursedOrNot = false
                                    self.isOwner = false
                                }
                                
                                completion(false)
                            }
                            self.progessDismiss()
                            
                            
                        
                            
                            completion(true)
                            //SVProgressHUD.dismiss()
                            
                            }
                            else
                            {
                                completion(false)
                                self.strAlertMsg = "Server Not Responding"
                                self.serverNotResponding()
                            }
                        }catch _ {
                            print ("OOps not good JSON formatted response")
                        }
                    }
                })
                task.resume()
            }catch let error {
                self.progessDismiss()
                self.strAlertMsg = "Server Not Responding"
                self.serverNotResponding()
                print(error)
            }
            
            //}
        }
        else
        {
            self.progessDismiss()
        }
        }
        else
         {
            self.progessDismiss()
            self.notInterNetConn(message: "No internet connection")
        }
    }
    func serverNotResponding()
    {
        self.progessDismiss()
        alertView = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertView, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            self.alertView.dismiss(animated: true, completion: nil)
        }
    }
}


