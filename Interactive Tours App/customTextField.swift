//
//  customTextField.swift
//  Interactive Tours App
//
//  Created by Brst on 02/08/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class customTextField: UITextField {

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        if action == #selector(UIResponderStandardEditActions.copy(_:) ) {
            return false
        }
        
        return super.canPerformAction(action, withSender: sender)
    }
    
    
}
