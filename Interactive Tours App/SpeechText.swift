import AVFoundation

// You can use an enum so you don't have to manually type out character strings. Look them up once and stick them in an enum. From there, you set the language with your enum rather than typing out the string.
enum Language: String {
    case english = "en-US"
    case arabic = "ar-SA"
}


class SpeechText: NSObject {
    
    let synth = AVSpeechSynthesizer()
    static let shared  = SpeechText()
    override init() {
        super.init()
    
        synth.delegate = (self as AVSpeechSynthesizerDelegate)
    }
    
    func speak(_ announcement: String, in language: String) {
        print("speak announcement in language \(language) called")
        prepareAudioSession()
        let utterance = AVSpeechUtterance(string: announcement.lowercased())
        utterance.voice = AVSpeechSynthesisVoice(language: language)
        utterance.rate = AVSpeechUtteranceMaximumSpeechRate / 2.5
        synth.speak(utterance)
    }
    
    private func prepareAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient, with: .mixWithOthers)
        } catch {
            print(error)
        }
        
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
    }
    
    func stop() {
        //if synth.isSpeaking {
            synth.stopSpeaking(at: .immediate)
       // }
    }
    func continueSpeaking() {
            synth.continueSpeaking()
        
    }
}

extension SpeechText: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        print("Speaker class started")
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        print("Speaker class finished")
    }
}
