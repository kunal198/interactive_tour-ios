//
//  QuesAnsViewController.swift
//  Interactive Tours App
//
//  Created by Brst on 17/07/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import Speech
import GoogleMaps
import Photos
import  AVFoundation
class QuesAnsViewController: UIViewController,UITextFieldDelegate,UIScrollViewDelegate,UIWebViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate,PuzzleSolvedProtocol,ISSpeechRecognitionDelegate,AVAudioPlayerDelegate {
    
    
    var recognition: ISSpeechRecognition?
    @IBOutlet weak var quesLabel = UILabel()
    @IBOutlet weak var  contentLabel = UILabel()
     var matchDragDrop_point = NSString()
    var option1Btn = UIButton()
    var option2Btn = UIButton()
    var option3Btn = UIButton()
    var option4Btn = UIButton()
    var submitBtn = UIButton()
    var nextButton = UIButton()
    var posOnSubmit = NSString()
    var backButton = UIButton()
    var uploadBtn = UIButton()
    var correctIncorrect = Int()
    var pageItem = Int()
    var firstTime = NSInteger()
    var pageUiView = UIView()
    var strQuesCheck = NSString()
    var audioUrlString = NSString()
    var arrayForPositionsOfImageOptions = NSMutableArray()
    @IBOutlet weak var scroll_View = UIScrollView()
    @IBOutlet weak var congratsScrollView = UIScrollView()
    @IBOutlet weak var scrollContentView = UIView()
    @IBOutlet weak var congratsContentView = UIView()
    
    @IBOutlet weak var itemsContentView = UIView()
    var answerLabel = UILabel()
    @IBOutlet weak var imageView = UIImageView()
    
    var  answerTxt = UITextField()
    @IBOutlet weak var  correctLabel = UILabel()
    @IBOutlet weak var  inCorrectLabel = UILabel()
    @IBOutlet weak var  scroedLabel = UILabel()
    @IBOutlet weak var  welcomeView = UILabel()
    @IBOutlet weak var  toursNameLbl = UILabel()
    @IBOutlet weak var  welcomeNextBtn = UIButton()
    @IBOutlet weak var  welcomlogoImage = UIImageView()
    @IBOutlet weak var  backView = UIView()
    
    @IBOutlet weak var  contentWebView = UIWebView()
    @IBOutlet weak var  quesWebView = UIWebView()
    @IBOutlet weak var  doneBtnBckView = UIWebView()
    var question_type = NSString()
    var backTag = NSString()
    var bacNxt = Int()
    var answer = NSString()
    var uploadImage = UIImage()
    @IBOutlet weak var  slideUiView = UIView()
    @IBOutlet weak var  congratsPlayBtn = UIButton()
    
    @IBOutlet weak var questionLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var quesWebViewYPos: NSLayoutConstraint!
    @IBOutlet weak var quesWebVHeight: NSLayoutConstraint!
    @IBOutlet weak var contentWebVHeight: NSLayoutConstraint!
    @IBOutlet weak var itemsViewYPos: NSLayoutConstraint!
    @IBOutlet weak var welcomNxtYPos: NSLayoutConstraint!
    @IBOutlet weak var welcomDescHeight: NSLayoutConstraint!
    @IBOutlet weak var welcomLogYPos: NSLayoutConstraint!
    @IBOutlet weak var contentLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var congratScrollHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollViewYPos: NSLayoutConstraint!
    
    
    
    
    @IBOutlet weak var slideViewheight: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var contentViewYPos: NSLayoutConstraint!
    @IBOutlet weak var puzzleViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tileViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tileImgViewHeight: NSLayoutConstraint!
    var slidingScrollView: UIScrollView!
    var pageControl: UIPageControl!
    @IBOutlet weak var congratsBackBtn = UIButton()
    @IBOutlet weak var scrollYPos: NSLayoutConstraint!
    var strAlertMsg = String()
    var serVerAns = Int()
    var isOwner = NSString()
    var btnTag = Int()
    var correctAns = Int()
    var wrongAns = Int()
    var selectOption = Int()
    var submitCheck = Int()
    var answerIndex = Int()
    var optionID1 = Int()
    var optionID2 = Int()
    var optionID3 = Int()
    var optionID4 = Int()
    var labelHeight = CGFloat()
    
    var soundBtnTag = Int()
    var flag = Bool()
    var arrQuesAndAnswer = NSMutableArray()
    var arrData = NSMutableArray()
    var arrData2 = NSMutableArray()
    var resultAlert = UIAlertController()
    var correctAnswer = NSString()
    var selectedBtnTitle = NSString()
    var backNextTag = NSString()
    var ID = Int()
    var buttonsView: UIView!
    var getAnswer = String()
    var arrID = NSMutableArray()
    var model = NSString()
    var notInterNetAlert = UIAlertController()
    var activeField: UITextField?
    var macButton = UIButton()
    var macButtonTitle = UILabel()
    var playAudioBtn = UIButton()
    @IBOutlet weak var congratsSubScroll = UIScrollView()
    var tourID = Int()
    
    var firstX: CGFloat = 0.0
    var firstY: CGFloat = 0.0
    
    var dropItemFirstXPos: CGFloat = 0.0
    var dropItemFirstYPos: CGFloat = 0.0
    
    var dropItemSecXPos: CGFloat = 0.0
    var dropItemSecYPos: CGFloat = 0.0
    
    var dropItemThirdXPos: CGFloat = 0.0
    var dropItemThirdYPos: CGFloat = 0.0
    
    
    var optionImageView = UIImageView()
    var dragDropImgView = UIImageView()
    var dragDropBckView = UIView()
    var panRecognizer = UIPanGestureRecognizer()
    var dragDropAnsewr = Int()
    var imgIDArr = NSMutableArray()
    var optionsArr = NSArray()
    var dragDropimgOptionsID = Int()
    var avPlayerTag = Int()
    var avPlayer = AVPlayer()
    
    
    //puzzle process
    
    let userDefaults = UserDefaults.standard
    @IBOutlet weak var puzzleView: UIView!
    @IBOutlet weak var originalImageView: UIImageView!
    var currentImagePackage : ImagePackage?
    @IBOutlet weak var tileArea:  TileAreaView!
    var congratsMessages : [String]!
    var tilesPerRow = 3
    var originalImageShown = false
    // MARK: VARS
    let colorPalette = ColorPalette()
    var topGrips = [UIImageView]()
    var leftGrips = [UIImageView]()
    var rowColumnGrip : UIImageView?
    var firstGripOriginalFrame : CGRect?
    var firstLineOfTiles: [Tile]?
    
    @IBOutlet weak var congratsMessage: UILabel!
    @IBOutlet weak var topBank: UIView!
    @IBOutlet weak var leftBank: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var imageCaptionLabel: UILabel!
    var curr_imageView = UIImageView()
    var alertView  = UIAlertController()
    var Droplabel = UILabel()
    var playAudioCheck = Int()
    var alertLogoImgView = UIImageView()
    var audioCount = Int()
    @IBOutlet weak var headCapImgView = UIImageView()
    @IBOutlet weak var headImgView = UIImageView()
    @IBOutlet weak var bodyImgView = UIImageView()
    @IBOutlet weak var leftLegImgView = UIImageView()
    @IBOutlet weak var rightLegImgView = UIImageView()
    @IBOutlet weak var leftArmsImgView = UIImageView()
    @IBOutlet weak var rightArmsImgView = UIImageView()
    
    
    @IBOutlet weak var makeCartoonView = UIView()
    var arrMatch = NSMutableArray()
    var audioUrlArr = NSMutableArray()
    var contentVal = NSString()
    var txtFieldPosition = CGFloat()
    var notiPlayAudio = Int()
    var showCorrImagView = UIImageView()
    var showCorrImgLabel = UILabel()
    var JoinGroupCheck = NSString()
    @IBOutlet weak var congratsScreenLogo = UIImageView()
    var openMapBtn = UIButton()
    var checkUnchekImg = UIImageView()
    
    var userID = String()
    var locationManager = CLLocationManager()
    
    var uploadPicLat = String()
    var uploadPicLong = String()
    
    var currentLocLat = CLLocation()
    var currentLocLong = CLLocation()
    
    
    var latituteArr = NSArray()
    var longituteArr = NSArray()
    var toursNameArr = NSArray()
    var saveTourDetailArr = NSMutableArray()
    var groupCodeBtn = UIButton()
    var groupName = String()
    var sizeFont = CGFloat()
    var CurrentLocLatVal = Double()
    var CurrentLocLangVal = Double()
    
    var groupCodeIcon = UIImageView()
    var groupCodeImgView : UIImageView!
    @IBOutlet weak var unScrambleImgView : UIImageView!
    var titleLabel  = UILabel()
    var congPlayCheck  = Bool()
    var congInitialAudioChk  = Bool()
    var player: AVAudioPlayer?
    var playerItem: AVPlayerItem?
    var kbHeight: CGFloat!
    override func viewDidLoad() {
        
        super.viewDidLoad()
         IQKeyboardManager.shared().isEnabled = false
        
        congPlayCheck = false
        unScrambleImgView.isHidden = true
        //Get Current location's lang,lat
        initialLocMethod()
        let modelName = UIDevice.current.model
        model = modelName as NSString
        
        groupCodeBtn = UIButton.init(frame: CGRect(x: self.view.frame.size.width-100, y: 12, width: 90, height: 30))
        groupCodeBtn.setTitle("Group Code", for: .normal)
        groupCodeBtn.titleLabel?.lineBreakMode = .byWordWrapping
        groupCodeBtn.addTarget(self, action: #selector(self.groupCodeBtnAction(_:)), for: .touchUpInside)
        groupCodeBtn.titleLabel?.font = UIFont .systemFont(ofSize: 13)
        groupCodeBtn.setTitleColor(.black, for: .normal)
        self.navigationController?.navigationBar .addSubview(groupCodeBtn)
        
        
        groupCodeIcon = UIImageView(frame: CGRect(x: self.view.frame.size.width-65 , y: -3, width: 22, height: 22))
        groupCodeIcon.contentMode = .scaleAspectFit
        groupCodeIcon.image = #imageLiteral(resourceName: "JoinGroupIcon")
        self.navigationController?.navigationBar .addSubview(groupCodeIcon)
        
        groupCodeBtn.isHidden = true
        groupCodeIcon.isHidden = true
        
        
        
        groupCodeImgView = UIImageView(frame: CGRect(x: self.view.frame.size.width-93 , y: -3, width: 100, height: 40))
        groupCodeImgView.image = UIImage(named:"greenBlank.png")
        self.navigationController?.navigationBar .addSubview(groupCodeImgView)
        groupCodeImgView.isHidden = true
   
        //Hide Tab Bar and Showing Navigation Bar
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        
        
        UserDefaults.standard.removeObject(forKey: "HomeView")
        UserDefaults.standard.removeObject(forKey: "TourView")
        bacNxt = 0
        notiPlayAudio = 0
        playAudioCheck = 0
        headCapImgView?.isHidden = true
        headImgView?.isHidden = true
        bodyImgView?.isHidden = true
        leftLegImgView?.isHidden = true
        rightLegImgView?.isHidden = true
        makeCartoonView?.isHidden = true
        leftArmsImgView?.isHidden = true
        rightArmsImgView?.isHidden = true
        soundBtnTag = 0
        avPlayerTag = 0
        puzzleView.isHidden = true
      
        if UserDefaults.standard.value(forKey: "tour_name") != nil {
        title =  UserDefaults.standard.object(forKey: "tour_name") as? String
        }
        if model .isEqual(to: "iPhone") {
            if self.view.frame.size.height < 600 {
                sizeFont = 12
            }
            else {
                sizeFont = 15
            }
        }
        else {
            sizeFont = 21
        }
        //Create uilabel to set navigation bar title
        
        
        self.navigationController!.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont .boldSystemFont(ofSize: sizeFont),NSForegroundColorAttributeName : UIColor.black];
        
        if (UserDefaults.standard.value(forKey: "user_id") != nil)
        {
            userID =  (UserDefaults.standard.value(forKey: "user_id"))! as! String
        }
        
        
        dragDropImgView = UIImageView(frame:CGRect(x:(self.view.frame.size.width/2)-90, y:500, width:180, height:180))
        itemsContentView?.addSubview(dragDropImgView)

        let sizeAudio = self.view.frame.size.width
        let pos = self.view.frame.size.width/2
        
        
        if model .isEqual(to: "iPhone")
        {
            playAudioBtn = UIButton(frame: CGRect(x:sizeAudio-50, y:300,width: 40, height: 40))
            pageUiView = UIView(frame: CGRect(x: pos-75, y: 200 , width: 150, height: 50))
        }
        else{
            playAudioBtn = UIButton(frame: CGRect(x:sizeAudio-60, y:660,width: 40, height: 40))
            pageUiView = UIView(frame: CGRect(x: pos-75, y: 500 , width: 150, height: 50))
        }
        playAudioBtn.setImage(UIImage(named: "audioPlay.png"), for: UIControlState.normal)
        playAudioBtn.addTarget(self, action:#selector(playAudioAction(sender:)), for: .touchUpInside)
        scroll_View?.addSubview(playAudioBtn)
        playAudioBtn.backgroundColor = UIColor .clear
        playAudioBtn.isHidden = true
        pageUiView.backgroundColor = UIColor .clear
        pageControl = UIPageControl()
        pageControl.frame = CGRect(x: 0, y: 0, width: 150, height: 50)
        let size = self.view.frame.size.width/2
        
        
        macButton = UIButton(frame: CGRect(x:size-25, y: self.view.frame.size.height/2,width: 50, height: 50))
        
        macButton.setImage(UIImage(named: "mic.png"), for: UIControlState.normal)
        macButton.addTarget(self, action:#selector(macButtonAction(sender:)), for: .touchUpInside)
        itemsContentView?.addSubview(macButton)
        macButton.isHidden = false
        
        
        if model .isEqual(to: "iPhone")
        {
            
            macButtonTitle = UILabel(frame: CGRect(x:size-150, y: (self.view.frame.size.height/2)+80,width: 300, height: 50))
        }
        else{
            slideViewheight.constant = 550
            imageViewHeight.constant = 550
            scrollViewYPos.constant = 565
            backeViewHeight.constant = 552
            puzzleViewHeight.constant = 700
            tileViewHeight.constant = 700
            tileImgViewHeight.constant = 700
            macButtonTitle = UILabel(frame: CGRect(x:size-200, y: (self.view.frame.size.height/2)+80,width: 400, height: 50))
        }
        macButtonTitle.textAlignment = .center
        macButtonTitle.font = UIFont .systemFont(ofSize: 17)
        macButtonTitle.numberOfLines = 2
        
        macButtonTitle.text = "press the mic to answer the above question"
        itemsContentView?.addSubview(macButtonTitle)
        macButtonTitle.isHidden = true
        firstTime = 0
        getAnswer = "getAnswer"
        self.registerForKeyboardNotifications()
        doneBtnBckView?.isHidden = false
        quesWebView?.delegate = self
        contentWebView?.delegate = self
        contentWebView?.isOpaque = false
        quesWebView?.isOpaque = false
        contentWebView?.scrollView.delegate = self
        quesWebView?.scrollView.delegate = self
        congratsScrollView?.delegate = self
        contentWebView?.isUserInteractionEnabled = false
        quesWebView?.isUserInteractionEnabled = false
        contentWebView?.backgroundColor = UIColor .clear
        quesWebView?.backgroundColor = UIColor .clear
        submitCheck = 0
        
        
        welcomeNextBtn?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        self.congratScrollHeight.constant = 500
        navigationItem.setLeftBarButtonItems(nil, animated: true)
        navigationItem.setRightBarButtonItems(nil, animated: true)
        quesWebView?.isHidden = true
        contentWebView?.isHidden = true
        
        congratsBackBtn?.layer.cornerRadius = 20
        answerTxt = UITextField(frame: CGRect(x:self.view.frame.origin.x + 10, y:83, width:self.view.frame.size.width - 20,height: 50))
        answerTxt.placeholder = "Answer"
        answerTxt.font = UIFont.systemFont(ofSize: 18)
        itemsContentView?.addSubview(answerTxt)
        
        answerTxt.isHidden = true
        welcomeView?.isHidden = true
        congratsScrollView?.isHidden = true
        itemsContentView?.isHidden = true
        slideUiView?.isHidden = true
        
        
        answerTxt.delegate = self
        answerLabel = UILabel(frame: CGRect(x: 10, y: 133, width: self.view.frame.size.width-20, height: 45))
        answerLabel.numberOfLines = 3
        answerLabel.textAlignment = .center
        answerLabel.text = ""
        itemsContentView?.addSubview(answerLabel)
        
        
        option1Btn = UIButton(frame: CGRect(x: 10, y: (quesWebView?.frame.origin.y)!, width: self.view.frame.size.width-20, height: 55))
        option1Btn.setTitle("option1Btn", for: .normal)
        option1Btn.addTarget(self, action:#selector(option1Action(sender:)), for: .touchUpInside)
        itemsContentView?.addSubview(option1Btn)
        
        option2Btn = UIButton(frame: CGRect(x: 10, y: option1Btn.frame.origin.y+option1Btn.frame.size.height+10, width: self.view.frame.size.width-20, height: 55))
        
        option2Btn.setTitle("option2Btn", for: .normal)
        option2Btn.addTarget(self, action:#selector(option2Action(sender:)), for: .touchUpInside)
        itemsContentView?.addSubview(option2Btn)
        
        option3Btn = UIButton(frame: CGRect(x: 10, y: option2Btn.frame.origin.y+option2Btn.frame.size.height+10, width: self.view.frame.size.width-20, height: 55))
        option3Btn.setTitle("option3Btn", for: .normal)
        option3Btn.addTarget(self, action:#selector(option3Action(sender:)), for: .touchUpInside)
        itemsContentView?.addSubview(option3Btn)
        
        option4Btn = UIButton(frame: CGRect(x: 10, y: option3Btn.frame.origin.y+option3Btn.frame.size.height+10, width: self.view.frame.size.width-20, height: 55))
        option4Btn.setTitle("option4Btn", for: .normal)
        option4Btn.addTarget(self, action:#selector(option4Action(sender:)), for: .touchUpInside)
        itemsContentView?.addSubview(option4Btn)
        
        unselectColoredBtn()
        
        option1Btn.isHidden = true
        option2Btn.isHidden = true
        option3Btn.isHidden = true
        option4Btn.isHidden = true
        
        option1Btn.layer.cornerRadius = 20
        option2Btn.layer.cornerRadius = 20
        option3Btn.layer.cornerRadius = 20
        option4Btn.layer.cornerRadius = 20
        if (UserDefaults.standard.object(forKey: "IsOwner") != nil) {
            isOwner = UserDefaults.standard.object(forKey: "IsOwner") as! NSString
            
        }
        
        
        selectOption = 0
        
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 35/255, green: 230/255, blue: 6/255, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 55))
        myView.backgroundColor =  UIColor(red: 35/255, green: 253/255, blue: 6/255, alpha: 1)
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.frame = CGRect(x: 0, y: (answerTxt.frame.size.height) - width, width:   self.view.frame.size.width, height: (answerTxt.frame.size.height))
        border.borderWidth = width
        answerTxt.layer.addSublayer(border)
        answerTxt.layer.masksToBounds = true
        
        
        welcomeNextBtn?.layer.cornerRadius = 20
        
        txtPaddingVw(txt: answerTxt)
        
        
        if UserDefaults.standard.object(forKey: "tour_id") != nil{
            self.tourID = UserDefaults.standard.object(forKey: "tour_id") as! Int
        }
        else
        {
            self.tourID = 1
        }
        
        
        //Show the data according saved index
        var index = Int()
        index = 0
        if (UserDefaults.standard.value(forKey: "saveTourDetail") != nil)
        {
            let saveTourDetail = UserDefaults.standard.value(forKey: "saveTourDetail") as! NSArray
            
            //print(saveTourDetail)
            if saveTourDetail.count != 0
            {
                var saveTourDetailDic = NSDictionary()
                // print(saveTourDetail.count)
                for i in 0..<saveTourDetail.count
                {
                    // print("Arr count\(self.saveTourDetailArr.count)")
                    saveTourDetailDic = saveTourDetail.object(at: i) as! NSDictionary
                    
                    if saveTourDetailDic.value(forKey: "tourID") != nil
                    {
                        
                        let id = saveTourDetailDic.value(forKey: "tourID") as! Int
                        
                        if self.tourID == id
                        {
                            index = saveTourDetailDic.value(forKey: "Index") as! Int
                            
                            if UserDefaults.standard.value(forKey: "arrID") != nil {
                                arrID = NSMutableArray(array: UserDefaults.standard.value(forKey: "arrID") as! NSArray)
                            }
                            
                            // print(index)
                            break
                        }
                        else
                        {
                            UserDefaults.standard.set("SavedTour", forKey: "SavedTour")
                            break
                        }
                        
                    }
                    
                }
            }
            // let saveTourDetail: NSDictionary = ["tourID":self.tourID,"Index":val]
        }
        
        
        if UserDefaults.standard.value(forKey: "correctAns") != nil {
            correctAns = UserDefaults.standard.value(forKey: "correctAns") as! Int
        }
        if UserDefaults.standard.value(forKey: "wrongAns") != nil {
            wrongAns = UserDefaults.standard.value(forKey: "wrongAns") as! Int
        }
        // print(correctAns)
        // print(wrongAns)
        //Save tour id
        var id = Int()
        if UserDefaults.standard.value(forKey: "SavedID") == nil {
            UserDefaults.standard.set(self.tourID, forKey: "SavedID")
            
        }
        
        
        //Get saved tour id
        if UserDefaults.standard.value(forKey: "SavedID") != nil {
            id = UserDefaults.standard.value(forKey: "SavedID") as! Int
        }
        
        //Match tour and saved tour id is same
        if id == self.tourID {
            //progessShow()
            getQuestionaireData(index:index,completion: { (result) in
                self.progessDismiss()
            })
        }
        else {
            savedDataAlertFunc(message: "Your previous saved tour progress will be lost if you press ok")
        }
        
        
        
        btnTag = 0
        slidingScrollView = UIScrollView(frame: CGRect(x: (slideUiView?.frame.origin.x)!, y: (slideUiView?.frame.origin.x)!, width: (self.view.frame.size.width), height: (slideUiView?.frame.size.height)!))
        slideUiView?.addSubview(slidingScrollView)
        
        buttonsView = UIScrollView(frame: CGRect(x: 0, y: self.view.frame.size.height-70, width: (self.view.frame.size.width), height: 70))
        buttonsView.backgroundColor = UIColor.white
        self.view.addSubview(buttonsView)
        if self.view.frame.size.height < 1000
        {
            self.congratsSubScroll?.contentSize = CGSize(width:self.view.frame.size.width, height:1000)
        }
        else
        {
            self.congratsSubScroll?.contentSize = CGSize(width:self.view.frame.size.width, height:650)
        }
        
        
        macButton.isEnabled = false  //2
        
        dragDropBckView.backgroundColor = UIColor.clear
        
        //====================================
        
        //Create Image View To Show Drag Correct Answer
        if model .isEqual(to: "iPhone")
        {
            showCorrImagView  = UIImageView(frame:CGRect(x:10, y:(contentWebView?.frame.origin.y)!+55, width:100, height:100));
            checkUnchekImg  = UIImageView(frame:CGRect(x:45, y:(contentWebView?.frame.origin.y)!+145, width:30, height:30));
        }
        else
        {
            showCorrImagView  = UIImageView(frame:CGRect(x:110, y:(contentWebView?.frame.origin.y)!+150, width:130, height:130));
        }
        itemsContentView?.addSubview(showCorrImagView)
        
        
        dragDropImgView.frame.origin.y = dragDropBckView.frame.origin.y
        
        answerLabel.frame.origin.y = dragDropBckView.frame.origin.y-50
        answerLabel.frame.origin.y = 50
        
        if model .isEqual(to: "iPhone")
        {
            showCorrImgLabel = UILabel(frame: CGRect(x: 0, y: (contentWebView?.frame.origin.y)!+220, width: 100, height: 90))
            showCorrImgLabel.numberOfLines = 4
            
        }
        else
        {
            showCorrImgLabel = UILabel(frame: CGRect(x: 0, y: (contentWebView?.frame.origin.y)!+230, width: 300, height: 30))
            openMapBtn.layer.cornerRadius = 20
            checkUnchekImg  = UIImageView(frame:CGRect(x:155, y:(contentWebView?.frame.origin.y)!+270, width:30, height:30));
            
        }
        
        
        checkUnchekImg.image = UIImage(named: "tick_icon.png")
        itemsContentView?.addSubview(checkUnchekImg)
        
        showCorrImgLabel.textColor = UIColor .green
        itemsContentView?.addSubview(showCorrImgLabel)
        showCorrImgLabel.isHidden = true
        showCorrImagView.isHidden = true
        checkUnchekImg.isHidden = true
        
        
        
    }
    func playCongratesSong() {
        
        
        let sound = Bundle.main.url(forResource: "Last_Page", withExtension: "m4a")
        do {
            player = try AVAudioPlayer(contentsOf: sound!)
            guard let player = player else { return }
            player.prepareToPlay()
            player.delegate = self
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        congInitialAudioChk = false
        congratsPlayBtn?.setImage(UIImage(named:"playbtn.png"), for: .normal)
    }
    @IBAction func playCongratesSound(sender: UIButton) {
        if congInitialAudioChk == false {
            playCongratesSong()
            congPlayCheck = true
            congInitialAudioChk = true
            congratsPlayBtn?.setImage(UIImage(named:"stopAudio.png"), for: .normal)
        }
        else if congPlayCheck == false {
            congratsPlayBtn?.setImage(UIImage(named:"stopAudio.png"), for: .normal)
            player?.play()
            congPlayCheck = true
        }
        else {
            congratsPlayBtn?.setImage(UIImage(named:"playbtn.png"), for: .normal)
            player?.pause()
            congPlayCheck = false
        }
    }
    func progessDismiss() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func progessShow() {
        //DispatchQueue.main.async {
        SVProgressHUD.show()
        //}
    }
    func groupCodeBtnAction(_ sender: UIButton) {
        
        showGroupCodeAlert(title: "Tour Group Name:",message:groupName)
        //Do your stuff here
    }
    func savedDataAlertFunc(message:String)  {
        let savedDataAlert = UIAlertController(title: nil, message:message as String , preferredStyle: UIAlertControllerStyle.alert)
        let cancelBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (alertAction) -> Void in
            _ = self.navigationController?.popViewController(animated: true)
        }
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            UserDefaults.standard.removeObject(forKey: "SavedTour")
            UserDefaults.standard.removeObject(forKey: "saveTourDetail")
            UserDefaults.standard.removeObject(forKey: "correctAns")
            UserDefaults.standard.removeObject(forKey: "wrongAns")
            UserDefaults.standard.removeObject(forKey: "arrID")
            UserDefaults.standard.removeObject(forKey: "SavedID")
            
            
            //self.getQuestionaireData(index:0)
            self.getQuestionaireData(index:0,completion: { (result) in
                
            })
        }
        savedDataAlert.addAction(cancelBtn)
        savedDataAlert.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(savedDataAlert, animated: true, completion: nil)
        }
    }
    func removeValue()
    {
        
        if  UserDefaults.standard.object(forKey: "JoinGroupSavedTourID") != nil {
            
            var arrjoinId = [Int]()
            
            arrjoinId = UserDefaults.standard.object(forKey: "JoinGroupSavedTourID")as! [Int]
            // print(arrjoinId)
            
            UserDefaults.standard.set( arrjoinId, forKey: "GroupSavedTourID")
            
            let array = NSMutableArray(array: arrjoinId)
            
            if array.contains(tourID) {
                array.remove(tourID)
            }
            
            arrjoinId = array as NSArray as! [Int]
            
            //             if arrjoinId.count != 0 {
            //            for i in 0..<arrjoinId.count {
            //
            //                if tourID == arrjoinId[i] {
            //                    arrjoinId.remove(at: i)
            //                    break
            //                }
            //
            //            }
            //            }
            UserDefaults.standard.set(arrjoinId, forKey: "JoinGroupSavedTourID")
            
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
         locationManager.stopUpdatingLocation()
    }
    //    // Swift
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController:parent)
        if parent == nil {
            // The back button was pressed or interactive gesture used
            UserDefaults.standard.set("QuesBack", forKey: "QuesBack")
        }
    }
    func saveView() {
        //Save View
        UserDefaults.standard.set("QUESTION_VIEW", forKey: "SavedViewController")
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.avPlayer.pause()
    }
    override func viewWillAppear(_ animated: Bool) {
        
       IQKeyboardManager.shared().isEnabled = false
        congInitialAudioChk = false
        //Back Button Check Value Removed
        UserDefaults.standard.removeObject(forKey: "QuesBack")
        UserDefaults.standard.set("QuestionView", forKey: "QuestionView")
        saveView()
    }
    
    
    func mapViewTourLocations(latitute: Double,longitute: Double)
    {
        
    }
    //Customize the size of Uiimage
    func imageView(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    func createAvpPlayer(urlArray: NSMutableArray, incVal: NSInteger)
    {
        DispatchQueue.main.async {
            if(!self.audioUrlString .isEqual(to: "")) {
                self.audioUrlString = urlArray .object(at: self.audioCount) as! NSString
                let audUrl: URL! = URL(string: self.audioUrlString as String)
                self.avPlayer = AVPlayer(url: audUrl!)
                self.playerItem = AVPlayerItem(url: audUrl)
                self.avPlayer = AVPlayer(playerItem: self.playerItem)
            }
        }
    }
    func playAudioAction(sender: UIButton)
    {
        if soundBtnTag == 0
        {
            self.playAudioBtn.setImage(UIImage(named: "stopAudio.png"), for: UIControlState.normal)
            
            self.avPlayer.play()
            
            self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.none
            
            self.soundBtnTag = 1
            self.strAlertMsg = "Audio started"
            self.alertBar()
            
            
        }
        else{
            playAudioBtn.setImage(UIImage(named: "audioPlay.png"), for: UIControlState.normal)
            self.strAlertMsg = "Audio stopped"
            self.alertBar()
            avPlayer.pause()
            soundBtnTag = 0
            notiPlayAudio = 0
            playAudioCheck = 0
        }
    }
    
    func playerItemDidReachEnd(_ notification: Notification) {
        // start your next song here
        
        self.avPlayer.play()
        
        
    }
    
    @IBAction func macButtonAction(sender: UIButton)
    {
        
        recognition = ISSpeechRecognition()
        recognition?.freeformType = UInt(ISFreeFormTypeDictation)
        // let err: Error?
        recognition?.delegate = self
        
        if !(((try? recognition!.listenAndRecognize(withTimeout: 10)) != nil)) {
            // print("ERROR: \(err)")
            strAlertMsg = "Time out"
            alertBar()
        }
        
    }
    
    func recognition(_ speechRecognition: ISSpeechRecognition, didGet result: ISSpeechRecognitionResult) {
        print("Method: \(NSStringFromSelector(#function))")
        print("Result: \(result.text)")
        // label.text = result.text
        self.selectOption = 1
        let str = result.text as NSString

        let updatedString = str.replacingOccurrences(of: "\'", with: "")
        self.macButtonTitle.text = updatedString
    }
    
    
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(QuesAnsViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(QuesAnsViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //self.scroll_View?.isScrollEnabled = false
        //doneBtnBckView?.isHidden = false
 
                
                if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                    let keyboardRectangle = keyboardFrame.cgRectValue
                    let keyboardHeight = keyboardRectangle.height
                    
                    
            
                let pos = answerTxt.frame.origin.y+(slideUiView?.frame.origin.y)! + (slideUiView?.frame.size.height)!
                    
                    var fieldPos: CGFloat!
                    if pos > self.view.frame.size.height || pos > self.view.frame.size.height-50 || pos > self.view.frame.size.height-100 {
                        kbHeight = keyboardHeight-90
                    }
      
                    else {
                        fieldPos = self.view.frame.size.height - pos
                        kbHeight = (keyboardHeight-(fieldPos-40))
                    }
                    
      
                    self.animateTextField(up: true)
                    
        }
     
    }
    func animateTextField(up: Bool) {
        let movement = (up ? -kbHeight : kbHeight)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement!)
          //  self.view.frame = CGRectOffset(self.view.frame, 0, movement!)
        })
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        //doneBtnBckView?.isHidden = true
        //self.scroll_View?.isScrollEnabled = true
        self.animateTextField(up: false)

    }
    
    /// move scroll view up
    func keyboardWasShown(notification: Notification) {
        
         //self.navigationController?.navigationBar.frame.origin.y  = 100
        self.scroll_View?.isScrollEnabled = false
        doneBtnBckView?.isHidden = false
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            //if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            //}
        }    
    }
    
    /// move scrollview back down
    func keyboardWillBeHidden(notification: Notification) {
        // self.navigationController?.navigationBar.frame.origin.y  = 100
        doneBtnBckView?.isHidden = true
        self.scroll_View?.isScrollEnabled = true
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
          //  if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
           // }
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
    func puzzleAppearFunc()
    {
        // Initialize tileArea
        self.tileArea.delegate = self
        self.tileArea.imageToSolve = (self.currentImagePackage?.image!)!
        self.tileArea.tilesPerRow = self.tilesPerRow
        self.view.bringSubview(toFront: self.tileArea)
        
        
        self.tileArea.initialize()
        self.tileArea.layer.borderWidth = 2
        
        // Add row/column gesture
        // let rowColumnGripPanGesture = UIPanGestureRecognizer(target: self, action: #selector(handleRowColumnGripPan(_:)))
        //findRowColumnGripWithPoint self.puzzleView.addGestureRecognizer(rowColumnGripPanGesture)
        //  self.initializeRowColumnGrips()
        
        // Set text fields
        //congratsMessage.text = ""
        if self.currentImagePackage?.caption == "" {
            // self.imageCaptionLabel.text = ""
        } else {
            if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
                self.imageCaptionLabel.text = "\"\(self.currentImagePackage?.caption!)\"" + "\nby " + (self.currentImagePackage?.photographer)!
            }
            if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
                self.imageCaptionLabel.text = (self.currentImagePackage?.caption)! + " — " + (self.currentImagePackage?.photographer)!
            }
        }
        
    }
    //
    //    func handleRowColumnGripPan(_ gesture:UIPanGestureRecognizer) {
    //        if  !self.originalImageShown { // Gesture should be allowed
    //            switch gesture.state {
    //            case .began:
    //                let startingPoint:CGPoint = gesture.location(in: self.view)
    //                self.rowColumnGrip = self.findRowColumnGripWithPoint(startingPoint, first: true)
    //                if self.rowColumnGrip != nil { // The first handle was detected and stored for later
    //                    self.firstLineOfTiles = self.tileArea.makeLineOfTiles(self.rowColumnGrip!.tag)
    //                    for tile in self.firstLineOfTiles! {
    //                        self.tileArea.bringSubview(toFront: tile.imageView)
    //                        tile.originalFrame = tile.imageView.frame
    //                    }
    //                    self.view.bringSubview(toFront: self.rowColumnGrip!)
    //                    self.firstGripOriginalFrame = self.rowColumnGrip!.frame
    //                }
    //            case .changed:
    //                if self.rowColumnGrip != nil { // There is a grip selected. Determine translation
    //                    let translation = gesture.translation(in: self.view)
    //                    if (self.rowColumnGrip!.tag - 100) < 0 { // line 1 is a column
    //                        if self.rowColumnGrip!.frame.minX + translation.x > self.tileArea.frame.minX && self.rowColumnGrip!.frame.maxX + translation.x < self.tileArea.frame.maxX {
    //                            // Translation is valid - translate the line of tiles and grip
    //                            for tile in self.firstLineOfTiles! {
    //                                tile.imageView.center.x = tile.imageView.center.x + translation.x
    //                            }
    //                            self.rowColumnGrip!.center.x = self.rowColumnGrip!.center.x + translation.x
    //                        }
    //                    } else { // line 1 is a Row
    //                        if self.rowColumnGrip!.frame.minY + translation.y > self.tileArea.frame.minY && self.rowColumnGrip!.frame.maxY + translation.y < self.tileArea.frame.maxY {
    //                            // Translation is valid - translate the line of tiles and grip
    //                            for tile in self.firstLineOfTiles! {
    //                                tile.imageView.center.y = tile.imageView.center.y + translation.y
    //                            }
    //                            self.rowColumnGrip!.center.y = self.rowColumnGrip!.center.y + translation.y
    //                        }
    //                    }
    //                    gesture.setTranslation(CGPoint.zero, in: self.view)
    //                }
    //            case .ended:
    //                if self.rowColumnGrip != nil {
    //                    let endingPoint :CGPoint = gesture.location(in: self.view)
    //                    let secondRowColumnGrip = self.findRowColumnGripWithPoint(endingPoint, first: false)
    //                    if secondRowColumnGrip != nil { // A valid second grip was found at the endingPoint
    //                        // Swap the lines of tiles
    //                        let secondLineOfTiles = self.tileArea.makeLineOfTiles(secondRowColumnGrip!.tag)
    //                        self.tileArea.swapLines(self.firstLineOfTiles!, line2: secondLineOfTiles)
    //
    //                        // Swap the grips
    //                        UIView.animate(withDuration: 0.3, animations: { () -> Void in
    //                            self.rowColumnGrip!.frame = secondRowColumnGrip!.frame
    //                            secondRowColumnGrip!.frame = self.firstGripOriginalFrame!
    //                        })
    //
    //                        // Swap the tags of the buttons
    //                        let tempTag = self.rowColumnGrip!.tag
    //                        self.rowColumnGrip!.tag = secondRowColumnGrip!.tag
    //                        secondRowColumnGrip!.tag = tempTag
    //                        self.firstGripOriginalFrame = nil
    //                        self.rowColumnGrip = nil
    //
    //                    } else { // Send the button and line of tiles back to where they started
    //                        UIView.animate(withDuration: 0.3, animations: { () -> Void in
    //                            for tile in self.firstLineOfTiles! {
    //                                tile.imageView.frame = tile.originalFrame!
    //                            }
    //                            self.rowColumnGrip!.frame = self.firstGripOriginalFrame!
    //                            self.firstGripOriginalFrame = nil
    //                            self.rowColumnGrip = nil
    //                        })
    //                    }
    //                }
    //            case .possible:
    //                print("possible")
    //            case .cancelled:
    //                print("cancelled")
    //            case .failed:
    //                print("failed")
    //            }
    //        }
    //    }
    
    //    // This returns a grip that contains a CGPoint. Used to find the initial grip when first is true. Else, use a larger target space to find a grip when the pan gesture ends
    //    func findRowColumnGripWithPoint(_ point: CGPoint, first: Bool) -> UIImageView? {
    //        for index in 0..<self.tilesPerRow {
    //            var topArea : CGRect
    //            var leftArea : CGRect
    //            //if self.topGrips.count < index && self.leftGrips.count < index
    //              //    {
    //
    //            if self.topGrips.count != 0 && self.leftGrips.count != 0
    //            {
    //            let topGrip = self.topGrips[index as Int]
    //            let leftGrip = self.leftGrips[index as Int]
    //            if first { // Find the initial grip
    //                topArea = topGrip.frame
    //                leftArea = leftGrip.frame
    //                if topArea.contains(point) {
    //                    return topGrip
    //                }
    //                if leftArea.contains(point) {
    //                    return leftGrip
    //                }
    //            } else {
    //                // The target areas are larger to make it easier to find the second grip / column to swap
    //                // Ensure that it is not the same as the first grip
    //                // Check if it is of similar type (row vs column)
    //                topArea = CGRect(x: topGrip.frame.origin.x, y: topGrip.frame.origin.y - 200, width: topGrip.frame.size.width, height: topGrip.frame.size.height + 400)
    //                leftArea = CGRect(x: leftGrip.frame.origin.x - 200, y: leftGrip.frame.origin.y, width: leftGrip.frame.size.width + 400, height: leftGrip.frame.size.height)
    //                if topArea.contains(point) && topGrip != self.rowColumnGrip && abs(self.rowColumnGrip!.tag - topGrip.tag) < 11 {
    //                    return topGrip
    //                }
    //                if leftArea.contains(point) && leftGrip != self.rowColumnGrip && abs(self.rowColumnGrip!.tag - leftGrip.tag) < 11  {
    //                    return leftGrip
    //                }
    //            }
    //        }
    //        }
    //        return nil
    //    }
    func puzzleIsSolved() {
        selectOption = 2
        //puzzleView.isUserInteractionEnabled = false
        // submitBtn.isHidden = true
        // nextButton.isHidden = false
        
        //strAlertMsg = "Puzzle is completed"
        //alertBar()
        
        //        let when = DispatchTime.now() + 1
        //        DispatchQueue.main.asyncAfter(deadline: when){
        //            self.enableOptions()
        //            self.getData(val: self.btnTag)
        //            self.btnTag = self.btnTag+1
        //            self.selectOption = 0
        //            self.submitCheck = 0
        //        }
    }
    func createSlidingView(arrImages :NSMutableArray)
    {
        slidingScrollView = UIScrollView(frame: CGRect(x: (slideUiView?.frame.origin.x)!, y: (slideUiView?.frame.origin.x)!, width: (self.view.frame.size.width), height: (slideUiView?.frame.size.height)!))
        slideUiView?.addSubview(slidingScrollView)
        slidingScrollView.delegate = self
        backView?.isHidden = true
        congratsScrollView?.isHidden = true
        itemsContentView?.isHidden = true
        slideUiView?.isHidden = false
        
        if arrImages.count != 0
        {
            
            //create UIImageView object
            
            let count = CGFloat(arrImages.count)
            //print(arrImages)
            
            
            for i in 0 ..< arrImages.count {
                let count1 = CGFloat(i)
                let val = arrImages[i] as! String
                curr_imageView = UIImageView.init(frame: CGRect(x:count1 * slidingScrollView.frame.size.width , y:0, width: slidingScrollView.frame.size.width, height: slidingScrollView.frame.size.height))
                
                curr_imageView.setShowActivityIndicator(true)
                curr_imageView.setIndicatorStyle(.gray)
                curr_imageView.sd_setImage(with:  URL(string: val), completed: { (image, error, cache, url) in
                    // self.progessDismiss()
                    
                    self.curr_imageView.setShowActivityIndicator(false)
                })
                
                // curr_imageView.sd_setImage(with: URL(string: val), placeholderImage: UIImage(named: ""))
                
                let strVal = val as NSString
                
                if strVal .isEqual(to: "http://beta.brstdev.com/tours/uploads/questions/1.png") || strVal .isEqual(to: "http://beta.brstdev.com/tours/uploads/questions/newimg1.png") || strVal .isEqual(to: "http://beta.brstdev.com/tours/uploads/questions/newimage6.png") || strVal .isEqual(to: "http://beta.brstdev.com/tours/uploads/questions/family13.png") || strVal .isEqual(to: "http://beta.brstdev.com/tours/uploads/questions/family13.png") || strVal .isEqual(to: "http://beta.brstdev.com/tours/uploads/questions/take_photo.png")
                {
                    curr_imageView.contentMode = .scaleAspectFit
                }
                else{
                    curr_imageView.contentMode = .scaleToFill
                }
                
                
                self.slidingScrollView.addSubview(curr_imageView)
                
                
            }
            //DispatchQueue.main.async {
            self.slidingScrollView.contentSize = CGSize(width:count * self.curr_imageView.frame.size.width, height:self.curr_imageView.frame.size.height)
            //}
            if(arrImages.count > 1){
                slideUiView?.addSubview(pageUiView)
                pageControl.pageIndicatorTintColor = UIColor.white
                pageControl.currentPageIndicatorTintColor = UIColor(red: 35/255, green: 253/255, blue: 6/255, alpha: 1)
                self.pageControl.currentPage = 0
                pageControl.numberOfPages = Int(count)
                pageUiView.addSubview(pageControl)
                
            }
            else
            {
                pageUiView.removeFromSuperview()
            }
            
            self.slidingScrollView?.setContentOffset(CGPoint(x:0, y:0), animated: true)
            self.slidingScrollView.delegate = self
            slidingScrollView.isPagingEnabled = true
            slidingScrollView.showsVerticalScrollIndicator = false
            slidingScrollView.showsHorizontalScrollIndicator = false
            
        }
    }
    //MARK: UIScrollView Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((slidingScrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        // Change the text accordingly
    }
    @IBAction func backBtnAction (sender:UIButton) {
        backTag = "BACK"
        answerLabel.text = ""
        answerTxt.text = ""
        question_type = ""
        backNextTag = "Back"
        avPlayer.pause()
        self.slidingScrollView?.setContentOffset(CGPoint(x:0, y: CGFloat(0)), animated: true)
        answerTxt.resignFirstResponder()
        
        if btnTag > 0   {
            
            if btnTag < 2
            {
                congratsScrollView?.isHidden = true
                scroll_View?.isHidden = true
                welcomeView?.isHidden = true
                removeSavedData()
                _ = navigationController?.popViewController(animated: true)
            }
            else
            {
                btnTag = btnTag - 1
                if btnTag > 0
                {
                    btnTag = btnTag-1
                    getData(val: btnTag)
                    btnTag = btnTag + 1
                }
                
                selectOption = 0
                submitCheck = 0
                
                
            }
        }
        else{
            removeSavedData()
            _ = navigationController?.popViewController(animated: true)
            
        }
    }
    @IBAction func welcomNextAction (sender:UIButton) {
        btnTag = 1
        welcomeView?.isHidden = true
        congratsScrollView?.isHidden = true
        
        SVProgressHUD.show()
        scroll_View?.isHidden = false
        itemsContentView?.isHidden = false
        
        
        
    }
    func enableOptions()
    {
        option1Btn.isUserInteractionEnabled = true
        option2Btn.isUserInteractionEnabled = true
        option3Btn.isUserInteractionEnabled = true
        option4Btn.isUserInteractionEnabled = true
    }
    
    @IBAction func nextBtnAction (sender:UIButton) {
        backTag = ""
        
        avPlayer.pause()
        backButton.isHidden = false
        answerTxt.resignFirstResponder()
        buttonsView.frame.origin.y = self.view.frame.size.height-70
        self.slidingScrollView?.setContentOffset(CGPoint(x:0, y: CGFloat(0)), animated: true)
        backNextTag = "Next"
        answerLabel.text = ""
        answerTxt.text = ""
        question_type = ""
        answerTxt.resignFirstResponder()
        
        if btnTag < self.arrQuesAndAnswer.count {
            congratsScrollView?.isHidden = true
            if btnTag < self.arrQuesAndAnswer.count {
                
                enableOptions()
                getData(val: btnTag)
                btnTag = btnTag+1
                selectOption = 0
                submitCheck = 0
            }
            else
            {
                selectOption = 0
                unselectColoredBtn()
                btnTag = btnTag+1
                if flag == true {
                    correctAns = correctAns+1
                }
                else  if flag == false{
                    wrongAns = wrongAns+1
                    
                }
            }
            
        }
        else
        {
            buttonsView.isHidden = true
            doneBtnBckView?.isHidden = false
            playAudioBtn.isHidden = true
            
            progessDismiss()
            if isOwner .isEqual(to: "IsOwner") {
                let strCorrect = String(correctAns)
                let strWrong = String(wrongAns)
                strAlertMsg = "Your correct answers: "+strCorrect+"\n"+"Your wrong answers: "+strWrong
                // self.navigationController?.navigationBar.isHidden = true
                self.navigationItem.setHidesBackButton(true, animated:true);
                scroll_View?.isHidden = true
                slideUiView?.isHidden = true
                backView?.isHidden = true
                congratsScrollView?.isHidden = false
                
                let correctAn = strCorrect
                let inCorrectAns = strWrong
                correctLabel?.text = correctAn
                inCorrectLabel?.text = inCorrectAns
                
                let total1 = Int(strCorrect)
                let total2 = Int(strWrong)
                
                let corr = Double(correctAns)
                
                let flo = Double(total1!+total2!)
                
                let percent = Double(corr / flo) * 100
                let formattedNumber = String(format: "%.2f", percent)
                
                let scoredLab = String(formattedNumber + "%")
                scroedLabel?.text = scoredLab
                
                
                
            }
            else{
                
                buttonsView.isHidden  = true
                doneBtnBckView?.isHidden = true
                //                if JoinGroupCheck .isEqual(to: "JoinGroupCheck")
                //                {
                //                 strAlertMsg = "You have successfully completed the tour. To see your group results check your group leaders device."
                //                }
                //                else
                //                {
                strAlertMsg = "You have successfully completed the tour. To see your group results check your group leaders device."
                //}
                
                finalResultAlerBar()
                
            }
        }
        
    }
    @IBAction func congtatsBackAction (sender: UIButton)
    {
        _ = navigationController?.popViewController(animated: true)
    }
    @IBAction func submitAction (sender:UIButton) {
        
        
        answerLabel.isHidden = false
        answerLabel.textColor = UIColor .green
        
        answerLabel.frame.origin.y = (quesWebView?.frame.origin.y)! + (quesWebView?.frame.size.height)!
        
        if question_type .isEqual(to: "subjective") {
            
            if submitCheck == 0
            {
                if (answerTxt.text?.isEmpty)!
                {
                    strAlertMsg = "Please enter answer"
                    alertBar()
                }
                else{
                    //  self.answerTxt.frame.origin.y = (answerLabel.frame.origin.y) + (answerLabel.frame.size.height)
                    
                    arrID.add(ID)
                    
                    let corr = "Correct answer is: "+(answer as String)
                    
                    var textVal = (answerTxt.text)! as String
                     textVal = textVal.trimmingCharacters(in: .whitespaces)

                    let strAns: String! = answer as String
                    if(textVal.caseInsensitiveCompare(strAns) == ComparisonResult.orderedSame) {
                        strAlertMsg = "Your answer is correct"
                        correctAns = correctAns+1
                        alertBar()
                    }
                    else
                    {
                        strAlertMsg = "Your answer is incorrect"
                        wrongAns = wrongAns+1
                        alertBar()
                        
                        
                    }
                    //Save Data
                    self.submitSavedData()
                    answerLabel.text = corr as String
                    
                    movPosOnSubmit(pos: answerTxt.frame.origin.y + answerTxt.frame.size.height + 40)
                    
                }
            }
        }
       // else if question_type .isEqual(to: "uploadphoto") || ID == 32 || ID == 38 || ID == 58 || ID == 89
          if ((!uploadPicLat .isEmpty && !uploadPicLong .isEmpty) || ID == 32 || ID == 38 || ID == 58 || ID == 89)
        {
            if selectOption == 0 {
                strAlertMsg = "Please upload image"
                alertBar()
                
            }
            else
            {
                arrID.add(ID)
                
                movPosOnSubmit(pos: answerTxt.frame.origin.y + answerTxt.frame.size.height + 40)
            }
        }
        else if question_type .isEqual(to: "unscramble")
        {
            
            
            puzzleView.isUserInteractionEnabled = false
            if selectOption == 0 {
                strAlertMsg = "Your answer is incorrect"
                self.wrongAns = self.wrongAns+1
                alertBar()
            }
            else
            {
                strAlertMsg = "Your answer is correct"
                self.correctAns = self.correctAns+1
                alertBar()
            }
            
            arrID.add(ID)
            self.submitSavedData()
            answerLabel.isHidden = false
            
            answerLabel.frame.origin.y = puzzleView.frame.origin.y + puzzleView.frame.size.height + 40
            movPosOnSubmit(pos: answerTxt.frame.origin.y + answerTxt.frame.size.height + 40)
            
        }
        else if question_type .isEqual(to: "audio")
        {
            if selectOption == 0 {
                alertBar()
                
            }
            else
            {
                arrID.add(ID)
                
                macButtonTitle.textColor = UIColor .green
                let corr = "Correct answer is: "+(getAnswer as String)
                self.macButton.isEnabled = false
                let an: String = self.macButtonTitle.text!
                
                if(an.caseInsensitiveCompare(self.getAnswer) == ComparisonResult.orderedSame) {
                    
               // if self.macButtonTitle.text == self.getAnswer
               // {
                    strAlertMsg = "Your answer is correct"
                    alertBar()
                    
                    self.correctAns = self.correctAns+1
                }
                else
                {
                    
                    strAlertMsg = "Your answer is incorrect"
                    alertBar()
                    self.wrongAns = self.wrongAns+1
                }
                macButtonTitle.text = corr
                answerLabel.isHidden = false
                answerLabel.text = ""
                
                //Save Data
                self.submitSavedData()
                
                movPosOnSubmit(pos: answerTxt.frame.origin.y + answerTxt.frame.size.height + 40)
            }
        }
        else if question_type .isEqual(to: "dragndrop")
        {
            self.scroll_View?.setContentOffset(CGPoint(x:0, y:0), animated: true)
            if selectOption == 0 {
                strAlertMsg = "Please select any one item"
                alertBar()
                
            }
            else
            {
                
                arrID.add(ID)
                
                dragDropBckView.isUserInteractionEnabled = false
                if self.dragDropAnsewr == dragDropimgOptionsID
                {
                    strAlertMsg = "Your answer is correct"
                    alertBar()
                    self.correctAns = self.correctAns+1
                    self.dragDropImgView.isHidden = true
                    
                    
                    //Save Data
                    self.submitSavedData()
                    
                    //  self.dragDropImgView.image = self.dragDropImgView.image!.withRenderingMode(.alwaysTemplate)
                    self.dragDropImgView.tintColor = UIColor.green
                    self.answerLabel.isHidden = true
                    //  makeCartoonView?.frame.origin.y = (slideUiView?.frame.origin.y)! + (slideUiView?.frame.size.height)!-100
                    self.macButtonTitle.text = ""
                    //self.macButtonTitle.text = "This graphic is the correct answer."
                    
                    cartonItems()
                    // arrMatch.removeAllObjects()
                }
                else
                {
                    cartonItems()
           
                    dragDropImgView.isHidden = true
                    answerLabel.isHidden = true
                    
                    showCorrImgLabel.isHidden = false
                    showCorrImagView.isHidden = false
                    checkUnchekImg.isHidden = false
                    
                    let corImgUrl = optionsArr[answerIndex] as! NSDictionary
                    let corDragImg = corImgUrl.object(forKey: "option")as! String
                    showCorrImagView.sd_setImage(with: URL(string: corDragImg), placeholderImage: UIImage(named: ""))
                    //====================================
                    strAlertMsg = "Your answer is incorrect"
                    alertBar()
                    self.macButtonTitle.text = "This graphic is the incorrect answer."
                    self.wrongAns = self.wrongAns+1
                    // self.answerLabel.isHidden = true
                    //Save Data
                    self.submitSavedData()
                    //  self.dragDropImgView.image = self.dragDropImgView.image!.withRenderingMode(.alwaysTemplate)
                    // self.dragDropImgView.tintColor = UIColor.red
                }
                macButtonTitle.frame.origin.y = dragDropImgView.frame.origin.y +  dragDropImgView.frame.size.height
                answerLabel.frame.origin.y = dragDropImgView.frame.origin.y +  dragDropImgView.frame.size.height
                answerLabel.isHidden = true
                macButtonTitle.isHidden = true
                movPosOnSubmit(pos: answerTxt.frame.origin.y + answerTxt.frame.size.height + 40)
            }
            
        }
            
        else{
            if submitCheck == 0
            {
                
                if btnTag < self.arrQuesAndAnswer.count {
                    if selectOption == 0 {
                        strAlertMsg = "Please select any one option"
                        alertBar()
                        
                    }
                    else
                    {
                        arrID.add(ID)
                        
                        //self.submitSavedData()
                        disableOptions()
                        submitCheck = 1
                        selectOption = 0
                        
                        for i in 0..<arrData.count {
                            let options = self.arrData[i] as! NSDictionary
                            let correct =  (options.value(forKey: "options_id") as? Int)!
                            let ans = (options.value(forKey: "option") as! NSString)
                            if serVerAns == correct {
                                correctAnswer = ans
                            }
                            else
                            {
                                
                            }
                        }
                        let corr = "Correct answer is: "+(correctAnswer as String)
                        movPosOnSubmit(pos:option4Btn.frame.origin.y+option4Btn.frame.size.height+55)
                        
                        answerLabel.text = corr
                        
                        
                        
                        if flag == true {
                            correctIncorrectColor(str: "Correct")
                            nextButton.isHidden = false
                            backButton.isHidden = false
                            correctAns = correctAns+1
                            strAlertMsg = "Your answer is correct"
                            alertBar()
                        }
                        else{
                            
                            strAlertMsg = "Your answer is incorrect"
                            alertBar()
                            correctIncorrectColor(str: "Wrong")
                            nextButton.isHidden = false
                            backButton.isHidden = false
                            wrongAns = wrongAns+1
                        }
                        
                        //Save Data
                        self.submitSavedData()
                    }
                }
                else {
                    let strCorrect = String(correctAns)
                    let strWrong = String(wrongAns)
                    strAlertMsg = "Your correct answers: "+strCorrect+"\n"+"Your wrong answers: "+strWrong
                    finalResultAlerBar()
                }
            }
            
        }
        
        print("Wrong Ans Is\(wrongAns)---Correct Ans Is\(correctAns)")
        
    }
    func submitSavedData()
    {
        // UserDefaults.standard.set(correctAns, forKey: "correctAns")
        //UserDefaults.standard.set(wrongAns, forKey: "wrongAns")
        // let saveTour = NSMutableArray()
        let ArrTourDetail = NSMutableArray()
        
        let saveTourDetail: NSDictionary = ["tourID":self.tourID,"Index":(btnTag)]
        
        
        if (UserDefaults.standard.value(forKey: "saveTourDetail") != nil)
        {
            
            savedData(ArrTourDetail: ArrTourDetail, saveTourDetail: saveTourDetail)
        }
        
        //print(correctAns)
        //print(wrongAns)
        UserDefaults.standard.set(correctAns, forKey: "correctAns")
        UserDefaults.standard.set(wrongAns, forKey: "wrongAns")
        UserDefaults.standard.set(arrID, forKey: "arrID")
        
    }
    func cartonItems()
    {
        
        let indexvalue = arrMatch[0] as! String
        if indexvalue.contains("Cromwell")
        {
            
            makeCartoonView?.frame.origin.y = (quesWebView?.frame.origin.y)!
            //  self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:self.view.frame.size.height)
            makeCartoonView?.isHidden = false
            headCapImgView?.isHidden = true
            headImgView?.isHidden = false
            bodyImgView?.isHidden = false
            leftArmsImgView?.isHidden = false
            rightArmsImgView?.isHidden = false
            
            leftLegImgView?.isHidden = true
            rightLegImgView?.isHidden = true
            
        }
        else if indexvalue.contains("on the tour I really need to find my legs")
        {
            
            if model .isEqual(to: "iPhone")
            {
                makeCartoonView?.frame.origin.y = (quesWebView?.frame.origin.y)!+50
                self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:self.view.frame.size.height)
            }
            else
            {
                makeCartoonView?.frame.origin.y =  (quesWebView?.frame.origin.y)!
                self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:self.view.frame.size.height-300)
            }
            
            makeCartoonView?.isHidden = false
            headCapImgView?.isHidden = true
            headImgView?.isHidden = false
            bodyImgView?.isHidden = false
            leftArmsImgView?.isHidden = false
            rightArmsImgView?.isHidden = false
            leftLegImgView?.isHidden = false
            rightLegImgView?.isHidden = false
            
        }
        else
        {
            
            if model .isEqual(to: "iPhone")
            {
                self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:self.view.frame.size.height+100)
            }
            else
            {
                self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:self.view.frame.size.height-200)
            }
            makeCartoonView?.frame.origin.y = (quesWebView?.frame.origin.y)! + (quesWebView?.frame.size.height)!
            makeCartoonView?.isHidden = false
            headCapImgView?.isHidden = false
            headImgView?.isHidden = false
            bodyImgView?.isHidden = false
            leftArmsImgView?.isHidden = false
            rightArmsImgView?.isHidden = false
            leftLegImgView?.isHidden = false
            rightLegImgView?.isHidden = false
            
        }
        
        
    }
    func movPosOnSubmit (pos : CGFloat)
    {
        submitBtn.isHidden = true
        nextButton.isHidden = false
        backButton.isHidden = false
        uploadBtn.isHidden = true
        answerTxt.text = ""
        nextButton.removeFromSuperview()
        submitBtn.removeFromSuperview()
        let height = self.view.frame.size.width/2 as CGFloat
        
        if model .isEqual(to: "iPhone")
        {
            nextButton = UIButton(frame: CGRect(x:height + 10, y: 10 , width:100, height: 50))
            backButton.frame.origin.x = height - 110
        }
        else
        {
            nextButton = UIButton(frame: CGRect(x:height + 10, y: 10 , width:150, height: 50))
            backButton.frame.origin.x = height - 160
        }
        nextButton.backgroundColor = UIColor.init(red: 108/255, green: 2/255, blue: 111/255, alpha: 1)
        if isOwner.isEqual(to: "IsOwner") {
            nextButton.setTitle("Next", for: .normal)
        }
        else
        {
            nextButton.setTitle("Next", for: .normal)
        }
        nextButton.addTarget(self, action:#selector(nextBtnAction(sender:)), for: .touchUpInside)
        buttonsView?.addSubview(nextButton)
        
        //        if question_type .isEqual(to: "subjective") {
        //            answerLabel.frame.origin.y = answerTxt.frame.origin.y + answerTxt.frame.size.height + 10
        //        }
        //        else if question_type .isEqual(to: "unscramble") {
        //        }
        //        else
        //        {
        //            answerLabel.frame.origin.y = option4Btn.frame.origin.y + option4Btn.frame.size.height + 10
        //        }
        
        submitBtn.layer.cornerRadius = 20
        nextButton.layer.cornerRadius = 20
        
        submitBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        nextButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        self.progessDismiss()
    }
    @IBAction func quesDoneAction (sender: UIButton)
    {
        player?.stop()
        self.completeTourFunc(tourID: String(describing:tourID), userID: userID, completion: { (result) in
            
        })
        
        
    }
    func createButtons(pos : CGFloat)
    {
        self.progessDismiss()
        nextButton.removeFromSuperview()
        submitBtn.removeFromSuperview()
        backButton.removeFromSuperview()
        uploadBtn.removeFromSuperview()
        let height = self.view.frame.size.width/2 as CGFloat
        
        
        uploadBtn = UIButton(frame: CGRect(x:height + 10, y: 10 , width:150, height: 50))
        uploadBtn.backgroundColor = UIColor.init(red: 108/255, green: 2/255, blue: 111/255, alpha: 1)
        uploadBtn.setTitle("Take Photo", for: .normal)
        uploadBtn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        uploadBtn.addTarget(self, action:#selector(uploadAction), for: .touchUpInside)
        buttonsView?.addSubview(uploadBtn)
        uploadBtn.layer.cornerRadius = 20
        uploadBtn.titleLabel?.textAlignment = NSTextAlignment .center
        if model .isEqual(to: "iPhone")
        {
            uploadBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
            uploadBtn.frame.size.width = 100
        }
        else
        {
            uploadBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        }
        if model .isEqual(to: "iPhone")
        {
            submitBtn = UIButton(frame: CGRect(x:height + 10, y: 10 , width:100, height: 50))
        }
        else{
            submitBtn = UIButton(frame: CGRect(x:height + 10, y: 10 , width:150, height: 50))
        }
        submitBtn.backgroundColor = UIColor.init(red: 108/255, green: 2/255, blue: 111/255, alpha: 1)
        submitBtn.setTitle("Submit", for: .normal)
        submitBtn.addTarget(self, action:#selector(submitAction(sender:)), for: .touchUpInside)
        buttonsView?.addSubview(submitBtn)
        submitBtn.isUserInteractionEnabled = false
        uploadBtn.isUserInteractionEnabled = false
        
        
        
        if model .isEqual(to: "iPhone")
        {
            backButton = UIButton(frame: CGRect(x: height-110, y: 10, width: 100, height: 50))
        }
        else{
            backButton = UIButton(frame: CGRect(x: height-160, y: 10, width: 150, height: 50))
        }
        backButton.backgroundColor = UIColor.init(red: 108/255, green: 2/255, blue: 111/255, alpha: 1)
        backButton.setTitle("Back", for: .normal)
        backButton.addTarget(self, action:#selector(backBtnAction(sender:)), for: .touchUpInside)
        buttonsView?.addSubview(backButton)
        
        
        if model .isEqual(to: "iPhone")
        {
            nextButton = UIButton(frame: CGRect(x:height + 10, y: 10 , width:100, height: 50))
        }
        else
        {
            nextButton = UIButton(frame: CGRect(x:height + 10, y: 10 , width:150, height: 50))
        }
        nextButton.backgroundColor = UIColor.init(red: 108/255, green: 2/255, blue: 111/255, alpha: 1)
        nextButton.setTitle("Next", for: .normal)
        nextButton.addTarget(self, action:#selector(nextBtnAction(sender:)), for: .touchUpInside)
        buttonsView?.addSubview(nextButton)
        
        submitBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        
        nextButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        backButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        
        submitBtn.isHidden = true
        
        
        if question_type .isEqual(to: "subjective")
        {
            hideShowButtons()
        }
        else if question_type .isEqual(to: "objective")
        {
            hideShowButtons()
        }
       // else if question_type .isEqual(to: "uploadphoto") || ID == 32 || ID == 38 || ID == 58 || ID == 89
        else  if ((!uploadPicLat .isEmpty && !uploadPicLong .isEmpty) || ID == 32 || ID == 38 || ID == 58 || ID == 89)
        {
            if isOwner.isEqual(to: "IsOwner") {
                submitBtn.isHidden = true
                backButton.isHidden = false
                nextButton.isHidden = true
            }
            else{
                submitBtn.isHidden = true
                backButton.isHidden = false
                nextButton.isHidden = false
                
            }
        }
        else if question_type .isEqual(to: "audio")
        {
            if isOwner.isEqual(to: "IsOwner") {
                submitBtn.isHidden = false
                nextButton.isHidden = true
                
            }
            else{
                macButton.isEnabled = false
                backButton.isHidden = false
                nextButton.isHidden = false
                submitBtn.isHidden = true
            }
        }
        else if question_type .isEqual(to: "unscramble")
        {
            hideShowButtons()
        }
        else if question_type .isEqual(to: "dragndrop")
        {
            if isOwner.isEqual(to: "IsOwner") {
                submitBtn.isHidden = false
                nextButton.isHidden = true
            }
            else{
                //dragDropBckView.isUserInteractionEnabled = false
                backButton.isHidden = false
                nextButton.isHidden = false
                submitBtn.isHidden = true
            }
        }
            
        else{
            backButton.isHidden = false
            nextButton.isHidden = false
        }
        
        firstTime = 0
        
        
        if arrID .contains( ID) {
            hideSubmit()
        }
        else
        {
            submitBtn.isHidden = false
        }
        submitBtn.layer.cornerRadius = 20
        nextButton.layer.cornerRadius = 20
        backButton.layer.cornerRadius = 20
        
    }
    func hideShowButtons()
    {
        if isOwner.isEqual(to: "IsOwner") {
            submitBtn.isHidden = false
            nextButton.isHidden = true
        }
        else{
            backButton.isHidden = false
            nextButton.isHidden = false
            submitBtn.isHidden = true
        }
    }
    func disableOptions()
    {
        option1Btn.isUserInteractionEnabled = false
        option2Btn.isUserInteractionEnabled = false
        option3Btn.isUserInteractionEnabled = false
        option4Btn.isUserInteractionEnabled = false
    }
    func hideSubmit()
    {
        if makeCartoonView?.isHidden == false
        {
            dragDropImgView.isHidden = true
            dragDropBckView.isHidden = true
            
            macButtonTitle.text = ""
            macButton.isHidden = true
        }
        var indexvalue = String()
        if arrMatch.count != 0
        {
            indexvalue = arrMatch[0] as! String
            if indexvalue.contains("Cromwell")
            {
                makeCartoonView?.isHidden = false
                headImgView?.isHidden = false
                headImgView?.isHidden = false
                bodyImgView?.isHidden = false
                leftArmsImgView?.isHidden = false
                rightArmsImgView?.isHidden = false
            }
            else if indexvalue.contains("on the tour I really need to find my legs")
            {
                makeCartoonView?.isHidden = false
                headCapImgView?.isHidden = true
                headImgView?.isHidden = false
                bodyImgView?.isHidden = false
                leftArmsImgView?.isHidden = false
                rightArmsImgView?.isHidden = false
                
                leftLegImgView?.isHidden = false
                rightLegImgView?.isHidden = false
            }
            else
            {
                makeCartoonView?.isHidden = false
                headCapImgView?.isHidden = false
                headImgView?.isHidden = false
                bodyImgView?.isHidden = false
                leftArmsImgView?.isHidden = false
                rightArmsImgView?.isHidden = false
                leftLegImgView?.isHidden = false
                rightLegImgView?.isHidden = false
                
            }
        }
        answerTxt.isHidden = true
        puzzleView.isUserInteractionEnabled = false
        disableOptions()
        self.macButton.isEnabled = false
        backButton.isHidden = false
        nextButton.isHidden = false
        submitBtn.isHidden = true
        uploadBtn.isUserInteractionEnabled = false
        dragDropBckView.isUserInteractionEnabled = false
    }
    func uploadAction()
    {
        itemsContentView?.backgroundColor = UIColor (red: 127/255, green: 127/255, blue: 127/255, alpha: 1)
        let alertController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
            self.itemsContentView?.backgroundColor = UIColor .white
        }
        alertController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Open Camera", style: .default)
        { _ in
            print("Open Camera")
            self.openCamera()
        }
        alertController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Open Gallery", style: .default)
        { _ in
            print("Open Gallery")
            self.openGallary()
        }
        alertController.addAction(deleteActionButton)
        
        alertController.popoverPresentationController?.sourceView = self.view
        
        alertController.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.size.width / 2.0,y: self.view.bounds.size.height / 2.0,width: 1.0,height: 1.0)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // var locationManager: CLLocationManager?
    
    //    func deviceLocation() -> String {
    //        let theLocation: String = "latitude: \(locationManager.location!.coordinate.latitude) longitude: \(locationManager.location!.coordinate.longitude)"
    //        return theLocation
    //    }
//    func getCurrentLocation () {
//        locationManager = CLLocationManager()
//        locationManager.distanceFilter = kCLDistanceFilterNone
//        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
//        locationManager.startUpdatingLocation()
//    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
          picker.dismiss(animated: true, completion: nil)
        var distance = CLLocationDistance()
        var distanceKM = CLLocationDistance()
        //distance = 0.003
        
        if UserDefaults.standard.object(forKey: "uploadPicLong") != nil {
            uploadPicLat = UserDefaults.standard.object(forKey: "uploadPicLat") as! String
            uploadPicLong = UserDefaults.standard.object(forKey: "uploadPicLong") as! String
        }
        
        //var latitude: Float = Float(locationManager.location!.coordinate.latitude)
        // var longitude: Float = Float(locationManager.location!.coordinate.longitude)
        
        var message = String()
        print(message)
        if locationManager.location != nil {
            
             // let loc1 = CLLocation(latitude: locationManager.location!.coordinate.latitude, longitude: locationManager.location!.coordinate.longitude)
 
            let loc1 = CLLocation(latitude: CurrentLocLatVal, longitude: CurrentLocLangVal)
            let loc3 = CLLocation(latitude: Double(uploadPicLat)!, longitude: Double(uploadPicLong)!)
            
            
       // let loc3 = CLLocation(latitude: Double("30.7069315")!, longitude: Double("76.6885024")!)
       // 30.7069315,76.6885024
            
            

            distance = loc1.distance(from: loc3)
            
            distanceKM = loc1.distance(from: loc3)
          //  distanceKM = distance/1000
            
             let formattedNumber = String(format: "%.3f", distanceKM)
            
           // let y = Double(distance)
            // let actualDis = Float(y)
            
           // distance = distance/1000
       
            message = "Distance is = \(formattedNumber) Meters\n\n Source lat-lang = \(CurrentLocLatVal) - \(CurrentLocLangVal)\n\n Destination lat-lang = \(uploadPicLat) - \(uploadPicLong)"
            //  message = "Distance is = \(formattedNumber)\n\n Source lat-lang = \(CurrentLocLatStr) - \(CurrentLocLangStr)\n\n Destination lat-lang = \(" 30.708309") - \("76.691625")"
           // 30.708309, 76.691625
        }
        
       
       // let y = Double(distance)
       // let actualD = Float(y)
        
        
        if distance <= 20 {
            self.correctAns+=1
            UserDefaults.standard.set(self.correctAns, forKey: "correctAns")
            // self.showToastMsg(color: .green, message: "Your answer is correct")
           // let msg = message + "\n\n Your answer is correct for captured image."
            let msg = message+"\n\n(Your answer is correct)"
           showGroupCodeAlert(title: msg, message: "")
        }
        else
        {
            self.wrongAns+=1
            UserDefaults.standard.set(self.wrongAns, forKey: "wrongAns")
          // self.showToastMsg(color: .red, message: "Your answer is incorrect")
           // let msg = message + "\n\n Your answer is incorrect for captured image."
             let msg = message+"\n\n(Your answer is incorrect)"
            showGroupCodeAlert(title: msg, message: "")
        }
        
        print("imagePickerController Wrong Ans Is\(wrongAns)---Correct Ans Is\(correctAns)")
        self.submitSavedData()
        //print(distance)
        
        itemsContentView?.backgroundColor = UIColor .white
        uploadImage = info[UIImagePickerControllerEditedImage] as! UIImage
        selectOption = 1
        submitBtn.isHidden = true
        nextButton.isHidden = false
        //Save Captured Image ID
        arrID.add(ID)
        //Save Captured Image ID
        UserDefaults.standard.set(arrID, forKey: "arrID")
        
        answerLabel.text = ""
        answerLabel.isHidden = false
        answerLabel.textColor = UIColor .green
        
        // let saveTour = NSMutableArray()
        let ArrTourDetail = NSMutableArray()
        
        let saveTourDetail: NSDictionary = ["tourID":self.tourID,"Index":(btnTag)]
        
        
        if (UserDefaults.standard.value(forKey: "saveTourDetail") != nil)
        {
            
            savedData(ArrTourDetail: ArrTourDetail, saveTourDetail: saveTourDetail)
        }
        
      
        
    }
    
    func showToastMsg(color: UIColor,message: String)
    {
        
        // create the alert
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        
        //   let message  = strAlertMsg
        let messageMutableString = NSMutableAttributedString(string: message, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 18.0)!])
        
        // let matchStr = strAlertMsg as NSString
        //if matchStr .isEqual(to: "Answer is correct")
        //{
        messageMutableString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSRange(location:0,length:message.characters.count))
        // }
        
        alert.setValue(messageMutableString, forKey: "attributedMessage")
        // add an action (button)
        //  alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
        
       
    }
    func showLocationUpdates(message: String) {
        DispatchQueue.main.async {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-110, width: 300,  height : 35))
        toastLabel.textColor = UIColor.white
        toastLabel.backgroundColor = UIColor .red
        toastLabel.textAlignment = NSTextAlignment.center;
        self.view.addSubview(toastLabel)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 4.0, delay: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations: {
            toastLabel.alpha = 0.0
            
        })
    }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.itemsContentView?.backgroundColor = UIColor .white
        dismiss(animated: true, completion: nil)
    }
    func correctIncorrectColor (str: NSString)
    {
        if correctIncorrect == 0
        {
            if str .isEqual(to: "Correct")
            {
                option1Btn.backgroundColor = UIColor (red: 122/255, green: 229/255, blue: 69/255, alpha: 1)
            }
            else{
                option1Btn.backgroundColor = UIColor (red: 234/255, green: 52/255, blue: 35/255, alpha: 1)
            }
        }
        if correctIncorrect == 1
        {
            if str .isEqual(to: "Correct")
            {
                option2Btn.backgroundColor = UIColor (red: 122/255, green: 229/255, blue: 69/255, alpha: 1)
            }
            else{
                option2Btn.backgroundColor = UIColor (red: 234/255, green: 52/255, blue: 35/255, alpha: 1)
            }
            
        }
        if correctIncorrect == 2
        {
            if str .isEqual(to: "Correct")
            {
                option3Btn.backgroundColor = UIColor (red: 122/255, green: 229/255, blue: 69/255, alpha: 1)
            }
            else{
                option3Btn.backgroundColor = UIColor (red: 234/255, green: 52/255, blue: 35/255, alpha: 1)
            }
            
            
        }
        if correctIncorrect == 3
        {
            if str .isEqual(to: "Correct")
            {
                option4Btn.backgroundColor = UIColor (red: 122/255, green: 229/255, blue: 69/255, alpha: 1)
            }
            else{
                option4Btn.backgroundColor = UIColor (red: 234/255, green: 52/255, blue: 35/255, alpha: 1)
            }
            
        }
    }
    func unselectColoredBtn()
    {
        option1Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option2Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option3Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option4Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
    }
    @IBAction func option1Action (sender:UIButton) {
        selectOption = 1
        matchingValues(valMatch: option1Btn,tag: 0)
        option1Btn.backgroundColor = UIColor (red: 182/255, green: 180/255, blue: 180/255, alpha: 1)
        option2Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option3Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option4Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        
    }
    @IBAction func option2Action (sender:UIButton) {
        selectOption = 1
        matchingValues(valMatch: option2Btn,tag: 1)
        option2Btn.backgroundColor = UIColor (red: 182/255, green: 180/255, blue: 180/255, alpha: 1)
        option1Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option3Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option4Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
    }
    @IBAction func option3Action (sender:UIButton) {
        selectOption = 1
        matchingValues(valMatch: option3Btn,tag: 2)
        option3Btn.backgroundColor = UIColor (red: 182/255, green: 180/255, blue: 180/255, alpha: 1)
        option1Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option2Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option4Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
    }
    @IBAction func option4Action (sender:UIButton) {
        selectOption = 1
        matchingValues(valMatch: option4Btn,tag: 3)
        option4Btn.backgroundColor = UIColor (red: 182/255, green: 180/255, blue: 180/255, alpha: 1)
        option1Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option2Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
        option3Btn.backgroundColor = UIColor (red: 127/255, green: 124/255, blue: 123/255, alpha: 1)
    }
    func matchingValues (valMatch: UIButton, tag: Int)
    {
        selectOption = 1
        correctIncorrect = tag
        if  answerIndex == tag  {
            
            flag = true
            correctAnswer = (valMatch.titleLabel?.text)! as NSString
            
        }
        else{
            flag = false
        }
    }
    //Save the number of questions completed
    func savedData(ArrTourDetail:NSMutableArray,saveTourDetail:NSDictionary)
    {
        ArrTourDetail.addObjects(from: UserDefaults.standard.value(forKey: "saveTourDetail") as! [Any])
        
        for i in 0..<ArrTourDetail.count{
            //   print("Arr count\(ArrTourDetail.count)")
            
            let dic = ArrTourDetail.object(at: i) as! NSDictionary
            
            let id = dic.value(forKey: "tourID") as! Int
            if id == tourID
            {
                ArrTourDetail.removeObject(at: i)
                break
                
            }
        }
        ArrTourDetail.add(saveTourDetail)
        UserDefaults.standard.set(ArrTourDetail, forKey: "saveTourDetail")
        // print(saveTourDetailArr)
        
    }
    
    func getData(val: Int) {
        
        progessShow()
        uploadPicLat = ""
        uploadPicLong = ""
        
        let saveTour = NSMutableArray()
        let ArrTourDetail = NSMutableArray()
        let saveTourDetail: NSDictionary = ["tourID":self.tourID,"Index":val]
        
        
        if (UserDefaults.standard.value(forKey: "saveTourDetail") != nil)
        {
            
            savedData(ArrTourDetail: ArrTourDetail, saveTourDetail: saveTourDetail)
        }
        else
        {
            // print("Arr saveTourDetail\(saveTourDetail)")
            //saveTour.add(saveTourDetail)
            UserDefaults.standard.set(saveTour, forKey: "saveTourDetail")
        }
        
        audioCount = 0
        
        playAudioCheck = 0
        showCorrImgLabel.isHidden = true
        showCorrImagView.isHidden = true
        checkUnchekImg.isHidden = true
        macButtonTitle.textColor = UIColor .black
        arrMatch.removeAllObjects()
        puzzleView.isHidden = true
        makeCartoonView?.isHidden = true
        puzzleView.isUserInteractionEnabled = true
        avPlayer.pause()
        soundBtnTag = 0
        playAudioBtn.setImage(UIImage(named: "audioPlay.png"), for: UIControlState.normal)
        if congratsScrollView?.isHidden == false
        {
            scroll_View?.isHidden = true
            itemsContentView?.isHidden = true
            congratsScrollView?.isHidden = false
        }
        
        uploadBtn.isUserInteractionEnabled = true
        self.macButton.isEnabled = true
        playAudioBtn.isHidden = true
        dragDropImgView.removeFromSuperview()
        dragDropBckView.removeFromSuperview()
        slidingScrollView.removeFromSuperview()
        macButtonTitle.text = "press the mic to answer the above question"
        macButtonTitle.textColor = UIColor .black
        if btnTag == 1
        {
            backButton.isHidden = true
        }
        let getVal = val
        self.welcomlogoImage?.isHidden = true
        if arrQuesAndAnswer.count != 0 && getVal < arrQuesAndAnswer.count
        {   macButton.isHidden = true
            macButtonTitle.isHidden = true
            option1Btn.isHidden = true
            option2Btn.isHidden = true
            option3Btn.isHidden = true
            option4Btn.isHidden = true
            unselectColoredBtn()
            
            
            let quesAnsDic = self.arrQuesAndAnswer[getVal] as! NSDictionary
            print("arrQuesAndAnswer\(quesAnsDic)")
            
           if let stringName = quesAnsDic.object(forKey: "group_name") as? String
           {
            
            let string: String! = stringName
            
            if !string .isEmpty
            {
                if isOwner.isEqual(to: "IsOwner")
                {
                    self.groupName = (quesAnsDic ["group_name"] as? String)!
                    self.groupCodeImgView.isHidden = true
                    self.groupCodeBtn.isHidden = false
                    self.groupCodeIcon.isHidden = false
                    
                    titleLabel.frame.size.width = self.view.frame.size.width-167
                }
                else
                {
                    
                    self.groupCodeBtn.isHidden = true
                    self.groupCodeIcon.isHidden = true
                    self.groupCodeImgView.isHidden = false
                    
                    titleLabel.frame.size.width = self.view.frame.size.width-150
                }
            }
            else
            {
                self.groupCodeBtn.isHidden = true
                self.groupCodeIcon.isHidden = true
                self.groupCodeImgView.isHidden = false
                titleLabel.frame.size.width = self.view.frame.size.width-150
            }
            
        }
            let value: AnyObject? = quesAnsDic.object(forKey: "content") as AnyObject?
            
            var content = NSString()
            if(value is NSString)
            {
                content = quesAnsDic.object(forKey: "content") as! NSString
            }
            else
            {
                //self.progessDismiss()
            }
            let id = quesAnsDic.object(forKey: "id") as! Int
            ID = id
            
            var questype = quesAnsDic.object(forKey: "type") as! NSString
            
            if quesAnsDic.object(forKey: "sound") as? NSString != nil{
                
                let fullName = quesAnsDic.object(forKey: "sound") as! NSString
                
                //if !fullName .isEqual(to: "")
               // {
                    let fullNameArr = fullName.components(separatedBy: ",")
                     audioUrlString = quesAnsDic.object(forKey: "sound") as! NSString
                    DispatchQueue.main.asyncAfter(deadline: .now()+3) {
                    self.playAudioBtn.isHidden = false
                    }
                    audioUrlArr = NSMutableArray(array: fullNameArr as NSArray)
                    
                    createAvpPlayer(urlArray: audioUrlArr as NSMutableArray,incVal: 0)
              //  }
            }
            else
            {
                playAudioBtn.isHidden = true
                
            }
            
            
            //let valueCheck: AnyObject? = quesAnsDic.value(forKey: "content") as AnyObject?
            if ((quesAnsDic.value(forKey: "content") as? String) != nil)
            {
                contentVal = quesAnsDic.object(forKey: "content") as! NSString
            }
            else
            {
                contentVal = ""
            }
            questype = questype.lowercased as NSString
            if questype .isEqual(to: "content") {
                
                question_type =  "content"
                contentWebView?.isHidden = false
                quesWebView?.isHidden = true
                answerTxt.isHidden = true
                let font = UIFont.systemFont(ofSize: 17)
                let height = self.widthForView(text:content as String, font: font, width: 0)
                // print(height)
                self.labelHeight = height+80
                self.createButtons(pos: self.labelHeight)
                
                
                
                let body: String = ((content) as NSString) as String
                contentVal = body as NSString
                
                uploadPicLat = quesAnsDic.value(forKey: "lat") as! String
                uploadPicLong = quesAnsDic.value(forKey: "long") as! String
                
                //if ID == 32 || ID == 38 || ID == 58 || ID == 89
                if ((!uploadPicLat .isEmpty && !uploadPicLong .isEmpty) || (ID == 32 || ID == 38 || ID == 58 || ID == 89))
                {
                    createButtons(pos : 15)
                    UserDefaults.standard.set(uploadPicLat, forKey: "uploadPicLat")
                    UserDefaults.standard.set(uploadPicLong, forKey: "uploadPicLong")

                }
                
               
                
                // print(uploadPicLat)
                //print(uploadPicLat)
                
                let htmlString: String = "<font size='4'>\(body)"
                contentWebView?.loadHTMLString(htmlString, baseURL: nil)
                //Method to show images...
                showImage(quesAnsDic: quesAnsDic, content: content, getVal:getVal)
                
                
                if contentVal.contains("Take a photo beside") {
                    self.playAudioBtn.frame.origin.y = 50
                }
                else if contentVal.contains("You have already been acquainted with the bandstand in the town park.") {
                    self.playAudioBtn.frame.origin.y = 130
                }
                
            }
                
                
            else
            {
                
                self.slideUiView?.isHidden = true
                question_type = quesAnsDic.object(forKey: "question_type") as! NSString
                backView?.isHidden = true
                imageView?.isHidden = true
                
                question_type = question_type.lowercased as NSString
                
                if question_type .isEqual(to: "subjective")
                {
                    if quesAnsDic.value(forKey: "answer") as? NSString != nil{
                        
                        answer = quesAnsDic.object(forKey: "answer") as! NSString
                        getAnswer = answer as String
                        
                        for i in 0..<arrData.count {
                            let options = self.arrData[i] as! NSDictionary
                            let correct =  (options.value(forKey: "options_id") as? Int)!
                            if serVerAns == correct {
                                answerIndex = i
                            }
                        }
                    }
                    else{
                        answer = "subjective"
                    }
                    
                    itemsContentView?.isHidden = false
                    backView?.isHidden = true
                    contentWebView?.isHidden = true
                    quesWebView?.isHidden = false
                    
                    let font = UIFont.systemFont(ofSize: 17)
                    let question = quesAnsDic.object(forKey: "question") as! NSString
                    
                    
                    strQuesCheck = question
                    let body: String = ("<br>\(contentVal)\(question)" as NSString) as String
                    let htmlString: String = "<font size='4'>\(body)"
                    self.quesWebView?.loadHTMLString(htmlString, baseURL: nil)
                    
                    let height = self.widthForView(text:question as String, font: font, width: 0)
                    // print(height)
                    showImage(quesAnsDic: quesAnsDic, content: content, getVal:getVal)
                    
                    answerTxt.isHidden = false
                    if isOwner.isEqual(to: "IsOwner") {
                        submitBtn.isHidden = false
                        nextButton.isHidden = true
                        //answerTxt.isHidden = false
                        self.createButtons(pos: height + 180)
                    }
                    else
                    {
                        nextButton.isHidden = false
                        backButton.isHidden = false
                        //  answerTxt.isHidden = true
                        self.createButtons(pos: height + 150)
                    }
                    
                }
                else if question_type .isEqual(to: "unscramble")
                {
                    SVProgressHUD.show()
                    if quesAnsDic.value(forKey: "question") as? NSString != nil{
                        
                        
                        let unscramble = quesAnsDic.value(forKey: "answer_ios_unscramble") as! String
                        
                        let url = URL(string:unscramble )
                        
                        unScrambleImgView.setShowActivityIndicator(true)
                        unScrambleImgView.setIndicatorStyle(.gray)
                        unScrambleImgView.isHidden = false
                        self.currentImagePackage?.image = nil
                        unScrambleImgView.sd_setImage(with: url!, completed: { (image, error, cache, url) in
                            
                            DispatchQueue.main.async {
                                self.playAudioBtn.frame.origin.y = 0
                                let img: UIImage! = self.unScrambleImgView.image
                                self.dragDropImgView.isHidden = true
                                let newSize = CGSize(width: 900, height: 900)
                                self.dragDropImgView.image =  self.imageView(with:  img, scaledTo: newSize)
                                self.currentImagePackage = nil;
                                self.currentImagePackage = ImagePackage(baseFileName: "", caption: "", photographer: "")
                                self.currentImagePackage?.image =  self.dragDropImgView.image
                                self.unScrambleImgView.isHidden = true
                                self.unScrambleImgView.setShowActivityIndicator(false)
                                self.puzzleAppearFunc()
                            }
                        })
                     
                        self.itemsContentView?.isHidden = false
                        self.backView?.isHidden = true
                        self.contentWebView?.isHidden = true
                        self.quesWebView?.isHidden = false
                        self.puzzleView.isHidden = false
                        self.puzzleView.frame.size.height = 500
                        let font = UIFont.systemFont(ofSize: 17)
                        let question = quesAnsDic.object(forKey: "question") as! NSString
                        
                        let body: String = ("<br>\(contentVal)\(question)" as NSString) as String
                        let htmlString: String = "<font size='4'>\(body)"
                        self.quesWebView?.loadHTMLString(htmlString, baseURL: nil)
                        
                        let height = self.widthForView(text:question as String, font: font, width: 0)
                        // print(height)
                        scrollViewYPos.constant = 10
                        itemsViewYPos.constant = 0
                        contentViewYPos.constant = 0
                        quesWebViewYPos.constant = 0
                        
                        
                        //self.progessDismiss()
                        if self.isOwner.isEqual(to: "IsOwner") {
                            self.submitBtn.isHidden = false
                            self.nextButton.isHidden = true
                            self.answerTxt.isHidden = true
                            self.createButtons(pos: height + 180)
                        }
                        else
                        {
                            self.nextButton.isHidden = false
                            self.backButton.isHidden = false
                            self.answerTxt.isHidden = true
                            self.createButtons(pos: height + 150)
                        }
                        
                    }
                }
                    
                else if question_type .isEqual(to: "audio")
                {
                    strAlertMsg = "Please tap on mic"
                    
                    if quesAnsDic.value(forKey: "answer") as? NSString != nil{
                        macButton.isHidden = false
                        macButtonTitle.isHidden = false
                        answer = quesAnsDic.object(forKey: "answer") as! NSString
                        getAnswer = answer as String
                        itemsContentView?.isHidden = false
                        backView?.isHidden = true
                        contentWebView?.isHidden = true
                        quesWebView?.isHidden = false
                        for i in 0..<arrData.count {
                            let options = self.arrData[i] as! NSDictionary
                            let correct =  (options.value(forKey: "options_id") as? Int)!
                            if serVerAns == correct {
                                answerIndex = i
                            }
                        }
                        
                        let font = UIFont.systemFont(ofSize: 17)
                        let question = quesAnsDic.object(forKey: "question") as? NSString
                        let body: String = ("\(contentVal)\(String(describing: question))" as NSString) as String
                        let htmlString: String = "<font size='4'>\(body)"
                        self.quesWebView?.loadHTMLString(htmlString, baseURL: nil)
                        
                        let height = self.widthForView(text:question! as String, font: font, width: 0)
                        //  print(height)
                        showImage(quesAnsDic: quesAnsDic, content: content, getVal:getVal)
                        
                        if isOwner.isEqual(to: "IsOwner") {
                            submitBtn.isHidden = false
                            nextButton.isHidden = true
                            answerTxt.isHidden = true
                            self.createButtons(pos: height + 180)
                        }
                        else
                        {
                            nextButton.isHidden = false
                            backButton.isHidden = false
                            answerTxt.isHidden = true
                            self.createButtons(pos: height + 150)
                        }
                    }
                }
                else if question_type .isEqual(to: "uploadphoto")
                {
                    
                    if quesAnsDic.value(forKey: "question") as? NSString != nil{
                        
                        itemsContentView?.isHidden = false
                        backView?.isHidden = true
                        contentWebView?.isHidden = true
                        quesWebView?.isHidden = false
                        
                        
                        let font = UIFont.systemFont(ofSize: 17)
                        let question = quesAnsDic.object(forKey: "question") as! NSString
                        let body: String = ("\(contentVal)\(question)" as NSString) as String
                        let htmlString: String = "<font size='4'>\(body)"
                        self.quesWebView?.loadHTMLString(htmlString, baseURL: nil)
                        
                        let height = self.widthForView(text:question as String, font: font, width: 0)
                        //print(height)
                        showImage(quesAnsDic: quesAnsDic, content: content, getVal:getVal)
                        if isOwner.isEqual(to: "IsOwner") {
                            submitBtn.isHidden = false
                            nextButton.isHidden = true
                            answerTxt.isHidden = true
                            self.createButtons(pos: height + 180)
                        }
                        else
                        {
                            nextButton.isHidden = false
                            backButton.isHidden = false
                            answerTxt.isHidden = true
                            self.createButtons(pos: height + 150)
                        }
                    }
                }
                else if question_type .isEqual(to: "dragndrop")
                {
                    answerTxt.isHidden = true
                    self.arrData.removeAllObjects()
                    if quesAnsDic.value(forKey: "answer") as? NSString != nil{
                        
                        
                        answer = quesAnsDic.object(forKey: "answer") as! NSString
                        getAnswer = answer as String
                        let arrGetDicData = quesAnsDic.value(forKey: "options") as? NSArray
                        self.arrData = NSMutableArray(array: arrGetDicData!)
                        itemsContentView?.isHidden = false
                        backView?.isHidden = true
                        contentWebView?.isHidden = true
                        quesWebView?.isHidden = false
                        for i in 0..<arrData.count {
                            let optionsDic = self.arrData[i] as! NSDictionary
                            let correct =  (optionsDic.value(forKey: "options_id") as? Int)
                            // print(arrData)
                            //print(optionsDic)
                            let ansCor = Int(answer as String)
                            if ansCor == correct {
                                answerIndex = i
                            }
                        }
                        print(quesAnsDic)
                        if ((quesAnsDic.object(forKey: "match_point") as? NSString) != nil)  {
                              matchDragDrop_point = quesAnsDic.object(forKey: "match_point") as! NSString
                        }
                      print(matchDragDrop_point)
                        
                        
                        let font = UIFont.systemFont(ofSize: 17)
                        let question = quesAnsDic.object(forKey: "question") as! NSString
                        arrMatch.add(question)
                        let body: String = ("\(contentVal)\(question)" as NSString) as String
                        let htmlString: String = "<font size='4'>\(body)"
                        self.quesWebView?.loadHTMLString(htmlString, baseURL: nil)
                        
                        var height = self.widthForView(text:question as String, font: font, width: 0)
                        // print(height)
                        showImage(quesAnsDic: quesAnsDic, content: content, getVal:getVal)
                        optionsArr = (quesAnsDic.value(forKey: "options")) as! NSArray
                        
                        if model .isEqual(to: "iPhone")
                        {
                            dragDropImgView = UIImageView(frame:CGRect(x:(self.view.frame.size.width/2)-63.5, y:height+30, width:120, height:120))
                        }
                        else
                        {
                            dragDropImgView = UIImageView(frame:CGRect(x:(self.view.frame.size.width/2)-57, y:height+30, width:120, height:120))
                        }
                        dragDropImgView.image = UIImage(named:"lineBorder.jpeg")
                        itemsContentView?.addSubview(dragDropImgView)
                        
                        
                        Droplabel = UILabel(frame: CGRect(x:5,y: 32,width: dragDropImgView.frame.size.width-10,height: 60))
                        Droplabel.textAlignment = .center
                        Droplabel.textColor = .gray
                        Droplabel.numberOfLines = 5
                        Droplabel.font = UIFont .systemFont(ofSize: 14)
                        Droplabel.text = "Drag and drop the correct image here"
                        dragDropImgView.addSubview(Droplabel)
                        
                        dragDropBckView = UIView(frame: CGRect(x:0 , y:Int(dragDropImgView.frame.origin.y + 160), width: Int(self.view.frame.size.width-20), height:150))
                        dragDropBckView.backgroundColor  = UIColor.clear
                        itemsContentView?.addSubview(dragDropBckView)
                        
                        
                        if optionsArr.count != 0
                        {
                            if arrayForPositionsOfImageOptions.count > 0
                            {
                                arrayForPositionsOfImageOptions.removeAllObjects()
                            }
                            var framePos = CGFloat()
                            framePos = 10
                            for i in 0 ..< optionsArr.count {
                                let urlImgsDic = optionsArr[i] as! NSDictionary
                                let urlImgs = urlImgsDic.object(forKey: "option")as? String
                                
                                let tagID = urlImgsDic.object(forKey: "options_id")as? NSNumber
                                imgIDArr.add(tagID)
                                let answer  = quesAnsDic.object(forKey: "answer")as? NSString
                                dragDropAnsewr = Int(answer as! String)!
                                
                                if model .isEqual(to: "iPhone")
                                {
                                    optionImageView = UIImageView.init(frame: CGRect(x:Int(framePos) , y:10, width: 120, height:120))
                                }
                                else
                                {
                                    optionImageView = UIImageView.init(frame: CGRect(x:Int(framePos) , y:10, width: 150, height:150))
                                }
                                
                                
                                optionImageView.isUserInteractionEnabled = true
                                optionImageView.tag = i+200
                                framePos = 0
                                panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.move))
                                panRecognizer.minimumNumberOfTouches = 1
                                panRecognizer.maximumNumberOfTouches = 1
                                panRecognizer.delegate = self
                                optionImageView.addGestureRecognizer(panRecognizer)
                                if i == 0
                                {
                                    if model .isEqual(to: "iPhone")
                                    {
                                        framePos = (self.view.frame.size.width/2)-60
                                    }
                                    else
                                    {
                                        framePos = (self.view.frame.size.width/2)-75
                                    }
                                }
                                else  if i == 1
                                {
                                    if model .isEqual(to: "iPhone")
                                    {
                                        framePos = self.view.frame.size.width - 130
                                    }
                                    else
                                    {
                                        framePos = self.view.frame.size.width - 160
                                    }
                                }
                                optionImageView.sd_setImage(with: URL(string: urlImgs!), placeholderImage: UIImage(named: ""))
                                
                                arrayForPositionsOfImageOptions.add(optionImageView.frame)
                                
                                dragDropBckView.addSubview(optionImageView)
                                
                                if i > 2
                                {
                                    height = 200
                                }
                                
                            }
                            
                            
                            macButtonTitle.isHidden = false
                            macButtonTitle.frame.origin.y = dragDropBckView.frame.origin.y +  dragDropBckView.frame.height + 10
                            macButtonTitle.text = ""
                            //Select correct images and drop in the above box
                            self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:macButtonTitle.frame.origin.y+100)
                        }
                        
                        
                        
                        
                        if isOwner.isEqual(to: "IsOwner") {
                            submitBtn.isHidden = false
                            nextButton.isHidden = true
                            self.createButtons(pos: height + 180)
                        }
                        else
                        {
                            nextButton.isHidden = false
                            backButton.isHidden = false
                            self.createButtons(pos: height + 150)
                        }
                    }
                }
                else if question_type .isEqual(to: "objective")
                {
                    option1Btn.isHidden = false
                    option2Btn.isHidden = false
                    option3Btn.isHidden = false
                    option4Btn.isHidden = false
                    if isOwner.isEqual(to: "IsOwner") {
                        submitBtn.isHidden = false
                        backButton.isHidden = false
                        nextButton.isHidden = true
                    }
                    else{
                        nextButton.isHidden = false
                        backButton.isHidden = false
                    }
                    contentWebView?.isHidden = true
                    backView?.isHidden = false
                    quesWebView?.isHidden = false
                    answerTxt.isHidden = true
                    
                    
                    if arrQuesAndAnswer.count != 0
                    {
                        print("arrQuesAndAnswer\(arrQuesAndAnswer)")
                        // self.progessDismiss()
                        let quesAns = self.arrQuesAndAnswer[getVal] as! NSDictionary
                        print("arrQuesAndAnswer\(quesAns)")
                        
                        var arrOptions = NSArray()
                        if ((quesAns.value(forKey: "options") as? NSArray) != nil) {
                             arrOptions = quesAns.value(forKey: "options") as! NSArray
                              arrData = NSMutableArray(array: arrOptions)
                        }  else {
                            
                              //arrData = quesAns
                        }
                        
                      
                        // print(arrData)
                        
                        if quesAns.value(forKey: "answer") as? NSString != nil{
                            
                            let answer = quesAns.value(forKey: "answer") as? NSString
                            if quesAns.value(forKey: "answer") as? NSString != nil{
                                serVerAns = Int(answer! as String)!
                            }
                            
                            
                            for i in 0..<arrData.count {
                                let options = self.arrData[i] as! NSDictionary
                                let correct =  (options.value(forKey: "options_id") as? Int)!
                                if serVerAns == correct {
                                    answerIndex = i
                                    getAnswer = options.value(forKey: "option") as! String
                                    
                                }
                            }
                            
                            
                            let font = UIFont.systemFont(ofSize: 17)
                            
                            
                            let question = quesAnsDic.object(forKey: "question") as! String
                            
                          
                            _ = self.widthForView(text:question as String, font: font, width: 0)
                            // print(height)
                            let appendString = (question as String)
                            self.createButtons(pos: self.labelHeight)
                            
                            let body: String = ("\(contentVal)\(appendString)" as NSString) as String
                            //let body: String = (appendString) as String
                            let htmlString: String = "<font size='4'>\(body)"
                            self.quesWebView?.loadHTMLString(htmlString, baseURL: nil)
                            
                            //Method to show images...
                            showImage(quesAnsDic: quesAnsDic, content: content, getVal:getVal)
                            
                            //Check if string contains a string
                            if question.contains("commemoration to those from Waterford City and County who lost their") {
                                 playAudioBtn.frame.origin.y = 7
                            }
                            // self.movePosition(pos: Int(height+80))
                            
                            var btn = NSDictionary()
                            if arrOptions.count != 0 {
                             btn = arrOptions[0] as! NSDictionary
                            }
                            let strBtn1 = btn.value(forKey: "option") as? String
                            self.option1Btn.setTitle(strBtn1, for: .normal)
                            if strBtn1 != nil {
                            //  self.progessDismiss()
                                if arrData.count == 1
                                {
                                    let btn1Height = self.widthForView(text:strBtn1!, font: font, width: 0)
                                    //print(btn1Height)
                                    self.option1Btn.frame.size.height = btn1Height+30
                                    option1Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                    option2Btn.isHidden = true
                                    option3Btn.isHidden = true
                                    option4Btn.isHidden = true
                                    
                                }
                                else if arrData.count <= 2
                                {
                                
                                let btn1Height = self.widthForView(text:strBtn1!, font: font, width: 0)
                                //print(btn1Height)
                                self.option1Btn.frame.size.height = btn1Height+30
                                option1Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                let btn2 = arrOptions[1] as! NSDictionary
                                let strBtn2 = btn2.value(forKey: "option") as? String
                                self.option2Btn.setTitle(strBtn2, for: .normal)
                                
                                let btn2Height = self.widthForView(text:strBtn2!, font: font, width: 0)
                                // print(btn2Height)
                                self.option2Btn.frame.size.height = btn2Height+30
                                
                                option2Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                
//                                let btn3 = arrOptions[2] as? NSDictionary
//                                let strBtn3 = btn3?.value(forKey: "option") as? String
//                                self.option3Btn.setTitle(strBtn3, for: .normal)
//
//                                let btn3Height = self.widthForView(text:strBtn2!, font: font, width: 0)
//                                // print(btn3Height)
//                                self.option3Btn.frame.size.height = btn3Height+30
//                                option3Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                
                                option3Btn.isHidden = true
                                option4Btn.isHidden = true
                            }
                            else if arrData.count > 2 && arrData.count < 4{
                                    let btn1Height = self.widthForView(text:strBtn1!, font: font, width: 0)
                                    //print(btn1Height)
                                    self.option1Btn.frame.size.height = btn1Height+30
                                    option1Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                    let btn2 = arrOptions[1] as! NSDictionary
                                    let strBtn2 = btn2.value(forKey: "option") as? String
                                    self.option2Btn.setTitle(strBtn2, for: .normal)
                                    
                                    let btn2Height = self.widthForView(text:strBtn2!, font: font, width: 0)
                                    // print(btn2Height)
                                    self.option2Btn.frame.size.height = btn2Height+30
                                    
                                    option2Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                    
                                    let btn3 = arrOptions[2] as? NSDictionary
                                    let strBtn3 = btn3?.value(forKey: "option") as? String
                                    self.option3Btn.setTitle(strBtn3, for: .normal)
                                    
                                    let btn3Height = self.widthForView(text:strBtn2!, font: font, width: 0)
                                    // print(btn3Height)
                                    self.option3Btn.frame.size.height = btn3Height+30
                                    option3Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                    
                                    option4Btn.isHidden = true
                                }
                        
                            else
                            {
                                
                                let btn1Height = self.widthForView(text:strBtn1!, font: font, width: 0)
                                // print(btn1Height)
                                self.option1Btn.frame.size.height = btn1Height+30
                                option1Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                
                                let btn2 = arrOptions[1] as! NSDictionary
                                let strBtn2 = btn2.value(forKey: "option") as? String
                                self.option2Btn.setTitle(strBtn2, for: .normal)
                                
                                let btn2Height = self.widthForView(text:strBtn2!, font: font, width: 0)
                                //  print(btn2Height)
                                self.option2Btn.frame.size.height = btn2Height+30
                                
                                option2Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                
                                let btn3 = arrOptions[2] as! NSDictionary
                                let strBtn3 = btn3.value(forKey: "option") as? String
                                self.option3Btn.setTitle(strBtn3, for: .normal)
                                
                                let btn3Height = self.widthForView(text:strBtn2!, font: font, width: 0)
                                // print(btn3Height)
                                self.option3Btn.frame.size.height = btn3Height+30
                                option3Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                
                                let btn4 = arrOptions[3] as! NSDictionary
                                let strBtn4 = btn4.value(forKey: "option") as? String
                                self.option4Btn.setTitle(strBtn4, for: .normal)
                                let btn4Height = self.widthForView(text:strBtn2!, font: font, width: 0)
                                // print(btn3Height)
                                self.option4Btn.frame.size.height = btn4Height+30
                                option4Btn.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                                
                            }
                            option1Btn.setTitleColor(UIColor (red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
                            option2Btn.setTitleColor(UIColor (red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
                            option3Btn.setTitleColor(UIColor (red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
                            option4Btn.setTitleColor(UIColor (red: 255/255, green: 255/255, blue: 255/255, alpha: 1), for: .normal)
                                if arrData.count == 0 {
                                    option1Btn.isHidden = true;
                                    option2Btn.isHidden = true;
                                    option3Btn.isHidden = true;
                                    option4Btn.isHidden = true;
                                    
                                }
                            
                 
                            }
                        }
                       
                    }
                    
                    
                }
            }
        }
        
    }
    
    func move(_ sender: UIPanGestureRecognizer ) {
        view.bringSubview(toFront: sender.view!)
        
        var translatedPoint: CGPoint = sender.translation(in: sender.view!.superview)
        // print("TAP IMAGE: \(sender.view!.tag)")
        if sender.state == .began {
            firstX = (sender.view?.center.x)!
            firstY = (sender.view?.center.y)!
            
        }
        
        selectOption = 0
        translatedPoint = CGPoint(x: CGFloat((sender.view?.center.x)! + translatedPoint.x), y: CGFloat((sender.view?.center.y)! + translatedPoint.y))
        sender.view?.center = translatedPoint
        sender.setTranslation(CGPoint.zero, in: sender.view)
        
        
        let velocityX: CGFloat = (0.2 * sender.velocity(in: view).x)
        let velocityY: CGFloat = (0.2 * sender.velocity(in: view).y)
        var finalX: CGFloat = translatedPoint.x + velocityX
        let finalY: CGFloat = translatedPoint.y + velocityY
        // translatedPoint.y + (.35*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
        if finalX < 0 {
            finalX = 0
        }
        else if finalX > view.frame.size.width {
            finalX = view.frame.size.width
        }
        // print("The X pos of image \(finalX)")
        //print("The Y pos of image \(finalY)")
        
        //   print("The X pos of DragDropImage \(self.dragDropImgView.frame.origin.x)")
        // print("The Y pos of DragDropImage \(self.dragDropImgView.frame.origin.y)")
        
        let xPosWidth = self.dragDropImgView.frame.origin.x + self.dragDropImgView.frame.size.width
        
        if sender.state == .ended {
            if (finalX > self.dragDropImgView.frame.origin.x && finalX < xPosWidth) || (finalY < -160 && finalY > -10) {
                // to avoid status bar
                // finalY = 50
                if model .isEqual(to: "iPhone")
                {
                    // self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:750)
                }
                else{
                    self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:700)
                }
               // let origin: CGPoint = optionImageView.frame.origin
                //print(origin)
                
                Droplabel.isHidden = true
                let urlImgsDic = optionsArr[((sender.view?.tag)! - 200)] as! NSDictionary
                let option = urlImgsDic.object(forKey: "option")as! String
                
                dragDropImgView.sd_setImage(with: URL(string: option), placeholderImage: UIImage(named: ""))
                
                //  dragDropImgView.backgroundColor = UIColor .red
                macButtonTitle.isHidden = true
                //dragDropBckView.isHidden = true
                
                let optionsID = urlImgsDic.object(forKey: "options_id")as! Int
                dragDropimgOptionsID = optionsID
                selectOption = 1
                answerLabel.frame.origin.y = dragDropImgView.frame.origin.y + dragDropImgView.frame.size.height
                
                //print("Sender View Tag value\(sender.view?.tag)")
                
                for i in arrayForPositionsOfImageOptions{
                    if arrayForPositionsOfImageOptions.index(of: i) != (sender.view?.tag)!-200
                    {
                        self.view?.viewWithTag((arrayForPositionsOfImageOptions.index(of: i))+200)?.isHidden = false
                        self.view?.viewWithTag((arrayForPositionsOfImageOptions.index(of: i))+200)?.frame = i as! CGRect
                    }
                    else
                    {
                        self.view?.viewWithTag((arrayForPositionsOfImageOptions.index(of: i))+200)?.isHidden = true
                    }
                    
                }
            }
            else{
                sender.view?.center.x = firstX
                sender.view?.center.y = firstY
            }
            
            quesWebView?.isHidden = false
            
        }
        
    }
    func addImageInImageview(tagVal: Int)
    {
        let urlImgsDic = optionsArr[tagVal] as! NSDictionary
        let option = urlImgsDic.object(forKey: "option")as! String
        
        dragDropImgView.sd_setImage(with: URL(string: option), placeholderImage: UIImage(named: ""))
    }
    func showImage(quesAnsDic: NSDictionary, content : NSString, getVal: Int )
    {
        var url = NSURL()
        self.progessDismiss()
        let value: AnyObject? = quesAnsDic.value(forKey: "image") as AnyObject?
        if question_type .isEqual(to: "dragndrop")
        {
            changeScrollPos(content: content)
        }
        else if(value is NSString)
        {
            if model .isEqual(to: "iPhone")
            {
                if question_type .isEqual(to: "content") && btnTag > 0
                {
                    playAudioBtn.frame.origin.y = 18
                }
                else
                {
                    if contentVal .isEqual(NSNull.self)
                    {
                        playAudioBtn.frame.origin.y = 0
                    }
                    else
                    {
                        if btnTag > 0
                        {
                            playAudioBtn.frame.origin.y = 18
                        }
                        else{
                            playAudioBtn.frame.origin.y = 23
                        }
                    }
                }
            }
            else
                
            {
                if question_type .isEqual(to: "content") && btnTag > 0
                {
                    playAudioBtn.frame.origin.y = 26
                }
                else
                {
                    if contentVal .isEqual(NSNull.self)
                    {
                        playAudioBtn.frame.origin.y = 0
                    }
                    else
                    {
                        if btnTag > 0
                        {
                            playAudioBtn.frame.origin.y = 26
                        }
                        else{
                            playAudioBtn.frame.origin.y = 23
                        }
                    }
                }
            }
            if model .isEqual(to: "iPhone")
            {
                quesWebViewYPos.constant = 0
                contentViewYPos.constant = 0
                if playAudioBtn.isHidden == false
                {
                    scrollViewYPos.constant = (slideUiView?.frame.origin.y)! + (slideUiView?.frame.size.height)! - 55
                }
                else
                {
                    scrollViewYPos.constant = (slideUiView?.frame.origin.y)! + (slideUiView?.frame.size.height)! - 75
                }
            }
            else
            {
                quesWebViewYPos.constant = 0
                contentViewYPos.constant = 0
                if playAudioBtn.isHidden == false
                {
                    scrollViewYPos.constant = (slideUiView?.frame.origin.y)! + (slideUiView?.frame.size.height)! - 55
                }
                else
                {
                    scrollViewYPos.constant = (slideUiView?.frame.origin.y)! + (slideUiView?.frame.size.height)! - 75
                }
            }
            url = NSURL(string:(quesAnsDic.value(forKey: "image") as! String))!
            
            let str = String(describing: url)
            let stringUrl = str as NSString
            let sepArr = stringUrl.components(separatedBy: ",")
            
            //print(sepArr)
            
            
            let arrSlideImages = NSMutableArray()
            for i in 0..<sepArr.count
            {
                
                let strUrl = sepArr[i] as String
                arrSlideImages.add(strUrl)
                
                if i == sepArr.count - 1 {
                    self.createSlidingView(arrImages: arrSlideImages)
                }
                
                
                self.backView?.isHidden = true
                self.welcomlogoImage?.isHidden = true
                self.itemsContentView?.isHidden = false
                
            }
        }
        else
        {
            changeScrollPos(content: content)
        }
    }
    func changeScrollPos(content : NSString)
    {
        playAudioBtn.frame.origin.y = 0
        slideUiView?.isHidden = true
        backView?.isHidden = true
        if playAudioBtn.isHidden == false
        {
            scrollViewYPos.constant = 40
        }
        else
        {
            scrollViewYPos.constant = 10
        }
        itemsViewYPos.constant = 0
        contentViewYPos.constant = 0
        self.slideUiView?.isHidden = true
        let font = UIFont.systemFont(ofSize: 18)
        let height = self.widthForView(text:content as String, font: font, width: 0)
        //print(height)
        if question_type .isEqual(to: "objective") {
            
        }
        else  if question_type .isEqual(to: "subbjective") {
            
            
        }
        else{
            self.labelHeight = height+50
            self.createButtons(pos: self.labelHeight)
        }
        
        self.progessDismiss()
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        firstTime = 1
        
        
        webView.frame.size.height = 80
        webView.frame.size = (webView.sizeThatFits(.zero))
        
        if question_type .isEqual(to: "subjective")
        {
            self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:(self.quesWebView?.frame.origin.y)! + (self.quesWebView?.frame.size.height)!+170)
        }
        else  if question_type .isEqual(to: "objective")
        {
            
            // movePosition(pos: Int((quesWebView?.frame.origin.y)! + (quesWebView?.frame.size.height)! + 120))
            self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:(self.option4Btn.frame.origin.y) + (self.option4Btn.frame.size.height)+60)
        }
        else if question_type .isEqual(to: "audio")
        {
            
            macButton.frame.origin.y = (webView.frame.origin.y) + (webView.frame.size.height)+60
            macButtonTitle.frame.origin.y = macButton.frame.origin.y + 80
            
            
            self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:(self.macButton.frame.origin.y) + (self.macButton.frame.size.height)+200)
        }
        else if question_type .isEqual(to: "dragndrop")
        {
            let indexvalue = arrMatch[0] as! String
            if indexvalue.contains("Cromwell") || matchDragDrop_point.contains("Body") || matchDragDrop_point.contains("body")
            {
                makeCartoonView?.isHidden = false
                headImgView?.isHidden = false
                makeCartoonView?.frame.origin.y = (webView.frame.origin.y) + (webView.frame.size.height)-85
                dragDropImgView.frame.origin.y = (headImgView?.frame.origin.y)! + (headImgView?.frame.size.height)!+16
                dragDropBckView.frame.origin.y = (dragDropImgView.frame.origin.y) + (dragDropImgView.frame.size.height)
            }
//            else if matchDragDrop_point .isEqual(to: "") {
//
//            }
            else if indexvalue.contains("find my legs") || matchDragDrop_point.contains("Legs") || matchDragDrop_point.contains("legs")
            {
                makeCartoonView?.isHidden = false
                headCapImgView?.isHidden = true
                headImgView?.isHidden = false
                bodyImgView?.isHidden = false
                leftArmsImgView?.isHidden = false
                rightArmsImgView?.isHidden = false
                
                leftLegImgView?.isHidden = true
                rightLegImgView?.isHidden = true
                if model .isEqual(to: "iPhone")
                {
                    makeCartoonView?.frame.origin.y = (webView.frame.origin.y) + (webView.frame.size.height)-45
                    if self.view.frame.size.height>700
                    {
                        dragDropImgView.frame.origin.y = (bodyImgView?.frame.origin.y)! + (bodyImgView?.frame.size.height)!+22
                    }
                    else
                    {
                        dragDropImgView.frame.origin.y = (bodyImgView?.frame.origin.y)! + (bodyImgView?.frame.size.height)!+27
                    }
                    
                    dragDropBckView.frame.origin.y = (dragDropImgView.frame.origin.y) + (dragDropImgView.frame.size.height)
                }
                else
                    
                {
                    makeCartoonView?.frame.origin.y = (webView.frame.origin.y) + (webView.frame.size.height)-72
                    dragDropImgView.frame.origin.y = (bodyImgView?.frame.origin.y)! + (bodyImgView?.frame.size.height)!
                    dragDropBckView.frame.origin.y = (dragDropImgView.frame.origin.y) + (dragDropImgView.frame.size.height)
                }
            }
            else
            {
                makeCartoonView?.isHidden = false
                headCapImgView?.isHidden = true
                headImgView?.isHidden = false
                bodyImgView?.isHidden = false
                leftArmsImgView?.isHidden = false
                rightArmsImgView?.isHidden = false
                leftLegImgView?.isHidden = false
                rightLegImgView?.isHidden = false
                
                
                makeCartoonView?.frame.origin.y = (webView.frame.origin.y) + (webView.frame.size.height)+18
                dragDropImgView.frame.origin.y = (webView.frame.origin.y) + (webView.frame.size.height)+50
                dragDropBckView.frame.origin.y = (makeCartoonView?.frame.origin.y)! + (makeCartoonView?.frame.size.height)!-60
            }
            
            // answerLabel.frame.origin.y =  (dragDropBckView.frame.origin.y) + (dragDropBckView.frame.size.height)+10
            macButtonTitle.frame.origin.y =  (dragDropBckView.frame.origin.y) + (dragDropBckView.frame.size.height)+10
            self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:(self.dragDropBckView.frame.origin.y) + (self.dragDropBckView.frame.size.height)+350)
            
            
        }
        else if question_type .isEqual(to: "unscramble")
        {
            puzzleView.frame.origin.y = (webView.frame.origin.y) + (webView.frame.size.height)+10
            self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:(self.puzzleView.frame.origin.y) + (self.puzzleView.frame.size.height)+200)
            answerLabel.frame.origin.y =  self.puzzleView.frame.origin.y + self.puzzleView.frame.size.height + 280
            macButtonTitle.frame.origin.y = self.puzzleView.frame.origin.y + self.puzzleView.frame.size.height + 100
        }
        else if question_type .isEqual(to: "content")
        {
            self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:(self.contentWebView?.frame.origin.y)! + (self.contentWebView?.frame.size.height)!+100)
        }
        self.scroll_View?.setContentOffset(CGPoint(x:0, y:0), animated: true)
        
        self.answerTxt.frame.origin.y = (webView.frame.origin.y) + (webView.frame.size.height)+30
       // if question_type .isEqual(to: "uploadphoto") || ID == 32 || ID == 38 || ID == 58 || ID == 89
          if ((!uploadPicLat .isEmpty && !uploadPicLong .isEmpty) || ID == 32 || ID == 38 || ID == 58 || ID == 89)
        {
            answerLabel.frame.origin.y =  (webView.frame.origin.y) + (webView.frame.size.height)+10
        }
        let posit = CGFloat((webView.frame.origin.y) + (webView.frame.size.height))
        txtFieldPosition = posit
        movePosition(pos: Int(posit) + 50)
        
        if arrData.count == 0 {
            option1Btn.isHidden = true;
            option2Btn.isHidden = true;
            option3Btn.isHidden = true;
            option4Btn.isHidden = true;
            
        }
    }
    func txtPaddingVw(txt:UITextField) {
        let paddingView = UIView(frame: CGRect (x:0, y:0 , width:10 , height:20))
        txt.leftViewMode = .always
        txt.leftView = paddingView
    }
    
    func movePosition(pos: Int) {
        
        submitBtn.isUserInteractionEnabled = true
        uploadBtn.isUserInteractionEnabled = true
        if arrID .contains(ID) {
            hideSubmit()
        }
        else
        {
            if question_type .isEqual(to: "unscramble")
            {
                puzzleView.isHidden = false
                //if isOwner.isEqual(to: "IsOwner") {
                
                //                }
                //                else
                //                {
                //                    self.puzzleAppearFunc()
                //                   // puzzleView.isUserInteractionEnabled = false
                //                }
            }
        }
        self.option1Btn.frame.origin.y = CGFloat(pos)
        self.option2Btn.frame.origin.y =   (self.option1Btn.frame.origin.y) + ((self.option1Btn.frame.size.height)+25)
        self.option3Btn.frame.origin.y =   (self.option2Btn.frame.origin.y) + ((self.option2Btn.frame.size.height)+25)
        
        self.option4Btn.frame.origin.y =   (self.option3Btn.frame.origin.y) + ((self.option3Btn.frame.size.height)+25)
        
        
        
        //if question_type .isEqual(to: "uploadphoto") || ID == 32 || ID == 38 || ID == 58 || ID == 89
          if ((!uploadPicLat .isEmpty && !uploadPicLong .isEmpty) || ID == 32 || ID == 38 || ID == 58 || ID == 89)
        {
            
        }
        else
        {
            self.answerLabel.frame.origin.y =   (self.option4Btn.frame.origin.y) + ((self.option4Btn.frame.size.height)+3)
        }
        if isOwner.isEqual(to: "IsOwner") {
           // if question_type .isEqual(to: "uploadphoto") || ID == 32 || ID == 38 || ID == 58 || ID == 89
              if ((!uploadPicLat .isEmpty && !uploadPicLong .isEmpty) || ID == 32 || ID == 38 || ID == 58 || ID == 89)
            {
                submitBtn.isHidden = true
                uploadBtn.isHidden = false
            }
            backButton.isHidden = false
        }
        else
        {
            submitBtn.isHidden = true
            backButton.isHidden = false
            uploadBtn.isHidden = true
        }
        if question_type .isEqual(to: "objective")
        {
            self.scroll_View?.contentSize = CGSize(width: self.view.frame.size.width,height:(self.option4Btn.frame.origin.y) + (self.option4Btn.frame.size.height)+120)
        }
    }
    func widthForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 15, y: 0, width: self.view.frame.size.width-30, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    func alertBar() {
        alertView = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        let message  = strAlertMsg
        let messageMutableString = NSMutableAttributedString(string: message, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 18.0)!])
        
        let matchStr = strAlertMsg as NSString
        if matchStr .isEqual(to: "Your answer is correct")
        {
            messageMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.green, range: NSRange(location:0,length:message.characters.count))
        }
        else if matchStr .isEqual(to: "Your answer is incorrect")
        {
            messageMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red, range: NSRange(location:0,length:message.characters.count))
        }
        else if matchStr .isEqual(to: "Puzzle is completed")
        {
            messageMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.green, range: NSRange(location:0,length:message.characters.count))
        }
        else
        {
            messageMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location:0,length:message.characters.count))
            
        }
        alertLogoImgView =  UIImageView(frame: CGRect(x: -15, y: -15, width: 22, height: 22))
        alertLogoImgView.image = UIImage(named: "logo.png")
        alertView.view.addSubview(self.alertLogoImgView)
        
        
        alertView.setValue(messageMutableString, forKey: "attributedMessage")
        
        
        self.present(alertView, animated: true, completion: nil)
        
        self.performOperation(alertToBeClosedOnFinish: alertView)
        // change to desired number of seconds (in this case 5 seconds)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
//            // your code with delay
//            self.alertView.dismiss(animated: true, completion: nil)
//        }
    }
    
    func performOperation(alertToBeClosedOnFinish: UIAlertController) {
        let hideAlertController = { () -> Void in
            alertToBeClosedOnFinish.dismiss(animated: false, completion: nil)
        }
        DispatchQueue.global().asyncAfter(deadline: .now() + 1, execute: {
            DispatchQueue.main.async {
                hideAlertController();
            }
        });
    }
    
    func finalResultAlerBar()  {
        resultAlert = UIAlertController(title: nil, message:strAlertMsg as String , preferredStyle: UIAlertControllerStyle.alert)
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.removeValue()
            self.okAction()
        }
        resultAlert.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(self.resultAlert, animated: true, completion: nil)
        }
    }
    
    func noDataAlert(message: String)  {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            _ = self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func okAction() {
        //        backView?.isHidden = true
        //        itemsContentView?.isHidden = true
        //
        //        congratsScrollView?.isHidden = true
        //        scroll_View?.isHidden = true
        //        welcomeView?.isHidden = true
        
        
        // _ = self.navigationController?.popViewController(animated: true)
        
        self.completeTourFunc(tourID: String(describing:tourID), userID: userID, completion: { (result) in
            
            self.congratsScrollView?.isHidden = true
            self.scroll_View?.isHidden = true
            self.welcomeView?.isHidden = true
        })
        
        //   / self.completeTourFunc(tourID: String(describing:self.tourID), userID: userID)
        
        
    }
    func showGroupCodeAlert(title: String,message: String)
    {
        
        //var message = strAlertMsg as NSString
        //if message .isEqual(to: "No internet connection")
        //{
        // message = "No internet connection"
        //  }
        notInterNetAlert = UIAlertController(title: title+"\n"+message, message:nil, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.OkAction()
        }
        notInterNetAlert.addAction(okBtn)
        DispatchQueue.main.async {
            self.present(self.notInterNetAlert, animated: true, completion: nil)
        }
        
        
        
    }
    func OkAction() {
        
        notInterNetAlert.dismiss(animated: true, completion: nil)
    }
    func getQuestionaireData(index:Int, completion:@escaping (_ result: Bool?) -> Void)
    {
        if Reachability.shared.isConnectedToNetwork() {
            
            SVProgressHUD .show()
            //DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
            let url = "http://beta.brstdev.com/tours/api/web/v1/tours/questionair"
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            
            var tourid = Int()
            tourid = self.tourID
            
            if self.tourID == 1 || self.tourID == 4
            {
                tourid = 1
                self.congratsScreenLogo?.image = UIImage(named:"logo.png")
                
            }
            else
            {
                self.congratsScreenLogo?.image = UIImage(named:"Tour Details  vs.9 First  Tour.jpg")
            }
            let params:[String: AnyObject] = ["tour_id" : tourid as AnyObject,"user_id" : userID as AnyObject]
            //print(params)
            //print(params)
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                
                
                
                let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                    if let response = response {
                        let nsHTTPResponse = response as! HTTPURLResponse
                        let statusCode = nsHTTPResponse.statusCode
                        print ("status code = \(statusCode)")
                    }
                    if let error = error {
                        print ("\(error)")
                        self.strAlertMsg = "The network connection was lost."
                        self.alertBar()
                        
                    }
                    if let data = data {
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            print ("getQuestionaireData =>> \(jsonResponse)")
                            
                            var dataDic = NSDictionary()
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                                
                                var strSuccess_code = jsonResponse ["status_code"] as! NSString
                                
                                let strDescription = jsonResponse ["description"] as! String
                                if strSuccess_code .isEqual(to: "400")
                                {
                                    
                                    self.noDataAlert(message: strDescription)
                                    completion(false)
                                }
                                else
                                {
                                    
                                    dataDic = jsonResponse.value(forKey: "data") as! NSDictionary
                                    strSuccess_code = dataDic ["Success_code"] as! NSString
                                    
                                    //print(dataDic)
                                    if strSuccess_code .isEqual(to: "201")
                                    {
                                        DispatchQueue.main.async {
                                            let arrGetData = dataDic.object(forKey: "tour_questionairs") as! NSArray
                                            
                                            self.arrQuesAndAnswer = NSMutableArray(array: arrGetData)
                                            print(self.arrQuesAndAnswer)
                                            self.getData(val: index)
                                            self.btnTag = index+1
                                            
                                        }
                                        
                                        completion(true)
                                    }
                                        
                                    else
                                    {
                                        self.strAlertMsg = strDescription
                                        
                                        
                                    }
                                }
                            }
                                
                            else
                            {
                                self.strAlertMsg = "Something went wrong."
                                self.alertBar()
                                completion(false)
                            }
                            
                        }catch _ {
                            self.strAlertMsg = "Server error"
                            self.alertBar()
                            completion(false)
                        }
                        
                    }
                })
                task.resume()
            }catch let error {
                self.progessDismiss()
                self.strAlertMsg = (error as! NSString) as String
                self.serverNotResponding()
                print(error)
            }
            //  }
        }
        else
        {
            self.OkAction()
        }
    }
    
    func completeTourFunc(tourID: String, userID: String,completion: @escaping (_ result: String)->Void)
    {
        //declare parameter as a dictionary which contains string as key and value combination. considering inputs are valid
         SVProgressHUD.show()
        
        let score: String!
        
        if isOwner .isEqual(to: "IsOwner") {
            score = scroedLabel?.text!
        }
        else
        {
            score = ""
        }
        let parameters = ["tour_id": tourID, "user_id": userID, "score": score] as [String : String]
        
        var toID = String()
        
        if UserDefaults.standard.value(forKey: "TourId") != nil
        {
            toID = UserDefaults.standard.value(forKey: "TourId") as! String
        }
        toID.append(","+tourID)
        UserDefaults.standard.set(toID, forKey: "TourId")
        
        //create the url with URL
        let url = URL(string: "http://beta.brstdev.com/tours/api/web/v1/tours/completeTour")! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    // print(json)
                    
                    var dataDic = NSDictionary()
                    self.progessDismiss()
                    if json.value(forKey: "description") as? String != nil
                    {
                        var desc = json.value(forKey: "description") as! String
                        if json.value(forKey: "data") != nil
                        {
                            dataDic = json.value(forKey: "data") as! NSDictionary
                            desc = dataDic.value(forKey: "description") as! String
                            
                            let success_code = dataDic.value(forKey: "Success_code") as! String
                            self.removeSavedData()
                            if success_code .contains("401") {
                                self.strAlertMsg = desc
                                self.alertBar()
                                let when = DispatchTime.now() + 2
                                DispatchQueue.main.asyncAfter(deadline: when){
                                    // your code with delay
                                    
                                    self.congratsScrollView?.isHidden = true
                                    self.scroll_View?.isHidden = true
                                    self.welcomeView?.isHidden = true
                                    //UserDefaults.standard.removeObject(forKey: "QuestionView")
                                    _ = self.navigationController?.popViewController(animated: true)
                                    //}
                                }
                                
                            }
                            
                        }
                    }
                    else
                    {
                        self.progessDismiss()
                        self.strAlertMsg = "Connection failed"
                        self.finalResultAlerBar()
                    }
                }
            } catch let error {
                
                print(error.localizedDescription)
                self.progessDismiss()
                self.strAlertMsg = "Saved Successfully"
                self.alertBar()
                let when = DispatchTime.now() + 2
                DispatchQueue.main.asyncAfter(deadline: when){
                    _ = self.navigationController?.popViewController(animated: true)
                }
                
                
            }
        })
        task.resume()
        
    }
    func removeSavedData()
    {
        UserDefaults.standard.removeObject(forKey: "SavedTour")
        UserDefaults.standard.removeObject(forKey: "saveTourDetail")
        UserDefaults.standard.removeObject(forKey: "correctAns")
        UserDefaults.standard.removeObject(forKey: "wrongAns")
        UserDefaults.standard.removeObject(forKey: "arrID")
        UserDefaults.standard.removeObject(forKey: "SavedID")
        UserDefaults.standard.removeObject(forKey: "JoinGroupTourID")
    }
    func serverNotResponding()
    {
        self.progessDismiss()
        let alertView = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertView, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alertView.dismiss(animated: true, completion: nil)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        answerTxt.resignFirstResponder()
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension QuesAnsViewController: CLLocationManagerDelegate {
    
    func initialLocMethod() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        
        CurrentLocLatVal = locValue.latitude
        CurrentLocLangVal = locValue.longitude
        
       //  currentLocLat = locValue.latitude
        // currentLocLong = locValue.longitude
        
        print(currentLocLat)
         print(currentLocLong)
        
        //let loc1 = CLLocation(latitude:CurrentLocLatVal, longitude: CurrentLocLangVal)
        //let loc3 = CLLocation(latitude: Double("30.7069315")!, longitude: Double("76.6885024")!)
        
       // let distance = loc1.distance(from: loc3)
        //showLocationUpdates(message: String(describing:distance))
    }
    
}
extension UIView {
    
    func firstResponder() -> UIView? {
        if self.isFirstResponder {
            return self
        }
        
        for subview in self.subviews {
            if subview.isFirstResponder {
                return subview
            }
        }
        
        return nil
    }
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
