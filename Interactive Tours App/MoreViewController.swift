//
//  MoreViewController.swift
//  Interactive Tours App
//
//  Created by Brst on 22/06/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit
import AVFoundation
import MessageUI
class MoreViewController: UIViewController,UITabBarControllerDelegate ,UIScrollViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var moreViewheight: NSLayoutConstraint!
    @IBOutlet weak var phoneNumYPos: NSLayoutConstraint!
    @IBOutlet weak var moreOptionView : UIView?
    @IBOutlet weak var myProfileBtn : UIButton?
    @IBOutlet weak var logOutBtn : UIButton?
    @IBOutlet weak var aboutUsBtn : UIButton?
    @IBOutlet weak var contactUsBtn : UIButton?
    @IBOutlet weak var scrollView : UIScrollView?
    @IBOutlet weak var profileContentView : UIView?
    @IBOutlet weak var aboutUsContentView : UIView?
    @IBOutlet weak var contactUsContentView : UIView?
    @IBOutlet weak var headingLabel : UILabel?
    @IBOutlet weak var profileImageView : UIImageView?
    @IBOutlet weak var myPointBtn : UIButton?
    @IBOutlet weak var localInfoWebView : UIWebView!
    
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var LastNameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneNumTxt: UITextField!
    @IBOutlet weak var countryTxt: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var createProfileView: UIView!
    @IBOutlet weak var editProfileBtn: UIButton!
      @IBOutlet weak var speechButton: UIButton!
    
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userSubNameLbl: UILabel!
    @IBOutlet weak var occupationLbl: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var emailLabel: UILabel!
    
    //@IBOutlet weak var purchasedTourList: UIButton!
    @IBOutlet weak var editUserProfileView: UIView!
    @IBOutlet weak var editUserProfileImgView: UIImageView!
    
    @IBOutlet weak var showGroupCodeView = UIView()
   
    @IBOutlet weak var upDownArrowImgView = UIImageView()
    
    var shwoGrouptag = NSInteger()
    
    var arrGroupCode = NSMutableArray()
   var arrTourHistList = NSMutableArray()
    var arrUpCommTourList = NSMutableArray()
    
    var userImage = UIImage()
    var strProfileCheck = NSString()
    
    var profielAlertView = UIAlertController()
    var groupCountAlert = UIAlertController()
    var strAlertMsg = NSString()
    
    @IBOutlet weak var createProfileCheck = NSString()
    @IBOutlet weak var table_View = UITableView()
    @IBOutlet weak var groupCodeBtn = UIButton()
    @IBOutlet weak var purcTourListBtn = UIButton()
    
    @IBOutlet weak var myToursBtn: UIButton!
    @IBOutlet weak var myTourHistBtn: UIButton!
    @IBOutlet weak var upComToursBtn: UIButton!
    @IBOutlet weak var myPointsBtn: UIButton!
    
    
    @IBOutlet weak var headerLabel = UILabel()
    var statusCode = NSString()
    var purcTourTag = NSString()
     @IBOutlet weak var tourDetailsView = UIView()
     @IBOutlet weak var tourDetailsImgView = UIImageView()
     @IBOutlet weak var tourDetailsTxtView = UITextView()
     @IBOutlet weak var tourDetailsSpeakBtn = UIButton()
     @IBOutlet weak var backButton = UIButton()
    @IBOutlet weak var backIcon: UIImageView!
     @IBOutlet weak var tourNamelabel = UILabel()
     let speechSynthesizer = AVSpeechSynthesizer()
     var logoutAlertView = UIAlertController()
    
    var readMoreCheck = Int()
    @IBOutlet weak var submitTopPos: NSLayoutConstraint!
      @IBOutlet weak var tourDetailsTxtVHeight: NSLayoutConstraint!
     @IBOutlet weak var speechBtnYPos: NSLayoutConstraint!
    var tapGesture: UITapGestureRecognizer!
    var speechTag = Int()
    var readMoreButton = UIButton()
      @IBOutlet weak var tourDetailsLabel: UILabel!
    @IBOutlet var userNameFieldYpos: NSLayoutConstraint!
    @IBOutlet var LastNameFieldYpos: NSLayoutConstraint!
     @IBOutlet var emailFieldYpos: NSLayoutConstraint!
     @IBOutlet var phoneFieldYpos: NSLayoutConstraint!
     @IBOutlet var countryFieldYpos: NSLayoutConstraint!
 
    func sendEmail() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(MoreViewController.tapFunction))
        emailLabel.isUserInteractionEnabled = true
        emailLabel.addGestureRecognizer(tap)
    }
    
    func tapFunction(sender:UITapGestureRecognizer) {
        sendMail()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        sendEmail()
        
          UserDefaults.standard.removeObject(forKey: "TourController")
        
        table_View?.separatorStyle = .none
          profileImageView?.contentMode = .scaleAspectFill
         profileImageView?.contentMode = .scaleAspectFill
        //upComToursBtn.frame.origin.y = myToursBtn.frame.origin.y
      //  groupCodeBtn?.frame.origin.y = myPointsBtn.frame.origin.y
        
        backButton?.isHidden = true
        backIcon.isHidden = true
        headerLabel?.text = "My profile"
        tourDetailsTxtView?.isScrollEnabled = false
        tourDetailsTxtView?.isUserInteractionEnabled = false
        readMoreCheck = 0
        
        tourDetailsTxtView?.frame.size.height = 130
        tourDetailsLabel.frame.size.height = 130
        readMoreCheck = 0
        //descriptionTxtview.backgroundColor = UIColor .green
        // descriptionTxtview.frame.size.height = 100
        let pos = self.view.frame.size.width/2 as CGFloat
            
        readMoreButton = UIButton(frame: CGRect(x:pos, y: ((tourDetailsLabel?.frame.size.height)!-20), width: self.view.frame.size.width, height: 20))
        readMoreButton.setTitle("Read More...", for: .normal)
        readMoreButton.backgroundColor = UIColor .white
        readMoreButton.setTitleColor(UIColor (red: 108/255, green: 2/255, blue: 111/255, alpha: 1), for: .normal)
        
        readMoreButton.contentHorizontalAlignment = .left
        readMoreButton.titleLabel?.font = UIFont.italicSystemFont(ofSize: 16)
        readMoreButton.addTarget(self, action:#selector(readMoreAction(sender:)), for: .touchUpInside)
        tourDetailsView?.addSubview(readMoreButton)

        //readMoreButton.titleEdgeInsets = UIEdgeInsetsMake(4, 10.0, 0.0, 0.0)
        //readMoreButton.titleEdgeInsets = UIEdgeInsetsMake(10,10,10,10)
        
        //make the buttons content appear in the top-left
        readMoreButton.contentHorizontalAlignment = .left
        readMoreButton.contentVerticalAlignment = .top
        
        //move text 10 pixels down and right
        
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(MoreViewController.dismissView))
        createProfileView?.addGestureRecognizer(tapGesture)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(MoreViewController.dismissView))
        profileContentView?.addGestureRecognizer(tap)
        
        submitButton.setTitle("Sign in", for: .normal)
       // UserDefaults.standard.set("4", forKey: "ClickedTabIndex")
        scrollView?.isHidden=true
        tourDetailsView?.isHidden = true
        scrollView?.delegate=self
        table_View?.delegate = self
        makeAwareOfKeyboard()
        showGroupCodeView?.isHidden = true
        shwoGrouptag = 0
       // tourDetailsTxtView?.layer.borderColor = UIColor .lightGray .cgColor
        //tourDetailsTxtView?.layer.borderWidth = 0.5
    
        // submitButton.frame.size.width = self.view.frame.size.width-20
        
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MoreViewController.hideView))
//       // tap.delegate = self
//        table_View?.canCancelContentTouches = true
//      scrollView?.addGestureRecognizer(tap)
        
       
            if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
            if (UserDefaults.standard.value(forKey: "ProfileImage") != nil)
            {
            let imageData: Data? = UserDefaults.standard.object(forKey: "ProfileImage") as! Data?
            let image = UIImage(data: imageData!)
            editUserProfileImgView.image = image
            profileImageView?.image = image
            }
            
            
        }
        aboutUsContentView?.isHidden=true
        contactUsContentView?.isHidden=true
        moreOptionView?.isHidden=false
        profileImageView?.layer.masksToBounds = false
        profileImageView?.layer.cornerRadius = (profileImageView?.frame.height)!/2
        profileImageView?.clipsToBounds = true
        editUserProfileImgView?.layer.masksToBounds = false
        editUserProfileImgView?.layer.cornerRadius = (editUserProfileImgView?.frame.height)!/2
        editUserProfileImgView?.clipsToBounds = true
        
        
       // profileImageView?.contentMode = .scaleAspectFill
        
        firstNameTxt.isEnabled = true
        LastNameTxt.isEnabled = true
        emailTxt.isEnabled = true
        phoneNumTxt.isEnabled = true
        let border = CALayer()
        let width = CGFloat(1.0)
        border.frame = CGRect(x: 0, y:  firstNameTxt.frame.size.height - width, width:   self.view.frame.size.width, height: firstNameTxt.frame.size.height)
        border.borderWidth = width
        firstNameTxt.layer.addSublayer(border)
        firstNameTxt.layer.masksToBounds = true
        
        
        
        let border4 = CALayer()
        let width4 = CGFloat(1.0)
        border4.frame = CGRect(x: 0, y:  LastNameTxt.frame.size.height - width4, width:   self.view.frame.size.width, height: emailTxt.frame.size.height)
        border4.borderWidth = width4
        LastNameTxt.layer.addSublayer(border4)
        LastNameTxt.layer.masksToBounds = true
        
        
        let border2 = CALayer()
        let width2 = CGFloat(1.0)
        border2.frame = CGRect(x: self.view.frame.origin.x+0, y: emailTxt.frame.size.height - width2, width:  self.view.frame.size.width-0, height: emailTxt.frame.size.height)
        border2.borderWidth = width2
        emailTxt.layer.addSublayer(border2)
        emailTxt.layer.masksToBounds = true
        
        let border3 = CALayer()
        let width3 = CGFloat(1.0)
        border3.frame = CGRect(x: self.view.frame.origin.x+0, y: emailTxt.frame.size.height - width2, width:  self.view.frame.size.width-0, height: emailTxt.frame.size.height)
        border3.borderWidth = width3
        phoneNumTxt.layer.addSublayer(border3)
        phoneNumTxt.layer.masksToBounds = true
        
        let border8 = CALayer()
        let width8 = CGFloat(1.0)
        border8.frame = CGRect(x: self.view.frame.origin.x+0, y: emailTxt.frame.size.height - width8, width:  self.view.frame.size.width-0, height: emailTxt.frame.size.height)
        border8.borderWidth = width8
        countryTxt.layer.addSublayer(border8)
        countryTxt.layer.masksToBounds = true
        
        
        
        phoneNumTxt.keyboardType = UIKeyboardType.decimalPad
        txtPaddingVw(txt: firstNameTxt!)
        txtPaddingVw(txt: LastNameTxt!)
        txtPaddingVw(txt: emailTxt!)
        txtPaddingVw(txt: phoneNumTxt!)
      
        txtPaddingVw(txt: countryTxt!)
        
        
        submitButton.layer.cornerRadius = 20
       
        createProfileView.isHidden = true
        
        firstNameTxt.delegate = self
        LastNameTxt.delegate = self
        emailTxt.delegate = self
        phoneNumTxt.delegate = self
      
             countryTxt.delegate = self
        editUserProfileView.isHidden = true
        
        
        table_View?.delegate = self
        
       phoneNumTxt.tag = 4
       // 0956f9851cd283c6bf7da8cf7786dd09c9b5f1aa
       //  readMoreButton.frame.origin.y = (tourDetailsTxtView?.frame.origin.y)! + (tourDetailsTxtView?.frame.size.height)!-35
         readMoreButton.frame.size.width = self.view.frame.size.width
         //UserDefaults.standard.set("4", forKey: "ClickedTabIndex")
        
        scrollView?.isScrollEnabled = true
        scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height: 500)
       // self.textView?.setContentOffset(CGPoint.zero, animated: false)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneNumTxt {
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    @IBAction func readMoreAction(sender: UIButton)
    {
        if readMoreCheck == 0
        {
            readMoreButton.setTitle("Show Less", for: .normal)
            let fixedWidth = tourDetailsTxtView?.frame.size.width
            tourDetailsTxtView?.sizeThatFits(CGSize(width: fixedWidth!, height: CGFloat.greatestFiniteMagnitude))
            let newSize = tourDetailsTxtView?.sizeThatFits(CGSize(width: fixedWidth!, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = tourDetailsTxtView?.frame
            newFrame?.size = CGSize(width: max((newSize?.width)!, fixedWidth!), height: (newSize?.height)!)
            tourDetailsTxtView?.frame = newFrame!;
            tourDetailsLabel?.frame = newFrame!;
            readMoreButton.frame.origin.y = (tourDetailsTxtView?.frame.origin.y)! + (tourDetailsTxtView?.frame.size.height)!-10
             speechButton?.frame.origin.y = (tourDetailsTxtView?.frame.origin.y)! + (tourDetailsTxtView?.frame.size.height)!+1
            readMoreButton.frame.origin.x = 10
            readMoreButton.frame.size.width = 200
            readMoreButton.frame.size.height = 20
             //scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:(readMoreButton.frame.origin.y)+((readMoreButton.frame.size.height))+20)
            
            readMoreCheck = 1
//speechBtnYPos
        }
        else
        {
            movePositions()
        }
    }
    func movePositions()
    {
        readMoreCheck = 0
        readMoreButton.frame.size.height = 20
        readMoreButton.backgroundColor = UIColor .white
        let pos = self.view.frame.size.width/2 as CGFloat
        readMoreButton.frame.origin.x = pos
        readMoreButton.setTitle("Read More...", for: .normal)
        tourDetailsTxtView?.frame.size.height = 130
        tourDetailsLabel.frame.size.height = 130

       readMoreButton.frame.origin.y = (tourDetailsTxtView?.frame.origin.y)! + (tourDetailsTxtView?.frame.size.height)!-20
         speechButton?.frame.origin.y = (tourDetailsTxtView?.frame.origin.y)! + (tourDetailsTxtView?.frame.size.height)!+10
         readMoreButton.frame.size.width = self.view.frame.size.width
        
      //  scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:(readMoreButton.frame.origin.y)+((readMoreButton.frame.size.height))+100)
        
    }
    @IBAction func tourDetailsSpeakAction (sender:UIButton)
    {
        // Line 1. Create an instance of AVSpeechSynthesizer.
        if speechTag == 0
        {
            speechTag = 1
        // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.
        let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: tourDetailsTxtView!.text)
        //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
        speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 3.0
        // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        // Line 5. Pass in the urrerance to the synthesizer to actually speak.
        speechSynthesizer.speak(speechUtterance)
            strAlertMsg = "Audio started"
            alertBar()

        
        }
        else
        {
            speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
            speechTag = 0;
            strAlertMsg = "Audio stopped"
            alertBar()

        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  purcTourTag .isEqual(to: "PurcTourList") {
            return arrTourHistList.count
        }
        else  if  purcTourTag .isEqual(to: "upComingTours") {
            return arrUpCommTourList.count
        }
        else {
            return arrGroupCode.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table_View?.dequeueReusableCell(withIdentifier: "Cell") as! GroupNameTableViewCell
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        table_View?.separatorColor = UIColor.clear
     
        if  purcTourTag .isEqual(to: "PurcTourList") {
            cell.completeLabel?.text = "Completed"
            cellData(cell: cell, indexPath: indexPath,DataArray: arrTourHistList)
        }
        else if purcTourTag .isEqual(to: "upComingTours") {
            cell.completeLabel?.text = "Pending"
            cellData(cell: cell, indexPath: indexPath,DataArray: arrUpCommTourList)
        }
        else {
            cell.purcCellContentView?.isHidden = true
            cell.cellContentView?.isHidden = false
            let groupData = self.arrGroupCode[indexPath.row] as! NSDictionary
            
            if groupData.value(forKey: "tour_name") as! String != ""{
                let tourName = groupData.value(forKey: "tour_name") as? String
                cell.tourNameLabel?.text = "Dungarvan - "+tourName!
                cell.tourNameLabel.numberOfLines = 2
               cell.groupNamelabel?.text = groupData.value(forKey: "group_name") as? String
                print(groupData.value(forKey: "purchase_date") as? String as Any)
            cell.dateLabel?.text = getLocalDateTime(fromUTC: (groupData.value(forKey: "create_at") as? String)!)
            }
        }
        return cell
    }
    
    
    
    func getLocalDateTime(fromUTC strDate: String) -> String {
        let dtFormat = DateFormatter()
        dtFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dtFormat.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        
        let aDate: Date? = dtFormat.date(from: strDate)
        dtFormat.dateFormat = "dd/MM/yyyy hh:mm a"
        dtFormat.timeZone = NSTimeZone.system
        return dtFormat.string(from: aDate!)
    }
    func cellData(cell: GroupNameTableViewCell,indexPath: IndexPath,DataArray: NSMutableArray)  {
        print(indexPath.row )
        cell.purcCellContentView?.isHidden = false
        cell.cellContentView?.isHidden = true
        let strTourList = DataArray[indexPath.row] as! NSDictionary
        print(strTourList)
        let tourName = strTourList.value(forKey: "tour_name") as! String
            cell.label1?.text = "Dungarvan - "+tourName
        cell.scoreView?.isHidden = false
        if strTourList.value(forKey: "score") as? String != nil {
            let  score = strTourList.value(forKey: "score") as? NSString
            cell.label1?.frame.size.width = self.view.frame.size.width - 250
            if (score?.isEqual(to: ""))! {
                cell.scoreLabel?.text = "Check your group score on the team leader's device"
            }
            else {
            cell.scoreLabel?.text = "Overall Score: "+(score! as String)

            }
        }
        else {
            cell.scoreView?.isHidden = true
           // DispatchQueue.main.async {
                 cell.label1?.frame.size.width = self.view.frame.size.width - 120
           // }
           
        }
        
        cell.completedDateLbl?.text = getLocalDateTime(fromUTC: (strTourList.value(forKey: "completed_date") as? String)!)

        
        cell.purcImageView?.sd_setImage(with: URL(string: strTourList.value(forKey: "image_large") as! String), placeholderImage: UIImage(named: "demoImage.jpg"))
        

        cell.completeLabel?.layer.cornerRadius = 5
        cell.completeLabel?.layer.masksToBounds = true

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
//      movePositions()
//        if  purcTourTag .isEqual(to: "PurcTourList") {
//            self.backButton?.isHidden = false
//            tourDetailsTxtView?.frame.size.height = 140
//            
//          readMoreButton.frame.origin.y = (tourDetailsLabel?.frame.origin.y)! + (tourDetailsLabel?.frame.size.height)!-20
//            tableView.isHidden = true
//            tourDetailsView?.isHidden = false
//            scrollView?.isHidden = false
//            purcTourTag = "Tour Details"
//            let strCities = self.arrGroupCode[indexPath.row] as! NSDictionary
//            let tour_name = strCities.value(forKey: "tour_name") as? String
//            tourNamelabel?.text = tour_name
//             //let tour_id = strCities.value(forKey: "tour_id") as? String
//           // headerLabel?.text = tour_name
//            
//            let tour_description = strCities.value(forKey: "tour_description") as? String
//            tourDetailsTxtView?.text = tour_description
//            tourDetailsLabel.text = tour_description
//            let cell: GroupNameTableViewCell! = (table_View!.cellForRow(at: indexPath)) as! GroupNameTableViewCell
//            tourDetailsImgView?.image = cell.purcImageView?.image
//            
//             scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:(readMoreButton.frame.origin.y)+((readMoreButton.frame.size.height))+100)
//        }
//       
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
         if  purcTourTag .isEqual(to: "PurcTourList") || purcTourTag .isEqual(to: "upComingTours") {
            return 150
        }
         else{
        return 130
        }
    }
    
    func txtPaddingVw(txt:UITextField) {
        let paddingView = UIView(frame: CGRect (x:0, y:0 , width:10 , height:20))
        txt.leftViewMode = .always
        txt.leftView = paddingView
    }
    func saveView() {
        //Save View
        UserDefaults.standard.set("JOIN_VIEW", forKey: "SavedViewController")
    }
    override func viewWillDisappear(_ animated: Bool) {
         speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
    }
    override func viewWillAppear(_ animated: Bool) {
        saveView()
        IQKeyboardManager.shared().isEnabled = true
        //Remove All Arr
        removeAllArrItems()
        
        speechTag  = 0
        emailTxt.isHidden  = false
        countryTxt.isHidden  = false
        localInfoWebView.isHidden = true
        showGroupCodeView?.isHidden = true
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
        speechTag = 0;
         self.tabBarController?.delegate=self
        hideKeypad()

        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
            UserDefaults.standard.removeObject(forKey: "MoreView")
            moreViewheight.constant = 237
                       
           scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
           
           
            if self.strProfileCheck.isEqual(to: "EditProfile")
            {
                 submitButton.setTitle("Update", for: .normal)
                 createProfileView?.isHidden=false
                 purcTourTag = "EditProfile"
                moreOptionView?.isHidden = true
                editUserProfileView.isHidden = false
                
            }
            else
            {
                createProfileView?.isHidden=true
                scrollView?.isHidden=false
                profileContentView?.isHidden = false
                headerLabel?.text = "My Profile"
                if (UserDefaults.standard.value(forKey: "username") != nil)
                {
                    showProfileData()
                }
                moreOptionView?.isHidden = true
                
            }

            //Show Number of tours History
            self.tourHistory(completion: {(result) in
                
            })
            //Show Number of UpcommingTours
            self.upComingTours(completion: {(result)in
                
            })
            //Show Number of Group Tour List
            getGroupList(completion: { (result) in
                
            })
            
        }
        else{
            
             UserDefaults.standard.set("MoreView", forKey: "MoreView")
            
            submitButton.setTitle("Submit", for: UIControlState .normal)
            if UserDefaults.standard.object(forKey: "JoinGroupCheck") != nil
            {
                DispatchQueue.main.async {
                    self.headerLabel?.text = "My Profile"
                    self.backButton?.isHidden = true
                    self.backIcon.isHidden = true
                    self.purcTourTag = ""
                    self.firstNameTxt.text = ""
                    self.LastNameTxt.text = ""
                    self.phoneNumTxt.text = ""
                self.phoneNumTxt.frame.origin.y=self.firstNameTxt.frame.origin.y+70
                self.submitButton.frame.origin.y=self.phoneNumTxt.frame.origin.y+75
                self.emailTxt.isHidden  = true
                self.countryTxt.isHidden  = true
                self.moreOptionView?.isHidden = true
                }
            }
            else
            {
                okAction()
            }
        
         //   logOutBtn?.isHidden = true
            moreViewheight.constant = 175
          //  scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height: 460)
            createProfileView?.isHidden=false
            profileContentView?.isHidden = true
            scrollView?.isHidden = false
            
            
            
        }
        
        
       // strProfileCheck = ""
    }
    @IBAction func submitButtonAction (sender :UIButton)
    {
        emailTxt.isUserInteractionEnabled = true
        phoneNumTxt.isUserInteractionEnabled = true
        if UserDefaults.standard.object(forKey: "JoinGroupCheck") != nil
        {
            if firstNameTxt.text?.isEmpty ?? true {
                
                strAlertMsg = "Please enter first name"
                alertBar()
            }
            else if LastNameTxt.text?.isEmpty ?? true {
                
                strAlertMsg = "Please enter last name"
                alertBar()
            }
            else if phoneNumTxt.text?.isEmpty ?? true
            {
                strAlertMsg = "Please enter phone number"
                alertBar()
            }
            else
            {
                createProfile()
            }
        }
        else if strProfileCheck.isEqual(to: "EditProfile")
        {
            if firstNameTxt.text?.isEmpty ?? true {
                
                strAlertMsg = "Please enter first name"
                alertBar()
            }
            else if LastNameTxt.text?.isEmpty ?? true {
                
                strAlertMsg = "Please enter last name"
                alertBar()
            }
            else if phoneNumTxt.text?.isEmpty ?? true
            {
                strAlertMsg = "Please enter phone number"
                alertBar()
            }
            else
            {
                editProfile()
            }
        }
        else
        {
        if firstNameTxt.text?.isEmpty ?? true {
            
            strAlertMsg = "Please enter first name"
            alertBar()
        }
       else if LastNameTxt.text?.isEmpty ?? true {
            
            strAlertMsg = "Please enter last name"
            alertBar()
        }
        else if emailTxt.text?.isEmpty ?? true
        {
            strAlertMsg = "Please enter email id"
            alertBar()
        }
        else if !isValidEmail(testStr: emailTxt.text!)
        {
            strAlertMsg = "Please enter valid email"
            alertBar()
            
            print("not valid")
        }
        else if phoneNumTxt.text?.isEmpty ?? true
        {
            strAlertMsg = "Please enter phone number"
            alertBar()
        }
    
        
        else if countryTxt.text?.isEmpty ?? true
        {
            strAlertMsg = "Please enter country"
            alertBar()
        }
            
        else{
            
            if strProfileCheck.isEqual(to: "EditProfile") {
                editProfile()
            }
            else
            {
                createProfile()
            }
            
        }
        }
        
    }
    
    
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool
    {
        
//        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
//        {
//        }
//        else
//        {
        strProfileCheck = ""
        let selectedIndex = tabBarController.selectedIndex
        _ = String(selectedIndex)
       // UserDefaults.standard.set(selectInd, forKey: "ClickedTabIndex")
        print("Selected shouldSelect: \(tabBarController.selectedIndex)")
        if tabBarController.selectedIndex == 4
        {
            self.view?.addSubview(scrollView!)
            self.view?.addSubview(moreOptionView!)
            moreOptionView?.isHidden=false
        }
      //  }
        return (viewController != tabBarController.selectedViewController);
    }
    func dispatchQueue()
    {
        DispatchQueue.main.async {
            self.alertBar()
            
        }
    }
    @IBAction func myProfileBtnAction (sender : UIButton){
        localInfoWebView.isHidden = true
        aboutUsContentView?.isHidden = true
        submitButton.setTitle("sign in", for: .normal)
        showGroupCodeView?.isHidden = true
        headerLabel?.text = "My Profile"
        movePositions()
     
        
        if (UserDefaults.standard.value(forKey: "profileCreated") != nil)
        {
            scrollView?.isHidden=false
            profileContentView?.isHidden=false
            aboutUsContentView?.isHidden=true
            moreOptionView?.isHidden=true
            contactUsContentView?.isHidden=true
            editUserProfileView.isHidden = true
            createProfileView.isHidden = true
          
            scrollView?.addSubview(profileContentView!)
        }
        else
        {
            UserDefaults.standard.removeObject(forKey: "JoinGroupCheck")
            DispatchQueue.main.async {
                self.phoneNumTxt.frame.origin.y=self.emailTxt.frame.origin.y+75
                self.submitButton.frame.origin.y=self.countryTxt.frame.origin.y+75
                self.emailTxt.isHidden  = false
                self.countryTxt.isHidden  = false

            self.moreOptionView?.isHidden = true
                
            }
             profileAlertbar()
        }
      
    }
    
    func showProfileData()
    {
        
        let username = UserDefaults.standard.value(forKey: "username")as! String
        userSubNameLbl.text = username
        let firstname = UserDefaults.standard.value(forKey: "firstname")as! String
        let lastname = UserDefaults.standard.value(forKey: "lastname")as! String
        let appendName: String? = String(firstname) + " " + String(lastname)
        userNameLbl.text = appendName as String?
        let occupation = UserDefaults.standard.value(forKey: "occupation")as! String
        occupationLbl.text = occupation as String?
        
        let country = UserDefaults.standard.value(forKey: "country")as! String
        let appendString = country
        locationLabel.text = appendString as String?
        
    }
    func editProfileData()
    {
        purcTourTag = "EditProfile"
        strProfileCheck = "EditProfile"
       // DispatchQueue.main.async {
            self.submitButton.setTitle("Update", for: .normal)
       // }
        
        emailTxt.isUserInteractionEnabled = false
        phoneNumTxt.isUserInteractionEnabled = false
        let firstName = UserDefaults.standard.value(forKey: "firstname")as! String
        firstNameTxt.text = firstName
        
        let LastName = UserDefaults.standard.value(forKey: "lastname")as! String
        LastNameTxt.text = LastName
        
        let email = UserDefaults.standard.value(forKey: "email")as! String
        emailTxt.text = email
        
        let phone = UserDefaults.standard.value(forKey: "phone")as! String
        phoneNumTxt.text = phone
        
        
        let country = UserDefaults.standard.value(forKey: "country")as! String
        countryTxt.text = country
   
        
    }
    func dismissView() {
        moreOptionView?.isHidden = true
        self.view.removeGestureRecognizer(tapGesture)
    }
    @IBAction func editProfileButton (sender : UIButton){
        
        showGroupCodeView?.isHidden = true
        moreOptionView?.isHidden = true
        localInfoWebView.isHidden = true
        aboutUsContentView?.isHidden = true
        hideKeypad()
        tourDetailsView?.isHidden = true
        headerLabel?.text = "Edit profile"
        backButton?.isHidden = false
        backIcon.isHidden = false
        contactUsContentView?.isHidden = true
        if (UserDefaults.standard.value(forKey: "profileCreated") != nil) {
            emailTxt.textColor = UIColor .lightGray
            phoneNumTxt.textColor = UIColor .lightGray
            moreOptionView?.isHidden = true
            scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height: 550)
            editUserProfileView.isHidden = false
            createProfileView.isHidden = false
            profileContentView?.isHidden = true
            scrollView?.isHidden = false
            scrollView?.bringSubview(toFront: profileContentView!)
            DispatchQueue.main.async {
            self.firstNameTxt.frame.origin.y=150
            self.LastNameTxt.frame.origin.y=150
            
            self.emailTxt.frame.origin.y=225
            self.phoneNumTxt.frame.origin.y=300
            
            self.countryTxt.frame.origin.y=375
            
            
           self.submitButton.frame.origin.y=self.countryTxt.frame.origin.y+100
          
            
            self.editProfileData()
            }
        }
        else{
           profileAlertbar()
        }
        
       
    }
    
    /// listen to keyboard event
    func makeAwareOfKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidHide), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
   
    /// move scroll view up
    func keyBoardDidShow(notification: Notification) {
        
        //scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:700)
        
//        let info = notification.userInfo as NSDictionary?
//        let rectValue = info![UIKeyboardFrameBeginUserInfoKey] as! NSValue
//        let kbSize = rectValue.cgRectValue.size
//        
//        let contentInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0)
//        self.scrollView?.contentInset = contentInsets
//        self.scrollView?.scrollIndicatorInsets = contentInsets
        
       // scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:300)
        
    }
    
    /// move scrollview back down
    func keyBoardDidHide(notification: Notification) {
//        DispatchQueue.main.async {
//            self.scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:600)
//        }
        
        // restore content inset to 0
//        let contentInsets = UIEdgeInsetsMake(0, 0, 0, 0)
//        self.scrollView?.contentInset = contentInsets
//        self.scrollView?.scrollIndicatorInsets = contentInsets
    }


    @IBAction func logOutBtnAction (sender : UIButton){
        //headerLabel?.text = "HighJinx Tours"
        showGroupCodeView?.isHidden = true
        strAlertMsg = "Are you sure you want to logout?. All your information related to groups join have been lost once you logout."
        LogOutAlertBar()
    }
    @IBAction func aboutUsBtnAction (sender : UIButton){
         headerLabel?.text = "Local Information"
        //call Local Info Web View
        showPdfFile(pdfUrl: URL(string:"http://beta.brstdev.com/tours/uploads/Things_to_Do_in_Area.pdf")!)
        
        showGroupCodeView?.isHidden = true
        scrollView?.isHidden=false
        profileContentView?.isHidden=true
        aboutUsContentView?.isHidden=true
        moreOptionView?.isHidden=true
        contactUsContentView?.isHidden=true
        headingLabel?.text="Local Information"
        editUserProfileView.isHidden = true
        createProfileView.isHidden = true
        localInfoWebView.isHidden = false
        table_View?.isHidden = true
        tourDetailsView?.isHidden = true
        scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
    }
    @IBAction func contactUsBtneAction (sender : UIButton){
        headerLabel?.text = "Contact Us"
        scrollView?.isHidden=false
        profileContentView?.isHidden=true
        aboutUsContentView?.isHidden=false
        moreOptionView?.isHidden=true
        contactUsContentView?.isHidden=false
        headingLabel?.text="Contact Us"
        editUserProfileView.isHidden = true
        localInfoWebView.isHidden = true
        table_View?.isHidden = true
         tourDetailsView?.isHidden = true
        createProfileView.isHidden = true
       scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == scrollView {
            let
            a = scrollView.contentOffset
            if a.y <= 0 {
                scrollView.contentOffset = CGPoint.zero
                // this is to disable tableview bouncing at top.
            }
        }
        
    }
    
    
    func okAction()
    {
        editUserProfileView.isHidden = true
        createProfileView.isHidden = false
        scrollView?.addSubview(createProfileView)
        profileContentView?.isHidden = true
        
        
    
        
        
        
        if UserDefaults.standard.object(forKey: "JoinGroupCheck") != nil
        {
//            emailTxt.isHidden  = true
//            countryTxt.isHidden  = true
//            phoneNumYPos.constant =  -70
//            moreOptionView?.isHidden = true
//            submitBtnYPOS.constant = -50
//            //readMoreButton.isUserInteractionEnabled = false
            //submitBtnYPOS.constant = 10
            
        }
        else
        {
            dataProfileshow()
        }
        
       // scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height: 460)
        scrollView?.isHidden=false
        createProfileView?.isHidden=false
        profileContentView?.isHidden=true
        aboutUsContentView?.isHidden=true
        moreOptionView?.isHidden=true
        contactUsContentView?.isHidden=true
        strProfileCheck = "CreateProfile"
    }
    func dataProfileshow()
    {
        firstNameTxt.frame.origin.y=20
        firstNameTxt.text = ""
        
        LastNameTxt.frame.origin.y=20
        LastNameTxt.text = ""
        
        emailTxt.frame.origin.y=95
        emailTxt.text = ""
        
        phoneNumTxt.frame.origin.y=170
        phoneNumTxt.text = ""
        
        countryTxt.frame.origin.y=245
        countryTxt.text = ""
        submitButton.frame.origin.y=320

    }
    func cancelAction()
    {

    }
    func alertBar()
    {
        // the alert view
        let alert = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
        self.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 1 seconds)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
    }
    func profileAlertbar()
    {
        profielAlertView = UIAlertController(title: nil, message:"Firstly, You have to create Account Profile" as String?, preferredStyle: UIAlertControllerStyle.alert)
        let canclBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.cancelAction()
        }
        let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.okAction()
        }
        profielAlertView.addAction(canclBtn)
        profielAlertView.addAction(okBtn)
        //  DispatchQueue.main.sync {
        //  }
        self.present(profielAlertView, animated: true, completion: nil)
        
        
    }
    func groupCountAlertBar()
    {
        
            groupCountAlert = UIAlertController(title: nil, message:strAlertMsg as String?, preferredStyle: UIAlertControllerStyle.alert)
            let okBtn = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alertAction) -> Void in
                self.headerLabel?.text = "My Profile"
                self.groupCountOkBtn()
            }
        
            groupCountAlert.addAction(okBtn)
            //  DispatchQueue.main.sync {
            //  }
            self.present(groupCountAlert, animated: true, completion: nil)
            
            

    }
    func groupCountOkBtn()
    {
        groupCountAlert.dismiss(animated: true, completion: nil)
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        print(emailTest)
        return emailTest.evaluate(with: testStr)
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeypad()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        hideKeypad()
        moreOptionView?.isHidden=true
    }
    func hideKeypad()
    {
        firstNameTxt.resignFirstResponder()
        LastNameTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        phoneNumTxt.resignFirstResponder()
        countryTxt.resignFirstResponder()
    }
    @IBAction func editProfileCameraButton(sender: UIButton)
    {
        moreOptionView?.isHidden = true
        
        let alertController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        alertController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Open Camera", style: .default)
        { _ in
            print("Open Camera")
            self.openCamera()
        }
        alertController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Open Gallery", style: .default)
        { _ in
            print("Open Gallery")
            self.openGallary()
        }
        alertController.addAction(deleteActionButton)
        
        alertController.popoverPresentationController?.sourceView = self.view
        
        alertController.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.size.width / 2.0,y: self.view.bounds.size.height / 2.0,width: 1.0,height: 1.0)
        
        self.present(alertController, animated: true, completion: nil)
        
        
        
        
    }
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        userImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        
        self.dismiss(animated: true, completion: nil)
        
        moreOptionView?.isHidden = true
        editUserProfileView.isHidden = false
        //SVProgressHUD.show()
        
        var userID = String()
        if (UserDefaults.standard.value(forKey: "user_id") != nil)
        {
            userID =  UserDefaults.standard.value(forKey: "user_id") as! String
        }
        
        self.imageUploadRequest(image: userImage, parameters: ["user_id": userID], keyName: "profile_image") { (result) in
        print("hello")
        
        }
        
        
    }
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func progessDismiss()
    {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    func progessShow()
    {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    func createProfile()
    {
        moreOptionView?.isHidden = true
        if Reachability.shared.isConnectedToNetwork(){
            
            SVProgressHUD .show()
           // DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
               self.progessDismiss()
            }
                let url = "http://beta.brstdev.com/tours/api/web/v1/tours/userprofile"
                let session = URLSession.shared
                let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
                request.httpMethod = "POST"
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                
                
                let params:[String: AnyObject] = ["firstname" : self.firstNameTxt.text as AnyObject,"lastname" : self.LastNameTxt.text as AnyObject,"email" : self.emailTxt.text as AnyObject,"phone" : self.phoneNumTxt.text as AnyObject,"state" : "state" as AnyObject,"city" : "city"as AnyObject,"occupation" : "occupation" as AnyObject,"country" : self.countryTxt.text as AnyObject]
               // print(params)
                do{
                    request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                    let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                        if let response = response {
                            let nsHTTPResponse = response as! HTTPURLResponse
                            let statusCode = nsHTTPResponse.statusCode
                            print ("status code = \(statusCode)")
                        }
                        if let error = error {
                            print ("\(error)")
                        }
                        if let data = data {
                            do{
                                let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                                print ("Json Data is = \(jsonResponse)")
                                
                                var dataDic = NSDictionary()
                                
                                
                                    
                                    var strSuccess_code = jsonResponse ["status_code"] as! NSString
                                    
                                    let strDescription = jsonResponse ["description"] as! NSString
                                    
                                    if strSuccess_code .isEqual(to: "400")
                                    {
                                        self.strAlertMsg = strDescription
                                        self.dispatchQueue()
                                        SVProgressHUD.dismiss()
                                    }
                                    else
                                    {
                                        if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                                        {
                                             _ = jsonResponse ["message"] as! NSString
                                             SVProgressHUD.dismiss()
                                        dataDic = jsonResponse.value(forKey: "data") as! NSDictionary
                                        print ("Profile Data = \(dataDic)")
                                        strSuccess_code = dataDic ["Success_code"] as! NSString
                                        
                                        
                                        if strSuccess_code .isEqual(to: "201")
                                        {
                                          //  UserDefaults.standard.removeObject(forKey: "JoinGroupCheck")
                                            //self.logOutBtn?.isHidden = false
                                            self.moreViewheight.constant = 237
                                            UserDefaults.standard.set("", forKey: "CreateProfile")
                                            
                                            UserDefaults.standard.set("profileCreatedSuccessFully", forKey: "profileCreated")
                                            
                                            if dataDic ["email"] as? NSString != nil
                                            {
                                            let  city = dataDic ["city"] as! NSString
                                            UserDefaults.standard.set(city, forKey: "city")
                                            
                                            let  country = dataDic ["country"] as! NSString
                                            
                                            if (country.isEqual(to: "") || country.isEqual(to: "nil"))
                                            {
                                                
                                                UserDefaults.standard.set(self.countryTxt.text, forKey: "country")
                                            }
                                            else
                                            {
                                                UserDefaults.standard.set(country, forKey: "country")
                                            }
                                            UserDefaults.standard.set(country, forKey: "country")
                                            
                                            let  email = dataDic ["email"] as! NSString
                                            UserDefaults.standard.set(email, forKey: "email")
                                            
                                            let  firstname = dataDic ["firstname"] as! NSString
                                            UserDefaults.standard.set(firstname, forKey: "firstname")
                                            
                                            let  lastname = dataDic ["lastname"] as! NSString
                                            UserDefaults.standard.set(lastname, forKey: "lastname")
                                            
                                            let  occupation = dataDic ["occupation"] as! NSString
                                            UserDefaults.standard.set(occupation, forKey: "occupation")
                                            
                                            let  phone = dataDic ["phone"] as! NSString
                                            UserDefaults.standard.set(phone, forKey: "phone")
                                            
                                            let  profile_image = dataDic ["profile_image"] as? NSString
                                            UserDefaults.standard.set(profile_image, forKey: "profile_image")
                                            
                                            let  state = dataDic ["state"] as! NSString
                                            UserDefaults.standard.set(state, forKey: "state")
                                            
                                            let user_idStr = dataDic.value(forKey: "user_id") as! Int
                                            let stringID = String(user_idStr)
                                          
                                            UserDefaults.standard.set(stringID, forKey: "user_id")
                                 
                                            let  username = dataDic ["username"] as! NSString
                                            UserDefaults.standard.set(username, forKey: "username")
                                            self.strAlertMsg = "profile created successFully"
                                            DispatchQueue.main.async {
                                                self.showProfileData()
                                                self.hideKeypad()
                                                self.submitButton.setTitle("Update", for: .normal)
                                                self.editUserProfileView.isHidden = true
                                                self.createProfileView.isHidden = true
                                                 self.profileContentView?.isHidden = false
                                                self.alertBar()
                                                SVProgressHUD.dismiss()
                                              
                                                let getVal = UserDefaults.standard.value(forKey:"ClickedTabIndex") as? String
                                                let selectInd = Int(getVal!)
                                                
                                                if selectInd == 0
                                                {
                                                    UserDefaults.standard.set("0", forKey: "HomeView")
                                                }
                                                else if selectInd == 1
                                                {
                                                    UserDefaults.standard.set("1", forKey: "TourView")
                                                }
                                                
                                                self.tabBarController?.selectedIndex = selectInd!
                                                
                                            }
                                            
                                            SVProgressHUD.dismiss()
            
                                            self.scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                                            
                                        }
                                            else
                                            {
                                                self.strAlertMsg = strDescription
                                                self.dispatchQueue()
                                                SVProgressHUD.dismiss()
                                            }
                                        }
                                        else
                                        {
                                            self.strAlertMsg = strDescription
                                            self.dispatchQueue()
                                            self.scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                                            
                                            
                                            
                                        }
                                    }
                                    
                                    
                                        else
                                        {
                                            SVProgressHUD.dismiss()
                                            self.strAlertMsg = "Server Not Responding"
                                            self.alertBar()
                                        }
                                }
                               
                                
                            }catch _ {
                                print ("OOps not good JSON formatted response")
                            }
                        }
                    })
                    task.resume()
                }catch _ {
                    print ("Oops something happened buddy")
                }
            //}
        }
        else
        {
            strAlertMsg = "Not internet connection"
           groupCountAlertBar()
        }
    }

    func editProfile()
    {
        moreOptionView?.isHidden = true
        if Reachability.shared.isConnectedToNetwork(){
            var userID = String()
            if UserDefaults.standard.value(forKey: "user_id") != nil {
                 userID =  UserDefaults.standard.value(forKey: "user_id") as! String
            }
        print(userID)
        SVProgressHUD .show()
        //DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
        let url = "http://beta.brstdev.com/tours/api/web/v1/tours/editprofile"
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let params:[String: AnyObject] = ["firstname" : self.firstNameTxt.text as AnyObject,"lastname" : self.LastNameTxt.text as AnyObject,"email" : self.emailTxt.text as AnyObject,"phone" : self.phoneNumTxt.text as AnyObject,"state" : "city" as AnyObject,"city" : "city" as AnyObject,"occupation" : "occupation" as AnyObject,"country" : self.countryTxt.text as AnyObject,"user_id" : userID as AnyObject]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code = \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print ("Json Data is = \(jsonResponse)")
                        
                        
                           
                        var dataDic = NSDictionary()
                        
                        var strSuccess_code = jsonResponse ["status_code"] as! NSString
                        
                        let strDescription = jsonResponse ["description"] as! NSString
                         SVProgressHUD.dismiss()
                        if strSuccess_code .isEqual(to: "400")
                        {
                            self.strAlertMsg = strDescription
                            if self.strProfileCheck.isEqual(to: "EditProfile") {
                                self.editUserProfileImgView.isHidden = false
                            }
                            else
                            {
                                self.createProfileView.isHidden = false
                            }
                            self.profileContentView?.isHidden = true
                            self.dispatchQueue()
                        }
                        else
                        {
                            if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                            {
                            dataDic = jsonResponse.value(forKey: "data") as! NSDictionary
                            print ("Profile Data = \(dataDic)")
                            strSuccess_code = dataDic ["Success_code"] as! NSString
                            
                            
                            if strSuccess_code .isEqual(to: "201")
                            {
                               //  self.scrollView?.contentSize = CGSize(width: self.view.frame.size.width,height:(self.purcTourListBtn?.frame.origin.y)!+((self.purcTourListBtn?.frame.size.height)!)+20)
                                
                                  DispatchQueue.main.async {
                                UserDefaults.standard.set("profileCreatedSuccessFully", forKey: "profileCreated")
                                
                                let  city = dataDic ["city"] as! NSString
                                UserDefaults.standard.set(city, forKey: "city")
                                
                                let  country = dataDic ["country"] as! NSString
                                if (country .isEqual(to: "")||country .isEqual(to: "nil"))
                                {
                                    UserDefaults.standard.set(self.countryTxt.text, forKey: "country")
                                }
                                else
                                {
                                    UserDefaults.standard.set(country, forKey: "country")
                                }
                                
                                let  email = dataDic ["email"] as! NSString
                                UserDefaults.standard.set(email, forKey: "email")
                                
                                let  firstname = dataDic ["firstname"] as! NSString
                                UserDefaults.standard.set(firstname, forKey: "firstname")
                                
                                let  lastname = dataDic ["lastname"] as! NSString
                                UserDefaults.standard.set(lastname, forKey: "lastname")
                                
                                let  occupation = dataDic ["occupation"] as! NSString
                                UserDefaults.standard.set(occupation, forKey: "occupation")
                                
                                let  phone = dataDic ["phone"] as! NSString
                                UserDefaults.standard.set(phone, forKey: "phone")
                                
                                let  profile_image = dataDic ["profile_image"] as! NSString
                                UserDefaults.standard.set(profile_image, forKey: "profile_image")
                                
                                let  state = dataDic ["state"] as! NSString
                                UserDefaults.standard.set(state, forKey: "state")
                                
                                let  user_id = dataDic ["user_id"] as! NSInteger
                                UserDefaults.standard.set(String(describing:user_id), forKey: "user_id")
                                
                                let  username = dataDic ["username"] as! NSString
                                UserDefaults.standard.set(username, forKey: "username")
                                self.strProfileCheck = ""
                                
                                    self.showProfileData()
                                    self.hideKeypad()
                                    self.backButton?.isHidden = true
                                    self.backIcon.isHidden = true
                                    self.createProfileView.isHidden = true
                                    self.profileContentView?.isHidden = false
                                    self.scrollView?.isHidden = false
                                    self.editUserProfileView.isHidden = true
                                    self.strProfileCheck = ""
                                    self.scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                                    self.strAlertMsg = strDescription
                                    self.alertBar()
                                }
                               
                                
                                
                            }
                                
                            else
                            {
                                
                                self.strAlertMsg = strDescription
                                self.dispatchQueue()
                                
                                self.createProfileView.isHidden = true
                                self.profileContentView?.isHidden = false
                                self.editUserProfileView.isHidden = true
                                
                            }
                        }
                        
                        
                            else
                            {
                                SVProgressHUD.dismiss()
                                self.strAlertMsg = "Server Not Responding"
                                self.alertBar()
                            }
                        }
                        
                        
                    }catch _ {
                        print ("OOps not good JSON formatted response")
                    }
                }
            })
            task.resume()
        }catch _ {
            print ("Oops something happened buddy")
        }
        //}
        }
        else
        {
           groupCountAlertBar()
        }
    }
    
    
    
    
    func imageUploadRequest (image:UIImage, parameters: [String: String]? , keyName:String, completion:@escaping (_ result: Bool?) -> Void){
        
        if Reachability.shared.isConnectedToNetwork(){
            
            SVProgressHUD.show()
            //DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.asyncAfter(deadline: .now() + 6){
                self.progessDismiss()
            }
                let myUrl = NSURL(string: "http://beta.brstdev.com/tours/api/web/v1/tours/editprofileimage");
                let request = NSMutableURLRequest(url: myUrl! as URL)
                
                request.httpMethod = "POST";
                
                
                let boundary = self.generateBoundaryString()
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                var imageData = NSData()
                imageData   = UIImageJPEGRepresentation(image, 0.3)! as NSData
                
                request.httpBody = self.createBodyWithParameters(parameters: parameters, keyName: keyName, imageDataKey: imageData, boundary: boundary, filename: "browsedThroughPhone.jpg", mimetype: "image/jpg") as Data
                
                
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    data, response, error in
                    
                    if error != nil {
                        print("error=\(String(describing: error))")
                        completion(false)
                        return
                    }
                    
                    do {
                         SVProgressHUD.dismiss()
                        var dataDic = NSDictionary()
                        let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                       // print(jsonResponse!)
                        
                        var strSuccess_code = jsonResponse? ["status_code"] as! NSString
                        
                        let strDescription = jsonResponse? ["description"] as! NSString
                        
                        if strSuccess_code .isEqual(to: "400")
                        {
                            self.strAlertMsg = strDescription
                            self.dispatchQueue()
                            
                        }
                        else
                        {
                            
                            dataDic = jsonResponse?.value(forKey: "data") as! NSDictionary
                            print ("Profile Data = \(dataDic)")
                            strSuccess_code = dataDic ["Success_code"] as! NSString
                            if strSuccess_code .isEqual(to: "201")
                            {
                                
                                self.strAlertMsg = "Image Uploaded Successfully"
                                
                                let imageurl = dataDic ["profile_image"] as! NSString
                                
                                DispatchQueue.main.async {
                                    self.profileImageView?.image = self.userImage
                                    self.editUserProfileImgView.image = self.userImage
                                    UserDefaults.standard.set(UIImagePNGRepresentation(image), forKey: "ProfileImage")
                                  //  self.alertBar()
                                    SVProgressHUD.dismiss()
                                }
                                
                                //  self.setImageFromURl(stringImageUrl: imageurl as String)
                                
                                UserDefaults.standard.value(forKey: "profileCreated")
                                UserDefaults.standard.set(imageurl, forKey: "ProfileUrl")
                                
                                // SVProgressHUD.dismiss()
                                //self.strAlertMsg = strDescription
                                //
                            }
                            else{
                                self.alertBar()
                            }
                            
                            
                        }
                        // SVProgressHUD.dismiss()
                        
                    }
                        
                    catch{
                        print(error)
                        completion(false)
                    }
                    
                }
                
                task.resume()
            //}
        }
        else
        {
         groupCountAlertBar()
        }
    }
    func createBodyWithParameters(parameters: [String: String]?, keyName:String, imageDataKey: NSData, boundary: String,filename:String, mimetype:String) -> NSData {
        
        var body = ""
        
        if let params = parameters {
            for (key, value) in params {
                body += "--\(boundary)\r\n"
                body += "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n"
                body += "\(value)\r\n"
            }
        }
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body += "--\(boundary)\r\n"
        body += "Content-Disposition: form-data; name=\"\(keyName)\"; filename=\"\(filename)\"\r\n"
        body += "Content-Type: \(mimetype)\r\n\r\n"
        
        var data = body.data(using: .utf8)!
        data.append(imageDataKey as Data)
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        return data as NSData
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    func getMyData(completion:(_ data: Data?) -> Void) {
        
    }
    
    @IBAction func upComingToursAction (sender: UIButton) {
         if arrUpCommTourList.count != 0 {
          scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
        self.headerLabel?.text = "Upcoming Tours"
        self.purcTourTag = "upComingTours"
        self.showTourData(result: arrUpCommTourList)
        }
         else {
            self.showGroupCodeView?.isHidden = true
            strAlertMsg = "No data found"
            self.groupCountAlertBar()
        }
    }
    @IBAction func tourHistoryAction (sender: UIButton) {
        if arrTourHistList.count != 0 {
             scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
            self.purcTourTag = "PurcTourList"
            self.headerLabel?.text = "Tour History"
            self.showTourData(result:arrTourHistList)
        }
        else {
            self.showGroupCodeView?.isHidden = true
            strAlertMsg = "No data found"
            self.groupCountAlertBar()
        }
    }

    func showTourData(result: NSMutableArray) {
        
        DispatchQueue.main.async {
            self.profileContentView?.isHidden=true
            self.aboutUsContentView?.isHidden=true
            self.backButton?.isHidden = false
            self.backIcon.isHidden = false
            self.scrollView?.isHidden = false
            self.tourDetailsView?.isHidden = true
            self.profileContentView?.isHidden=true
            self.showGroupCodeView?.isHidden=false
            self.table_View?.isHidden = false
            self.table_View?.reloadData()
        }
        
}
    @IBAction func openGroupCodeList (sender: UIButton) {
      moreOptionView?.isHidden = true
        
        if arrGroupCode.count != 0 {
       scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                   // print(self.arrGroupCode)
            DispatchQueue.main.async {
                self.purcTourTag = "Group Code";
            self.backButton?.isHidden = false
                self.backIcon.isHidden = false
            self.headerLabel?.text = "Group Code"
            self.tourDetailsView?.isHidden = true
            self.showGroupCodeView?.isHidden = false
            self.table_View?.isHidden = false
           
            self.profileContentView?.isHidden=true
            self.table_View?.reloadData()
            }
        }
        else {
            self.showGroupCodeView?.isHidden = true
            strAlertMsg = "No data found"
            self.groupCountAlertBar()
        }
    
    }
    @IBAction func backAction (sender:UIButton)
    {
        moreOptionView?.isHidden = true
        localInfoWebView.isHidden = true
        if purcTourTag .isEqual(to: "Group Code")
        {
            headerLabel?.text = "My Profile"
            self.backButton?.isHidden = true
            self.backIcon.isHidden = true
            showGroupCodeView?.isHidden = true
            self.table_View?.isHidden = true
            scrollView?.isHidden = false
            profileContentView?.isHidden = false
            
        }
        else if purcTourTag .isEqual(to: "PurcTourList")
        {
            headerLabel?.text = "My Profile"
            self.backButton?.isHidden = true
            self.backIcon.isHidden = true
            self.table_View?.isHidden = true
            scrollView?.isHidden = false
             showGroupCodeView?.isHidden = true
            profileContentView?.isHidden = false
            
        }
        else if purcTourTag .isEqual(to: "upComingTours")
        {
            headerLabel?.text = "My Profile"
            self.backButton?.isHidden = true
            self.backIcon.isHidden = true
            self.table_View?.isHidden = true
            scrollView?.isHidden = false
            showGroupCodeView?.isHidden = true
            profileContentView?.isHidden = false
            
        }
        else if purcTourTag .isEqual(to: "Tour Details")
        {
            speechSynthesizer.stopSpeaking(at: AVSpeechBoundary .immediate)
            speechTag = 0;
            movePositions()
            
            purcTourTag = "PurcTourList"
            moreOptionView?.isHidden = true
            tourDetailsView?.isHidden = true
            table_View?.isHidden = false
        }
        else if purcTourTag .isEqual(to: "EditProfile")
        {
            purcTourTag = ""
            headerLabel?.text = "My Profile"
            self.backButton?.isHidden = true
            self.backIcon.isHidden = true
            showGroupCodeView?.isHidden = true
            self.table_View?.isHidden = true
            scrollView?.isHidden = false
            createProfileView.isHidden = true
            profileContentView?.isHidden = false
        }
    }
    @IBAction func purchasedTourList (sender:UIButton)
    {
        moreOptionView?.isHidden = true
         scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
 
            DispatchQueue.main.async {
                self.purcTourTag = "PurcTourList"
                self.headerLabel?.text = "Purchase Tours"
                self.backButton?.isHidden = false
                self.backIcon.isHidden = false
                self.scrollView?.isHidden = false
                self.tourDetailsView?.isHidden = true
              
                self.profileContentView?.isHidden=true
                self.showGroupCodeView?.isHidden=false
                self.table_View?.isHidden = false
                self.table_View?.reloadData()
                
            }
            
        
    }
    func getGroupList(completion:@escaping (_ result: NSArray?) -> Void)
    {
        if Reachability.shared.isConnectedToNetwork(){

         var userID = NSString()
         if (UserDefaults.standard.value(forKey: "user_id") != nil)
         {
            userID = (UserDefaults.standard.value(forKey: "user_id") as? NSString)!
        }
        
        let url = "http://beta.brstdev.com/tours/api/web/v1/tours/getsinglegroup"
        let session = URLSession.shared
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let params:[String: AnyObject] = ["user_id" : userID as AnyObject]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                    print ("status code = \(statusCode)")
                }
                if let error = error {
                    print ("\(error)")
                }
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                        print ("Json Data is = \(jsonResponse)")
                        
                        var dataDic = NSDictionary()
                        
                        DispatchQueue.main.async {
                            if jsonResponse ["status_code"] as? NSString != nil
                            {
                                var strSuccess_code = jsonResponse ["status_code"] as! NSString
                                
                                let strDescription = jsonResponse ["description"] as! NSString
                                
                                
                                if strSuccess_code .isEqual(to: "400")
                                {
                                    self.statusCode = strSuccess_code
                                    self.strAlertMsg = strDescription
                                }
                                else
                                {
                                    if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                                    {
                                        dataDic = jsonResponse.value(forKey: "data") as! NSDictionary
                                        print ("Profile Data = \(dataDic)")
                                        strSuccess_code = dataDic ["Success_code"] as! NSString
                                        
                                        
                                        if strSuccess_code .isEqual(to: "201")
                                        {
                                            let getDataArr = dataDic.value(forKey: "group_list") as! NSArray
                                            self.statusCode = strSuccess_code
                                            
                                            self.table_View?.separatorColor = UIColor.clear
                                            DispatchQueue.main.async {
                                                self.arrGroupCode = NSMutableArray(array:getDataArr)
                                                self.strAlertMsg = strDescription
                                                self.scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                                            }
                                            
                                        }
                                        else
                                        {
                                            self.showGroupCodeView?.isHidden = true
                                        }
                                        completion(self.arrGroupCode )
                                        
                                    }
                                    
                                }
                            }
                            
                        }
                    }catch _ {
                        print ("OOps not good JSON formatted response")
                    }
                        
                }
            })
            task.resume()
        }catch _ {
            print ("Oops something happened buddy")
        }
        //}
        }
        else
        {
           groupCountAlertBar()
        }
    }
    func purchaseTourList(completion:@escaping (_ result: NSArray?) -> Void)
    {
        if Reachability.shared.isConnectedToNetwork(){
       
        var userID = NSString()
        if (UserDefaults.standard.value(forKey: "user_id") != nil)
        {
            userID =  (UserDefaults.standard.value(forKey: "user_id") as? NSString)!
        }

            let url = "http://beta.brstdev.com/tours/api/web/v1/tours/purchaselist"
            let session = URLSession.shared
            let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            
            let params:[String: AnyObject] = ["user_id" : userID as AnyObject]
            
            do{
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions())
                let task = session.dataTask(with: request as URLRequest, completionHandler: {(data, response, error) in
                    if let response = response {
                        let nsHTTPResponse = response as! HTTPURLResponse
                        let statusCode = nsHTTPResponse.statusCode
                        print ("status code = \(statusCode)")
                    }
                    if let error = error {
                        print ("Server response is\(error)")
                        
                    }
                    if let data = data {
                        do{
                            let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! NSDictionary
                            print ("purchaseTourList = \(jsonResponse)")
                            
                            var dataDic = NSDictionary()
                            
                                 SVProgressHUD.dismiss()
                            if jsonResponse ["status_code"] as? NSString != nil{
                            var strSuccess_code = jsonResponse ["status_code"] as! NSString
                            
                            let strDescription = jsonResponse ["description"] as! NSString
                            
                            if strSuccess_code .isEqual(to: "400")
                            {
                                self.statusCode = strSuccess_code
                            }
                            else
                            {
                                if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                                {
                                dataDic = jsonResponse.value(forKey: "data") as! NSDictionary
                                print ("Purchase tour List = \(dataDic)")
                                strSuccess_code = dataDic ["Success_code"] as! NSString
                                
                                
                                if strSuccess_code .isEqual(to: "201")
                                {
                                    let getDataArr = dataDic.value(forKey: "tour_purchase_list") as! NSArray
                                    self.statusCode = strSuccess_code
                                    
                                    completion(getDataArr)
                                    
                                        
                                        self.strAlertMsg = strDescription
                                        SVProgressHUD.dismiss()
                                        
                                        self.scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                                }
                                else
                                {
                                    self.showGroupCodeView?.isHidden = true
                                }
                                
                                
                            //}
                            
                            }
                                else
                                {
                                    self.strAlertMsg = "Server Not Responding"
                                    //self.alertBar()
                                }

                            }
                                                      //
                            }
                            else
                            {
                            SVProgressHUD.dismiss()
                            self.strAlertMsg = "Server Not Responding"
                            //self.alertBar()
                            }
                        }
                            catch _ {
                            print ("OOps not good JSON formatted response")
                        }
                    }
                })
                task.resume()
            }catch _ {
                print ("Oops something happened buddy")
            }
      //  }
        }

    else
    {
         groupCountAlertBar()
    }
    }
    func upComingTours(completion: @escaping (_ result: NSArray) -> Void)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 6){
            self.progessDismiss()
        }
        var userID = String()
        if UserDefaults.standard.value(forKey: "user_id") != nil {
             userID =  UserDefaults.standard.value(forKey: "user_id") as! String
        }
        //declare parameter as a dictionary which contains string as key and value combination. considering inputs are valid
        
        let parameters = ["user_id": userID]
        
        //create the url with URL
        let url = URL(string: "http://beta.brstdev.com/tours/api/web/v1/tours/upcomingTours")! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                    
                    
                    let description = jsonResponse.value(forKey: "description") as! String
                    
                    self.strAlertMsg = description as NSString
                    
                    var dataDict = NSDictionary()
                    if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                    {
                        dataDict = jsonResponse.object(forKey: "data") as! NSDictionary
                        
                        let strSuccess_code = dataDict ["Success_code"] as! NSString
                        
                        print(dataDict)
                        if strSuccess_code .isEqual(to: "201")
                        {
                            let getDataArr = dataDict.value(forKey: "tour_history_listing") as! NSArray
                            self.statusCode = strSuccess_code
                            self.arrUpCommTourList = NSMutableArray(array:getDataArr)
                            completion(getDataArr)
                        }
                        
                        
                        
                        
                    }
                    
                    //  self.alertBar()
                    
                    // handle json...
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
        
    }
    func tourHistory(completion: @escaping (_ result: NSArray) -> Void)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 6){
            self.progessDismiss()
        }
        
        var userID = String()
        if UserDefaults.standard.value(forKey: "user_id") != nil {
         userID =  UserDefaults.standard.value(forKey: "user_id") as! String
         }
        //declare parameter as a dictionary which contains string as key and value combination. considering inputs are valid
        
        let parameters = ["user_id": userID]
        
        //create the url with URL
        let url = URL(string: "http://beta.brstdev.com/tours/api/web/v1/tours/tourHistory")! //change the url
        
        //create the session object
        let session = URLSession.shared
        
        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                //create json object from data
                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? NSDictionary {
                   
                    
                    let description = jsonResponse.value(forKey: "description") as! String
                    
                    self.strAlertMsg = description as NSString
                    
                    var dataDict = NSDictionary()
                    if jsonResponse.value(forKey: "data") as? NSDictionary != nil
                    {
                        dataDict = jsonResponse.object(forKey: "data") as! NSDictionary

                       let strSuccess_code = dataDict ["Success_code"] as! NSString
                        
                        print(dataDict)
                        if strSuccess_code .isEqual(to: "201")
                        {
                            let getDataArr = dataDict.value(forKey: "tour_history_listing") as! NSArray
                            self.statusCode = strSuccess_code
                            
                            
                            self.arrTourHistList = NSMutableArray(array:getDataArr)
                            
                            self.strAlertMsg = description as NSString
                            SVProgressHUD.dismiss()
                            
                           // self.scrollView?.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
                        }
                        else
                        {
                            SVProgressHUD.dismiss()
                           // self.showGroupCodeView?.isHidden = true
                        }
                        
                    }
                    completion( self.arrTourHistList)
                  //  self.alertBar()
                    
                    // handle json...
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
        
    }
    //Log out Action
    func LogOutAlertBar()
    {
        logoutAlertView = UIAlertController(title: "Logout", message: strAlertMsg as String, preferredStyle: UIAlertControllerStyle.alert)
        // Action.
        let noBtn = UIAlertAction(title: "No", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.nBtnAction()
        }
        let yesBtn = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.yesBtnAction()
        }
        logoutAlertView.addAction(noBtn)
        logoutAlertView.addAction(yesBtn)
        DispatchQueue.main.async {
             self.present(self.logoutAlertView, animated: true, completion: nil)
        }
       
    }
    func removeAllArrItems() {
        self.arrGroupCode.removeAllObjects()
        self.arrTourHistList.removeAllObjects()
        self.arrUpCommTourList.removeAllObjects()
    }
    func yesBtnAction()
    {
         DispatchQueue.main.async {
            
           
            self.removeAllArrItems()
            self.emailTxt.isUserInteractionEnabled = true
            self.phoneNumTxt.isUserInteractionEnabled = true
            self.firstNameTxt.isUserInteractionEnabled = true
            self.LastNameTxt.isUserInteractionEnabled = true
            self.countryTxt.isUserInteractionEnabled = true
            
            self.profileImageView?.image = UIImage(named:"user.png")
            self.editUserProfileImgView?.image = UIImage(named:"user.png")
            self.locationLabel.text = ""
            self.userNameLbl.text = ""
            self.userSubNameLbl.text = ""
            
        self.phoneNumTxt.textColor = UIColor .black
         self.emailTxt.textColor = UIColor .black
        
            
            UserDefaults.standard.removeObject(forKey: "ProfileImage")
         UserDefaults.standard.removeObject(forKey: "JoinGroupSavedTourID")
        UserDefaults.standard.set("", forKey: "UserLoggedIn")
        UserDefaults.standard.removeObject(forKey: "JoinGroupCheck")
        
        UserDefaults.standard.removeObject(forKey: "JoinGroupTourID")
          UserDefaults.standard.removeObject(forKey: "UserLoggedIn")
        UserDefaults.standard.removeObject(forKey: "IsOwner")
        
        UserDefaults.standard.removeObject(forKey: "profileCreated")
        UserDefaults.standard.removeObject(forKey: "city")
       
        UserDefaults.standard.removeObject(forKey: "profileCreated")
        UserDefaults.standard.removeObject(forKey: "city")

        UserDefaults.standard.removeObject(forKey: "country")
        UserDefaults.standard.removeObject(forKey: "email")

        UserDefaults.standard.removeObject(forKey: "firstname")
        UserDefaults.standard.removeObject(forKey: "lastname")

        UserDefaults.standard.removeObject(forKey: "occupation")
        UserDefaults.standard.removeObject(forKey: "phone")

        UserDefaults.standard.removeObject(forKey: "profile_image")
        UserDefaults.standard.removeObject(forKey: "state")
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "username")
        UserDefaults.standard.removeObject(forKey: "TourId")
        
        //Removing saved data of quetionnaire
        UserDefaults.standard.removeObject(forKey: "SavedTour")
        UserDefaults.standard.removeObject(forKey: "saveTourDetail")
        UserDefaults.standard.removeObject(forKey: "correctAns")
        UserDefaults.standard.removeObject(forKey: "wrongAns")
        UserDefaults.standard.removeObject(forKey: "arrID")

        
        self.tabBarController?.selectedIndex = 0
        
        }
    }
    
    
    func nBtnAction()
    {
        logoutAlertView.dismiss(animated: true, completion: nil)
    }
    func hideView()
    {
        moreOptionView?.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension MoreViewController: MFMailComposeViewControllerDelegate
{
    func sendMail() {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["Highjinxtours@gmail.com"])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func showPdfFile(pdfUrl: URL){
        let req = NSURLRequest(url: pdfUrl) //pdf is your pdf path
        localInfoWebView.loadRequest(req as URLRequest)
        
        SVProgressHUD.show()
        DispatchQueue.main.asyncAfter(deadline: .now()+0.8) {
            SVProgressHUD.dismiss()
        }
       
        
        
        
    }
}


