//
//  MapTasks.swift
//  CrazyMindChat
//
//  Created by Brst on 12/09/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit
import CoreLocation
class MapTasks: NSObject {
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    
    var lookupAddressResults: NSMutableDictionary!
    
    var fetchedFormattedAddress: String!
    
    var fetchedAddressLongitude: Double!
    
    var fetchedAddressLatitude: Double!

    
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    
    var selectedRoute = NSDictionary()
    
    var overviewPolyline = NSDictionary()
    
    var originCoordinate: CLLocationCoordinate2D!
    
    var destinationCoordinate: CLLocationCoordinate2D!
    
    var originAddress: String!
    
    var destinationAddress: String!
    
    var totalDistanceInMeters: UInt = 0
    
    var totalDistance: String!
    
    var totalDurationInSeconds: UInt = 0
    
    var totalDuration: String!
    
    override init() {
        super.init()
    }
    //Find the searched location by field
    func geocodeAddress(address: String!, withCompletionHandler completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
        
        if let lookupAddress = address {
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress
            //geocodeURLString = geocodeURLString.addingPercentEncoding(withAllowedCharacters: NSUTF8StringEncoding)
           geocodeURLString = geocodeURLString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
            let geocodeURL = NSURL(string: geocodeURLString)
            
            DispatchQueue.main.async(execute: { () -> Void in
                let geocodingResultsData = NSData(contentsOf: geocodeURL! as URL)
                
                let request = NSMutableURLRequest(url: geocodeURL! as URL)
                
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    ( data, response, error) in
                    
                    if let _ = response as? HTTPURLResponse {
                        do {
                            let dictionary = try JSONSerialization.jsonObject(with: geocodingResultsData! as Data, options: .mutableContainers) as? NSDictionary
                            
                            if error != nil {
                                print("error=\(error!)")
                                return
                            }
                            
                            if dictionary != nil {
                                // Get the response status.
                                let status = dictionary?["status"] as! String
                                
                                if status == "OK" {
                                    let allResults = dictionary?["results"] as! NSMutableArray
                                    self.lookupAddressResults = allResults[0] as! NSMutableDictionary
                                    
                                    // Keep the most important values.
                                    self.fetchedFormattedAddress = self.lookupAddressResults["formatted_address"] as! String
                                    let geometry = self.lookupAddressResults["geometry"] as! NSDictionary
                                    self.fetchedAddressLongitude = ((geometry["location"] as! NSDictionary)["lng"] as! NSNumber).doubleValue
                                    self.fetchedAddressLatitude = ((geometry["location"] as! NSDictionary)["lat"] as! NSNumber).doubleValue
                                    
                                    
                                    completionHandler(status, true)
                                }
                                else {
                                    completionHandler(status, false)
                                }
                            }
                        } catch {
                            print("Over Linmit=\(error)")
                        }
                    }
                }
                
                task.resume()
            })
        }
        else {
            completionHandler("No valid address.", false)
        }
}

    func getDirections(origin: String!, destination: String!, waypoints: Array<String>!, travelMode: TravelModes!, completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
        
        
        
        if let originLocation = origin {
            if let destinationLocation = destination {
                var directionsURLString = baseURLDirections + "origin=" + originLocation + "&destination=" + destinationLocation
                
                if let routeWaypoints = waypoints {
                    directionsURLString += "&waypoints=optimize:true"
                    
                    for waypoint in routeWaypoints {
                        directionsURLString += "|" + waypoint
                    }
                }
                
                if (travelMode) != nil {
                    var travelModeString = ""
                    
                    switch travelMode.rawValue {
                    case TravelModes.walking.rawValue:
                        travelModeString = "walking"
                        
                    case TravelModes.bicycling.rawValue:
                        travelModeString = "bicycling"
                        
                    default:
                        travelModeString = "driving"
                    }
                    
                    
                    directionsURLString += "&mode=" + travelModeString
                }
                directionsURLString = directionsURLString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
                
                let directionsURL = NSURL(string: directionsURLString)
            
            DispatchQueue.main.async(execute: { () -> Void in
                let geocodingResultsData = NSData(contentsOf: directionsURL! as URL)
                
                let request = NSMutableURLRequest(url: directionsURL! as URL)
                
                let task = URLSession.shared.dataTask(with: request as URLRequest) {
                    ( data, response, error) in
                    
                    if let _ = response as? HTTPURLResponse {
                        do {
                            let dictionary = try JSONSerialization.jsonObject(with: geocodingResultsData! as Data, options: .mutableContainers) as? NSDictionary
                            
                            if error != nil {
                                print("error=\(error!)")
                                return
                            }
                            
                            if dictionary != nil {
                                // Get the response status.
                                let status = dictionary?["status"] as! String
                                
                                if status == "OK" {
                                    self.selectedRoute = (dictionary!["routes"] as? Array<Dictionary<String, AnyObject>> ?? [])[0] as NSDictionary
                                    if let overviewPol = self.selectedRoute["overview_polyline"] as? Dictionary<String, AnyObject> {
                                        self.overviewPolyline = overviewPol as NSDictionary
                                    }
                                    
                                    guard let legs = self.selectedRoute["legs"] as? Array<Dictionary<String, AnyObject>> else {return}
                                    let startLocationDictionary = legs[0]["start_location"] as? Dictionary<String, AnyObject> ?? [:]
                                    self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as? Double ?? 0, startLocationDictionary["lng"] as? Double ?? 0)
                                    let endLocationDictionary = legs[legs.count - 1]["end_location"] as? Dictionary<String, AnyObject> ?? [:]
                                    self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as? Double ?? 0, endLocationDictionary["lng"] as? Double ?? 0)
                                    
                                    self.originAddress = legs[0]["start_address"] as! String
                                    self.destinationAddress = legs[legs.count - 1]["end_address"] as! String
                                
                                    DispatchQueue.main.async {
                                    self.calculateTotalDistanceAndDuration()
                                    completionHandler(status, true)
                                    }
                                    
                                }
                                else {
                                    completionHandler(status, false)
                                }
                            }
                        } catch {
                            print(error)
                        }
                    }
                }
                
                task.resume()
            })
        }
        else {
            completionHandler("No valid address.", false)
        }
    }
    }
        func calculateTotalDistanceAndDuration() {
            let legs = self.selectedRoute["legs"] as? Array<Dictionary<String, AnyObject>> ?? []
            
            totalDistanceInMeters = 0
            totalDurationInSeconds = 0
            
            for leg in legs {
                totalDistanceInMeters += (leg["distance"] as? Dictionary<String, AnyObject> ?? [:])["value"] as? UInt ?? 0
                totalDurationInSeconds += (leg["duration"] as? Dictionary<String, AnyObject> ?? [:])["value"] as? UInt ?? 0
            }
            
            let distanceInKilometers: Double = Double(totalDistanceInMeters / 1000)
           // totalDistance = "\(distanceInKilometers)"
            totalDistance = "Total Distance: \(distanceInKilometers) Km"
            let mins = totalDurationInSeconds / 60
            totalDurationInSeconds = mins
            let hours = mins / 60
            let days = hours / 24
            let remainingHours = hours % 24
            let remainingMins = mins % 60
            let remainingSecs = totalDurationInSeconds % 60
            
            totalDuration = "Duration: \(days) d, \(remainingHours) h, \(remainingMins) mins, \(remainingSecs) secs"
            
        }
}
